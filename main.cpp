#include "mainwindow.h"
#include <QApplication>
#include <QFile>
#include <QTextStream>
#include <QTranslator>
#include <QLocale>
#include <stdio.h>
#include <stdlib.h>
#include "Database/options.h"
#include "Core/cache.h"
#include "Core/stylesheetmanager.h"

/*void customMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg) {
    QFile logFile( "starmod.log" );
    if( !logFile.open( QIODevice::WriteOnly | QIODevice::Append ) ) {
        std::cerr << "Cannot open log file" << std::endl;
    }
    QTextStream stream( &logFile );
    stream.setCodec( "UTF-8" );
    QByteArray localMsg = msg.toLocal8Bit();
    QString formattedMsg;
    switch( type ) {
        case QtDebugMsg:
            formattedMsg = QString( "%1 (%2:%3, %4)\n" ).arg( msg ).arg( context.file ).arg( context.line ).arg( context.function );
            printf( "%s", formattedMsg.toStdString().c_str() );
            stream << formattedMsg;
        break;

        case QtWarningMsg:
            formattedMsg = QString( "Warning:: %1 (%2:%3, %4)\n" ).arg( msg ).arg( context.file ).arg( context.line ).arg( context.function );
            std::cerr << formattedMsg.toStdString();
            stream << formattedMsg;
        break;

        case QtCriticalMsg:
            formattedMsg = QString( "Critical: %1 (%2:%3, %4)\n" ).arg( msg ).arg( context.file ).arg( context.line ).arg( context.function );
            std::cerr << formattedMsg.toStdString();
            stream << formattedMsg;
        break;

        case QtFatalMsg:
            formattedMsg = QString( "Fatal: %1 (%2:%3, %4)\n" ).arg( msg ).arg( context.file ).arg( context.line ).arg( context.function );
            std::cerr << formattedMsg.toStdString();
            stream << formattedMsg;
            logFile.close();
            abort();
        break;
    }
}
*/
int main(int argc, char *argv[])
{
    Cache::getInstance()->init();
    //qInstallMessageHandler( customMessageHandler );
    QApplication a(argc, argv);
    a.setStyleSheet( StylesheetManager::getStylesheet( "Starmod.css" ) );
    QString locale = QLocale::system().name().section('_', 0, 0);
    QTranslator translator;
    translator.load(QString("qt_") + locale, QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    a.installTranslator(&translator);

    MainWindow w;
    w.show();
    
    return a.exec();
}
