#include "jsonvalidator.h"

JsonValidator::JsonValidator( QString file )
{
    setFile( file );
}

JsonValidator::JsonValidator(QByteArray jsonContent) : m_jsonContent( jsonContent )
{

}

bool JsonValidator::validate()
{
    /*QFile file( m_fileName );
    if( !file.open( QIODevice::ReadOnly ) ) {
        throw FileException( QObject::tr( "Cannot open" ) + " " + file.fileName() + ", " + file.errorString() );
    }

    QByteArray fileContent = file.readAll();*/

    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson( m_jsonContent, &error );

    if( error.error != QJsonParseError::NoError ) {
        JsonError err;
        err.errorString = error.errorString();

        //Calculating line of the error;
        int line = 0;
        int posBegLine = 0;
        for( int i = 0; i < m_jsonContent.size(); i++ ) {
            if( i == error.offset ) {
                err.line = line + 1;
                err.col = posBegLine;
            }

            //Reset posBegLine if we are at the end of a line
            if( m_jsonContent.at( i ) == '\n' ) {
                posBegLine = 0;
                line++;
            } else {
                posBegLine++;
            }
        }
        m_lastError = err;
        return false;
    }
    return true;
}

QByteArray JsonValidator::correctJson()
{
    QByteArray data;
    /*QFile file( m_fileName );
    if( !file.open( QIODevice::ReadOnly ) ) {
        throw FileException( QObject::tr( "Cannot open" ) + " " + file.fileName() + ", " + file.errorString() );
    }

    QTextStream stream( &file );
    stream.setCodec( "UTF-8" );*/

    QTextStream stream( &m_jsonContent );

    while( !stream.atEnd() ) {
        QString line = stream.readLine();
        parseLine( line );

        line.append( '\n' );
        data.append( line );
    }
    return data;
}

void JsonValidator::setFile(QString file)
{
    if( !QFile::exists( file ) )
        qDebug() << file << "doesn't exists" << endl;

    //m_fileName = file;

    QFile jsonFile( file );

    if( !jsonFile.open( QIODevice::ReadOnly ) ) {
        throw FileException( QObject::tr( "Cannot open" ) + " " + jsonFile.fileName() + ", " + jsonFile.errorString() );
    }


    m_jsonContent = jsonFile.readAll();
}


QString JsonValidator::file()
{
    //return m_fileName;
    return QString();
}

void JsonValidator::parseLine(QString &text)
{
    int len = text.size();
    int pos = 0;

    while( pos < len ) {
        QChar ch = text[pos];
        if( ch == '/' ) {
            //If it's a mono line comment
            if( text.mid( pos, 2 ) == "//" ) {
                text.remove( pos, text.size() - 1 );
            }
        }

        ++pos;
    }
}
JsonError JsonValidator::lastError() const
{
    return m_lastError;
}

