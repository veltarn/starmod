#ifndef JSONCORRECTOR_H
#define JSONCORRECTOR_H

#include <QFile>
#include <QByteArray>
#include <QTextStream>
#include <QString>
#include <QDebug>
#include <QJsonParseError>
#include <QJsonDocument>
#include <QJsonObject>
#include "../Core/Exceptions/fileexception.h"

struct JsonError {
    int line;
    int col;
    QString errorString;
};

/**
 * @brief This class is used to validate the json file and correct some of it's errors (Comments on json files for example)
 */
class JsonValidator
{
public:
    JsonValidator( QString file );
    JsonValidator( QByteArray jsonContent );

    bool validate();
    QByteArray correctJson();

    void setFile( QString file );
    void setFile( QFile &file );
    QString file();

    JsonError lastError() const;

private:
    void parseLine( QString &text );
    QString readLine( QString data );
private:
    //QString m_fileName;
    QByteArray m_jsonContent;
    JsonError m_lastError;
};

#endif // JSONCORRECTOR_H
