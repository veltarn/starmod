#-------------------------------------------------
#
# Project created by QtCreator 2013-12-13T19:06:01
#
#-------------------------------------------------

QT       += core gui xml sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = StarMod
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Core/projectmanager.cpp \
    Core/Exceptions/projectexception.cpp \
    Core/project.cpp \
    Core/utility.cpp \
    Parser/luahighlighter.cpp \
    Core/assetsmanager.cpp \
    GUI/subwindow.cpp \
    GUI/sidewidget.cpp \
    GUI/projectpropertieswindow.cpp \
    Parser/jsonhighlighter.cpp \
    Models/filesystemmodel.cpp \
    Core/zip.cpp \
    Core/miniz.c \
    GUI/buildmoddialog.cpp \
    Core/modmanager.cpp \
    Models/starbounditemmodel.cpp \
    GUI/weaponeditiondialog.cpp \
    GUI/itemeditionwidget.cpp \
    GUI/previewwidget.cpp \
    Graphics/starboundcharacter.cpp \
    Starbound/Items/staritem.cpp \
    Starbound/statuseffect.cpp \
    GUI/projectileeditionwidget.cpp \
    GUI/projectileeditionwindow.cpp \
    Starbound/starprojectile.cpp \
    GUI/quickeditor.cpp \
    GUI/frameeditionwindow.cpp \
    GUI/frameimagepreview.cpp \
    Starbound/Items/starweapon.cpp \
    GUI/recipeswidget.cpp \
    Views/starbounditemview.cpp \
    Starbound/Items/starconsumable.cpp \
    GUI/objectinfowidget.cpp \
    Starbound/Items/starminingtool.cpp \
    Starbound/Items/starhandeditem.cpp \
    Starbound/Items/starequipableitem.cpp \
    GUI/Wizard/intropage.cpp \
    GUI/Wizard/starboundpathpage.cpp \
    GUI/Wizard/workspacepage.cpp \
    GUI/Wizard/endpage.cpp \
    GUI/assetsextractiondialog.cpp \
    Starbound/Items/starflashlight.cpp \
    Starbound/Items/starbeamaxe.cpp \
    Starbound/Items/starmatitem.cpp \
    Core/logger.cpp \
    GUI/loggerdialog.cpp \
    Starbound/Items/starinstrument.cpp \
    Starbound/Items/starsword.cpp \
    Starbound/Items/starthrown.cpp \
    Starbound/Items/starworldobject.cpp \
    GUI/colorpreviewwidget.cpp \
    GUI/progresssplashscreen.cpp \
    Core/stylesheetmanager.cpp \
    GUI/statuseffectchooser.cpp \
    GUI/jsonstatuseffecteditiondialog.cpp \
    Core/cache.cpp \
    GUI/blueprintchooserdialog.cpp \
    GUI/objectviewwidget.cpp \
    GUI/itempickerwindow.cpp \
    GUI/recipeitempicker.cpp \
    GUI/recipesgroupspicker.cpp \
    GUI/itemeditionwindow.cpp \
    Starbound/starrecipe.cpp \
    Core/Exceptions/assetsexception.cpp \
    Core/assetsparser.cpp \
    Core/Exceptions/assetsparserexception.cpp \
    Core/starvariant.cpp \
    Core/asset.cpp \
    GUI/actiononreapwidget.cpp \
    Core/projectilesutility.cpp \
    GUI/actiononreapwindow.cpp \
    Starbound/starphysics.cpp \
    Core/Exceptions/fileexception.cpp \
    Json/jsonvalidator.cpp \
    GUI/physicspropertieslist.cpp \
    GUI/projectilelistwindow.cpp \
    GUI/texteditorwidget.cpp \
    Starbound/Items/starcoin.cpp \
    GUI/objecteditionwindow.cpp \
    GUI/swordeditionwindow.cpp \
    GUI/statuseffectslistwindow.cpp \
    Database/options.cpp \
    Database/database.cpp \
    Core/Exceptions/sqlqueryexception.cpp

HEADERS  += mainwindow.h \
    constants.h \
    Core/Exceptions/projectexception.h \
    Core/project.h \
    Core/utility.h \
    Parser/luahighlighter.h \
    Core/assetsmanager.h \
    GUI/subwindow.h \
    GUI/sidewidget.h \
    GUI/projectpropertieswindow.h \
    Parser/jsonhighlighter.h \
    Models/filesystemmodel.h \
    Core/zip.h \
    GUI/buildmoddialog.h \
    Core/projectmanager.h \
    Core/modmanager.h \
    Models/starbounditemmodel.h \
    GUI/weaponeditiondialog.h \
    GUI/itemeditionwidget.h \
    GUI/previewwidget.h \
    Graphics/starboundcharacter.h \
    Starbound/Items/staritem.h \
    Starbound/statuseffect.h \
    GUI/projectileeditionwidget.h \
    GUI/projectileeditionwindow.h \
    Starbound/starprojectile.h \
    GUI/quickeditor.h \
    GUI/frameeditionwindow.h \
    GUI/frameimagepreview.h \
    Starbound/Items/starweapon.h \
    GUI/recipeswidget.h \
    Views/starbounditemview.h \
    Starbound/Items/starconsumable.h \
    GUI/objectinfowidget.h \
    Starbound/Items/starminingtool.h \
    Starbound/Items/starhandeditem.h \
    Starbound/Items/starequipableitem.h \
    GUI/Wizard/intropage.h \
    GUI/Wizard/starboundpathpage.h \
    GUI/Wizard/workspacepage.h \
    GUI/Wizard/endpage.h \
    GUI/assetsextractiondialog.h \
    Starbound/Items/starflashlight.h \
    Starbound/Items/starbeamaxe.h \
    Starbound/Items/starmatitem.h \
    Core/singleton.hpp \
    Core/logger.h \
    GUI/loggerdialog.h \
    Starbound/Items/starinstrument.h \
    Starbound/Items/starsword.h \
    Starbound/Items/starthrown.h \
    Starbound/Items/starworldobject.h \
    GUI/colorpreviewwidget.h \
    GUI/progresssplashscreen.h \
    Core/stylesheetmanager.h \
    GUI/statuseffectchooser.h \
    GUI/jsonstatuseffecteditiondialog.h \
    Core/cache.h \
    GUI/blueprintchooserdialog.h \
    GUI/objectviewwidget.h \
    GUI/itempickerwindow.h \
    GUI/recipeitempicker.h \
    GUI/recipesgroupspicker.h \
    GUI/itemeditionwindow.h \
    Starbound/starrecipe.h \
    Starbound/StarTypes.h \
    Core/Exceptions/assetsexception.h \
    Core/assetsparser.h \
    Core/Exceptions/assetsparserexception.h \
    Core/starvariant.h \
    Core/asset.h \
    GUI/actiononreapwidget.h \
    Core/projectilesutility.h \
    GUI/actiononreapwindow.h \
    Starbound/starphysics.h \
    Core/Exceptions/fileexception.h \
    Json/jsonvalidator.h \
    GUI/physicspropertieslist.h \
    GUI/projectilelistwindow.h \
    GUI/texteditorwidget.h \
    Starbound/Items/starcoin.h \
    GUI/objecteditionwindow.h \
    GUI/swordeditionwindow.h \
    GUI/statuseffectslistwindow.h \
    Database/options.h \
    Database/database.h \
    Core/Exceptions/sqlqueryexception.h

FORMS    += mainwindow.ui \
    GUI/sidewidget.ui \
    GUI/projectpropertieswindow.ui \
    GUI/buildmoddialog.ui \
    GUI/weaponeditiondialog.ui \
    GUI/itemeditionwidget.ui \
    GUI/previewwidget.ui \
    GUI/projectileeditionwidget.ui \
    GUI/projectileeditionwindow.ui \
    GUI/quickeditor.ui \
    GUI/frameeditionwindow.ui \
    GUI/frameimagepreview.ui \
    GUI/recipeswidget.ui \
    GUI/assetsextractiondialog.ui \
    GUI/loggerdialog.ui \
    GUI/statuseffectchooser.ui \
    GUI/jsonstatuseffecteditiondialog.ui \
    GUI/blueprintchooserdialog.ui \
    GUI/objectviewwidget.ui \
    GUI/itempickerwindow.ui \
    GUI/recipesgroupspicker.ui \
    GUI/itemeditionwindow.ui \
    GUI/actiononreapwidget.ui \
    GUI/actiononreapwindow.ui \
    GUI/physicspropertieslist.ui \
    GUI/projectilelistwindow.ui \
    GUI/texteditorwidget.ui \
    GUI/objecteditionwindow.ui \
    GUI/swordeditionwindow.ui \
    GUI/statuseffectslistwindow.ui

debug {
    QMAKE_LIBDIR += "C:/Users/Dante/Documents/Bibliotheques/QScintilla-gpl-2.8.1/qscintilla-Debug/release"
}

release {
    LIBS += -LC:/Users/Dante/Documents/Bibliotheques/QScintilla-gpl-2.8.1/qscintilla-Release/release -lqscintilla2
}


INCLUDEPATH += C:/Users/Dante/Documents/Bibliotheques/QScintilla-gpl-2.8.1/Qt4Qt5

OTHER_FILES += \
    TodoAndDontForget.txt \
    assets/css/ProgressBar.css \
    assets/css/StarMod.css \
    ressource.rc

#RC_FILE += ressource.rc
