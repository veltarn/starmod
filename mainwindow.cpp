#include "mainwindow.h"
#include "ui_mainwindow.h"

/*StarboundCharacter *star = NULL;
float rot = 0.f;

enum Order {
    ASC, DESC
};

Order cOrder = ASC;*/

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_currentActionGroup( NULL ),
    m_starProcess( NULL ),
    m_log( NULL )
{
    m_log = Logger::getInstance();
    if( m_log != NULL ) {
        connect( m_log, SIGNAL( errorMessage(LogMessage) ), this, SLOT( onErrorLogged(LogMessage) ) );
    }

    ui->setupUi(this);
    setWindowIcon( QIcon( QPixmap( "assets/images/starmod_icon.png")));
    ui->menuPhysics->setIcon( QIcon( QPixmap( "assets/images/atom.png" ) ) );
    this->setMinimumWidth( 850 );
    this->setMinimumHeight( 420 );
    this->setWindowTitle( QString( APP_NAME ) + " - " + QString( APP_VERSION ) + "[*]" );
    //m_hlt = new LuaHighlighter( this );

    StarModOptions opt = readOptions();

    if( m_starboundDir == "" ) {
        QWizard *wizard = createStarterWizard();
        wizard->exec();

        m_starboundDir = wizard->field( "starboundPath" ).toString();
        m_workspaceDir = wizard->field( "workspace" ).toString();

        QSettings settings( QString( CONFIG_FILE_PATH ), QSettings::IniFormat );
        if( settings.isWritable() ) {
            settings.setValue( "Starbound/Path", m_starboundDir );
            settings.setValue( "Starbound/Workspace", m_workspaceDir );
        }

        qDebug() << "Setting up workspace directory" << endl;
        QDir workspace( m_workspaceDir );

        if( workspace.mkpath( "Starbound/assets" ) &&  workspace.mkpath( "Projects" ) ){
            qDebug() << "Done" << endl;

            qDebug() << "Extracting Starbound assets..." << endl;

            QString extractorPath = getAssetsExtractorPath();

            QStringList arguments;
            QString source = m_starboundDir + "/assets/packed.pak";
            QString destination = m_workspaceDir + "/Starbound/assets";

            source = QDir::toNativeSeparators( source );
            destination = QDir::toNativeSeparators( destination );

            arguments.append( source );
            arguments.append( destination );

            AssetsExtractionDialog dialog( extractorPath, arguments, this );
            dialog.exec();


        } else {
            QMessageBox::critical( this, tr( "Error" ), tr( "An error occured during Paths setup..." ) );
        }

        /*QString path = "";
        QMessageBox::StandardButton btn = QMessageBox::NoButton;
        while( path == "" && btn != QMessageBox::Close ) {
            btn = QMessageBox::warning( this, tr( "Unknown Starbound Location" ), tr( "You must specify a path where Starbound is located" ), QMessageBox::Ok | QMessageBox::Close );
            if( btn != QMessageBox::Close ) {
                path = QFileDialog::getExistingDirectory( this, tr( "Starbound Location" ) );
                m_starboundDir = path;
            }
        }

        if( btn == QMessageBox::Close && path == "" ) {
            QMessageBox::warning( this, tr( "Warning" ), tr( "You won't be able to create a mod unless specify Starbound's location" ) );
        }  else if( path != "" ) {
            QSettings settings( QString( CONFIG_FILE_PATH ), QSettings::IniFormat );
            if( settings.isWritable() ) {
                settings.setValue( "Starbound/Path", path );
            }
        }*/

    }

    ProgressSplashScreen splash( QPixmap( "assets/images/StarmodLogo.png" ) );
    splash.showMessage( tr( "Loading Starmod..." ), Qt::AlignBottom, Qt::white);
    splash.show();
    QCoreApplication::processEvents();

    m_assetsManager = new AssetsManager( m_workspaceDir + "/Starbound/assets", m_starboundDir, this );
    m_assetsManager->parseAssets( &splash );

    splash.showMessage( tr( "Done" ) );


    buildInterface();
    defineShortcuts();

    m_centralWidget->setViewMode( opt.viewMode );

    if( opt.isMaximized ) {
        setWindowState( Qt::WindowMaximized );
    } else {
        resize( opt.size );
    }

    m_projectManager = new ProjectManager( m_starboundDir, m_workspaceDir + "/Projects", this );
    initEvents();
    refreshRecentProjectsList();

    splash.finish( this );
    qDebug() << "Loading done" << endl;
/*
    star = new StarboundCharacter( m_starboundDir + "/assets" );
    //star->setItemInHand( m_starboundDir + "/assets/items/tools/pickaxe.png" );
    star->setHandType( StarRotation );
    //star->setArmRotation( 90 );
    //star->setItemPosition( QPointF( 42.f, 18.f ) );
    //star->setItemPosition( QPointF( 4.f, 1.f ) );
    PreviewWidget *prev = new PreviewWidget;
    prev->setCharacter( star );
    prev->show();

    QTimer *timer = new QTimer( this );
    timer->setInterval( 128 );
    timer->start();

    connect( timer, SIGNAL( timeout() ), this, SLOT( onTimeout() ) );*/
    /*StarItem item;
    item.setItemName( "test" );
    item.setShortDescription( "Super object" );
    item.setDescription( "Plop" );
    item.addLearnBlueprintOnPickup( "Sous Objet" );

    item.save( "ds:;fjg" );*/
}

MainWindow::~MainWindow()
{
    saveWindowOptions();
    delete ui;
}

/*void MainWindow::onTimeout() {
    if( cOrder == ASC )
        rot += 10;
    else
        rot -= 10;

    if( rot >= 90 )
        cOrder = DESC;
    else if( rot <= -90 )
        cOrder = ASC;

    star->setArmRotation( rot );


}
*/
void MainWindow::buildInterface() {
    ui->menuRecents_Projects->clear();
    m_subWindowsMenu = new QMenu( tr( "Sub Windows" ), this );
    ui->menuWindow->addMenu( m_subWindowsMenu );

    m_centralWidget = new QMdiArea( this );
    m_centralWidget->setTabsClosable( true );
    m_assetsView = new SideWidget( m_assetsManager, this );

    ui->assetsList->setWidget( m_assetsView );

    ui->actionNew_Project->setIcon( QIcon( QPixmap( "assets/images/newProject.png" ) ) );
    ui->actionOpen_Project->setIcon( QIcon( QPixmap( "assets/images/open.png" ) ) );
    ui->actionSave->setIcon( QIcon( QPixmap( "assets/images/save.png" ) ) );
    ui->actionQuit->setIcon( QIcon( QPixmap( "assets/images/exit.png" ) ) );

    ui->actionOptions->setIcon( QIcon( QPixmap( "assets/images/options.png" ) ) );

    ui->actionRun_test->setIcon( QIcon( QPixmap( "assets/images/testRun.png" ) ) );
    ui->actionBuild->setIcon( QIcon( QPixmap( "assets/images/buildMod.png" ) ) );
    ui->actionPublish->setIcon( QIcon( QPixmap( "assets/images/network.png" ) ) );

    ui->actionAdd_Armor->setIcon( QIcon( QPixmap( "assets/images/armor.png" ) ) );
    ui->actionAdd_Weapon->setIcon( QIcon( QPixmap( "assets/images/weapon.png" ) ) );
    ui->actionAdd_Sword->setIcon( QIcon( QPixmap( "assets/images/sword.png" ) ) );
    ui->menuProjectiles->setIcon( QIcon( QPixmap( "assets/images/bullet.png" ) ) );

    ui->mainToolBar->addAction( ui->actionNew_Project );
    ui->mainToolBar->addAction( ui->actionOpen_Project );
    ui->mainToolBar->addAction( ui->actionSave );
    ui->mainToolBar->addSeparator();
    ui->mainToolBar->addAction( ui->actionRun_test );
    ui->mainToolBar->addAction( ui->actionBuild );
    ui->mainToolBar->addSeparator();

    ui->assetsList->setContentsMargins( 0, 0, 0, 0 );
    this->setCentralWidget( m_centralWidget );
}

void MainWindow::defineShortcuts() {
    ui->actionNew_Project->setShortcut( QKeySequence( "Ctrl+Shift+N" ) );
    ui->actionOpen_Project->setShortcut( QKeySequence( "Ctrl+O" ) );
    ui->actionSave->setShortcut( QKeySequence( "Ctrl+Shift+S" ) );
    ui->actionQuit->setShortcut( QKeySequence( "Ctrl+Q" ) );
    ui->actionNew_file->setShortcut( QKeySequence( "Ctrl+N" ) );

}

StarModOptions MainWindow::readOptions() {
    QSettings settings( QString( CONFIG_FILE_PATH ), QSettings::IniFormat );
    StarModOptions opt;

    uint width = settings.value( "Geometry/Size/Width", this->minimumWidth() ).toUInt();
    uint height = settings.value( "Geometry/Size/Height", this->minimumHeight() ).toUInt();

    QSize size;
    size.setWidth( width );
    size.setHeight( height );
    opt.size = size;

    opt.isMaximized = settings.value( "Geometry/IsMaximized", false ).toBool();

    int viewMode = settings.value( "Windows/ViewMode", 0 ).toInt();
    opt.viewMode == viewMode == 1 ? QMdiArea::TabbedView : QMdiArea::SubWindowView;

   m_starboundDir = settings.value( "Starbound/Path", "" ).toString();
   m_workspaceDir = settings.value( "Starbound/Workspace", "" ).toString();

   return opt;
}

void MainWindow::saveWindowOptions() {
    QSettings settings( QString( CONFIG_FILE_PATH ), QSettings::IniFormat );

    if( settings.isWritable() ) {
        uint w = this->width();
        uint h = this->height();
        bool maximized = this->isMaximized();

        settings.setValue( "Geometry/Size/Width", w );
        settings.setValue( "Geometry/Size/Height", h );
        settings.setValue( "Geometry/IsMaximized", maximized );

        settings.setValue( "Windows/ViewMode", m_centralWidget->viewMode() );
    }
}

void MainWindow::onNewProjectBtnClicked() {
    if( m_projectManager->newProject() ) {
        enableProjectRelatedActions( true );
        addProjectToRecents( m_projectManager->getProject() );
        loadAssetsList();
    }
}

void MainWindow::enableProjectRelatedActions( bool enable ) {
    ui->actionSave->setEnabled( enable );
    ui->actionSave_as->setEnabled( enable );
    ui->actionProject_Properties->setEnabled( enable );
    ui->actionRun_test->setEnabled( enable );
    ui->actionBuild->setEnabled( enable );
    ui->menuWeapons->setEnabled( enable );
    ui->actionAdd_Weapon->setEnabled( enable );
    ui->actionAdd_Sword->setEnabled( enable );
    ui->actionAdd_Armor->setEnabled( enable );
    ui->menuAdd_Item->setEnabled( enable );
    ui->actionAdd_Basic_Item_item->setEnabled( enable );
    ui->actionAdd_Consumable_Item_consumable->setEnabled( enable );
    ui->menuProjectiles->setEnabled( enable );
    ui->actionProjectiles_List->setEnabled( enable );
    ui->action_addProjectile->setEnabled( enable );
    ui->actionPublish->setEnabled( enable );
    ui->menuPhysics->setEnabled( enable );
    ui->actionList_of_Physics_properties->setEnabled( enable );
}

void MainWindow::loadAssetsList() {
    //qDebug() << m_starboundDir << endl;
    //Loading object view

    QSortFilterProxyModel *model = m_assetsManager->createObjectsModel();

    if( model != NULL ) {
        m_assetsView->itemView()->setModel( model );
        model->sort( 0 );
        //m_assetsView->updateObjectNumberLabel();
    }

    //Loading Raw view
    QDir starDir( m_assetsManager->assetsPath());
    //if( starDir.cd( "assets" ) ) {
        FileSystemModel *rawModel = new FileSystemModel( this );

        //qDebug() << starDir.path() << endl;
        rawModel->setRootPath( starDir.path() );


        QTreeView *view = m_assetsView->treeView();
        view->setModel( rawModel );

        QModelIndex idx = rawModel->index( starDir.path() );
        view->setRootIndex( idx );

     //Loading project dir view
    if( m_projectManager->isOpen() ) {
        QDir projectDir = QFileInfo( m_projectManager->getProject()->projectPath() ).path();

        if( projectDir.cd( "files" ) ) {
            QFileSystemModel *projectModel = new QFileSystemModel( this );

            projectModel->setRootPath( projectDir.path() );
            qDebug() << projectModel->rowCount() << "rows" << endl;
            QTreeView *view = m_assetsView->projectView();
            view->setModel( projectModel );

            QModelIndex idx = projectModel->index( projectDir.path() );
            view->setRootIndex( idx );

            connect( view, SIGNAL( doubleClicked(QModelIndex) ), this, SLOT( onRowDoubleClicked(QModelIndex) ) );
            //connect( model)
        } else {
            qDebug() << "Cannot find files directory in" << projectDir.path() << endl;
        }
    }

    ui->assetsList->setWidget( m_assetsView );

    //connect( view, SIGNAL( expanded(QModelIndex) ), this, SLOT( onRowExpanded(QModelIndex) ) );
    connect( view, SIGNAL( doubleClicked(QModelIndex) ), this, SLOT( onRowDoubleClicked(QModelIndex) ) );
    connect( rawModel, SIGNAL( directoryLoaded(QString) ), this, SLOT( onDirectoryLoaded( QString ) ) );
        //}
}

void MainWindow::loadProjectDirectory()
{
    if( m_projectManager->isOpen() ) {
        QDir projectDir = QFileInfo( m_projectManager->getProject()->projectPath() ).path();

        if( projectDir.cd( "files" ) ) {
            QFileSystemModel *model = new QFileSystemModel( this );

            model->setRootPath( projectDir.path() );

            /*QTreeView *view = m_assetsView->projectView();
            view->setModel( model );

            QModelIndex idx = model->index( projectDir.path() );
            view->setRootIndex( idx );*/
            m_assetsView->setProjectModel( model, projectDir.path() );

            connect( m_assetsView->projectView(), SIGNAL( doubleClicked(QModelIndex) ), this, SLOT( onRowDoubleClicked(QModelIndex) ) );
            //connect( model)
        } else {
            qDebug() << "Cannot find files directory in" << projectDir.path() << endl;
        }
    }
}

void MainWindow::initEvents() {
    connect( ui->actionSave, SIGNAL( triggered() ), this, SLOT( onSaveBtnClicked() ) );
    connect( ui->actionNew_Project, SIGNAL( triggered() ), this, SLOT( onNewProjectBtnClicked() ) );
    connect( ui->action_addProjectile, SIGNAL( triggered() ), this, SLOT( onAddProjectileActionClicked() ) );
    connect( ui->actionQuit, SIGNAL( triggered() ), qApp, SLOT( quit() ) );
    connect( ui->actionTab_View, SIGNAL( triggered() ), this, SLOT( onTabViewTriggered() ) );
    connect( ui->actionOpen_Project, SIGNAL( triggered() ), this, SLOT( onOpenProjectBtnClicked() ) );
    connect( ui->actionProject_Properties, SIGNAL( triggered() ), this, SLOT( onProjectPropertiesClicked() ) );    
    connect( m_projectManager, SIGNAL( projectClosed() ), this, SLOT( onProjectClosed() ) );
    connect( m_centralWidget, SIGNAL( subWindowActivated(QMdiSubWindow*) ), this, SLOT( onSubWindowActivated(QMdiSubWindow*) ) );
    //connect( m_assetsView->listWidget(), SIGNAL( doubleClicked(QModelIndex) ), this, SLOT(onRelatedFileDbClicked(QModelIndex) ) );
    connect( ui->actionRun_test, SIGNAL( triggered() ), this, SLOT( onTestRunBtnClicked() ) );
    connect( ui->actionBuild, SIGNAL( triggered() ), this, SLOT( onBuildClicked() ) );
    connect( ui->actionAbout_Qt, SIGNAL( triggered() ), qApp, SLOT( aboutQt() ) );
    connect( ui->actionAdd_Basic_Item_item, SIGNAL( triggered() ), this, SLOT( onCreateItemClicked() ) );
    connect( ui->actionAdd_Weapon, SIGNAL( triggered() ), this, SLOT( onCreateWeaponClicked() ) );
    connect( ui->actionAdd_Sword, SIGNAL( triggered() ), this, SLOT( onCreateSwordClicked() ) );
    connect( ui->actionUpdate_Assets, SIGNAL( triggered() ), this, SLOT( onAssetsUpdateClicked() ) );

    connect( ui->actionConsole, SIGNAL( triggered() ), this, SLOT( openConsole() ) );
    connect( ui->actionList_of_Physics_properties, SIGNAL( triggered() ), this, SLOT( openPhysicsProperties() ) );
    connect( ui->actionProjectiles_List, SIGNAL( triggered() ), this, SLOT( openProjectileList() ) );
    connect( m_assetsManager, SIGNAL(newProjectFiles(QStringList) ), m_assetsView, SLOT( addNewRelatedFiles(QStringList) ) );
    connect( ui->actionNew_file, SIGNAL( triggered( ) ), this, SLOT( createRawFile() ) );
}

void MainWindow::fillEditor( QsciScintilla *editor, QString filepath ) {
    QFile file( filepath );
    if( file.open( QIODevice::ReadOnly ) ) {
        QTextStream stream( &file );
        editor->setText( stream.readAll() );
        editor->setMarginLineNumbers( 1, true );
        editor->setMarginWidth( 1, "-----" );
    }
}

void MainWindow::onTestRunBtnClicked() {
    if( m_projectManager->isOpen() ) {
        bool error = false;
        enableGui( false );
        qDebug() << *m_projectManager->getProject() << endl;
        bool ok = m_projectManager->writeModinfoFile();

        if( !ok ) {
            qDebug() << "Cannot write .modinfo file" << endl;
            return;
        }

        QString projectPath = m_projectManager->getProject()->projectPath();

        QString modPath = m_starboundDir + "/mods/" + m_projectManager->getProject()->projectName();

        ok = ModManager::installMod( projectPath, m_projectManager->getProject()->projectName(), modPath );

        if( ok ) {
            //Starting Starbound
            QString binFile = m_starboundDir + "/" + STARBOUND_APP_PATH;
            m_starProcess = new QProcess( this );
            m_starProcess->setWorkingDirectory( QFileInfo( binFile ).path() );
            m_starProcess->start( binFile, QStringList() );

            connect( m_starProcess, SIGNAL( started() ), this, SLOT( onProcessStarted() ) );
            connect( m_starProcess, SIGNAL( finished(int,QProcess::ExitStatus) ), this, SLOT( onProcessFinished(int, QProcess::ExitStatus) ) );
        } else {
            qDebug() << "Some error occured during copying process, cannot start the game" << endl;
            statusBar()->showMessage( tr( "Some error occured during copying process, cannot start the game" ) );
            enableGui( true );
            ModManager::uninstallMod( modPath );
        }
    }
}

void MainWindow::onProcessStarted() {
    statusBar()->showMessage( "<font color='green'>" + tr( "Starbound is running" ) + "</font>" );
}

void MainWindow::onProcessFinished(int exitCode, QProcess::ExitStatus exitStatus) {
    qDebug() << "=======PROCESS FINISHED========" << endl;

    enableGui( true );
    if( exitStatus == QProcess::Crashed ) {
        statusBar()->showMessage( "<font color='red'>" + tr( "Oh no... Starbound crashed..." ) + "</font>" );
    }

    m_starProcess->disconnect( SIGNAL( started() ) );
    m_starProcess->disconnect( SIGNAL( finished(int,QProcess::ExitStatus) ) );

    m_starProcess->deleteLater();
    m_starProcess = NULL;

    if( m_projectManager->isOpen() ) {
        QString projectName = m_projectManager->getProject()->projectName();
        QString projectStarPath = m_starboundDir + "/mods/" + projectName;

        ModManager::uninstallMod( projectStarPath ) ;
    }
}


void MainWindow::onBuildClicked() {
    if( m_projectManager->isOpen() ) {
        QString modPath = m_starboundDir + "/mods/" + m_projectManager->getProject()->projectName();
        BuildModDialog bmd( m_projectManager->getProject(), modPath, this );

        if( bmd.exec() != QDialog::Accepted ) {
            qDebug() << "An error occured at mainwindow.cpp:290" << endl;
        }

    }
}

void MainWindow::onErrorLogged( LogMessage msg ) {
    QString message = msg.message;
    message.replace( "<br \/>", ", " );
    statusBar()->showMessage( msg.message );
}

void MainWindow::openConsole()
{
    LoggerDialog *logger = new LoggerDialog( m_log, this );
    logger->exec();
}

void MainWindow::openPhysicsProperties()
{
    PhysicsPropertiesList ppl( m_assetsManager, this );
    ppl.exec();
}

void MainWindow::openProjectileList()
{
    ProjectileListWindow plw( m_assetsManager, this );

    plw.exec();
}

void MainWindow::validateJson()
{
    SubWindow *win = qobject_cast<SubWindow*>( m_centralWidget->activeSubWindow() );

    if( win != NULL ) {
        TextEditorWidget *widget = qobject_cast<TextEditorWidget*>( win->widget() );

        if( widget ) {
            QsciScintilla *editor = widget->editor();
            if( editor ) {
                QString filepath = m_subwindowsList[ win ];
                JsonValidator validator( editor->text().toUtf8() );
                if( !validator.validate() ) {
                    JsonError lastError = validator.lastError();
                    qDebug() << lastError.line << endl;
                    int number = editor->markerDefine( QsciScintilla::RightTriangle );
                    editor->setMarkerForegroundColor( QColor( 255, 20, 0 ), number );
                    editor->markerDeleteAll();
                    editor->markerAdd( lastError.line - 1, number );

                    widget->console()->append( "<font color='red'>" + lastError.errorString + "</font> line " + QString::number( lastError.line ) + ":" + QString::number( lastError.col ) + " in " + m_subwindowsList[win] );
                    widget->setConsoleEnabled( true );
                } else {
                    widget->console()->append( "<font color='green'>" + tr( "Your JSON is valid!" ) + "</font>" );
                }
            }
        }
    }
}

void MainWindow::toggleSubWindowConsole()
{
    SubWindow *win = qobject_cast<SubWindow*>( m_centralWidget->activeSubWindow() );

    if( win ) {
        TextEditorWidget *widget = qobject_cast<TextEditorWidget *>( win->widget() );

        if( widget ) {
            widget->toggleConsole();
        }
    }
}

void MainWindow::clearTextEditorConsole()
{
    SubWindow *win = qobject_cast<SubWindow*>( m_centralWidget->activeSubWindow() );

    if( win ) {
        TextEditorWidget *widget = qobject_cast<TextEditorWidget*>( win->widget() );

        if( widget ) {
            widget->console()->clear();
        }
    }
}

void MainWindow::createRawFile()
{
    if( m_projectManager->isOpen() ) {
        QString projectPath = QFileInfo( m_projectManager->getProject()->projectPath() ).path() + "/files";
        QString file = QFileDialog::getSaveFileName( this, tr( "Create a new file" ), projectPath, QString( "Config File( *.config );;Lua Files( *.lua );;All files (*.*)") );

        if( file != "" ) {
            if( file.contains( projectPath ) ) {
                //Trying to create the file
                QFile fileF( file );
                if( !fileF.open( QIODevice::WriteOnly ) ) {
                    throw FileException( tr( "Cannot open" ) + file + ", " + fileF.errorString() );
                } else {
                    QString filename = QFileInfo( file ).fileName();
                    openSubWindow( filename, file );
                    //m_assetsView->addElementToListWidget( filename, file );
                }
            } else {
                QMessageBox::warning( this, tr( "Wrong save folder" ), tr( "Cannot save " ) + QFileInfo( file ).fileName() + tr( " outside of the files project directory" ) );
            }
        }
    }
}

void MainWindow::closeEvent( QCloseEvent *ce ) {
    if( shutdown() ) {
        ce->accept();
    } else {
        ce->ignore();
    }
}

void MainWindow::onAddProjectileActionClicked() {
    if( m_projectManager->isOpen() ) {
        ProjectileEditionWindow pew( m_assetsManager, QFileInfo( m_projectManager->getProject()->projectPath() ).path() + "/files", this );

        if( pew.exec() == QDialog::Accepted ) {

        }
    }
}

bool MainWindow::shutdown() {
    if( isWindowModified() ) {
        int rep = QMessageBox::question( this, tr( "Confirm quitting" ), tr( "Some of your work has not be saved and will be lost" ), QMessageBox::Save | QMessageBox::Close | QMessageBox::Cancel );

        if( rep == QMessageBox::Save ) {
            saveProject();
            return true;
        } else if( rep == QMessageBox::Close ) {
            return true;
        } else {
            return false;
        }
    }
    return true;
}

void MainWindow::onSaveBtnClicked() {
    saveProject();

    this->statusBar()->showMessage( m_projectManager->getProject()->projectName() + tr( " successfully saved" ), 5000 );
}

void MainWindow::saveProject() {    
    saveAllSubWindows();
    m_projectManager->save();
    setWindowModified( false );
}

void MainWindow::onRowSelected( QModelIndex idx ) {
    //qDebug() << "Selected " << idx.data().toString() << endl;
    QModelIndex child = idx.child( 0, 0 );
    qDebug() << "r: " << idx.row() << " col: " << idx.column() << "child: " << child.data() << endl;
}


void MainWindow::onRowDoubleClicked( QModelIndex idx ) {
    idx = idx.sibling( idx.row(), 0 );

    QStringList list;
    QModelIndex parent = idx.parent();
    //Get parents tree until assets item
   while( parent.isValid() && ( parent.data().toString() != "assets" || parent.data().toString() != "files" ) ){
        list.append( parent.data().toString() );
        parent = parent.parent();
    }
    //Reversing array
   list = Utility::reverseArray( list );
   //Building full path
   //QString assetsPath = m_assetsManager->assetsPath() + "/";
   QString assetsPath = "";
   for( QStringList::iterator it = list.begin(); it != list.end(); ++it ) {
       QString item = *it;
       assetsPath += item + "/";
   }
   assetsPath += idx.data().toString();

   if( !m_projectManager->isEditedFile( assetsPath ) )
    openSubWindow( idx.data().toString(), assetsPath );
   else {
       QString relatedFile = m_projectManager->getEditedFileFromStarBPath( assetsPath );
       openSubWindow( QFileInfo( assetsPath ).fileName(), relatedFile );
   }
}

void MainWindow::onDirectoryLoaded( QString d ) {
    Q_UNUSED( d);
    /*qDebug() << d << endl;
    FileSystemModel *model = qobject_cast<FileSystemModel*>( m_assetsView->treeView()->model() );

    QModelIndex idx = model->index( d );
    int rows = model->rowCount( idx );
    for( int i = 0; i < rows; i++ ) {
        QModelIndex childIndex =  idx.child( i, 0 );
        if( childIndex.isValid() ) {
            QString filepath = model->filePath( childIndex );

            if( m_projectManager->isEditedFile( filepath ) ) {
                qDebug() << "Yolo" << filepath << endl;
                model->setData( childIndex, QBrush( QColor( 255, 0, 0 ) ), Qt::BackgroundRole );
                m_assetsView->treeView()->repaint();
            }
        }
    }*/
    //qDebug() << idx.child( 0, 0 ).data() << endl;
}

void MainWindow::openSubWindow( QString windowTitle, QString filepath ) {
    FileType type = m_assetsManager->getFileType( filepath );
    SubWindow *window = NULL;
    if( type == Image ) {
        QLabel *label = new QLabel();
        label->setPixmap( QPixmap( filepath ) );
        label->setAlignment( Qt::AlignCenter );

        window = new SubWindow( this );
        window->setWidget( label );
        window->setWindowIcon( QIcon( QPixmap( "assets/images/imageViewer.png" ) ) );
    } else if( type == Lua ) {
        QsciScintilla *editor = new QsciScintilla;
        QsciLexerLua *lexer = new QsciLexerLua;

        editor->setLexer( lexer );
        editor->setFolding( QsciScintilla::BoxedTreeFoldStyle );
        editor->setAutoIndent( true );
        editor->setAutoCompletionSource( QsciScintilla::AcsAll );
        editor->setAutoCompletionThreshold( 2 );

        fillEditor( editor, filepath );

        //window = qobject_cast<SubWindow*>( m_centralWidget->addSubWindow( editor ) );
        window = new SubWindow( this );
        QActionGroup *group = addTextEditorTools( editor );
        window->setActionGroup( group );
        window->setWidget( editor );
        window->setWindowIcon( QIcon( QPixmap( "assets/images/textEditor.png" ) ) );
        connect( editor, SIGNAL( textChanged() ), this, SLOT( onTextEditorChanged() ) );

    } else if( type == Json ) {
        /*QTextEdit *editor = new QTextEdit( this );
        JsonHighlighter *json = new JsonHighlighter( editor->document() );*/
        QsciScintilla *editor = new QsciScintilla;
        QsciLexerJavaScript *lexer = new QsciLexerJavaScript;

        editor->setLexer( lexer );

        editor->setFolding( QsciScintilla::BoxedTreeFoldStyle );
        editor->setAutoIndent( true );
        editor->setAutoCompletionSource( QsciScintilla::AcsAll );
        editor->setAutoCompletionThreshold( 2 );

        fillEditor( editor, filepath );

        window = new SubWindow( this );
        TextEditorWidget *textEditorWidget = new TextEditorWidget( editor, this );
        QActionGroup *group = addJsonEditorTools( editor );
        window->setActionGroup( group );
        window->setWidget( textEditorWidget );
        window->setWindowIcon( QIcon( QPixmap( "assets/images/textEditor.png" ) ) );
        connect( editor, SIGNAL( textChanged() ), this, SLOT( onTextEditorChanged() ) );
    }

    if( window != NULL ) {
        m_centralWidget->addSubWindow( window );
        window->resize( 460, 320 );
        window->setAttribute( Qt::WA_DeleteOnClose );
        window->setWindowTitle( windowTitle + "[*]" );
        window->show();
        addSubWindowToWindowList( window, filepath );
        connect( window, SIGNAL( aboutToClose(QCloseEvent*) ), this, SLOT( onSubWindowClose( QCloseEvent* ) ) );
    }

}

QActionGroup* MainWindow::addTextEditorTools( QsciScintilla *editor ) {
    QActionGroup *tools = new QActionGroup( this );

    QAction *save = new QAction( QIcon( QPixmap( "assets/images/saveFile.png" ) ), tr( "Save file" ), this );
    QAction *cut = new QAction( QIcon( QPixmap( "assets/images/cut.png" ) ), tr( "Cut" ), this );
    QAction *copy = new QAction( QIcon( QPixmap( "assets/images/copy.png" ) ), tr( "Copy" ), this );
    QAction *paste = new QAction( QIcon( QPixmap( "assets/images/paste.png" ) ), tr( "Paste" ), this );

    QAction *separator = new QAction( this );
    separator->setSeparator( true );

    QAction *toggleWindow = new QAction( QIcon( QPixmap( "assets/images/window.png" ) ), tr( "Toggle window console" ), this );
    QAction *clearConsole = new QAction( QIcon( QPixmap( "assets/images/clear.png" ) ), tr( "Clear console" ), this );

    save->setShortcut( QKeySequence( "Ctrl+S" ) );

    save->setToolTip( tr( "Save current file" ) );
    save->setStatusTip( tr( "Save current file. The file will be copied into project directory, original file will NOT be modified" ) );
    cut->setToolTip( tr( "Cut selected text" ) );
    copy->setToolTip( tr( "Copy selected text" ) );
    paste->setToolTip( tr( "Paste previously copied text" ) );
    toggleWindow->setToolTip( tr( "Hide or display window's console" ) );
    clearConsole->setToolTip( tr( "Clear the contents of the debug console" ) );

    tools->addAction( save );
    tools->addAction( cut );
    tools->addAction( copy );
    tools->addAction( paste );
    tools->addAction( separator );
    tools->addAction( toggleWindow );
    tools->addAction( clearConsole );

    connect( save, SIGNAL( triggered() ), this, SLOT( onSaveFile() ) );
    connect( editor, SIGNAL( copyAvailable(bool) ), this, SLOT( copyAvailable( bool ) ) );
    connect( copy, SIGNAL( triggered() ), this, SLOT( onCopy() ) );
    connect( paste, SIGNAL( triggered() ), this, SLOT( onPaste() ) );
    connect( toggleWindow, SIGNAL( triggered() ), this, SLOT( toggleSubWindowConsole() ) );
    connect( clearConsole, SIGNAL( triggered() ), this, SLOT( clearTextEditorConsole() ) );

    return tools;
}

QActionGroup *MainWindow::addJsonEditorTools(QsciScintilla *editor)
{
    QActionGroup *tools = addTextEditorTools( editor );

    QAction *separator = new QAction( this );
    separator->setSeparator( true );

    QAction *validateJson = new QAction( QIcon( QPixmap( "assets/images/jsonValidator.png" ) ), tr( "Json Validation" ), this );

    validateJson->setToolTip( tr( "Validate your json file and highlight errors" ) );

    tools->addAction( separator );
    tools->addAction( validateJson );

    connect( validateJson, SIGNAL( triggered() ), this, SLOT( validateJson() ) );

    return tools;
}

void MainWindow::onSaveFile() {
    SubWindow *win = qobject_cast<SubWindow*>( m_centralWidget->activeSubWindow() );

    if( win ) {
        TextEditorWidget *widget = qobject_cast<TextEditorWidget*>( win->widget() );

        if( widget ) {
            QsciScintilla *editor = qobject_cast<QsciScintilla*>( widget->editor() );

            if( editor ) {
                QString filepath = m_subwindowsList[win];


                if( saveFile( editor->text(), filepath ) ) {
                    win->setWindowModified( false );
                }
            }
        }
    }
}

void MainWindow::saveAllSubWindows() {
    for( QMap<SubWindow*, QString>::iterator it = m_subwindowsList.begin(); it != m_subwindowsList.end(); ++it ) {
        SubWindow *win = it.key();
        TextEditorWidget *widget = qobject_cast<TextEditorWidget*>( win->widget() );

        if( widget ) {
            QsciScintilla *editor = qobject_cast<QsciScintilla*>( widget->editor() );
            QString filepath = it.value();

            if( editor ) {
                if( win->isWindowModified() ) {
                    if( saveFile( editor->text(), filepath ) ) {
                        m_projectManager->getProject()->addOpenFileToQueue( m_subwindowsList[ win ] );
                        win->setWindowModified( false );
                    }
                }
            }
        }
    }
}

bool MainWindow::saveFile( QString content, QString filepath ) {
    SubWindow *sub = qobject_cast< SubWindow* >( m_centralWidget->activeSubWindow() );
    bool isStarboundFile = false;
    if( !m_projectManager->isEditedFile( filepath ) )
        isStarboundFile = true;

    if( !m_projectManager->saveFile( content, filepath, isStarboundFile ) ) {
        this->statusBar()->showMessage( tr( "Cannot save " ) + filepath, 10000 );
        return false;
    }

    //If we save a non-edited file ( a file which is not yet in the edited files list)
    if( isStarboundFile ) {
        QString commonPath = Utility::getCommonPath( filepath, "assets/" );
        QString projectFilePath = QFileInfo( m_projectManager->getProject()->projectPath() ).path() + "/files/" + commonPath;

        //We change the path of the file of the sub window, when the project will be reopen, the file in project folder will be open
        m_subwindowsList[ sub ] = projectFilePath;
        m_assetsView->addElementToListWidget( QFileInfo(filepath).fileName(), filepath );
    }

    return true;
}

void MainWindow::onCopy() {
    QsciScintilla *editor = qobject_cast<QsciScintilla*>( m_centralWidget->activeSubWindow()->widget() );

    if( editor != NULL ) {
        editor->copy();
    }
}

void MainWindow::onPaste() {
    QsciScintilla *editor = qobject_cast<QsciScintilla*>( m_centralWidget->activeSubWindow()->widget() );

    editor->paste();
}

void MainWindow::copyAvailable( bool available ) {
    Q_UNUSED( available );

}

void MainWindow::onSubWindowClose( QCloseEvent *ce ) {
    SubWindow *window = qobject_cast<SubWindow*>( sender() );
    if( window != NULL ) {
        if( window->isWindowModified() ) {
            int rep = QMessageBox::question( this, tr( "Confirm close" ), tr( "Some of your work has not been saved and will be lost" ), QMessageBox::Save | QMessageBox::Close | QMessageBox::Cancel );

            if( rep == QMessageBox::Save ) {
                //SaveSubWindow...
                removeSubWindowFromWindowList( window );
                ce->accept();
            } else if( rep == QMessageBox::Close ) {
                removeSubWindowFromWindowList( window );
                ce->accept();
            } else {
                ce->ignore();
            }
        }
    }
}

void MainWindow::onTabViewTriggered() {
    if( ui->actionTab_View->isChecked() ) {
        m_centralWidget->setViewMode( QMdiArea::TabbedView );
    } else {
        m_centralWidget->setViewMode( QMdiArea::SubWindowView );
    }
}

void MainWindow::addSubWindowToWindowList(SubWindow *win , QString filepath) {
    m_subwindowsList[win] = filepath;
    //refreshSubWindowsList();
}

void MainWindow::removeSubWindowFromWindowList( SubWindow *win ) {
    qDebug() << m_subwindowsList.size() << endl;
    QString filepath = m_subwindowsList[win];
    m_subwindowsList.remove( win );
    m_projectManager->getProject()->removeOpenFile( filepath );
    qDebug() << m_subwindowsList.size() << endl;
    m_centralWidget->removeSubWindow( win );
    //refreshSubWindowsList();
}

void MainWindow::refreshSubWindowsList() {
   /* m_subWindowsMenu->clear();

    for( QMap<SubWindow*, QString>::iterator it = m_subwindowsList.begin(); it != m_subwindowsList.end(); ++it ) {
        QString title = it.key()->windowTitle();
        QAction *a = m_subWindowsMenu->addAction( title );
        connect( a, SIGNAL( triggered() ), this, SLOT( onSubWindowSelected() ) );
    }*/
}

void MainWindow::onSubWindowSelected() {
    /*QAction *a = qobject_cast<QAction*>( sender() );
    if( a != NULL ) {
        QList<QAction*> actions = m_subWindowsMenu->actions();
        //Uncheck every other actions
        for( QList<QAction*>::iterator it = actions.begin(); it != actions.end(); ++it ) {
            QAction *ac = *it;
            if( ac == a ) {
                a->setChecked( true );
            } else {
                a->setChecked( false );
            }
        }
    }
    SubWindow *sw = getSubWindow( a->text() );
    sw->activateWindow();*/
}

void MainWindow::onSubWindowActivated( QMdiSubWindow *s ) {
    SubWindow *win = qobject_cast<SubWindow*>( s );

    if( win ) {
        if( m_currentActionGroup != NULL ) {
            QList<QAction*> actionList = m_currentActionGroup->actions();

            //Removing old actions from tool bar
            for( QList<QAction*>::iterator it = actionList.begin(); it != actionList.end(); ++it ) {
                ui->mainToolBar->removeAction( *it );
            }
        }

        //Adding new actions
        if( win->actionGroup() != NULL ) {
            ui->mainToolBar->addActions( win->actionGroup()->actions() );
            m_currentActionGroup = win->actionGroup();
        }
    }
}

SubWindow *MainWindow::getSubWindow( QString windowTitle ) {
    Q_UNUSED( windowTitle );
    /*for( QMap<SubWindow*, QString >::iterator it = m_subwindowsList.begin(); it != m_subwindowsList.end(); ++it ) {
        SubWindow *sb = it.key();
        if( sb->windowTitle() == windowTitle ) {
            return sb;
        }
    }*/
    return NULL;
}

void MainWindow::onOpenProjectBtnClicked() {
    QString path = m_projectManager->getWorkspace().path();

    QString projectFile = QFileDialog::getOpenFileName( this, tr( "Open a StarMod project" ), path , QString( "StarMod Projects (*.smp)") );

    if( projectFile != "" ) {
        openProject( projectFile );
    }
}

/*void MainWindow::onSubWindowModified( bool m ) {
    SubWindow *win = qobject_cast<SubWindow*>( sender()->parent() );

    if( win ) {
        win->setWindowModified( m );
    }
}*/

bool MainWindow::openProject( QString projectPath ) {
    if( m_projectManager->openProject( projectPath ) ) {
        //qDebug() << m_projectManager->getProject() << endl;
        enableProjectRelatedActions( true );

        QStringList relatedFilesList = m_projectManager->getProject()->getEditedFiles();
        for( QStringList::iterator it = relatedFilesList.begin(); it != relatedFilesList.end(); ++it ) {
            //QString file = Utility::getCommonPath( *it, "files" + QString( QDir::separator() ) );
            //QString file = Utility::getCommonPath( *it, "files/" );
            QString file = *it;
            //m_assetsView->addElementToListWidget( QFileInfo( file ).fileName(), file );

        }

        QStringList openFilesList = m_projectManager->getProject()->getOpenFilesList();

        if( !openFilesList.empty() ) {
            //Opening Files
            for( QStringList::iterator it = openFilesList.begin(); it != openFilesList.end(); ++it ) {
                QString file = *it;
                QFileInfo fifo( file );

                /*
                 * Verifying if the file has been edited in previous
                 * sessions
                 * If it was, opening the edited file into the project directory
                 * else, opening the file in starbound directory
                 */
                /*if( m_projectManager->isEditedFile( file ) ) {
                    QString relatedFile = m_projectManager->getEditedFileFromStarBPath( file );
                    qDebug() << file << endl;
                    openSubWindow( fifo.fileName(), relatedFile );
                } else {*/
                openSubWindow( fifo.fileName(), file );
                //}
            }
        }

        addProjectToRecents( m_projectManager->getProject() );
        //loadProjectDirectory();
        loadAssetsList();
        return true;
    }
    return false;
}

void MainWindow::onTextEditorChanged() {
    QsciScintilla *editor = qobject_cast<QsciScintilla*>( sender() );
    if( editor != NULL )
    {
        SubWindow *subWindow = qobject_cast<SubWindow*>( editor->parent()->parent()->parent() );
        subWindow->setWindowModified( true );
        this->setWindowModified( true );
    }
}

void MainWindow::onProjectPropertiesClicked() {
    if( m_projectManager->isOpen() ) {
        ProjectPropertiesWindow ppw( tr( "Project Properties" ), m_projectManager->getWorkspace().path(), this );
        ppw.setProjectName( m_projectManager->getProject()->projectName() );
        ppw.setProjectAuthor( m_projectManager->getProject()->projectAuthor() );
        ppw.setProjectPath( m_projectManager->getProject()->projectPath() );
        ppw.setProjectVersion( m_projectManager->getProject()->projectVersion() );
        ppw.setStarboundVersion( m_projectManager->getProject()->starboundVersion() );

        if( ppw.exec() == QDialog::Accepted ) {
            m_projectManager->getProject()->setProjectName( ppw.getProjectName() );
            m_projectManager->getProject()->setProjectAuthor( ppw.getProjectAuthor() );
            m_projectManager->getProject()->setProjectPath( ppw.getProjectPath() );
            m_projectManager->getProject()->setProjectVersion( ppw.getProjectVersion() );
            m_projectManager->getProject()->setStarboundVersion( ppw.getStarboundVersion() );
        }
    }
}

void MainWindow::addProjectToRecents( const Project *prj ) {
    QSettings recentFile( RECENT_PROJECT_PATH, QSettings::IniFormat );


    QStringList projects = recentFile.value( "Projects", QStringList() ).toStringList();
    bool found = false;
    if( projects.size() > 0 ) {
        for( int i = 0; i < projects.size(); ++i ) {
            QString cProject = projects.at( i );
            if( cProject == prj->projectPath() ) {
                found = true;
            }
         }

        if( !found ) {
            projects.push_front( prj->projectPath() );
            while( projects.size() > 5 ) {
                projects.pop_back();
            }
            recentFile.setValue( "Projects", projects );
        }
    } else {
        projects.push_back( prj->projectPath() );
        recentFile.setValue( "Projects", projects );
    }
    refreshRecentProjectsList();
}

void MainWindow::refreshRecentProjectsList() {
    freeRecentsProjectsMenu();

    QSettings recentFiles( RECENT_PROJECT_PATH, QSettings::IniFormat );

    QStringList fileList = recentFiles.value( "Projects", QStringList() ).toStringList();
    for( QStringList::iterator it = fileList.begin(); it != fileList.end(); ++it ) {
        QString file = *it;
        QFileInfo fifo( file );
        QAction *a = ui->menuRecents_Projects->addAction( fifo.fileName() );
        a->setData( file );
        a->setCheckable( true );
        if( m_projectManager->isProjectOpen( file ) ) {
            a->setChecked( true );
            a->setEnabled( false );
        } else {
            a->setChecked( false );
            a->setEnabled( true );
        }

        connect( a, SIGNAL( triggered() ), this, SLOT( onRecentProjectClicked() ) );
    }
}

void MainWindow::onRecentProjectClicked() {
    QAction *a = qobject_cast<QAction*>( sender() );

    openProject( a->data().toString() );
}

void MainWindow::freeRecentsProjectsMenu() {
    QList<QAction*> actionsList = ui->menuRecents_Projects->actions();

    for( QList<QAction*>::iterator it = actionsList.begin(); it != actionsList.end(); ++it ) {
        QAction *a = *it;
        a->disconnect( SIGNAL( triggered() ) );
    }
    ui->menuRecents_Projects->clear();
}

void MainWindow::onProjectClosed() {    
    ui->actionSave->setEnabled( false );
    ui->actionSave_as->setEnabled( false );
    ui->actionProject_Properties->setEnabled( false );
    ui->actionRun_test->setEnabled( false );
    ui->actionBuild->setEnabled( false );

   //m_assetsView->listWidget()->clear();
    m_assetsView->deleteProjectTree();
   //Delete the model
   m_assetsView->itemView()->model()->deleteLater();
   for( QMap<SubWindow*, QString>::iterator it = m_subwindowsList.begin(); it != m_subwindowsList.end(); ++it ) {
        SubWindow *win = it.key();
        win->disconnect( SIGNAL( aboutToClose(QCloseEvent*) ) );
        win->close();
    }
    m_subwindowsList.clear();
    QTreeView *view = m_assetsView->treeView();

    view->disconnect( SIGNAL( expanded( QModelIndex ) ) );
    view->disconnect( SIGNAL( clicked( QModelIndex ) ) );
    view->disconnect( SIGNAL( doubleClicked( QModelIndex ) ) );
    refreshRecentProjectsList();
}

void MainWindow::onRelatedFileDbClicked( QModelIndex item ) {
    QString file = item.data( Qt::UserRole + 1 ).toString();

    if( m_projectManager->isOpen() ) {
        //QFileInfo prjPath( m_projectManager->getProject()->projectPath() );
        QString fullPath = /*prjPath.path() + "/files/" + */file;

        openSubWindow( QFileInfo( fullPath ).fileName(), fullPath );
    }
}


void MainWindow::enableGui( bool enable ) {
    this->setEnabled( enable );
}


void MainWindow::onCreateItemClicked()
{
    if( m_projectManager->isOpen() ) {
        ItemEditionWindow editionWin( m_assetsManager, QFileInfo( m_projectManager->getProject()->projectPath() ).path() + "/files", true, this );

        if( editionWin.exec() == QDialog::Accepted ) {

        }
    }
}

void MainWindow::onCreateWeaponClicked() {
    if( m_projectManager->isOpen() ) {
        WeaponEditionDialog wed( m_assetsManager, QFileInfo( m_projectManager->getProject()->projectPath() ).path() + "/files", true, this );

        if( wed.exec() == QDialog::Accepted ) {
            //Processing stuff
        }
    }
}

void MainWindow::onCreateSwordClicked()
{
    if( m_projectManager->isOpen() ) {
        SwordEditionWindow sew( m_assetsManager, QFileInfo( m_projectManager->getProject()->projectPath() ).path() + "/files", true, this );

        if( sew.exec() == QDialog::Accepted ) {

        }
    }
}

void MainWindow::onAssetsUpdateClicked() {
    QString msg = tr( "You are about to update the assets cache, this operation will erase the current extracted assets and extract the packed.pak file into Starbound.") + "<br />" +
                  tr( "This can be usefull to do after an update of the game to keep the cache up to date." ) + "<br/>";

    if( m_projectManager->isOpen() ) {
        msg += "<strong>" + tr( "Your current project will be closed before the update operation" ) + "</strong><br />";
    }

    msg += tr( "The operation is quite slow and the software may not respond for a while, do you want to proceed?");

    int rep = QMessageBox::question( this, tr( "Update confirmation" ), msg, QMessageBox::Yes | QMessageBox::No );

    if( rep == QMessageBox::Yes ) {
        if( m_projectManager->isOpen() ) {
            if( !m_projectManager->closeProject() ) {
                return;
            }
        }

        QDir workspace( m_workspaceDir );
        workspace.cd( "Starbound/assets" );
        QStringList filesToRemove;
        Utility::recurseAddDir( workspace, filesToRemove );

        QProgressDialog progress;
        progress.setWindowTitle( tr( "Deleting cache..." )  );
        progress.setCancelButton( 0 );
        progress.setMinimum( 0 );
        progress.setMaximum( filesToRemove.size() );
        progress.setMinimumDuration( 1000 );
        progress.show();

        for( int i = 0; i < filesToRemove.size(); i++ ) {
            QFile::remove( filesToRemove[i] );
            progress.setLabelText( tr( "Deleted " ) + filesToRemove[i] );
            progress.setValue( i + 1 );
            QCoreApplication::processEvents( );
        }

        workspace.removeRecursively();

        workspace.cdUp();
        workspace.mkdir( "assets" );

        //Extracting
        QString extractorPath = getAssetsExtractorPath();

        QStringList arguments;
        QString source = m_starboundDir + "/assets/packed.pak";
        QString destination = m_workspaceDir + "/Starbound/assets";

        source = QDir::toNativeSeparators( source );
        destination = QDir::toNativeSeparators( destination );

        arguments.append( source );
        arguments.append( destination );

        AssetsExtractionDialog dialog( extractorPath, arguments, this );
        dialog.exec();

        //Parsing assets
        m_assetsManager->clearAssetsList();
        m_assetsManager->parseAssets( NULL, true );

        QMessageBox::information( this, tr( "Success" ), tr( "The assets cache has successfully been updated" ) );
    }
}

QWizard *MainWindow::createStarterWizard() {
    QWizard *wizard = new QWizard( this );
    wizard->setWindowTitle( tr( "Starmod Configuration" ) );
    wizard->setPage( INTRO, new IntroPage );
    wizard->setPage( PATH, new StarboundPathPage );
    wizard->setPage( WORKSPACE, new WorkspacePage );
    wizard->setPage( FINISH, new EndPage );

    connect( wizard, SIGNAL( currentIdChanged(int) ), this, SLOT( onPageChanged( int ) ) );

    return wizard;
}

void MainWindow::onPageChanged( int pageId ) {
    switch( pageId ) {
        case INTRO:
            qDebug() << "Intro" << endl;
            break;
        case PATH:
            qDebug() << "Path" << endl;
            break;
        case WORKSPACE:
            qDebug() << "Workspace" << endl;
            break;
        case FINISH:
            qDebug() << "Finish" << endl;
            break;
    }
}

QString MainWindow::getAssetsExtractorPath() {
    QString extractorPath;

#ifdef Q_OS_WIN32
    extractorPath = m_starboundDir + "/win32/asset_unpacker.exe";
#elif defined( Q_OS_LINUX )
    #ifdef Q_PROCESSOR_X86_32
        extractorPath = m_starboundDir + "/linux32/asset_unpacker";
    #elif defined( Q_PROCESSOR_X86_64 )
        extractorPath = m_starboundDir + "/linux64/asset_unpacker";
#endif
    #elif Q_OS_MAC
        extractorPath = m_starboundDir + "/Starbound.app/Contents/MacOS/asset_unpacker";
#endif

    return extractorPath;
}
