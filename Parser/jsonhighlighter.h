#ifndef JSONHIGHLIGHTER_H
#define JSONHIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QTextDocument>
#include <QDebug>

enum JsonConstruction {
    JsonString, JsonComment, JsonNumeric
};

enum JsonState {
    JsonNormalState = -1,
    JsonInComment,
    JsonInString
};

class JsonHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT
public:
    explicit JsonHighlighter(QObject *parent = 0);
    JsonHighlighter( QTextDocument *doc );

    void setFormatFor( JsonConstruction construct, const QTextCharFormat &format );
    QTextCharFormat formatFor( JsonConstruction construct ) const {
        return m_formats[ construct ];
    }

protected:
    virtual void highlightBlock( const QString &text );

private:
    void init();

    bool isNumeric( QChar ch );
private:
    QTextCharFormat m_formats[ JsonNumeric + 1];
    
};

#endif // JSONHIGHLIGHTER_H
