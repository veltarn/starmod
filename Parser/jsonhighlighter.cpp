#include "jsonhighlighter.h"

JsonHighlighter::JsonHighlighter(QObject *parent) :
    QSyntaxHighlighter(parent)
{
    init();
}

JsonHighlighter::JsonHighlighter( QTextDocument *doc ) : QSyntaxHighlighter( doc ) {
    init();
}

void JsonHighlighter::init() {
    QTextCharFormat commentFormat;
    commentFormat.setForeground( QColor( 150, 158, 186 ) );
    commentFormat.setFontItalic( true );
    setFormatFor( JsonComment, commentFormat );

    QTextCharFormat stringFormat;
    stringFormat.setForeground( QColor( 9, 115, 21 ) );
    setFormatFor( JsonString, stringFormat );

    QTextCharFormat numericFormat;
    numericFormat.setForeground( QColor( 148, 18, 12 ) );
    setFormatFor( JsonNumeric, numericFormat );
}

void JsonHighlighter::setFormatFor( JsonConstruction construct, const QTextCharFormat &format ) {
    m_formats[ construct ] = format;
    rehighlight();
}

void JsonHighlighter::highlightBlock( const QString &text ) {
    int state = previousBlockState();
    int len = text.size();
    int start = 0;
    int pos = 0;
    int posBegWord = 0;
    QChar prevChar = QChar::Null;
    Q_UNUSED ( posBegWord );
    Q_UNUSED( prevChar );
    QChar ch;

    while ( pos < len ) {
        switch( state ) {
            case JsonNormalState:
                ch = text.at( pos );
                if( ch == '/' ) {
                    //Multi-line comment case
                    if( text.mid( pos, 2 ) == "/*" ) {
                        state = JsonInComment;
                        setFormat( pos, len - pos, m_formats[ JsonComment ] );
                    } else if( text.mid( pos, 2 ) == "//" ) {
                        setFormat( pos, len - pos, m_formats[ JsonComment ] );
                    }
                } else if( ch == '"' ) { //Begin string case
                    state = JsonInString;
                    setFormat( pos, 1, m_formats[ JsonString ] );
                } else if( isNumeric( ch ) ) {
                    setFormat( pos, 1, m_formats[ JsonNumeric ] );
                }
            break;

            case JsonInComment:
                start = pos;
                while( pos < len ) {
                    ch = text.at( pos );
                    if( ch == '*' ) {
                        if( text.mid( pos, 2 ) == "*/" ) {
                            state = JsonNormalState;
                            break;
                        }
                    }
                    ++pos;
                }
                setFormat( start, ( pos + 2 ) - start, m_formats[ JsonComment ] );
            break;

            case JsonInString:
                start = pos;
                while( pos < len ) {
                    ch = text.at( pos );
                    if( ch == '"' ) {
                        state = JsonNormalState;
                        break;
                    }
                    ++pos;
                }
                setFormat( start, ( pos + 1 ) - start, m_formats[ JsonString ] );
            break;
        }
        ch = QChar::Null;
        ++pos;
        setCurrentBlockState( state );
    }
}

bool JsonHighlighter::isNumeric( QChar ch ) {
    int n = QVariant( ch ).toInt();
    if( n >= 48 && n <= 57 ) {
        return true;
    }
    return false;
}
