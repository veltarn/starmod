#include "luahighlighter.h"
/*
LuaHighlighter::LuaHighlighter(QObject *parent) :
    QSyntaxHighlighter(parent)
{
    init();
}

LuaHighlighter::LuaHighlighter( QTextDocument *doc ) :
    QSyntaxHighlighter( doc )
{
    init();
}

void LuaHighlighter::init() {
    QTextCharFormat keywordFormat;
    keywordFormat.setForeground( QColor( 109, 9, 84 ) );
    keywordFormat.setFontWeight( QFont::Bold );
    setFormatFor( Keyword, keywordFormat );

    QTextCharFormat symbolsFormat;
    symbolsFormat.setForeground( QColor( 148, 76, 12 ) );
    setFormatFor( Symbols, symbolsFormat );

    QTextCharFormat numericFormat;
    numericFormat.setForeground( QColor( 148, 18, 12 ) );
    setFormatFor( Numeric, numericFormat );

    QTextCharFormat stringFormat;
    stringFormat.setForeground( QColor( 9, 115, 21 ) );
    setFormatFor( String, stringFormat );

    QTextCharFormat commentFormat;
    commentFormat.setForeground( QColor( 150, 158, 186 ) );
    commentFormat.setFontItalic( true );
    setFormatFor( Comment, commentFormat );

    QTextCharFormat parameterFormat;
    parameterFormat.setFontItalic( true );
    setFormatFor( Parameter, parameterFormat );

    m_keywords.append( "and" );
    m_keywords.append( "break" );
    m_keywords.append( "do" );
    m_keywords.append( "else" );
    m_keywords.append( "elseif" );
    m_keywords.append( "end" );
    m_keywords.append( "false" );
    m_keywords.append( "for" );
    m_keywords.append( "function" );
    m_keywords.append( "if" );
    m_keywords.append( "in" );
    m_keywords.append( "local" );
    m_keywords.append( "nil" );
    m_keywords.append( "not" );
    m_keywords.append( "or" );
    m_keywords.append( "repeat" );
    m_keywords.append( "return" );
    m_keywords.append( "then" );
    m_keywords.append( "true" );
    m_keywords.append( "until" );
    m_keywords.append( "while" );

    m_symbols.append( '(' );
    m_symbols.append( ')' );
    m_symbols.append( '{' );
    m_symbols.append( '}' );
    m_symbols.append( '[' );
    m_symbols.append( ']' );
    m_symbols.append( '=' );
    m_symbols.append( '+' );
    m_symbols.append( '-' );
    m_symbols.append( '*' );
    m_symbols.append( '/' );

}

void LuaHighlighter::setFormatFor( LuaConstruction construct, const QTextCharFormat &format ) {
    m_formats[construct] = format;
    rehighlight();
}

bool LuaHighlighter::isSymbol( QChar ch ) {
    for( int i = 0; i < m_symbols.size(); ++i ) {
        if( ch == m_symbols.at( i ) )
            return true;
    }
    return false;
}

void LuaHighlighter::highlightBlock( const QString &text ) {
    int state = previousBlockState();
    int len = text.size();
    int start = 0;
    int pos = 0;
    int posBegWord = 0;
    QChar prevChar = QChar::Null;
    QByteArray buffer;
    /*QString trimmed = text.trimmed();
    QStringList words = trimmed.split( " " );*//*
    QChar ch;
    while( pos < len ) {
        switch( state ) {
            case NormalState:
            default:
                ch = text.at( pos );
                if( ch == '-' ) {
                    if( text.mid( pos, 2 ) == "--" ) {
                        setFormat( pos, len, m_formats[ Comment ] );
                        pos = len - 1;
                    }
                } else if( isNumber( ch ) ) {
                    setFormat( pos, 1, m_formats[ Numeric ] );
                } else if( ch == '"') {
                    setFormat( pos, 1, m_formats[ String ] );
                    state = InString;
                } else {
                    if( ( prevChar == ' ' || prevChar == QChar::Null ) && ch != ' ' ) //New word
                    {
                        posBegWord = pos;
                        buffer.clear();
                    }

                    if( ch != ' ' && ch != '\t' && !isSymbol( ch ) )
                        buffer.append( ch );

                    prevChar = ch;

                    //if( ch == ' ' ) {

                        //If the buffer is a keyword AND current caracter is not a special symbol
                        if( isKeyword( buffer ) ) {
                            setFormat( posBegWord, ( pos + 1 ) - posBegWord, m_formats[Keyword] );
                            /*buffer.clear();
                            posBegWord = 0;*//*
                        } else if( ch != ' ' ) {
                            QTextCharFormat format;
                            format.setForeground( QColor( 0, 0, 0 ) );
                            format.setFontWeight( QFont::Normal );
                            setFormat( posBegWord, ( pos + 1 ) - posBegWord, format );
                        }
                    //}
                }
                break;
            case InString:
                start = pos;
                while( pos < len ) {
                    ch = text.at( pos );
                    if( ch == '"' ) {
                        state = NormalState;
                        break;
                    }
                    ++pos;
                }
                setFormat( start, ( pos + 1 ) - start, m_formats[ String ] );
                posBegWord = pos + 1;
            break;
        }
        ch = QChar::Null;
        ++pos;
        setCurrentBlockState( state );
    }

}

bool LuaHighlighter::isNumber( QChar ch ) {
    int n = QVariant( ch ).toInt();
    //if n is between 48 and 57 in Ascii table
    if( n >= 48 && n <= 57 ) {
        return true;
    }
    return false;
}

bool LuaHighlighter::isKeyword( QByteArray buffer ) {
    for( int i = 0; i < m_keywords.size(); ++i ) {
        if( m_keywords[i] == buffer ) {
            return true;
        }
    }
    return false;
}
*/
