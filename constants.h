#ifndef CONSTANTS_H
#define CONSTANTS_H

#define DATABASE_NAME "assets/starmodConfig.db3"
#define CONFIG_FILE_PATH "data/config.ini"
#define RECENT_PROJECT_PATH "data/recents_projects.ini"
#define APP_NAME "StarMod"
#define APP_VERSION QString( "0.2.8 - Pre-Alpha" )

#ifdef WIN32
    #define STARBOUND_APP_PATH "win32/launcher/launcher.exe"
#endif

#ifdef __x86_64__
    #define STARBOUND_APP_PATH "linux64/starbound"
#elif defined( __linux__ )
    #define STARBOUND_APP_PATH "linux32/starbound"
#endif


#endif // CONSTANTS_H
