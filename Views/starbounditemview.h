#ifndef STARBOUNDITEMVIEW_H
#define STARBOUNDITEMVIEW_H

#include <QAbstractItemView>
#include <QRubberBand>
#include <QPainter>
#include <QBrush>
#include <QPen>
#include <QColor>
#include "../Starbound/Items/staritem.h"

class StarboundItemView : public QAbstractItemView
{
    Q_OBJECT
public:
    explicit StarboundItemView(QWidget *parent = 0);

    virtual QModelIndex indexAt( const QPoint &point ) const;
    virtual void scrollTo( const QModelIndex &index, ScrollHint hint );
    virtual QRect visualRect( const QModelIndex &index ) const;
    
signals:
    
public slots:
    
protected:
    virtual int horizontalOffset() const;
    virtual bool isIndexHidden( const QModelIndex &index ) const;
    virtual QModelIndex moveCursor( CursorAction cursorAction, Qt::KeyboardModifiers modifiers );
    virtual void setSelection( const QRect &rect, QItemSelectionModel::SelectionFlags command );
    virtual int verticalOffset() const;
    virtual QRegion visualRegionForSelection( const QItemSelection &selection ) const;
    void paintEvent( QPaintEvent *pe );
private:
    QRubberBand *m_selectionBand;
};

#endif // STARBOUNDITEMVIEW_H
