#include "starbounditemview.h"

StarboundItemView::StarboundItemView(QWidget *parent) :
    QAbstractItemView(parent)
{
}

QModelIndex StarboundItemView::indexAt( const QPoint &point ) const {
    return QModelIndex();
}

void StarboundItemView::scrollTo( const QModelIndex &index, ScrollHint hint ) {

}

QRect StarboundItemView::visualRect( const QModelIndex &index ) const {
    return QRect();
}

int StarboundItemView::horizontalOffset() const {
    return 0;
}

bool StarboundItemView::isIndexHidden( const QModelIndex &index ) const {
    return false;
}

QModelIndex StarboundItemView::moveCursor( CursorAction cursorAction, Qt::KeyboardModifiers modifiers ) {
    return QModelIndex();
}

void StarboundItemView::setSelection( const QRect &rect, QItemSelectionModel::SelectionFlags command ) {

}

int StarboundItemView::verticalOffset() const {
    return 0;
}

QRegion StarboundItemView::visualRegionForSelection( const QItemSelection &selection ) const {
    return QRegion();
}

void StarboundItemView::paintEvent( QPaintEvent *pe ) {
    Q_UNUSED( pe );
    QPainter painter( viewport() );
    painter.setRenderHint(QPainter::Antialiasing);

    if( model() ) {
        qDebug() << this->model()->rowCount() << endl;
        for( int i = 0; i < this->model()->rowCount(); i++ ) {
            QModelIndex index = this->model()->index( i, 0 );
            qDebug() << index.isValid() << endl;
            void *ptr = index.data().value<void*>( );
            StarItem *item = static_cast<StarItem*>( ptr );
            qDebug() << sizeof( *item ) << endl;
        }
    } else {
        int width_v = width();
        int height_v = height();

        painter.drawText(QRect( 10, height_v / 2, width_v - 20, height_v - 10 ), tr( "No items" ) );
    }
}
