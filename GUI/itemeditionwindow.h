#ifndef ITEMEDITIONWINDOW_H
#define ITEMEDITIONWINDOW_H

#include <QDialog>
#include <QString>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include "../Starbound/Items/staritem.h"
#include "itemeditionwidget.h"
#include "recipeswidget.h"
#include "../Core/assetsmanager.h"

namespace Ui {
class ItemEditionWindow;
}

class ItemEditionWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ItemEditionWindow( AssetsManager *assetsManager, QString projectPath, bool createItem = false, QWidget *parent = 0);
    ~ItemEditionWindow();
public slots:
    void onValidate();
private:
    void buildInterface();
    void initEvents();
private:
    Ui::ItemEditionWindow *ui;
    AssetsManager *m_assetsManager;

    ItemEditionWidget *m_itemEditionWidget;
    RecipesWidget *m_recipesWidget;
    QString m_projectPath;
    bool m_createItem;

};

#endif // ITEMEDITIONWINDOW_H
