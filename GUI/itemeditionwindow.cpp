#include "itemeditionwindow.h"
#include "ui_itemeditionwindow.h"

ItemEditionWindow::ItemEditionWindow( AssetsManager *assetsManager, QString projectPath, bool createItem, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ItemEditionWindow),
    m_assetsManager( assetsManager ),
    m_recipesWidget( NULL ),
    m_projectPath( projectPath ),
    m_createItem( createItem )
{
    ui->setupUi(this);

    if( createItem )
        setWindowTitle( tr( "New Item" ) );
    else
        setWindowTitle( tr( "Item Edition" ) );

    buildInterface();
    initEvents();

}

ItemEditionWindow::~ItemEditionWindow()
{
    delete ui;
}

void ItemEditionWindow::onValidate()
{
    QStringList errorLists = m_itemEditionWidget->verifyFields();

    if( errorLists.size() > 0 )  {
        QString msg = tr( "The following fields are not completed" ) + "<ul>";
        int i = 0;
        foreach( QString err, errorLists ) {
            msg += "<li><strong>" + err + "</strong></li>";

        }
        msg += "</ul>";

        QMessageBox::warning( this, tr( "Incomplete fields" ), msg );
    } else {
        StarItem *item = new StarItem;

        item->setItemName( m_itemEditionWidget->getItemId() );
        item->setShortDescription( m_itemEditionWidget->getItemName() );

        if( m_itemEditionWidget->getInventoryIconPath() != "" )
            item->setInventoryIcon( m_itemEditionWidget->getInventoryIconPath() );

        if( m_itemEditionWidget->getItemDescription() != "" )
            item->setDescription( m_itemEditionWidget->getItemDescription() );

        if( m_itemEditionWidget->getApexDescription() != "" )
            item->setApexDescription( m_itemEditionWidget->getApexDescription() );

        if( m_itemEditionWidget->getAvianDescription() != "" )
            item->setAvianDescription( m_itemEditionWidget->getAvianDescription() );

        if( m_itemEditionWidget->getFloranDescription() != "" )
            item->setFloranDescription( m_itemEditionWidget->getFloranDescription() );

        if( m_itemEditionWidget->getGlitchDescription() != "" )
            item->setGlitchDescription( m_itemEditionWidget->getGlitchDescription() );

        if( m_itemEditionWidget->getHumanDescription() != "" )
            item->setHumanDescription( m_itemEditionWidget->getHumanDescription() );

        if( m_itemEditionWidget->getHylotlDescription() != "" )
            item->setHylotlDescription( m_itemEditionWidget->getHylotlDescription() );

        item->setRarity( m_itemEditionWidget->getItemRarity() );

        if( m_itemEditionWidget->getMaxStack() > 0 )
            item->setMaxStack( m_itemEditionWidget->getMaxStack() );

        if( m_itemEditionWidget->getFuelAmount() > 0 )
            item->setFuelAmount( m_itemEditionWidget->getFuelAmount() );

        QStringList blueprints = m_itemEditionWidget->getBlueprintsLearned();
        if( blueprints.size() > 0 )
            item->setLearnBlueprintsOnPickup( blueprints );

        if( m_itemEditionWidget->getStatusEffect().size() > 0 )
            item->setEffects( m_itemEditionWidget->getStatusEffect() );

        m_recipesWidget->validate( item );

        if( !m_assetsManager->createItem( m_projectPath, item ) ) {
            QMessageBox::critical( this, tr( "Error" ), tr( "Cannot create item" ) );
        } else {
            accept();
        }
    }
}

void ItemEditionWindow::buildInterface()
{
    m_itemEditionWidget = new ItemEditionWidget( m_assetsManager, this );
    m_recipesWidget = new RecipesWidget( m_assetsManager, this );

    QVBoxLayout *itemLayout = new QVBoxLayout;
    QVBoxLayout *recipeLayout = new QVBoxLayout;

    itemLayout->setMargin( 2 );
    recipeLayout->setMargin( 2 );

    itemLayout->addWidget( m_itemEditionWidget );
    recipeLayout->addWidget( m_recipesWidget );

    ui->itemTab->setLayout( itemLayout );
    ui->recipeTab->setLayout( recipeLayout );
}

void ItemEditionWindow::initEvents()
{
    connect( ui->validateButton, SIGNAL( clicked() ), this, SLOT( onValidate() ) );
    connect( ui->cancelButton, SIGNAL( clicked() ), this, SLOT( reject() ) );
}
