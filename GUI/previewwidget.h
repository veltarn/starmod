#ifndef PREVIEWWIDGET_H
#define PREVIEWWIDGET_H

#include <QWidget>
#include <QGraphicsScene>
#include "../Graphics/starboundcharacter.h"

namespace Ui {
class PreviewWidget;
}

class PreviewWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit PreviewWidget( QWidget *parent = 0 );
    ~PreviewWidget();

    void setCharacter( StarboundCharacter *chara );
    StarboundCharacter *getStarboundCharacter() const;
private:
    void init();

private:
    Ui::PreviewWidget *ui;
    QGraphicsScene *m_scene;
    StarboundCharacter *m_starboundCharacter;
};

#endif // PREVIEWWIDGET_H
