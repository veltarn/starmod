#ifndef FRAMEEDITIONWINDOW_H
#define FRAMEEDITIONWINDOW_H

#include <QDialog>
#include <QFileDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QMessageBox>
#include <QStringList>
#include <QImage>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include "frameimagepreview.h"

namespace Ui {
class FrameEditionWindow;
}

class FrameEditionWindow : public QDialog
{
    Q_OBJECT
    
public:
    explicit FrameEditionWindow( QString modPath, QWidget *parent = 0 );
    ~FrameEditionWindow();
    
    QString getFrameFile();
public slots:
    void onImageBrowse();
    void onValidate();
    void onClose();

    void onWidthChanged( int w );
    void onHeightChanged( int h );
private:
    void initEvents();
    void changeFramesNumber( int x, int y );
private:
    Ui::FrameEditionWindow *ui;
    FrameImagePreview *m_frameImagePreview;
    QString m_modPath;
    QString m_frameFile;
};

#endif // FRAMEEDITIONWINDOW_H
