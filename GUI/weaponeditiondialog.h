#ifndef WEAPONEDITIONDIALOG_H
#define WEAPONEDITIONDIALOG_H

#include <QDebug>
#include <QDialog>
#include <QDir>
#include <QFile>
#include <QFileInfo>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QStringList>
#include <QJsonValue>
#include <QVBoxLayout>
#include <QFileDialog>
#include <QColorDialog>
#include <QMessageBox>
#include <QPalette>
#include "../Core/assetsmanager.h"
#include "itemeditionwidget.h"
#include "projectileeditionwindow.h"
#include "recipeswidget.h"
#include "../Core/projectilesutility.h"
#include "../Starbound/Items/starweapon.h"

namespace Ui {
class WeaponEditionDialog;
}

class WeaponEditionDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit WeaponEditionDialog( AssetsManager *assetsManager, QString projectPath, bool createWeapon = false, QWidget *parent = 0);
    ~WeaponEditionDialog();

    bool isTwoHanded() const;
    void setTwoHanded( bool twoHanded );

    bool hasProjectile() const;
    void setHasProjectile( bool hasProjectile );

    bool isMelee() const;
    void setIsMelee( bool melee );

    int getEnergyCost() const;
    void setEnergyCost( int energyCost );

    int getClassMultiplier() const;
    void setClassMultiplier( int classMultiplier );

    double getFireTime() const;
    void setFireTime( double firetime );

    QString getFireSound() const;
    void setFireSound( QString soundPath );
    
    QString getImagePath() const;
    void setImagePath( QString path );

    QPointF getHandPosition();
    void setHandPosition( QPointF handPosition );

    QPointF getFirePosition();
    void setFirePosition( QPointF firePosition );

    /*void getDropCollision();
    void setDropCollision(); */


public slots:
    void onValidate();
    void onClose();

    void onFireSoundBrowse();
    void onImageBrowse();
    void onProjectileChecked( bool checked );

    void onCreateProjectileClicked();
    void onEditProjectileClicked();
    void onProjectileColorChecked();
private:
    bool validateFields();
    void initEvents();
    void buildInterface();
    void loadProjectilesList();
    bool addToComboboxFromFile( QString file );
private:
    Ui::WeaponEditionDialog *ui;
    ItemEditionWidget *m_itemEditionWidget;
    RecipesWidget *m_recipesWidget;
    ProjectileEditionWidget *m_projectileWidget;
    bool m_createWeapon;
    AssetsManager *m_assetsManager;
    QString m_projectFilesPath;
    QColor m_projectileColor;
};

#endif // WEAPONEDITIONDIALOG_H
