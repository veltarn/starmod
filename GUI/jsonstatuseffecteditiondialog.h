#ifndef JSONSTATUSEFFECTEDITIONDIALOG_H
#define JSONSTATUSEFFECTEDITIONDIALOG_H

#include <QDialog>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonObject>
#include "../Core/assetsmanager.h"
#include "../Core/cache.h"
#include "../Core/utility.h"

namespace Ui {
class JsonStatusEffectEditionDialog;
}

class JsonStatusEffectEditionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit JsonStatusEffectEditionDialog( AssetsManager *assetsManager, QWidget *parent = 0);
    ~JsonStatusEffectEditionDialog();

    bool cancelled() const;

    void setKind( const QString kind );
    QString getKind() const;
    double getAmount() const;
    void setAmount( double amount );
    int getPercentage() const;
    void setPercentage( int percentage );
    int getRange() const;
    void setRange( int range );
    //QString getColor() const;

public slots:
    void validate();
    void cancel();
private:
    void initEvents();
    void loadStatusEffects();
private:
    Ui::JsonStatusEffectEditionDialog *ui;

    bool m_cancelled;
    AssetsManager *m_assetsManager;
};

#endif // JSONSTATUSEFFECTEDITIONDIALOG_H
