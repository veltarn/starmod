#ifndef QUICKEDITOR_H
#define QUICKEDITOR_H

#include <QDebug>
#include <QDialog>
#include <QFile>
#include <QTextStream>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <Qsci/qsciscintilla.h>
#include <Qsci/qscilexerlua.h>
#include <Qsci/qscilexerjavascript.h>
#include <QPushButton>
#include <QToolBar>
#include <QAction>
#include <QFileDialog>
#include <QMessageBox>

namespace Ui {
class QuickEditor;
}


class QuickEditor : public QDialog
{
    Q_OBJECT
    
public:
    enum Syntax {
        Json, Lua
    };

public:
    explicit QuickEditor( QString modPath = "", QWidget *parent = 0);
    ~QuickEditor();
    void setSyntax( int syntax );
    
    QString getSaveFile() const;

public slots:
    void onClose();
    void onSaveClick();
private:
    void buildInterface();
    void initEvents();
    void save( QString filepath );
private:
    Ui::QuickEditor *ui;

    QVBoxLayout *m_mainLayout;

    QToolBar *m_toolbar;

    QAction *m_actionSave;

    QsciScintilla *m_editor;

    QPushButton *m_closeButton;

    QString m_saveFile;
    QString m_modPath;
};

#endif // QUICKEDITOR_H
