#include "recipeswidget.h"
#include "ui_recipeswidget.h"

RecipesWidget::RecipesWidget( AssetsManager *assetsManager, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RecipesWidget),
    m_assetsManager( assetsManager )
{
    ui->setupUi(this);
    buildInterface();
    initEvents();
}

RecipesWidget::~RecipesWidget()
{
    delete ui;
}

void RecipesWidget::validate(StarItem *item)
{
    StarRecipe starRecipe;
    QList< Input > inputs;
    Output output;
    QStringList groups;

    if( m_inputItemsList.size() > 0 ) {
        for( int i = 0; i < m_model->rowCount(); ++i ) {
            QModelIndex idx = m_model->index( i, 0 );
            QString inputItem = idx.data( Qt::UserRole + 1 ).toString();
            if( idx.isValid() ) {
                Input input;
                for( int j = 0; j < m_inputItemsList.size(); j++ ) {
                    if( inputItem == m_inputItemsList[j].item ) {
                        input = m_inputItemsList[j];

                        inputs.append( input );
                    }
                }
            }
        }
        output.item = item->itemName();
        output.count = ui->countSpinBox->value();

        for( int i = 0; i < ui->groupsList->count(); i++ ) {
            QListWidgetItem *item = ui->groupsList->item( i );
            groups.append( item->text() );
        }

        starRecipe.setInput( inputs );
        starRecipe.setOutput( output );
        starRecipe.setGroups( groups );

        item->setRecipe( starRecipe );
    }

}

void RecipesWidget::buildInterface() {
    ui->inputList->setContextMenuPolicy( Qt::CustomContextMenu );
    m_model = new QStandardItemModel( this );

    QStringList headers;
    headers << tr( "Item Name") << tr( "Count" );
    m_model->setHorizontalHeaderLabels( headers );

    ui->inputList->setModel( m_model );

    ui->addBtn->setIcon( QIcon( QPixmap( "assets/images/add.png" ) ) );
    ui->removeBtn->setIcon( QIcon( QPixmap( "assets/images/remove.png" ) ) );

    ui->addFilterButton->setIcon( QIcon( QPixmap( "assets/images/add.png" ) ) );
    ui->removeFilterButton->setIcon( QIcon( QPixmap( "assets/images/remove.png" ) ) );
}

void RecipesWidget::initEvents() {
    connect( ui->addBtn, SIGNAL( clicked() ), this, SLOT( onAddBtnClicked() ) );
    connect( ui->removeBtn, SIGNAL( clicked() ), this, SLOT( onRemoveBtnClicked() ) );
    connect( ui->inputList, SIGNAL( customContextMenuRequested(QPoint) ), this, SLOT( onCustomContextRequest(QPoint) ) );
    connect( ui->inputList, SIGNAL( clicked(QModelIndex) ), this, SLOT( onItemClicked(QModelIndex) ) );
    connect( ui->addFilterButton, SIGNAL( clicked() ), this, SLOT( onAddFilter() ) );
    connect( ui->editButton, SIGNAL( clicked() ), this, SLOT( onEditClicked() ) );
    connect( ui->removeFilterButton, SIGNAL( clicked() ), this, SLOT( onRemoveFilter() ) );
    connect( ui->groupsList, SIGNAL( clicked(QModelIndex) ), this, SLOT( onFilterClicked(QModelIndex) ) );
}

void RecipesWidget::onAddBtnClicked() {
    RecipeItemPicker win( m_assetsManager, this );
    win.exec();

    if( !win.cancelled() ) {
        int amount = win.getAmount();
        StarItem *item = win.getSelectedItem();

        if( item != NULL ) {
            qDebug() << item->itemName() << endl;
            if( !Utility::exists( item->itemName(), m_inputItemsList ) ) {
                QString displayedName;
                if( item->shortDescription() != "" )
                    displayedName = item->shortDescription();
                else
                    displayedName = item->itemName();

                QString fullIconPath = QFileInfo( item->itemLocation() ).path() + "/" + item->inventoryIcon();

                QStandardItem *rootItem = new QStandardItem( QIcon( QPixmap( fullIconPath ) ) ,displayedName );
                rootItem->setData( item->itemName() );
                qDebug() << fullIconPath << endl;
                //rootItem->setData( QIcon( QPixmap( fullIconPath ) ), Qt::DecorationRole );

                QStandardItem *countdData = new QStandardItem( QString::number( amount ) );
                //rootItem->appendRow( countdData );

                int row = m_model->rowCount();
                //m_model->appendRow( rootItem );
                m_model->setItem( row, 0, rootItem );
                m_model->setItem( row, 1, countdData );
                Input input;
                input.item = item->itemName();
                input.count = amount;

                m_inputItemsList.append( input );
            }
        }
    }
}

void RecipesWidget::onRemoveBtnClicked() {
    QModelIndex idx = ui->inputList->currentIndex();
    if( idx.isValid() && !idx.parent().isValid() ) {
        StarItem *item = m_assetsManager->getItem( idx.data( Qt::UserRole + 1 ).toString() );
        int rep = QMessageBox::question( this, tr( "Confirmation" ), tr( "Do you really want to delete " ) + idx.data().toString() + " ? " );

        if( rep == QMessageBox::Yes ) {
            m_model->removeRow( idx.row() );
            qDebug() << m_inputItemsList.size() << endl;
            StarRecipe::removeInput( item->itemName(), m_inputItemsList );
            qDebug() << m_inputItemsList.size() << endl;
        }
    }
}

void RecipesWidget::onEditClicked()
{
    QModelIndex idx = ui->inputList->currentIndex();
    if( idx.isValid() && !idx.parent().isValid() ) {
        QModelIndex child = idx.sibling( idx.row(), 1 );
        QString oldItem = idx.data( Qt::UserRole + 1 ).toString();
        int amount = child.data().toInt();
        qDebug() << amount << endl;

        RecipeItemPicker win( m_assetsManager, this );
        win.setAmount( amount );
        win.selectItem( idx.data().toString() );
        win.exec();

        //Continue
        int newAmount = win.getAmount();
        StarItem *item = win.getSelectedItem();

        if( item != NULL ) {
            Input input;
            input.item = item->itemName();
            input.count = newAmount;

            QString iconPath = QFileInfo( item->itemLocation() ).path() + "/" + item->inventoryIcon();
            m_model->setData( idx, item->shortDescription(), Qt::DisplayRole );
            m_model->setData( idx, item->itemName(), Qt::UserRole + 1 );
            m_model->setData( idx, QIcon( QPixmap( iconPath ) ), Qt::DecorationRole );

            m_model->setData( child, newAmount, Qt::DisplayRole );

            for( int i = 0; i < m_inputItemsList.size(); ++i ) {
                if( m_inputItemsList[i].item == oldItem ) {
                    m_inputItemsList[i] = input;
                }
            }
        } else {
            qDebug() << "Item not selected" << endl;
        }
    }

}

void RecipesWidget::setOutputName( QString name ) {
    ui->itemLineEdit->setText( name );
}

void RecipesWidget::onCustomContextRequest(QPoint pt)
{
    QModelIndex idx = ui->inputList->indexAt( pt );

    //Is root object
    if( idx.isValid() && !idx.parent().isValid() ) {
        QMenu *contextMenu = new QMenu( this );

        QAction *editAction = new QAction( QIcon( QPixmap( "assets/images/edit.png" ) ), tr( "Edit Item" ), this );
        QAction *removeAction = new QAction( QIcon( QPixmap( "assets/images/delete.png" ) ), tr( "Delete Item" ), this );

        contextMenu->addAction( editAction );
        contextMenu->addAction( removeAction );

        QPoint globalPt = ui->inputList->mapToGlobal( pt );

        connect( editAction, SIGNAL( triggered() ), this, SLOT( onEditClicked() ) );
        connect( removeAction, SIGNAL( triggered() ), this, SLOT( onRemoveBtnClicked() ) );

        contextMenu->exec( globalPt );
    }
}

void RecipesWidget::onItemClicked(QModelIndex idx)
{
    if( idx.isValid() ) {
        ui->editButton->setEnabled( true );
        ui->removeBtn->setEnabled( true );
    } else {
        ui->editButton->setEnabled( false );
        ui->removeBtn->setEnabled( false );
    }
}

void RecipesWidget::onAddFilter()
{
    RecipesGroupsPicker groupPicker( m_assetsManager, this );
    int ret = groupPicker.exec();

    if( ret == QDialog::Accepted ) {
        QString group = groupPicker.getSelectedGroup();

        ui->groupsList->addItem( group );
    }
}

void RecipesWidget::onRemoveFilter()
{
    QModelIndex idx = ui->groupsList->currentIndex();

    if( idx.isValid() ) {
        int rep = QMessageBox::question( this, tr( "Confirmation" ), tr( "Do you really want to delete " ) + "<strong>" + idx.data().toString() + "</strong> " + tr( "from groups list ?" ) );

        if( rep == QMessageBox::Yes ) {
            QListWidgetItem *item = ui->groupsList->takeItem( idx.row() );
            delete item;
        }
    }
}

void RecipesWidget::onFilterClicked(QModelIndex idx)
{
    if( idx.isValid() ) {
        ui->removeFilterButton->setEnabled( true );
    } else {
        ui->removeFilterButton->setEnabled( false );
    }
}
