#ifndef STATUSEFFECTCHOOSER_H
#define STATUSEFFECTCHOOSER_H

#include <QDebug>
#include <QDialog>
#include <QString>
#include <QJsonDocument>
#include <QList>
#include <QJsonValue>
#include <QJsonObject>
#include <QFile>
#include "../Core/assetsmanager.h"
#include "../Core/cache.h"
#include "Core/logger.h"
#include "Core/utility.h"

namespace Ui {
class StatusEffectChooser;
}

class StatusEffectChooser : public QDialog
{
    Q_OBJECT

public:
    explicit StatusEffectChooser( AssetsManager *assetsManager, QWidget *parent = 0);
    ~StatusEffectChooser();
    QString getStatusEffect();
    bool cancelled() const;

    bool editStatus() const;
    void setEditStatus(bool editStatus);

public slots:
    void validate();
    void addAndEdit();
    void close();
private:
    void initEvents();
    void loadStatusEffects();
private:
    Ui::StatusEffectChooser *ui;
    Logger *m_log;
    AssetsManager *m_assetsManager;
    QList<StatusEffect*> m_statusEffectList;
    bool m_cancelled;
    bool m_editStatus;
};

#endif // STATUSEFFECTCHOOSER_H
