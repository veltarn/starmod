#ifndef STATUSEFFECTSLISTWINDOW_H
#define STATUSEFFECTSLISTWINDOW_H

#include <QDialog>
#include <QToolBar>
#include <QAction>
#include <QPushButton>
#include <QTreeView>
#include <QStandardItemModel>
#include <QList>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSpacerItem>
#include "../Core/assetsmanager.h"
#include "../Starbound/statuseffect.h"
#include "statuseffectchooser.h"

namespace Ui {
class StatusEffectsListWindow;
}

class StatusEffectsListWindow : public QDialog
{
    Q_OBJECT

public:
    explicit StatusEffectsListWindow( QList<StatusEffect> &statusEffectList, AssetsManager *assetsManager, QWidget *parent = 0, bool readOnly = false );
    ~StatusEffectsListWindow();

private:
    void buildInterface();
    void createList();
    void initEvents();

private slots:
    void onCloseButtonClicked();
    void onAddEffectClicked();


private:
    Ui::StatusEffectsListWindow *ui;

    QToolBar *m_toolBar;
    QAction *m_addEffectAction;
    QAction *m_removeEffectAction;
    QPushButton *m_validateButton;
    QPushButton *m_closeButton;

    QTreeView *m_statusEffectsView;
    QStandardItemModel *m_model;

    QList<StatusEffect> &m_statusEffectList;
    AssetsManager *m_assetsManager;

    bool m_readOnly;
};

#endif // STATUSEFFECTSLISTWINDOW_H
