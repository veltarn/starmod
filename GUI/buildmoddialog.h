#ifndef BUILDMODDIALOG_H
#define BUILDMODDIALOG_H

#include <QDialog>
#include <QString>
#include <QFileInfo>
#include <QMessageBox>
#include <QFileDialog>
#include <QDir>
#include <QDebug>
#include "../Core/modmanager.h"
#include "../Core/project.h"

namespace Ui {
class BuildModDialog;
}

class BuildModDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit BuildModDialog( Project *prj, QString starboundModPath, QWidget *parent = 0);
    ~BuildModDialog();
    
public slots:
    void onValidate();
    void onBrowse();
    void onCheckBoxToggled( bool toggled );
    void changeInfoModLabelPath( QString newPath );

private:
    void initEvents();
private:
    Ui::BuildModDialog *ui;

    QString m_starboundModPath;
    QString m_defaultBuildPath;
    Project *m_project;
};

#endif // BUILDMODDIALOG_H
