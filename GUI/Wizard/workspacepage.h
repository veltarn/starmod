#ifndef WORKSPACEPAGE_H
#define WORKSPACEPAGE_H

#include <QWizardPage>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFileDialog>
#include <QStandardPaths>

class WorkspacePage : public QWizardPage
{
    Q_OBJECT
public:
    explicit WorkspacePage(QWidget *parent = 0);
    virtual bool validatePage();
signals:

public slots:
    void onWorkspaceBrowseButtonClicked();
private:
    void buildInterface();
    void initEvents();
private:
    QLabel *m_label;
    QLineEdit *m_path;
    QPushButton *m_browseButton;
    QVBoxLayout *m_mainLayout;
    QHBoxLayout *m_browseLayout;
};

#endif // WORKSPACEPAGE_H
