#include "workspacepage.h"

WorkspacePage::WorkspacePage(QWidget *parent) :
    QWizardPage(parent)
{
    setTitle( tr( "Workspace location" ) );

    buildInterface();
    initEvents();
}

bool WorkspacePage::validatePage()
{
    return true;
}

void WorkspacePage::buildInterface() {

    m_label = new QLabel( tr( "Great! ") + "<br />" +
                         tr( "Now, you have to configure the workspace" ) + "<br/>" +
                         tr( "In Starmod, the workspace will store Starbound extracted assets and projects" ) );

    m_path = new QLineEdit;
    m_browseButton = new QPushButton( tr( "Browse" ) );

    m_mainLayout = new QVBoxLayout;
    m_browseLayout = new QHBoxLayout;

    m_browseLayout->addWidget( m_path, 4 );
    m_browseLayout->addWidget( m_browseButton, 1 );

    m_mainLayout->addWidget( m_label );
    m_mainLayout->addLayout( m_browseLayout );

    QString defaultLoc = QStandardPaths::writableLocation( QStandardPaths::HomeLocation ) + "/Starmod";

    setLayout( m_mainLayout );

    registerField( "workspace", m_path );
    m_path->setText( defaultLoc );
}

void WorkspacePage::initEvents() {
    connect( m_browseButton, SIGNAL( clicked() ), this, SLOT( onWorkspaceBrowseButtonClicked() ) );
}

void WorkspacePage::onWorkspaceBrowseButtonClicked() {
    QString path = QFileDialog::getExistingDirectory( this, tr( "Starmod Workspace" ), QString() );

    QString defaultLoc = QStandardPaths::writableLocation( QStandardPaths::HomeLocation ) + "/Starmod";
    if( path != "" ) {
        m_path->setText( path );
    } else {
        m_path->setText( defaultLoc );
    }
}
