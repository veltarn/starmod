#include "endpage.h"

EndPage::EndPage(QWidget *parent) :
    QWizardPage(parent)
{
    setTitle( tr( "Finished" ) );

    buildInterface();
}

void EndPage::buildInterface() {
    QLabel *label = new QLabel( tr( "Starmod Configuration is now over") );

    QVBoxLayout *mainLayout = new QVBoxLayout;

    mainLayout->addWidget( label );


    setLayout( mainLayout );
}
