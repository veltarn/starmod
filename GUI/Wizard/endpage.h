#ifndef ENDPAGE_H
#define ENDPAGE_H

#include <QWizardPage>
#include <QLabel>
#include <QVBoxLayout>

class EndPage : public QWizardPage
{
    Q_OBJECT
public:
    explicit EndPage(QWidget *parent = 0);

signals:

public slots:

private:
    void buildInterface();

};

#endif // ENDPAGE_H
