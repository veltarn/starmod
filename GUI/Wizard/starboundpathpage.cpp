#include "starboundpathpage.h"

StarboundPathPage::StarboundPathPage(QWidget *parent) :
    QWizardPage(parent)
{
    setTitle( tr( "Starbound Location" ) );
    buildInterface();
    initEvents();
}

bool StarboundPathPage::validatePage()
{
    return true;
}

void StarboundPathPage::buildInterface() {
    m_label = new QLabel( tr( "First of all, you have to put the path of Starbound" ) );

    m_path = new QLineEdit;
    m_browseButton = new QPushButton( tr( "Browse" ) );

    m_mainLayout = new QVBoxLayout;
    m_browseLayout = new QHBoxLayout;

    m_browseLayout->addWidget( m_path, 4 );
    m_browseLayout->addWidget( m_browseButton, 1 );

    m_mainLayout->addWidget( m_label );
    m_mainLayout->addLayout( m_browseLayout );



    setLayout( m_mainLayout );

    registerField( "starboundPath*", m_path );
}

void StarboundPathPage::initEvents() {
    connect( m_browseButton, SIGNAL( clicked() ), this, SLOT( onWizardStarPathBrowseButtonClicked() ) );
}

void StarboundPathPage::onWizardStarPathBrowseButtonClicked() {
    QString path = QFileDialog::getExistingDirectory( this, tr( "Locate Starbound directory" ), QString() );

    if( path != "" ) {
        m_path->setText( path );
    }
}
