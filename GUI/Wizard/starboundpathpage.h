#ifndef STARBOUNDPATHPAGE_H
#define STARBOUNDPATHPAGE_H

#include <QWizardPage>
#include <QLabel>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QFileDialog>

class StarboundPathPage : public QWizardPage
{
    Q_OBJECT
public:
    explicit StarboundPathPage(QWidget *parent = 0);
    virtual bool validatePage();
signals:

public slots:
    void onWizardStarPathBrowseButtonClicked();
private:
    void buildInterface();
    void initEvents();
private:
    QLabel *m_label;
    QLineEdit *m_path;
    QPushButton *m_browseButton;
    QVBoxLayout *m_mainLayout;
    QHBoxLayout *m_browseLayout;
};

#endif // STARBOUNDPATHPAGE_H
