#include "intropage.h"

IntroPage::IntroPage(QWidget *parent) :
    QWizardPage(parent)
{
    setTitle( tr( "Let's begin" ) );
    buildInterface();
}

void IntroPage::buildInterface() {
    QLabel *label  = new QLabel( "<strong>" + tr( "Welcome and thank you to choose Starmod!") + "</strong><br /><br />" +
                                     tr( "Apparently, this is the first time you start the software, you have to set some parameters up before using it." ) + "<br/>" +
                                     tr( "This assistant will help you to setup your software" ) + "<br />" +
                                     tr( "Please click Next to begin the setup process") );
    label->setWordWrap( true );

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget( label );

    setLayout( layout );
}
