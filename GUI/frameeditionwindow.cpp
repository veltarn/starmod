#include "frameeditionwindow.h"
#include "ui_frameeditionwindow.h"

FrameEditionWindow::FrameEditionWindow( QString modPath, QWidget *parent ) :
    QDialog(parent),
    ui(new Ui::FrameEditionWindow),
    m_frameImagePreview( NULL ),
    m_modPath( modPath )
{
    ui->setupUi(this);
    initEvents();
    setWindowTitle( tr( "Frame Creation" ) );
}

FrameEditionWindow::~FrameEditionWindow()
{
    delete ui;
}

void FrameEditionWindow::initEvents() {
    connect( ui->validateButton, SIGNAL( clicked() ), this, SLOT( onValidate() ) );
    connect( ui->closeButton, SIGNAL( clicked() ), this, SLOT( onClose() ) );
    connect( ui->frameImageBrowse, SIGNAL( clicked() ), this, SLOT( onImageBrowse() ) );
    connect( ui->widthSpinBox, SIGNAL( valueChanged(int) ), this, SLOT( onWidthChanged(int) ) );
    connect( ui->heightSpinBox, SIGNAL( valueChanged( int ) ), this, SLOT( onHeightChanged(int) ) );
}

void FrameEditionWindow::onValidate() {
    bool error = false;
    QStringList errorsList;

    if( ui->nameLineEdit->text() == "" ) {
        error = true;
        errorsList.append( tr( "Frame name is empty" ) );
    }

    if( ui->frameImageFileLabel->text() == "" ) {
        error = true;
        errorsList.append( tr( "No frame image" ) );
    }

    if( error ) {
        QString msg = tr( "Cannot create frameset because of the following errors" ) + "<ul>";

        foreach( QString err, errorsList ) {
            msg += "<li>" + err + "</li>";
        }
        msg += "</ul>";
        QMessageBox::warning( this, tr( "Invalid fields" ), msg );
    } else {
        QString projectilePath = m_modPath + "/animations/projectiles/" + ui->nameLineEdit->text();
        //Creating projectile directory (and parent dir if not exists)
        QDir modRoot( m_modPath );
        if( !modRoot.mkpath( "animations/projectiles/" + ui->nameLineEdit->text() ) ) {
            QMessageBox::critical( this, tr( "Error" ), tr( "Cannot create projectile path" ) );
            return;
        }

        QString framePath = projectilePath + "/" + ui->nameLineEdit->text() + ".frames";
        QFile framesFile( framePath );

        if( QFile::exists( framePath ) )
            QFile::remove( framePath );

        if( !framesFile.open( QIODevice::WriteOnly ) ) {
            QMessageBox::critical( this, tr( "Error" ), tr( "Cannot create frame file" ) + "<br />" + framesFile.errorString() );
            return;
        }

        QJsonDocument jsonFile;
        QJsonObject root = jsonFile.object();

        QJsonObject frameGrid;
        QJsonArray size;
        size.append( ui->widthSpinBox->value() );
        size.append( ui->heightSpinBox->value() );

        frameGrid.insert( "size", size );

        QJsonArray dimensions;
        dimensions.append( ui->xSpinBox->value() );
        dimensions.append( ui->ySpinBox->value() );

        frameGrid.insert( "dimensions", dimensions );

        QJsonArray names;
        QJsonArray sub;
        for( int i = 0; i < m_frameImagePreview->getFramesNumberX(); i++ ) {
            sub.append( QVariant( i ).toString() );
        }
        names.append( sub );
        frameGrid.insert( "names", names );

        root.insert( "frameGrid", frameGrid );

        jsonFile.setObject( root );

        QTextStream stream( &framesFile );
        stream.setCodec( "UTF-8" );

        stream << jsonFile.toJson();

        framesFile.flush();

        QString frameImageFile = projectilePath + "/" + ui->nameLineEdit->text() + ".png";
        if( QFile::exists( frameImageFile ) )
            QFile::remove( frameImageFile );

        if( !QFile::copy( ui->frameImageLineEdit->text(), frameImageFile ) ) {
            QMessageBox::critical( this, tr( "Copy error" ), tr( "Cannot copy frame image file" ) );
            return;
        }
        m_frameFile = frameImageFile;

        qDebug() << "Frame file created!" << endl;
    }
    accept();
}

QString FrameEditionWindow::getFrameFile() {
    return m_frameFile;
}

void FrameEditionWindow::onClose() {
    accept();
}

void FrameEditionWindow::onWidthChanged( int w ) {
    if( m_frameImagePreview != NULL ) {
        m_frameImagePreview->setRectangleWidth( w );
        changeFramesNumber( m_frameImagePreview->getFramesNumberX(), m_frameImagePreview->getFramesNumberY() );
    }
}

void FrameEditionWindow::onHeightChanged( int h ) {
    if( m_frameImagePreview != NULL ) {
        m_frameImagePreview->setRectangleHeight( h );
        changeFramesNumber( m_frameImagePreview->getFramesNumberX(), m_frameImagePreview->getFramesNumberY() );
    }

}

void FrameEditionWindow::onImageBrowse() {
    QString cPath = ui->frameImageLineEdit->text();

    QString path = QFileDialog::getOpenFileName( this, tr( "Find frame image" ), cPath, QString( "Png Files( *.png)" ) );

    if( path != "" ) {
        ui->frameImageLineEdit->setText( path );

        QImage img( path );
        ui->widthSpinBox->setValue( img.width() );
        ui->heightSpinBox->setValue( img.height() );

        ui->xSpinBox->setValue( 1 );
        ui->ySpinBox->setValue( 1 );

        m_frameImagePreview = new FrameImagePreview( this );
        m_frameImagePreview->setImage( path );
        m_frameImagePreview->show();
    } else {
        ui->frameImageFileLabel->setText( cPath );
    }
}

void FrameEditionWindow::changeFramesNumber( int x, int y ) {
    ui->xSpinBox->setValue( x );
    ui->ySpinBox->setValue( y );
}
