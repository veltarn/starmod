#ifndef BLUEPRINTCHOOSERDIALOG_H
#define BLUEPRINTCHOOSERDIALOG_H

#include <QDebug>
#include <QDialog>
#include <QAction>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonObject>
#include <QFile>
#include <QList>
#include <QDir>
#include <QString>
#include <QStringList>
#include "../Starbound/Items/staritem.h"
#include "../Core/assetsmanager.h"
#include "../Core/cache.h"
#include "../Core/utility.h"

namespace Ui {
class BlueprintChooserDialog;
}

class BlueprintChooserDialog : public QDialog
{
    Q_OBJECT

public:
    explicit BlueprintChooserDialog( AssetsManager* assetsManager, QWidget *parent = 0);
    ~BlueprintChooserDialog();

    QString getSelectedItem();
public slots:
    void validate();
private:
    void initEvents();
    void loadBlueprints();
private:
    Ui::BlueprintChooserDialog *ui;
    AssetsManager *m_assetsManager;
};

#endif // BLUEPRINTCHOOSERDIALOG_H
