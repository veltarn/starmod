#ifndef OBJECTEDITIONWINDOW_H
#define OBJECTEDITIONWINDOW_H

#include <QDialog>

namespace Ui {
class ObjectEditionWindow;
}

class ObjectEditionWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ObjectEditionWindow(QWidget *parent = 0);
    ~ObjectEditionWindow();

private:
    Ui::ObjectEditionWindow *ui;
};

#endif // OBJECTEDITIONWINDOW_H
