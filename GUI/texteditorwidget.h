#ifndef TEXTEDITORWIDGET_H
#define TEXTEDITORWIDGET_H

#include <QWidget>
#include <QVBoxLayout>
#include <QSplitter>
#include <Qsci/qsciscintilla.h>
#include <QTextBrowser>

namespace Ui {
class TextEditorWidget;
}

class TextEditorWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TextEditorWidget( QsciScintilla *editor, QWidget *parent = 0);
    ~TextEditorWidget();


    QsciScintilla *editor() const;
    void setEditor(QsciScintilla *editor);

    QTextEdit *console() const;

    void toggleConsole();
    void setConsoleEnabled( bool enabled );
private:
    void buildInterface();
    void initEvents();
private:
    Ui::TextEditorWidget *ui;

    QVBoxLayout *m_layout;
    QSplitter *m_widgetSplitter;
    QsciScintilla *m_editor;
    QTextBrowser *m_console;
};

#endif // TEXTEDITORWIDGET_H
