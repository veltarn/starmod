#include "objectinfowidget.h"

ObjectInfoWidget::ObjectInfoWidget( StarItem *item, QWidget *parent) :
    QDialog(parent),
    m_item( item )
{
    createWidget();
}

void ObjectInfoWidget::createWidget() {
    QToolBox *stackedWidget = new QToolBox( this );
    QVBoxLayout *mainLayout = new QVBoxLayout;

    if( m_item->idObject() == "item" ) {
        createItemWidget( stackedWidget );
    } else if( m_item->idObject() == "consumable" ) {
        createConsumableWidget( stackedWidget );
    } else if( m_item->idObject() == "weapon") {
        createWeaponWidget( stackedWidget );
    } else if( m_item->idObject() == "miningtool" ) {
        createMiningToolWidget( stackedWidget );
    } else if( m_item->idObject() == "flashlight" ) {
        createFlashLightWidget( stackedWidget );
    } else if( m_item->idObject() == "beamaxe" ) {
        createBeamaxeWidget( stackedWidget );
    } else if( m_item->idObject() == "matitem" ) {
        createMatItemWidget( stackedWidget );
    } else if( m_item->idObject() == "instrument" ) {
        createInstrumentWidget( stackedWidget );
    } else if( m_item->idObject() == "sword" ) {
        createSwordWidget( stackedWidget );
    } else if( m_item->idObject() == "thrownitem" ) {
        createThrownWidget( stackedWidget );
    } else if( m_item->idObject() == "object" ) {
        createStarWorldObjectWidget( stackedWidget );
    }

    mainLayout->addWidget( stackedWidget );
    mainLayout->setMargin( 0 );

    setLayout( mainLayout );
}

void ObjectInfoWidget::createItemWidget( QToolBox *stackedWidget ) {
    QWidget *widget = new QWidget( this );
    QVBoxLayout *mainLayout = new QVBoxLayout;

    mainLayout->setMargin( 3 );

    QLabel *itemType =          createLabel( tr( "Item type" ), m_item->idObject() );
    QLabel *itemName =          new QLabel( "<strong>" + tr( "Item ID" ) + ":</strong> " + m_item->itemName(), this );
    QLabel *shortDescription =  new QLabel( "<strong>" + tr( "Short Description" ) + ":</strong> " + m_item->shortDescription(), this );
    QLabel *description =       new QLabel( "<strong>" + tr( "Description") + ":</strong> " + m_item->description(), this );
    QLabel *rarity =            new QLabel( "<strong>" + tr( "Rarity" ) + ":</strong> " + m_item->rarity(), this );
    QLabel *maxStack =          new QLabel( "<strong>" + tr( "Max Stack" ) + ":</strong> " + QString::number( m_item->maxStack() ), this );
    QLabel *fuelAmount =        new QLabel( "<strong>" + tr( "Fuel amount" ) + ":</strong> " + QString::number( m_item->fuelAmount() ), this );

    shortDescription->setWordWrap( true );
    description->setWordWrap( true );

    mainLayout->addWidget( itemType );
    mainLayout->addWidget( itemName );
    mainLayout->addWidget( shortDescription );
    mainLayout->addWidget( description );
    mainLayout->addWidget( rarity );
    mainLayout->addWidget( maxStack );
    mainLayout->addWidget( fuelAmount );

    widget->setLayout( mainLayout );

    QScrollArea *scroll = new QScrollArea( this );
    scroll->setWidget( widget );

    stackedWidget->addItem( scroll, tr( "Item" ) );

}

void ObjectInfoWidget::createConsumableWidget( QToolBox *stackedWidget ) {
    StarConsumable *cons = dynamic_cast<StarConsumable*>( m_item );
    QWidget *widget = new QWidget( this );
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->setMargin( 3 );

    createItemWidget( stackedWidget );


    QLabel *emote =     new QLabel( "<strong>" + tr( "Emote" ) + ":</strong> " + cons->emote(), this );
    QLabel *handPos =   new QLabel( "<strong>" + tr( "Hand Position" ) + ":</strong><br />" + "\tX: " + QString::number( cons->handPosition().x() ) + "<br />\tY: " + QString::number( cons->handPosition().y() ), this );

    mainLayout->addWidget( emote );
    mainLayout->addWidget( handPos );

    widget->setLayout( mainLayout );

    stackedWidget->addItem( widget, tr( "Consumable" ) );
}

void ObjectInfoWidget::createWeaponWidget( QToolBox *stackedWidget ) {
    StarWeapon *weap = dynamic_cast<StarWeapon*>( m_item );
    createItemWidget( stackedWidget );

    //QScrollArea *weaponScroll = new QScrollArea;
    QWidget *widget = new QWidget( this );
    QVBoxLayout *weaponLayout = new QVBoxLayout;

    QLabel *isMelee = new QLabel( "<strong>" + tr( "Melee Weapon" ) + ":</strong> " + QVariant( weap->isMelee() ).toString(), this );
    QLabel *twoHanded = new QLabel( "<strong>" + tr( "Two Handed" ) + ":</strong> " + QVariant( weap->twoHanded() ).toString(), this );
    QLabel *fireTime = new QLabel( "<strong>" + tr( "Fire Time" ) + ":</strong> " + QVariant( weap->fireTime() ).toString(), this );
    QLabel *handPosition = new QLabel( "<strong>" + tr( "Hand Position" ) + ":</strong><br /><br />" + "X: " + QString::number( weap->handPosition().x() ) + "<br />Y: " + QString::number( weap->handPosition().y() ), this );
    QLabel *firePosition = new QLabel( "<strong>" + tr( "Fire Position" ) + ":</strong><br />X: " + QString::number( weap->firePosition().x() ) + "<br />Y: " + QString::number( weap->firePosition().y() ), this );
    QLabel *level = new QLabel( "<strong>" + tr( "Level" ) + ":</strong> " + QString::number( weap->level() ), this );
    QLabel *energyCost = new QLabel( "<strong>" + tr( "Energy Cost" ) + ":</strong> " + QString::number( weap->energyCost() ) );
    QLabel *classMultiplier = new QLabel( "<strong>" + tr( "Class Multiplier" ) + ":</strong> " + QString::number( weap->classMultiplier() ) );

    weaponLayout->addWidget( isMelee );
    weaponLayout->addWidget( twoHanded );
    weaponLayout->addWidget( fireTime );
    weaponLayout->addWidget( handPosition );
    weaponLayout->addWidget( firePosition );
    weaponLayout->addWidget( level );
    weaponLayout->addWidget( energyCost );
    weaponLayout->addWidget( classMultiplier );

    widget->setLayout( weaponLayout );
    //weaponScroll->setWidget( widget );
    stackedWidget->addItem( widget, tr( "Weapon" ) );

    if( weap->hasProjectile() ) {
        //QScrollArea *projScroll = new QScrollArea;
        QWidget *projWidget = new QWidget;
        QVBoxLayout *projLayout = new QVBoxLayout;

        QLabel *projectileName = new QLabel( "<strong>" + tr( "Projectile Name" ) + ":</strong> " + weap->projectileName(), this );
        QLabel *projectileSpeed = new QLabel( "<strong>" + tr( "Projectile Speed" ) + ":</strong> " + QString::number( weap->projectileSpeed() ), this );
        QLabel *projectilePower = new QLabel( "<strong>" + tr( "Projectile Power" ) + ":</strong> " + QVariant( weap->projectilePower() ).toString(), this );
        QLabel *projectileLife = new QLabel( "<strong>" + tr( "Projectile Life Time" ) + ":</strong> " + QString::number( weap->projectileLifeTime() ), this );
        QLabel *projectileColor = new QLabel( "<strong>" + tr( "Projectile Color" ) + ":</strong> r: " + QString::number( weap->projectileColor().red() ) + " g: " + QString::number( weap->projectileColor().green() ) + " b: " + QString::number( weap->projectileColor().blue() ), this );

        projLayout->addWidget( projectileName );
        projLayout->addWidget( projectileSpeed );
        projLayout->addWidget( projectilePower );
        projLayout->addWidget( projectileLife );
        projLayout->addWidget( projectileColor );

        projWidget->setLayout( projLayout );
        //projScroll->setWidget( projWidget );
        stackedWidget->addItem( projWidget, tr( "Projectile" ) );
    }

}

void ObjectInfoWidget::createMiningToolWidget( QToolBox *toolBox ) {
    StarMiningTool *tool = dynamic_cast<StarMiningTool*>( m_item );

    createItemWidget( toolBox );

    QWidget *widget = new QWidget( this );

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->setMargin( 3 );

    QLabel *pointable =         createLabel( tr( "Pointable" ), tool->pointable() );
    QLabel *strikeSound =       createLabel( tr( "Strike Sound" ), tool->strikeSound() );
    QLabel *idleSound =         createLabel( tr( "Idle Sound" ), tool->idleSound() );
    QLabel *swingStart =        createLabel( tr( "Swing Start" ), tool->swingStart() );
    QLabel *swingFinish =       createLabel( tr( "Swing Finish" ), tool->swingFinish() );
    QLabel *largeImage =        createLabel( tr( "Large Image" ), tool->largeImage() );
    QLabel *image =             createLabel( tr( "Image" ), tool->image() );
    QLabel *inspectionKind =    createLabel( tr( "Inspection Kind" ), tool->inspectionKind() );
    QLabel *tileDamage =        createLabel( tr( "Tile Damage" ), tool->tileDamage() );
    QLabel *tileDamageBlunted = createLabel( tr( "Tile Damage Blunted" ), tool->tileDamageBlunted() );
    QLabel *animationCycle =    createLabel( tr( "Animation Cycle" ), tool->animationCycle() );

    QString regenChart;
    qDebug() << pointable->text() << endl;
    for( int i = 0; i < tool->durabilityRegenChart().size(); i++ ) {
        DurabilityRegen tmp = tool->durabilityRegenChart()[i];
        QStringList itemsList = tmp.regenItems;
        QString namesList;
        int j = 0;
        foreach( QString i, itemsList ) {
            namesList += i;

            if( j < itemsList.size() ) {
                namesList += ", ";
            }
        }
        regenChart += namesList + ": " + QString::number( tmp.regenAmount );

        if( i > 0 ) {
            regenChart += "<br />";
        }
    }

    QLabel *regenerationChart = createLabel( tr( "Durability Regeneration Chart" ), regenChart );
    QLabel *durability =        createLabel( tr( "Durability" ), tool->durability() );
    QLabel *durabilityPerUse =  createLabel( tr( "Durability Per Use" ), tool->durabilityPerUse() );
    QLabel *blockRadius =       createLabel( tr( "Block Radius" ), tool->blockRadius() );
    QLabel *altBlockRadius =    createLabel( tr( "Alt Block Radius" ), tool->altBlockRadius() );
    QLabel *fireTime =          createLabel( tr( "Fire Time" ), tool->fireTime() );
    QLabel *frames =            createLabel( "Frames", tool->frames() );

    mainLayout->addWidget( pointable );
    mainLayout->addWidget( strikeSound );
    mainLayout->addWidget( idleSound );
    mainLayout->addWidget( swingStart );
    mainLayout->addWidget( swingFinish );
    mainLayout->addWidget( largeImage );
    mainLayout->addWidget( image );
    mainLayout->addWidget( inspectionKind );
    mainLayout->addWidget( tileDamage );
    mainLayout->addWidget( tileDamageBlunted );
    mainLayout->addWidget( regenerationChart );
    mainLayout->addWidget( durability );
    mainLayout->addWidget( durabilityPerUse );
    mainLayout->addWidget( blockRadius );
    mainLayout->addWidget( altBlockRadius );
    mainLayout->addWidget( fireTime );
    mainLayout->addWidget( animationCycle );
    mainLayout->addWidget( frames );

    /*QScrollArea *scroll = new QScrollArea( this );
    scroll->setWidget( widget );*/

    widget->setLayout( mainLayout );

    toolBox->addItem( widget, tr( "Mining Tool" ) );
}

void ObjectInfoWidget::createFlashLightWidget(QToolBox *toolBox)
{
    StarFlashlight *flash = dynamic_cast<StarFlashlight*>( m_item );

    createItemWidget( toolBox );

    QWidget *widget = new QWidget( this );
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->setMargin( 3 );

    QLabel *largeImage =        createLabel( tr( "Large Image" ), flash->largeImage() );
    QLabel *inspectionKind =    createLabel( tr( "Inspection Kind" ), flash->inspectionKind() );
    QLabel *lightPosition =     createLabel( tr( "Light Position" ), getPointInfo( flash->lightPosition() ));
    QLabel *lightColor =        createLabel( tr( "Light Color" ), "r: " + QString::number( flash->lightColor().red() ) + " g: " + QString::number( flash->lightColor().green() ) + " b: " + QString::number( flash->lightColor().blue() ) );
    QLabel *beamWidth =         createLabel( tr( "Beam Width" ), flash->beamWidth() );
    QLabel *ambientFactor =     createLabel( tr( "Ambient Factor" ), flash->ambientFactor() );

    mainLayout->addWidget( largeImage );
    mainLayout->addWidget( inspectionKind);
    mainLayout->addWidget( beamWidth );
    mainLayout->addWidget( ambientFactor );
    mainLayout->addWidget( lightColor );
    mainLayout->addWidget( lightPosition );

    widget->setLayout( mainLayout );

    toolBox->addItem( widget, tr( "Flash Light" ) );
}

void ObjectInfoWidget::createBeamaxeWidget( QToolBox *toolBox ) {
    StarBeamaxe *tool = dynamic_cast<StarBeamaxe*>( m_item );

    createItemWidget( toolBox );

    QWidget *widget = new QWidget( this );
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->setMargin( 3 );

    QLabel *twoHanded =         createLabel( tr( "Two Handed" ), tool->twoHanded() );
    QLabel *category =          createLabel( tr( "Category" ), tool->category() );
    QLabel *inspectionKind =    createLabel( tr( "Inspection Kind" ), tool->inspectionKind() );
    QLabel *fireTime =          createLabel( tr( "Fire Time" ), tool->fireTime() );
    QLabel *blockRadius =       createLabel( tr( "Block Radius" ), tool->blockRadius() );
    QLabel *altBlockRadius =    createLabel( tr( "Alt Block Radius" ), tool->altBlockRadius() );
    QLabel *critical =          createLabel( tr( "Critical" ), tool->critical() );
    QLabel *strikeSound =       createLabel( tr( "Strike Sound" ), tool->strikeSound() );
    QLabel *image =             createLabel( tr( "Image" ), tool->image() );

    QString endImagesValue;
    for( int i = 0; i < tool->endImages().size(); i++ ) {
        endImagesValue += tool->endImages()[i];

        if( i < tool->endImages().size() - 1 ) {
            endImagesValue += "<br />";
        }
    }
    QLabel *endImages =             createLabel( tr( "End Images" ), endImagesValue );
    QLabel *firePosition =          createLabel( tr( "FirePosition" ), getPointInfo( tool->firePosition() ) );
    QLabel *handPosition =          createLabel( tr( "Hand Position" ), getPointInfo( tool->handPosition() ) );
    QLabel *segmentPerUnit =        createLabel( tr( "Segment Per Unit" ), tool->segmentPerUnit() );
    QLabel *ncpe =                  createLabel( tr( "Near Control Point Elasticity" ), tool->nearControlPointElasticity() );
    QLabel *fcpe =                  createLabel( tr( "Far Control Point Elasticity" ), tool->farControlPointElasticity() );
    QLabel *ncpd =                  createLabel( tr( "Near Control Point Distance" ), tool->nearControlPointDistance() );
    QLabel *targetSegmentRun =      createLabel( tr( "Target Segment Run" ), tool->targetSegmentRun() );
    QLabel *innerBrightnessScale =  createLabel( tr( "Inner Brightness Scale" ), tool->innerBrightnessScale() );
    QLabel *firstStripeThickness =  createLabel( tr( "First Stripe Thickness" ), tool->firstStripeThickness() );
    QLabel *secondStripeThickness = createLabel( tr( "Second Stripe Thickness" ), tool->secondStripeThickness() );
    QLabel *minBeamWidth =          createLabel( tr( "Min Beam Width"), tool->minBeamWidth() );
    QLabel *maxBeamWidth =          createLabel( tr( "Max Beam Width" ), tool->maxBeamWidth() );
    QLabel *maxBeamJitter =         createLabel( tr( "Max Beam Jitter" ), tool->maxBeamJitter() );
    QLabel *minBeamJitter =         createLabel( tr( "Min Beam Jitter" ), tool->minBeamJitter() );
    QLabel *minBeamTrans =          createLabel(  tr( "Min Beam Trans" ), tool->minBeamTrans() );
    QLabel *maxBeamTrans =          createLabel( tr( "Max Beam Trans" ), tool->maxBeamTrans() );
    QLabel *minBeamLines =          createLabel( tr( "Min Beam Lines" ), tool->minBeamLines() );
    QLabel *maxBeamLines =          createLabel( tr( "Max Beam Lines" ), tool->maxBeamLines() );

    mainLayout->addWidget( category );
    mainLayout->addWidget( inspectionKind );
    mainLayout->addWidget( twoHanded );
    mainLayout->addWidget( handPosition );
    mainLayout->addWidget( firePosition );
    mainLayout->addWidget( fireTime );
    mainLayout->addWidget( blockRadius );
    mainLayout->addWidget( altBlockRadius );
    mainLayout->addWidget( critical );
    mainLayout->addWidget( image );
    mainLayout->addWidget( endImages );
    mainLayout->addWidget( strikeSound );
    mainLayout->addWidget( segmentPerUnit );
    mainLayout->addWidget( ncpe );
    mainLayout->addWidget( fcpe );
    mainLayout->addWidget( ncpd );
    mainLayout->addWidget( targetSegmentRun );
    mainLayout->addWidget( innerBrightnessScale );
    mainLayout->addWidget( firstStripeThickness );
    mainLayout->addWidget( secondStripeThickness );
    mainLayout->addWidget( minBeamWidth );
    mainLayout->addWidget( maxBeamWidth );
    mainLayout->addWidget( minBeamJitter );
    mainLayout->addWidget( maxBeamJitter );
    mainLayout->addWidget( minBeamTrans );
    mainLayout->addWidget( maxBeamTrans );
    mainLayout->addWidget( minBeamLines );
    mainLayout->addWidget( maxBeamLines );

    widget->setLayout( mainLayout );

    toolBox->addItem( widget, tr( "Beamaxe" ) );
}

void ObjectInfoWidget::createMatItemWidget(QToolBox *toolBox)
{
    StarMatItem *item = dynamic_cast<StarMatItem*>( m_item );

    createItemWidget( toolBox );

    QWidget *widget = new QWidget;
    QVBoxLayout *layout = new QVBoxLayout;
    layout->setMargin( 3 );

    QLabel *matId = createLabel( tr( "Material Id" ), item->materialId() );

    layout->addWidget( matId );
    widget->setLayout( layout );

    toolBox->addItem( widget, tr( "MatItem" ) );
}

void ObjectInfoWidget::createInstrumentWidget(QToolBox *toolBox)
{
    StarInstrument *item = dynamic_cast<StarInstrument*>( m_item );

    createItemWidget( toolBox );

    QWidget *widget = new QWidget( this );
    QVBoxLayout *mainLayout = new QVBoxLayout;

    mainLayout->setMargin( 3 );

    QLabel *handPosition = createLabel( tr( "Hand Position" ), getPointInfo( item->handPosition() ) );
    QLabel *twoHanded =         createLabel( tr( "Two Handed" ), item->twoHanded() );
    QLabel *inspectionKind =    createLabel( tr( "Inspection Kind" ), item->inspectionKind() );
    QLabel *activeHandPosition = createLabel( tr( "Active Hand Position" ), getPointInfo( item->activeHandPosition() ) );
    QLabel *largeImage = createLabel( tr( "Large Image" ), item->largeImage() );
    QLabel *image = createLabel( tr( "Image" ), item->image() );
    QLabel *activeImage = createLabel( tr( "Active Image" ), item->activeImage() );
    QLabel *kind = createLabel( tr( "Kind" ), item->kind() );
    QLabel *activeAngle = createLabel( tr( "Active Angle" ), item->activeAngle() );

    mainLayout->addWidget( twoHanded );
    mainLayout->addWidget( handPosition );
    mainLayout->addWidget( activeHandPosition );
    mainLayout->addWidget( inspectionKind );
    mainLayout->addWidget( kind );
    mainLayout->addWidget( image );
    mainLayout->addWidget( activeImage );
    mainLayout->addWidget( largeImage );
    mainLayout->addWidget( activeAngle );

    widget->setLayout( mainLayout );

    toolBox->addItem( widget, tr( "Instrument" ) );
}

void ObjectInfoWidget::createSwordWidget(QToolBox *toolBox)
{
    StarSword *item = dynamic_cast<StarSword*>( m_item );

    createItemWidget( toolBox );

    QWidget *widget = new QWidget( this );
    QVBoxLayout *mainLayout = new QVBoxLayout;

    mainLayout->setMargin( 3 );

    QLabel *kind = createLabel( tr( "Kind" ), item->kind() );
    QLabel *firePosition = createLabel( tr( "Fire Position" ), getPointInfo( item->firePosition() ) );

    //QString soundEffect;
    QLabel *fireAfterWindup = createLabel( tr( "FireAfterWindup" ), item->fireAfterWindup() );
    QLabel *twoHanded = createLabel( tr( "TwoHanded" ), item->twoHanded() );
    //QLabel *colorOptions
    QLabel *level = createLabel( tr( "Level" ), item->level() );
    QLabel *fireTime = createLabel( tr( "FireTime" ), item->fireTime() );

    mainLayout->addWidget( kind );
    mainLayout->addWidget( firePosition );
    mainLayout->addWidget( twoHanded );
    mainLayout->addWidget( fireAfterWindup );
    mainLayout->addWidget( fireTime );
    mainLayout->addWidget( level );

    widget->setLayout( mainLayout );

    toolBox->addItem( widget, tr( "Sword" ) );

    createPrimaryStancesWidget( toolBox );

}

void ObjectInfoWidget::createPrimaryStancesWidget(QToolBox *toolBox)
{
    StarSword *item = dynamic_cast<StarSword*>( m_item );

    PrimaryStances *stance = item->primaryStances();

    QWidget *widget = new QWidget;
    QVBoxLayout *mainLayout = new QVBoxLayout;

    mainLayout->setMargin( 3 );

    QGroupBox *projectileGroup = new QGroupBox( this );
    QVBoxLayout *projLayout = new QVBoxLayout;
    projLayout->setMargin( 2 );
    projectileGroup->setTitle( tr( "Projectile" ) );

    QLabel *projectileType = createLabel( tr( "Projectile Type" ), stance->projectileType );
    QLabel *projectilePower = createLabel( tr( "Projectile Power" ), stance->projectilePower );
    QLabel *projectileSpeed = createLabel( tr( "Projectile Speed" ), stance->projectileSpeed );

    projLayout->addWidget( projectileType );
    projLayout->addWidget( projectilePower );
    projLayout->addWidget( projectileSpeed );

    projectileGroup->setLayout( projLayout );

    QGroupBox *idleGroup = new QGroupBox( this );
    QVBoxLayout *idleLayout = new QVBoxLayout;
    idleLayout->setMargin( 2 );
    idleGroup->setTitle( tr( "Idle" ) );

    QLabel *idleDuration = createLabel( tr( "Duration" ), stance->idleDuration );
    QLabel *idleArmAngle = createLabel( tr( "Arm angle" ), stance->idleArmAngle );
    QLabel *idleArmFrameOverride = createLabel( tr( "Arm Frame Override" ), stance->armFrameOverride );
    QLabel *idleTwoHanded = createLabel( tr( "Two Handed" ), stance->idleTwoHanded );
    QLabel *idleSwordAngle = createLabel( tr( "Sword Angle" ), stance->idleSwordAngle );

    idleLayout->addWidget( idleDuration );
    idleLayout->addWidget( idleTwoHanded );
    idleLayout->addWidget( idleArmAngle );
    idleLayout->addWidget( idleArmFrameOverride );
    idleLayout->addWidget( idleSwordAngle );

    idleGroup->setLayout( idleLayout );

    QGroupBox *windupGroup = new QGroupBox( this );
    QVBoxLayout *windupLayout = new QVBoxLayout;
    windupLayout->setMargin( 2 );
    windupGroup->setTitle( tr( "Windup" ) );

    QLabel *windupDuration = createLabel( tr( "Duration" ), stance->windupDuration );
    QLabel *windupStatusEffects = createLabel( tr( "Status Effects" ), getStatusEffects( stance->windupStatusEffects ) );
    QLabel *windupArmAngle = createLabel( tr( "Arm Angle" ), stance->windupArmAngle );
    QLabel *windupTwoHanded = createLabel( tr( "Two Handed" ), stance->windupTwoHanded );
    QLabel *windupSwordAngle = createLabel( tr( "Sword Angle" ), stance->windupSwordAngle );
    QLabel *windupHandPosition = createLabel( tr( "Hand Position" ), getPointInfo( stance->windupHandPosition ) );

    windupLayout->addWidget( windupDuration );
    windupLayout->addWidget( windupTwoHanded );
    windupLayout->addWidget( windupArmAngle );
    windupLayout->addWidget( windupSwordAngle );
    windupLayout->addWidget( windupHandPosition );
    windupLayout->addWidget( windupStatusEffects );

    windupGroup->setLayout( windupLayout );

    QGroupBox *cooldownGroup = new QGroupBox( this );
    QVBoxLayout *cooldownLayout = new QVBoxLayout;
    cooldownLayout->setMargin( 2 );
    cooldownGroup->setTitle( tr( "Cooldown" ) );

    QLabel *cooldownDuration = createLabel( tr( "Duration" ) , stance->cooldownDuration );
    QLabel *cooldownStatus = createLabel( tr( "Status Effect" ), getStatusEffects( stance->cooldownStatusEffects) );
    QLabel *cooldownArmAngle = createLabel( tr( "Arm Angle" ), stance->cooldownArmAngle );
    QLabel *cooldownTwoHanded = createLabel( tr( "Two Handed" ), stance->cooldownTwoHanded );
    QLabel *cooldownSwordAngle = createLabel( tr( "Sword Angle" ), stance->cooldownSwordAngle );
    QLabel *cooldownHandPos = createLabel( tr( "Hand Position" ), getPointInfo( stance->cooldownHandPosition ) );

    cooldownLayout->addWidget( cooldownDuration );
    cooldownLayout->addWidget( cooldownTwoHanded );
    cooldownLayout->addWidget( cooldownArmAngle );
    cooldownLayout->addWidget( cooldownSwordAngle );
    cooldownLayout->addWidget( cooldownHandPos );
    cooldownLayout->addWidget( cooldownStatus );

    cooldownGroup->setLayout( cooldownLayout );

    mainLayout->addWidget( projectileGroup );
    mainLayout->addWidget( idleGroup );
    mainLayout->addWidget( windupGroup );
    mainLayout->addWidget( cooldownGroup );

    widget->setLayout( mainLayout );

    toolBox->addItem( widget, tr( "Primary Stances" ) );
}

void ObjectInfoWidget::createThrownWidget(QToolBox *toolBox)
{
    StarThrown *item = dynamic_cast<StarThrown*>( m_item );

    createItemWidget( toolBox );
    ProjectileConfig *config = item->projectileConfig();

    QWidget *widget = new QWidget( this );
    QWidget *projWidget = new QWidget( this );

    QVBoxLayout *layout = new QVBoxLayout;
    QVBoxLayout *projLayout = new QVBoxLayout;

    layout->setMargin( 3 );
    projLayout->setMargin( 3 );

    QLabel *ammoUsage = createLabel( tr( "AmmoUsage" ), item->ammoUsage() );
    QLabel *image = createLabel( tr( "Image" ), item->image() );
    QLabel *cooldown = createLabel( tr( "Cooldown" ), item->cooldown() );
    QLabel *windupTime = createLabel( tr( "WindupTime" ), item->windupTime() );
    QLabel *projectileType = createLabel( tr( "Projectile Type" ), item->projectileType() );

    QLabel *armorPene = createLabel( tr( "Armor Penetration" ), config->armorPenetration );
    QLabel *level = createLabel( tr( "Level" ), config->level );
    QLabel *speed = createLabel( tr( "Speed" ), config->speed );
    QLabel *power = createLabel( tr( "Power" ), config->power );

    layout->addWidget( image );
    layout->addWidget( ammoUsage );
    layout->addWidget( windupTime );
    layout->addWidget( cooldown );

    projLayout->addWidget( projectileType );
    projLayout->addWidget( armorPene );
    projLayout->addWidget( level );
    projLayout->addWidget( speed );
    projLayout->addWidget( power );

    widget->setLayout( layout );
    projWidget->setLayout( projLayout );

    toolBox->addItem( widget, tr( "ThrownItem" ) );
    toolBox->addItem( projWidget, tr( "Projectile" ) + " (" + item->projectileType() + ")");


}

void ObjectInfoWidget::createStarWorldObjectWidget(QToolBox *toolBox)
{
    createItemWidget( toolBox );

    StarWorldObject *item = dynamic_cast< StarWorldObject* >( m_item );

    if( m_item->hasProperty( "lightColor" ) || m_item->hasProperty( "lightColors" ) ) {

        QWidget *lightsWidget = new QWidget( this );

        QVBoxLayout *lightsLayout = new QVBoxLayout;

        lightsLayout->setMargin( 3 );

        QHBoxLayout *lightColorLayout = new QHBoxLayout;

        QLabel *lightColorLabel = new QLabel( tr( "Light Color" ), this );
        ColorPreviewWidget *colorPreview = new ColorPreviewWidget( item->lightColor(), this );
        colorPreview->setFixedSize( QSize( 50, 50 ) );

        lightColorLayout->addWidget( lightColorLabel, 1 );
        lightColorLayout->addWidget( colorPreview, 4 );

        lightsLayout->addLayout( lightColorLayout );

        lightsWidget->setLayout( lightsLayout );

        toolBox->addItem( lightsWidget, tr( "Lights" ) );
    }

}

void ObjectInfoWidget::createStarCoinWidget(QToolBox *toolBox)
{
    createItemWidget( toolBox );

    StarCoin *item = dynamic_cast< StarCoin *>( m_item );

    if( item ) {
        QWidget *widget = new QWidget( this );

        QVBoxLayout *layout = new QVBoxLayout;
        layout->setMargin( 3 );

        QLabel *value = createLabel( tr( "Coin Value" ), item->value() );
        QLabel *smallStackLimit = createLabel( tr( "Small Stack Limit" ), item->smallStackLimit() );
        QLabel *mediumStackLimit = createLabel( tr( "Medium Stack Limit" ), item->mediumStackLimit() );

        layout->addWidget( value );
        layout->addWidget( smallStackLimit );
        layout->addWidget( mediumStackLimit );

        toolBox->addItem( widget, tr( "Coin caracteristics" ) );

        //Sounds..

        widget->setLayout( layout );
    }
}

QLabel *ObjectInfoWidget::createLabel( QString labelName, QVariant value ) {
    QLabel *label = new QLabel( "<strong>" + labelName + ":</strong> " + value.toString(), this );
    //label->setWordWrap( true );

    return label;
}

QString ObjectInfoWidget::getPointInfo(QPoint point)
{
    QString msg = "<br />X: " + QString::number( point.x() ) + "<br />Y: " + QString::number( point.y() );

    return msg;
}

QString ObjectInfoWidget::getPointInfo(QPointF point)
{
    QString msg = "<br />X: " + QVariant( point.x() ).toString() + "<br />Y: " + QString::number( point.y() );

    return msg;
}

QString ObjectInfoWidget::getStatusEffects(QList<StatusEffect> &list)
{
    QString msg = tr( "Status Effects" ) + ": <br />";

    foreach( StatusEffect effect, list ) {
        msg += effect.kind() + "<br />";
        msg += QVariant( effect.duration() ).toString() + "<br /><br />";
    }

    return msg;
}
