#ifndef SWORDEDITIONWINDOW_H
#define SWORDEDITIONWINDOW_H

#include <QDebug>
#include <QDialog>
#include <QDir>
#include <QFile>
#include <QFileInfo>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QStringList>
#include <QJsonValue>
#include <QVBoxLayout>
#include <QFileDialog>
#include <QColorDialog>
#include <QMessageBox>
#include <QPalette>
#include <QList>
#include "../Core/assetsmanager.h"
#include "itemeditionwidget.h"
#include "projectileeditionwindow.h"
#include "recipeswidget.h"
#include "../Core/projectilesutility.h"
#include "statuseffectslistwindow.h"

#include "../Starbound/Items/starsword.h"

namespace Ui {
class SwordEditionWindow;
}

class SwordEditionWindow : public QDialog
{
    Q_OBJECT

public:
    explicit SwordEditionWindow(AssetsManager *assetsManager, QString projectFilesPath, bool createSword = false, QWidget *parent = 0);
    ~SwordEditionWindow();

public slots:
    void onClose();
    void onOpenWindupStatusEffectList();
    void onOpenCooldownStatusEffectList();
private:
    void buildInterface();
    void initEvents();
    void openStatusEffectList( QList<StatusEffect> &statusEffectList );
private:
    Ui::SwordEditionWindow *ui;
    AssetsManager *m_assetsManager;

    ItemEditionWidget *m_itemEditionWidget;
    RecipesWidget *m_recipeWidget;

    bool m_createSword;
    QString m_projectFilesPath;

    QList<StatusEffect> m_windupStatusEffectsList;
    QList<StatusEffect> m_cooldownStatusEffectsList;
};

#endif // SWORDEDITIONWINDOW_H
