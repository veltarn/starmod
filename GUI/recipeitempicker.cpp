#include "recipeitempicker.h"

RecipeItemPicker::RecipeItemPicker( AssetsManager *assetsManager, QWidget *parent ) : ItemPickerWindow( assetsManager, parent )
{
    buildInterface();
    initEvents();
}

void RecipeItemPicker::setAmount(int amount)
{
    m_spinBox->setValue( amount );
}

int RecipeItemPicker::getAmount()
{
    return m_spinBox->value();
}


void RecipeItemPicker::buildInterface()
{
    m_spinBox = new QSpinBox( this );
    m_spinBox->setMinimum( 0 );
    m_spinBox->setMaximum( 9999 );
    m_spinBox->setValue( 0 );

    addWidgetToSide( tr( "Item amount" ), m_spinBox );
}

void RecipeItemPicker::initEvents()
{
}
