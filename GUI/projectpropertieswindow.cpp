#include "projectpropertieswindow.h"
#include "ui_projectpropertieswindow.h"


ProjectPropertiesWindow::ProjectPropertiesWindow( QString title, QString workspace, QWidget *parent, bool createProject) :
    QDialog(parent),    
    ui(new Ui::ProjectPropertiesWindow),
    m_modified( false ),
    m_workspace( workspace ),
    m_createProject( createProject )
{
    ui->setupUi(this);
    this->setWindowTitle( title );
    //Displaying the User's name in the author line
    //Fucked up under W7
    /*QString homeName = QStandardPaths::displayName( QStandardPaths::HomeLocation );
    ui->projectAuthorLineEdit->setText( homeName );*/
    ui->starboundVersionComboBox->addItem( "Beta v. Offended Koala", "Beta v. Offended Koala" );

    initEvents();
}

ProjectPropertiesWindow::~ProjectPropertiesWindow()
{
    delete ui;
}

void ProjectPropertiesWindow::initEvents() {
    connect( ui->validateBtn, SIGNAL( clicked() ), this, SLOT( onValidate() ) );
    connect( ui->cancelBtn, SIGNAL( clicked() ), this, SLOT( onCancel() ) );
    connect( ui->projectAuthorLineEdit, SIGNAL( textChanged(QString) ), this, SLOT( onChange() ) );
    connect( ui->projectNameLineEdit, SIGNAL( textChanged(QString) ), this, SLOT( onChange() ) );
    connect( ui->projectPathLineEdit, SIGNAL( textChanged(QString) ), this, SLOT( onChange() ) );
    connect( ui->projectNameLineEdit, SIGNAL( textChanged(QString) ), this, SLOT( onNameChanged( QString ) ) );
}

void ProjectPropertiesWindow::onValidate() {
    if( verifyFields() )
        accept();
}

void ProjectPropertiesWindow::onCancel() {
    if( m_modified ) {
        int rep = QMessageBox::question( this, tr( "Confirm" ), tr( "You are about to cancel the creation of a new project. Do you confirm ?" ), QMessageBox::Yes | QMessageBox::No );
        if( rep == QMessageBox::Yes ) {
            reject();
        }
    } else {
        reject();
    }
}

void ProjectPropertiesWindow::onNameChanged( QString n ) {
    //Changing path name only if we create a new project (Some serious shit otherwise)
    if( m_createProject ) {
        QString folder = n;
        QString filename = n + ".smp";
        QString path = m_workspace + "/" + folder + "/" + filename ;
        ui->projectPathLineEdit->setText( path );

        if( projectExists( path ) ) {
            ui->labelWarning->setText( tr( "<strong><font color='orange'>A project with this name already exists</font></strong>" ) );
            ui->validateBtn->setEnabled( false );
        } else {
            ui->labelWarning->setText( "" );
            ui->validateBtn->setEnabled( true );
        }
    }
}

void ProjectPropertiesWindow::onChange() {
    m_modified = true;
}

void ProjectPropertiesWindow::setProjectName( const QString name ) {
    ui->projectNameLineEdit->setText( name );
}

QString ProjectPropertiesWindow::getProjectName() {
    return ui->projectNameLineEdit->text();
}

void ProjectPropertiesWindow::setProjectAuthor( const QString author ) {
    ui->projectAuthorLineEdit->setText( author );
}

QString ProjectPropertiesWindow::getProjectAuthor() {
    return ui->projectAuthorLineEdit->text();
}

void ProjectPropertiesWindow::setProjectPath( const QString path ) {
    ui->projectPathLineEdit->setText( path );
}

QString ProjectPropertiesWindow::getProjectPath() {
    return ui->projectPathLineEdit->text();
}

void ProjectPropertiesWindow::setProjectVersion( const QString version ) {
    ui->versionLineEdit->setText( version );
}

QString ProjectPropertiesWindow::getProjectVersion() {
    return ui->versionLineEdit->text();
}

void ProjectPropertiesWindow::setStarboundVersion( const QString version ) {
    int index = ui->starboundVersionComboBox->findData( version );

    if( index != -1 )
        ui->starboundVersionComboBox->setCurrentIndex( index );
}

QString ProjectPropertiesWindow::getStarboundVersion() {
    return ui->starboundVersionComboBox->currentText();
}

bool ProjectPropertiesWindow::verifyFields() {
    bool error = false;
    QStringList errorList;

    if( ui->projectNameLineEdit->text() == "" ) {
        error = true;
        errorList.append( tr( "Mod name is missing" ) );
    }

    if( ui->projectAuthorLineEdit->text() == "" ) {
        error = true;
        errorList.append( tr( "Author is missing" ) );
    }

    if( ui->projectPathLineEdit->text() == "" ) {
        error = true;
        errorList.append( tr( "Project Path is missing" ) );
    }

    if( ui->versionLineEdit->text() == "" ) {
        error = true;
        errorList.append( tr( "Project Version is missing" ) );
    }

    if( error ) {
        QString msg = tr( "The following fields are missing" ) + ": <br /><ul>";

        for( QStringList::iterator it = errorList.begin(); it != errorList.end(); ++it ) {
            msg += "<li>" + *it + "</li>";
        }
        msg += "</ul>";

        QMessageBox::warning( this, tr( "Some fields are missing" ), msg );
    }
    return !error;
}

bool ProjectPropertiesWindow::projectExists( QString path ) {
    QDir dir( path );
    QFileInfo fifo( path );
    qDebug() << fifo.exists() << endl;
    return fifo.exists();
}
