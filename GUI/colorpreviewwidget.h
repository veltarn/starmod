#ifndef COLORPREVIEWWIDGET_H
#define COLORPREVIEWWIDGET_H

#include <QWidget>
#include <QColor>
#include <QPainter>
#include <QBrush>
#include <QPaintEvent>

class ColorPreviewWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ColorPreviewWidget(QColor color, QWidget *parent = 0);

signals:

public slots:

protected:
    virtual void paintEvent( QPaintEvent *pe );
private:
    QColor m_color;
};

#endif // COLORPREVIEWWIDGET_H
