#ifndef RECIPEITEMPICKER_H
#define RECIPEITEMPICKER_H

#include <QSpinBox>
#include "itempickerwindow.h"
#include "../Core/assetsmanager.h"

class RecipeItemPicker : public ItemPickerWindow
{
    Q_OBJECT
public:
    RecipeItemPicker( AssetsManager *assetsManager, QWidget *parent = 0 );

    void setAmount( int amount );
    int getAmount();

public slots:

private:
    void buildInterface();
    void initEvents();
private:
    QSpinBox *m_spinBox;
};

#endif // RECIPEITEMPICKER_H
