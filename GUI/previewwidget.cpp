#include "previewwidget.h"
#include "ui_previewwidget.h"

PreviewWidget::PreviewWidget( QWidget *parent ) :
    QWidget(parent),
    ui(new Ui::PreviewWidget),
    m_scene( NULL ),
    m_starboundCharacter( NULL )
{
    ui->setupUi(this);
    init();
}

PreviewWidget::~PreviewWidget()
{
    delete ui;
}

void PreviewWidget::init() {
    m_scene = new QGraphicsScene( this );
    ui->graphicsView->setScene( m_scene );
    ui->graphicsView->scale( 3, 3 );
}

void PreviewWidget::setCharacter( StarboundCharacter *chara ) {
    m_starboundCharacter = chara;
    m_scene->clear();

    m_scene->addItem( m_starboundCharacter );
}

StarboundCharacter *PreviewWidget::getStarboundCharacter() const {
    return m_starboundCharacter;
}
