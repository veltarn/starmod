#ifndef SIDEWIDGET_H
#define SIDEWIDGET_H

#include <QDialog>
#include <QTreeView>
#include <QListWidget>
#include <QDebug>
#include <QSplitter>
#include <QVBoxLayout>
#include <QLabel>
#include <QSortFilterProxyModel>
#include <QFileSystemModel>
#include <QListView>
#include "../Core/assetsmanager.h"
#include "objectviewwidget.h"
//#include "Views/starbounditemview.h"

namespace Ui {
class SideWidget;
}

class SideWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit SideWidget( AssetsManager *assets, QWidget *parent = 0);
    ~SideWidget();

    QLineEdit *searchBar() const;
    QTreeView *treeView() const;
    QListWidget *listWidget() const;
    QTreeView *projectView() const;
    QListView *itemView();

    void addElementToListWidget(const QString elmt , QString data);
    //void removeElementFromListWidget( const QString elmt );
    void updateObjectNumberLabel();
    void updateObjectNumberLabel( QSortFilterProxyModel *model );

    void setAssetsManager( AssetsManager *manager );

    QFileSystemModel *projectModel() const;
    void setProjectModel(QFileSystemModel *projectModel, QString rootIndex );
    void deleteProjectTree();

public slots:
    void onSearch( QString text );
    void addNewRelatedFiles( QStringList list );
    /*void onObjectSearch( QString text );
    void onObjectClicked( QModelIndex index );
    void onCategoryChange( int index );*/
private:
    void buildInterface();
    void initEvents();
    void createInfoWindow( StarItem *item );

private:
    Ui::SideWidget *ui;
    QTreeView *m_assetsView;
    QListWidget *m_editedFileList;
    QTreeView *m_projectView;
    QFileSystemModel *m_projectModel;
    //StarboundItemView *m_starboundItemView;
    //QListView *m_starboundItemView;
    ObjectViewWidget *m_objectsView;
    AssetsManager *m_assetsManager;
};

#endif // SIDEWIDGET_H
