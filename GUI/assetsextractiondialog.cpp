#include "assetsextractiondialog.h"
#include "ui_assetsextractiondialog.h"

AssetsExtractionDialog::AssetsExtractionDialog( QString extractorPath, QStringList arguments,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AssetsExtractionDialog),
    m_extractorPath( extractorPath ),
    m_arguments( arguments ),
    m_processActive( false )
{
    ui->setupUi(this);
    setWindowTitle( tr( "Extraction" ) );
    ui->closeButton->setEnabled( false );
    ui->textEdit->append( tr( "Extraction of the assets in progress, it may take a long time" ) );

    m_process = new QProcess( this );
    m_process->start( m_extractorPath, m_arguments );
    m_processActive = true;

    initEvents();
}

AssetsExtractionDialog::~AssetsExtractionDialog()
{
    delete ui;
}

void AssetsExtractionDialog::initEvents() {
    connect( m_process, SIGNAL( readyRead() ), this, SLOT( onReadyRead() ) );
    connect( m_process, SIGNAL( finished(int) ), this, SLOT( onFinished( int ) ) );
    connect( ui->closeButton, SIGNAL( clicked() ), this, SLOT( accept() ) );
}

void AssetsExtractionDialog::onReadyRead() {
    QString data = m_process->readAll();

    ui->textEdit->append( data );
}

void AssetsExtractionDialog::onFinished( int code ) {
    if( code == 0 ) {
        ui->textEdit->append( "<font color='green'>" + tr( "Extraction successfully finished!" ) + "</font>" );
    } else {
        ui->textEdit->append( tr( "An error occured during the extraction process..." ) );
    }

    m_processActive = false;
    ui->closeButton->setEnabled( true );
}

void AssetsExtractionDialog::closeEvent( QCloseEvent *ce ) {
    if( m_processActive ) {
        int rep = QMessageBox::question( this, tr( "Confirmation" ), tr( "The extraction is still in progress, by cancelling it, Starmod will remain unusable. Do you really want to cancel the extraction?" ), QMessageBox::Yes | QMessageBox::No );
        if( rep == QMessageBox::Yes ) {
            m_process->kill();
            ce->accept();
        }
    } else {
        ce->accept();
    }
}
