#include "sidewidget.h"
#include "ui_sidewidget.h"

SideWidget::SideWidget( AssetsManager *assets, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SideWidget),
    m_assetsManager( assets ),
    m_projectModel( NULL )/*,
    m_infosObject( NULL )*/
{
    ui->setupUi(this);
    buildInterface();
    initEvents();
}

SideWidget::~SideWidget()
{
    delete ui;
}

void SideWidget::buildInterface() {
    m_assetsView = new QTreeView( this );
    //m_editedFileList = new QListWidget( this );
    m_projectView = new QTreeView( this );
    m_objectsView = new ObjectViewWidget( m_assetsManager, this );
    /*m_starboundItemView = new QListView( this );
    m_starboundItemView->setEditTriggers( QAbstractItemView::NoEditTriggers );*/

    QVBoxLayout *editedFilesLayout = new QVBoxLayout;
    QVBoxLayout *objectViewLayout = new QVBoxLayout;

    //editedFilesLayout->addWidget( m_editedFileList );
    editedFilesLayout->addWidget( m_projectView );
    objectViewLayout->addWidget( m_objectsView );

    ui->objectsView->setLayout( objectViewLayout );
    ui->rawViewMainLayout->addWidget( m_assetsView );

    ui->projectFiles->setLayout( editedFilesLayout );
    /*QStringList extensionsList = m_assetsManager->assetsExtensions();

    ui->categoryComboBox->addItem( tr( "All" ), "all" );
    foreach( QString ext, extensionsList ) {
        ui->categoryComboBox->addItem( ext, ext );
    }*/

}

void SideWidget::initEvents() {
    connect( ui->searchBar, SIGNAL( textChanged(QString) ), this, SLOT( onSearch(QString) ) );
    /*connect( ui->keywordLineEdit, SIGNAL( textChanged( QString ) ), this, SLOT( onObjectSearch(QString) ) );
    connect( m_starboundItemView, SIGNAL( clicked(QModelIndex) ), this, SLOT( onObjectClicked(QModelIndex) ) );
    connect( ui->categoryComboBox, SIGNAL( currentIndexChanged(int) ), this, SLOT( onCategoryChange(int) ) );*/
}
QFileSystemModel *SideWidget::projectModel() const
{
    return m_projectModel;
}

void SideWidget::setProjectModel(QFileSystemModel *projectModel, QString rootIndex)
{
    m_projectModel = projectModel;
    m_projectView->setModel( m_projectModel );

    QModelIndex idx = m_projectModel->index( rootIndex );
    m_projectView->setRootIndex( idx );
}

void SideWidget::deleteProjectTree()
{
    if( m_projectModel ) {
        delete m_projectModel;
        m_projectModel = NULL;
    }
}

/*
void SideWidget::onCategoryChange( int index ) {
    QString data = ui->categoryComboBox->itemData( index ).toString();
    QSortFilterProxyModel *model = qobject_cast<QSortFilterProxyModel*>( m_starboundItemView->model() );

    if( model != NULL ) {
        if( data == "all" ) {
            model->setFilterRegExp( "" );
        } else {
            QStringList filesType = m_assetsManager->getItemsListByCateory( data );

            QString regex( "(");

            for( int i = 0; i < filesType.size(); i++ ) {
                regex += "^" + filesType[i] + "$";

                if( i < filesType.size() - 1 ) {
                    regex += "|";
                }
            }

            regex += ")";

            model->setFilterRegExp( regex );
        }

        updateObjectNumberLabel( model );
    }
}
*/
QTreeView *SideWidget::treeView() const {
    return m_assetsView;
}

QListWidget *SideWidget::listWidget() const {
    return m_editedFileList;
}

QTreeView *SideWidget::projectView() const
{
    return m_projectView;
}

QListView *SideWidget::itemView()
{
    return m_objectsView->objectsView();
}

QLineEdit *SideWidget::searchBar() const {
    return ui->searchBar;
}

void SideWidget::onSearch( QString text ) {
    //m_assetsView->keyboardSearch( text );

    if( m_assetsView->model() != NULL ) {
        QModelIndexList mil = m_assetsView->model()->match( m_assetsView->model()->index( 0, 0 ), Qt::DisplayRole, text, -1, Qt::MatchRecursive | Qt::MatchContains );
        qDebug() << mil.size() << endl;

        QString occurencePhr = mil.size() != 1 ? tr( "occurences" ) : tr( "occurence" );
        ui->messageLabel->setText( QString::number( mil.size() ) + " " + occurencePhr + " " + tr( "found" ) );

        if( mil.size() > 0 ) {
            m_assetsView->setCurrentIndex( mil.at( 0 ) );
        }
    }

    if( text == "" ) {
        ui->messageLabel->setText( "" );
    }
}

void SideWidget::addNewRelatedFiles(QStringList list)
{
    QStringList fileToExclude; // If they already exists
    //Verification if those files are not already present into editedFIleList
    foreach( QString f, list ) {
        for( int i = 0; i < m_editedFileList->count(); i++ ) {
            QListWidgetItem *item = m_editedFileList->item( i );

            if( item->text() == f )
            {
                fileToExclude.append( f );
                break;
            }
        }
    }

    foreach( QString f, list ) {
        if( !Utility::exists( f, fileToExclude ) ) {
            addElementToListWidget( QFileInfo( f ).fileName(), f );
        }
    }
}
/*
void SideWidget::onObjectSearch( QString text ) {
    if( m_starboundItemView->model() != NULL ) {
        QSortFilterProxyModel *model = qobject_cast<QSortFilterProxyModel*>( m_starboundItemView->model() );
        if( model != NULL ) {
            if( !ui->useRegexCheckBox->isChecked() ) {
                model->setFilterFixedString( text );
            } else {
                model->setFilterRegExp( text );
            }

            updateObjectNumberLabel( model );
        }

    }
}
*//*
void SideWidget::updateObjectNumberLabel() {
    if( m_starboundItemView->model() != NULL ) {
        QSortFilterProxyModel *model = qobject_cast<QSortFilterProxyModel*>( m_starboundItemView->model() );
        if( model != NULL ) {
            updateObjectNumberLabel( model );
        }
    }
}
*//*
void SideWidget::updateObjectNumberLabel( QSortFilterProxyModel *model ) {
    QString msg;
    int nbItems = model->rowCount();

    QString matchesWord = nbItems != 1 ? tr( "items" ) : tr( "item" );
    msg = QString::number( nbItems ) + " " + matchesWord + " " + tr( "found" );
    ui->objectMatchesLabel->setText( msg );
}*/

void SideWidget::setAssetsManager(AssetsManager *manager)
{
    m_assetsManager = manager;
}

void SideWidget::addElementToListWidget( const QString elmt, QString data ) {
    if( m_editedFileList->findItems( elmt, Qt::MatchExactly ).size() == 0 ) {
        QListWidgetItem *item = new QListWidgetItem( elmt );
        item->setToolTip( data );
        item->setData( Qt::UserRole + 1, data );
        m_editedFileList->addItem( item );
    }
}
/*
void SideWidget::onObjectClicked( QModelIndex index ) {
    qDebug() << index.data( Qt::UserRole + 1 ).toString() << endl;
    StarItem *item = m_assetsManager->getItem( index.data( Qt::UserRole + 1 ).toString() );

    if( item != NULL ) {
        createInfoWindow( item );
    }
}*/
/*
void SideWidget::createInfoWindow( StarItem *item ) {
    if( m_infosObject != NULL ) {
        m_infosObject->deleteLater();
        m_infosObject = NULL;
    }

    m_infosObject = new ObjectInfoWidget( item, this );
    m_infosObject->setGeometry( m_starboundItemView->x() + m_starboundItemView->width(), QCursor::pos().y(), 200, 300 );
    m_infosObject->setWindowFlags( Qt::Tool );
    m_infosObject->setModal( false );


    QFileInfo itemInfo( item->itemLocation() );
    m_infosObject->setWindowTitle( item->shortDescription() );
    m_infosObject->setWindowIcon( QIcon( QPixmap( itemInfo.path() + "/" + item->inventoryIcon() ) ) );
    m_infosObject->show();


}*/
