#include "statuseffectslistwindow.h"
#include "ui_statuseffectslistwindow.h"

StatusEffectsListWindow::StatusEffectsListWindow(QList<StatusEffect> &statusEffectList, AssetsManager *assetsManager, QWidget *parent, bool readOnly ) :
    QDialog(parent),
    ui(new Ui::StatusEffectsListWindow),
    m_statusEffectList( statusEffectList ),
    m_assetsManager( assetsManager ),
    m_readOnly( readOnly )
{
    ui->setupUi(this);
    setWindowTitle( tr( "Status Effect List" ) );

    buildInterface();
    initEvents();

    createList();

    m_statusEffectsView->setModel( m_model );

    QStringList labels;
    labels << tr( "Effect Name" )
           << tr( "Duration" )
           << tr( "Percentage" );

    m_model->setHorizontalHeaderLabels( labels );
}

StatusEffectsListWindow::~StatusEffectsListWindow()
{
    delete ui;
}

void StatusEffectsListWindow::buildInterface()
{
    m_toolBar = new QToolBar( tr( "Status Effect List Toolbar" ), this );

    m_addEffectAction = new QAction( QIcon( QPixmap( "assets/images/addEffect.png" ) ), tr( "Add Status Effect" ), this );
    m_removeEffectAction = new QAction( QIcon( QPixmap( "assets/images/removeEffect.png" ) ),  tr( "Remove Status Effect" ), this );

    m_validateButton = new QPushButton( tr( "Ok" ), this );
    m_closeButton = new QPushButton( tr( "Close" ), this );

    m_model = new QStandardItemModel( this );

    m_statusEffectsView = new QTreeView( this );

    QVBoxLayout *mainLayout = new QVBoxLayout;
    QHBoxLayout *buttonLayout = new QHBoxLayout;
    QSpacerItem *buttonLayoutSpacer = new QSpacerItem( 999, 0, QSizePolicy::Expanding );

    m_toolBar->addAction( m_addEffectAction );
    m_toolBar->addAction( m_removeEffectAction );

    if( m_readOnly )
        m_toolBar->setEnabled( false );

    buttonLayout->addSpacerItem( buttonLayoutSpacer );
    buttonLayout->addWidget( m_validateButton );
    buttonLayout->addWidget( m_closeButton );

    mainLayout->addWidget( m_toolBar, 1 );
    mainLayout->addWidget( m_statusEffectsView, 4 );
    mainLayout->addLayout( buttonLayout, 1 );

    this->setLayout( mainLayout );
}

void StatusEffectsListWindow::createList()
{
    int row = 0;
    foreach( StatusEffect effect, m_statusEffectList ) {
        QStandardItem *kind = new QStandardItem( effect.kind() );
        QStandardItem *duration = new QStandardItem( QString::number( effect.duration() ) );
        QStandardItem *percentage = new QStandardItem( QString::number( effect.percentage() ) );

        m_model->setItem( row, 0, kind );
        m_model->setItem( row, 1, duration );
        m_model->setItem( row, 2, percentage );

        row++;
    }
}

void StatusEffectsListWindow::initEvents()
{
    connect( m_closeButton, SIGNAL( clicked() ), this, SLOT( onCloseButtonClicked() ) );
}

void StatusEffectsListWindow::onCloseButtonClicked()
{
    accept();
}

void StatusEffectsListWindow::onAddEffectClicked()
{

}
