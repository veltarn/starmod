#include "objectviewwidget.h"
#include "ui_objectviewwidget.h"

ObjectViewWidget::ObjectViewWidget(AssetsManager *assetsManager, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ObjectViewWidget),
    m_assetsManager( assetsManager ),
    m_infosObject( NULL )
{
    ui->setupUi(this);

    buildInterface();
    initEvents();
}

ObjectViewWidget::~ObjectViewWidget()
{
    delete ui;
}

QListView *ObjectViewWidget::objectsView() const
{
    return ui->objectsView;
}

void ObjectViewWidget::updateObjectNumberLabel()
{
    if( ui->objectsView->model() != NULL ) {
        QSortFilterProxyModel *model = qobject_cast<QSortFilterProxyModel*>( ui->objectsView->model() );
        if( model != NULL ) {
            updateObjectNumberLabel( model );
        }
    }
}

void ObjectViewWidget::updateObjectNumberLabel(QSortFilterProxyModel *model)
{
     QString msg;
     int nbItems = model->rowCount();

     QString matchesWord = nbItems != 1 ? tr( "items" ) : tr( "item" );
     msg = QString::number( nbItems ) + " " + matchesWord + " " + tr( "found" );
     ui->objectMatchesLabel->setText( msg );
}

StarItem *ObjectViewWidget::getSelectedItem()
{
    if( m_currentIndex.isValid() ) {
        QString itemName = m_currentIndex.data( Qt::UserRole + 1 ).toString();

        StarItem *item = m_assetsManager->getItem( itemName );
        return item;
    } else {
        return NULL;
    }
}

void ObjectViewWidget::selectItem(QString name)
{
    if( ui->objectsView->model() != NULL ) {
        QSortFilterProxyModel *model = qobject_cast<QSortFilterProxyModel*>( ui->objectsView->model() );
        QModelIndexList mil = model->match( model->index( 0, 0 ), Qt::DisplayRole, name, -1, Qt::MatchExactly );
        if( mil.size() > 0 ) {
            ui->objectsView->selectionModel()->select( mil.at( 0 ), QItemSelectionModel::Select );
            ui->objectsView->selectionModel()->setCurrentIndex( mil.at( 0 ), QItemSelectionModel::Select );
            m_currentIndex = ui->objectsView->currentIndex();
        }
    }
}

void ObjectViewWidget::onObjectSearch(QString text)
{
    if( ui->objectsView->model() != NULL ) {
        QSortFilterProxyModel *model = qobject_cast<QSortFilterProxyModel*>( ui->objectsView->model() );
        if( model != NULL ) {
            if( !ui->useRegexCheckBox->isChecked() ) {
                model->setFilterFixedString( text );
            } else {
                model->setFilterRegExp( text );
            }

            updateObjectNumberLabel( model );
        }

    }
}

void ObjectViewWidget::onObjectClicked(/*QModelIndex index*/)
{
    if( m_currentIndex.isValid() ) {
        StarItem *item = m_assetsManager->getItem( m_currentIndex.data( Qt::UserRole + 1 ).toString() );

        if( item != NULL ) {
            createInfoWindow( item );
        } else {
            qDebug() << "Item is null" << endl;
        }
    }
}

void ObjectViewWidget::onObjectClicked( QModelIndex index )
{
    if( index.isValid() ) {
        m_currentIndex = index;
        /*StarItem *item = m_assetsManager->getItem( index.data( Qt::UserRole + 1 ).toString() );

        if( item != NULL ) {
            createInfoWindow( item );
        }*/
    }
}

void ObjectViewWidget::onCategoryChange(int index)
{
    QString data = ui->categoryComboBox->itemData( index ).toString();
    QSortFilterProxyModel *model = qobject_cast<QSortFilterProxyModel*>( ui->objectsView->model() );

    if( model != NULL ) {
        if( data == "all" ) {
            model->setFilterRegExp( "" );
        } else {
            QStringList filesType = m_assetsManager->getItemsListByCateory( data );

            QString regex( "(");

            for( int i = 0; i < filesType.size(); i++ ) {
                regex += "^" + filesType[i] + "$";

                if( i < filesType.size() - 1 ) {
                    regex += "|";
                }
            }

            regex += ")";

            model->setFilterRegExp( regex );
        }

        updateObjectNumberLabel( model );
    }
}

void ObjectViewWidget::customContextMenu(QPoint pt)
{
    QModelIndex idx = ui->objectsView->indexAt( pt );
    onObjectClicked( idx );

    QAction *action = new QAction( tr( "Show Info" ), this );

    QMenu *contextMenu = new QMenu( this );
    contextMenu->addAction( action );

    QPoint globalPt = ui->objectsView->mapToGlobal( pt );

    connect( action, SIGNAL( triggered() ), this, SLOT( onObjectClicked() ) );

    contextMenu->exec( globalPt );
}

void ObjectViewWidget::initEvents()
{
    connect( ui->keywordLineEdit, SIGNAL( textChanged( QString ) ), this, SLOT( onObjectSearch(QString) ) );
    connect( ui->objectsView, SIGNAL( clicked(QModelIndex) ), this, SLOT( onObjectClicked(QModelIndex) ) );
    connect( ui->categoryComboBox, SIGNAL( currentIndexChanged(int) ), this, SLOT( onCategoryChange(int) ) );
    connect( ui->objectsView, SIGNAL( customContextMenuRequested(QPoint) ), this, SLOT( customContextMenu( QPoint ) ) );
}

void ObjectViewWidget::buildInterface()
{
    QStringList extensionsList = m_assetsManager->assetsExtensions();

    ui->categoryComboBox->addItem( tr( "All" ), "all" );

    foreach( QString ext, extensionsList ) {
        ui->categoryComboBox->addItem( ext, ext );
    }

    ui->objectsView->setContextMenuPolicy( Qt::CustomContextMenu );
}

void ObjectViewWidget::createInfoWindow(StarItem *item)
{
    if( m_infosObject != NULL ) {
        m_infosObject->deleteLater();
        m_infosObject = NULL;
    }

    m_infosObject = new ObjectInfoWidget( item, this );
    m_infosObject->setGeometry( ui->objectsView->x() + ui->objectsView->width(), QCursor::pos().y(), 200, 300 );
    m_infosObject->setWindowFlags( Qt::Tool );
    m_infosObject->setModal( false );


    QFileInfo itemInfo( item->itemLocation() );
    m_infosObject->setWindowTitle( item->shortDescription() );
    m_infosObject->setWindowIcon( QIcon( QPixmap( itemInfo.path() + "/" + item->inventoryIcon() ) ) );
    m_infosObject->show();
}
