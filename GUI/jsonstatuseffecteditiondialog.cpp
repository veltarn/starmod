#include "jsonstatuseffecteditiondialog.h"
#include "ui_jsonstatuseffecteditiondialog.h"

JsonStatusEffectEditionDialog::JsonStatusEffectEditionDialog( AssetsManager *assetsManager, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::JsonStatusEffectEditionDialog),
    m_cancelled( false ),
    m_assetsManager( assetsManager )
{
    ui->setupUi(this);
    setWindowTitle( tr( "Status Effect Edition" ) );
    loadStatusEffects();
    initEvents();
}

JsonStatusEffectEditionDialog::~JsonStatusEffectEditionDialog()
{
    delete ui;
}

void JsonStatusEffectEditionDialog::initEvents()
{
    connect( ui->validateButton, SIGNAL( clicked() ), this, SLOT( validate() ) );
    connect( ui->cancelButton, SIGNAL( clicked() ), this, SLOT( cancel() ) );
}

void JsonStatusEffectEditionDialog::loadStatusEffects()
{
    QStringList filesList;
    Cache *cache = Cache::getInstance();

    if( !cache->exists( "item_statuseffect" ) ) {
        QDir d( m_assetsManager->assetsPath() );
        Utility::recurseAddDir( d, filesList, QStringList( "statuseffect" ) );
        cache->setFileContent( "item_statuseffect", filesList );
    } else {
        filesList = cache->getFileContent( "item_statuseffect" );
    }

    foreach( QString file, filesList ) {
        QFile statusFile( file );

        if( !statusFile.exists() ) {
            qDebug() << statusFile.fileName() << " no such file or directory" << endl;
            continue;
        }

        if( !statusFile.open( QIODevice::ReadOnly ) ) {
            qDebug() << "Cannot open" << statusFile.fileName() << ", reason: " << statusFile.errorString() << endl;
            continue;
        }

        QJsonParseError error;
        QJsonDocument doc = QJsonDocument::fromJson( statusFile.readAll(), &error );

        if( error.error != QJsonParseError::NoError ) {
            qDebug() << "Cannot parse" << statusFile.fileName() << endl
                     << error.errorString() << endl;
            continue;
        }

        QJsonObject root = doc.object();

        QJsonValue kind = root[ "kind" ];

        if( !kind.isUndefined() ) {
            ui->kindComboBox->addItem( kind.toString() );
        }
    }
}


bool JsonStatusEffectEditionDialog::cancelled() const
{
    return m_cancelled;
}

void JsonStatusEffectEditionDialog::setKind(const QString kind)
{
    int index = ui->kindComboBox->findText( kind );

    if( index != -1 ) {
        ui->kindComboBox->setCurrentIndex( index );
    }
}

QString JsonStatusEffectEditionDialog::getKind() const
{
    return ui->kindComboBox->currentText();
}

double JsonStatusEffectEditionDialog::getAmount() const
{
    return ui->amountDoubleSpinBox->value();
}

void JsonStatusEffectEditionDialog::setAmount(double amount)
{
    ui->amountDoubleSpinBox->setValue( amount );
}

int JsonStatusEffectEditionDialog::getPercentage() const
{
    return ui->percentageSpinBox->value();
}

void JsonStatusEffectEditionDialog::setPercentage(int percentage)
{
    ui->percentageSpinBox->setValue( percentage );
}

int JsonStatusEffectEditionDialog::getRange() const
{
    return ui->rangeSpinBox->value();
}

void JsonStatusEffectEditionDialog::setRange(int range)
{
    ui->rangeSpinBox->setValue( range );
}

void JsonStatusEffectEditionDialog::validate()
{
    m_cancelled = false;
    accept();
}

void JsonStatusEffectEditionDialog::cancel()
{
    m_cancelled = true;
    reject();
}

