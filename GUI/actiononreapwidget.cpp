#include "actiononreapwidget.h"
#include "ui_actiononreapwidget.h"

ActionOnReapWidget::ActionOnReapWidget(AssetsManager *assetsManager, QString projectPath, bool createAction, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ActionOnReapWidget),
    m_assetsManager( assetsManager ),
    m_projectPath( projectPath ),
    m_createAction( createAction )
{
    ui->setupUi(this);

    buildInterface();
    initEvents();
}

ActionOnReapWidget::~ActionOnReapWidget()
{
    delete ui;
}

void ActionOnReapWidget::populateForm(ActionOnReap &aor)
{
    //Changing combo box and stacked widget values
    ui->actionTypeComboBox->setCurrentIndex( aor.action );

    ui->delayStepsSpinBox->setValue( aor.delaySteps );

    int index = ui->projectileTypeComboBox->findText( aor.projectileType );

    ui->projectileTypeComboBox->setCurrentIndex( index );
    ui->inheritDamageFactorDoubleSpinBox->setValue( aor.inheritDamageFactor );
    ui->angleSpinBox->setValue( aor.angle );
    ui->adjustAngleSpinBox->setValue( aor.adjustAngle );
    ui->fuzzAngleSpinBox->setValue( aor.fuzzAngle );

    index = 0;
    index = ui->liquidIdComboBox->findData( aor.liquidId );
    ui->liquidIdComboBox->setCurrentIndex( index );
    ui->quantitySpinBox->setValue( aor.quantity );

    ui->foregroundRadiusSpinBox->setValue( aor.foregroundRadius );
    ui->backgroundRadiusSpinBox->setValue( aor.backgroundRadius );
    ui->explosiveDamageAmountSpinBox->setValue( aor.explosiveDamageAmount );

    ui->soundsList->addItems( aor.sounds );

    ui->lineEditConfigFile->setText( aor.configFile );

}

int ActionOnReapWidget::actionType()
{
    return ui->actionTypeComboBox->currentData().toInt();
}

int ActionOnReapWidget::delaySteps()
{
    return ui->delayStepsSpinBox->value();
}

QString ActionOnReapWidget::projectileType()
{
    return ui->projectileTypeComboBox->currentText();
}

double ActionOnReapWidget::inheritDamageFactor()
{
    return ui->inheritDamageFactorDoubleSpinBox->value();
}

int ActionOnReapWidget::angle()
{
    return ui->angleSpinBox->value();
}

int ActionOnReapWidget::adjustAngle()
{
    return ui->adjustAngleSpinBox->value();
}

int ActionOnReapWidget::fuzzAngle()
{
    return ui->fuzzAngleSpinBox->value();
}

int ActionOnReapWidget::liquidId()
{
    return ui->liquidIdComboBox->currentData().toInt();
}

int ActionOnReapWidget::quantity()
{
    return ui->quantitySpinBox->value();
}

int ActionOnReapWidget::foregroundRadius()
{
    return ui->foregroundRadiusSpinBox->value();
}

int ActionOnReapWidget::backgroundRadius()
{
    return ui->backgroundRadiusSpinBox->value();
}

int ActionOnReapWidget::explosiveDamageAmount()
{
    return ui->explosiveDamageAmountSpinBox->value();
}

QStringList ActionOnReapWidget::sounds()
{
    QStringList sounds;
    for( int i = 0; i < ui->soundsList->count(); i++ ) {
        QListWidgetItem *item = ui->soundsList->item( i );
        sounds.append( item->data( Qt::UserRole + 1 ).toString() );
    }
    return sounds;
}

QString ActionOnReapWidget::config()
{
    return ui->lineEditConfigFile->text();
}

bool ActionOnReapWidget::validate()
{
    int actionType = ui->actionTypeComboBox->currentData().toInt();
    bool valid = true;
    switch( actionType ) {
        case Projectile:
            if( projectileType() == "-" ) {
                valid = false;
                m_lastValidationErrorsList.append( tr( "Projectile Type is empty" ) );
            }
        break;

        case Sounds:
            if( ui->soundsList->count() == 0 ) {
                valid = false;
                m_lastValidationErrorsList.append( tr( "Sounds list is empty" ) );
            }
        break;

        case Config:
            if( ui->lineEditConfigFile->text() == "" ) {
                valid = false;
                m_lastValidationErrorsList.append( tr( "Config file is empty" ) );
            }
        break;
    }
    return valid;
}

void ActionOnReapWidget::onWidgetChanged(int index)
{
    if( index == 1 || index == 2 || index == 4 ) {
        ui->delayStepsSpinBox->setEnabled( true );
    } else {
        ui->delayStepsSpinBox->setEnabled( false );
    }
}

void ActionOnReapWidget::onActionTypeChanged(int idx)
{
    Q_UNUSED( idx );
    int widgetIdx = ui->actionTypeComboBox->currentData().toInt();

    ui->actionTypeStackedWidget->setCurrentIndex( widgetIdx );
}

void ActionOnReapWidget::onBrowseSound()
{
    QString path = QFileDialog::getOpenFileName( this, tr( "Add a sound file" ), m_projectPath, QString( "*.wav" ) );

    if( path != "" ) {
        QListWidgetItem *item = new QListWidgetItem();
        QFileInfo fifo( path );
        item->setText( fifo.fileName() );
        item->setData( Qt::UserRole + 1, path );

        ui->soundsList->addItem( item );
    }
}

void ActionOnReapWidget::onSoundItemSelected(QModelIndex index)
{
    if( index.isValid() ) {
        ui->removeSoundButton->setEnabled( true );
    } else {
        ui->removeSoundButton->setEnabled( false );
    }
}

void ActionOnReapWidget::onRemoveSound()
{
    QModelIndex idx = ui->soundsList->currentIndex();

    if( idx.isValid() ) {
        QListWidgetItem *item = ui->soundsList->item( idx.row() );
        delete item;
        item = NULL;
        ui->removeSoundButton->setEnabled( false );
    }
}

void ActionOnReapWidget::onBrowseConfig()
{
    QString path = QFileDialog::getOpenFileName( this, tr( "Get a config file" ), m_projectPath, QString( "*.config" ) );

    if( path != "" ) {
        ui->lineEditConfigFile->setText( path );
    }
}

void ActionOnReapWidget::buildInterface()
{
    //Adding actions type
    ui->actionTypeComboBox->addItem( "-", 0 );
    ui->actionTypeComboBox->addItem( tr( "Projectile" ), 1 );
    ui->actionTypeComboBox->addItem( tr( "Explosion" ), 2 );
    ui->actionTypeComboBox->addItem( tr( "Liquid" ), 3 );
    ui->actionTypeComboBox->addItem( tr( "Sounds" ), 4 );
    ui->actionTypeComboBox->addItem( tr( "Config" ), 5 );

    //Loading projectiles
    ProjectilesUtility::loadProjectilesList( ui->projectileTypeComboBox, m_assetsManager->assetsPath(), m_projectPath );

    //Adding liquid id
    ui->liquidIdComboBox->addItem( "-", "-" );
    ui->liquidIdComboBox->addItem( tr( "Water" ), 1 );
    ui->liquidIdComboBox->addItem( tr( "Endless Water" ), 2 );
    ui->liquidIdComboBox->addItem( tr( "Lava" ), 3 );
    ui->liquidIdComboBox->addItem( tr( "Acid" ), 4 );
    ui->liquidIdComboBox->addItem( tr( "Endless Lava" ), 5 );
    ui->liquidIdComboBox->addItem( tr( "Tentacle Juice" ), 6 );
    ui->liquidIdComboBox->addItem( tr( "Tar" ), 7 );
}

void ActionOnReapWidget::initEvents()
{
    connect( ui->actionTypeStackedWidget, SIGNAL( currentChanged(int) ), this, SLOT( onWidgetChanged(int) ) );
    connect( ui->addSoundButton, SIGNAL( clicked() ), this, SLOT( onBrowseSound() ) );
    connect( ui->soundsList, SIGNAL( clicked(QModelIndex) ), this, SLOT( onSoundItemSelected(QModelIndex) ) );
    connect( ui->removeSoundButton, SIGNAL( clicked() ), this, SLOT( onRemoveSound() ) );
    connect( ui->actionTypeComboBox, SIGNAL( currentIndexChanged(int) ), this, SLOT( onActionTypeChanged( int ) ) );
    connect( ui->browseConfigFile, SIGNAL( clicked() ), this, SLOT( onBrowseConfig() ) );
}
bool ActionOnReapWidget::createAction() const
{
    return m_createAction;
}

QStringList ActionOnReapWidget::lastValidationErrorsList() const
{
    return m_lastValidationErrorsList;
}
