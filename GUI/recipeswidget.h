#ifndef RECIPESWIDGET_H
#define RECIPESWIDGET_H

#include <QDebug>
#include <QMenu>
#include <QAction>
#include <QWidget>
#include <QFileInfo>
#include <QStringList>
#include <QMessageBox>
#include <QStandardItemModel>
#include "../Starbound/Items/staritem.h"
#include "../Starbound/starrecipe.h"
#include "recipeitempicker.h"
#include "recipesgroupspicker.h"
#include "../Core/assetsmanager.h"
#include "../Core/utility.h"


namespace Ui {
class RecipesWidget;
}

class RecipesWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit RecipesWidget( AssetsManager *assetsManager, QWidget *parent = 0);
    ~RecipesWidget();

    void validate( StarItem *item );
public slots:
    void onAddBtnClicked();
    void onRemoveBtnClicked();
    void onEditClicked();
    void setOutputName( QString name );
    void onCustomContextRequest( QPoint pt );
    void onItemClicked( QModelIndex idx );

    void onAddFilter();
    void onRemoveFilter();
    void onFilterClicked( QModelIndex idx );
private:
    void buildInterface();
    void initEvents();

private:
    Ui::RecipesWidget *ui;
    AssetsManager *m_assetsManager;
    QStandardItemModel *m_model;

    QList< Input > m_inputItemsList;
};

#endif // RECIPESWIDGET_H
