#ifndef PROJECTILEEDITIONWINDOW_H
#define PROJECTILEEDITIONWINDOW_H

#include <QDialog>
#include <QString>
#include "../Core/assetsmanager.h"
#include "projectileeditionwidget.h"

namespace Ui {
class ProjectileEditionWindow;
}

class ProjectileEditionWindow : public QDialog
{
    Q_OBJECT
    
public:
    explicit ProjectileEditionWindow( AssetsManager *m_assetsManager, QString modPath, bool createProjectile = false, QWidget *parent = 0);
    ~ProjectileEditionWindow();
    
public slots:
    void onValidate();
    void onClose();

private:
    void initEvents();
    void buildInterface();

private:
    Ui::ProjectileEditionWindow *ui;
    ProjectileEditionWidget *m_projectileEditionWidget;
    AssetsManager *m_assetsManager;
    QString m_modPath;
    bool m_createProjectile;
};

#endif // PROJECTILEEDITIONWINDOW_H
