#include "physicspropertieslist.h"
#include "ui_physicspropertieslist.h"

PhysicsPropertiesList::PhysicsPropertiesList(AssetsManager *assetsManager, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PhysicsPropertiesList),
    m_assetsManager( assetsManager )
{
    ui->setupUi(this);

    m_physicsList = new QTreeView( this );
    m_model = new QStandardItemModel( this );

    QStringList headers;
    headers << tr( "Property Name" )
            << tr( "Mass" )
            << tr( "Gravity Multiplier" )
            << tr( "Bounce Factor" )
            << tr( "Max Movement Per Step" )
            << tr( "Maximum Correction" )
            << tr( "Air Friction" )
            << tr( "Liquid Friction" )
            << tr( "Ground Friction" )
            << tr( "Collision Poly" )
            << tr( "Ignore Platform Collision" )
            << tr( "Ground Slide Movement Enabled" );
    m_model->setHorizontalHeaderLabels( headers );

    m_physicsList->setModel( m_model );

    buildInterface();
    initEvents();
    createList();
}

PhysicsPropertiesList::~PhysicsPropertiesList()
{
    delete ui;
}

void PhysicsPropertiesList::buildInterface()
{
    m_toolbar = new QToolBar( tr( "Physics Properties toolbar" ), this );

    m_addPhysicsPropAction = new QAction( QIcon( QPixmap( "assets/images/add.png" ) ), tr( "Add Physical"), this );
    m_editPhysicsPropAction = new QAction( QIcon( QPixmap( "assets/images/edit.png" ) ), tr( "Edit Physical Property" ), this );
    m_removePhysicsPropAction = new QAction( QIcon( QPixmap( "assets/images/remove.png" ) ), tr( "Remove Physical Proprety" ), this );

    m_editPhysicsPropAction->setEnabled( false );
    m_removePhysicsPropAction->setEnabled( false );

    m_toolbar->addAction( m_addPhysicsPropAction );
    m_toolbar->addAction( m_editPhysicsPropAction );
    m_toolbar->addAction( m_removePhysicsPropAction );


    m_closeButton = new QPushButton( tr( "Close" ), this );

    m_layout = new QVBoxLayout;
    m_buttonsLayout = new QHBoxLayout;

    /*QSpacerItem *item = new QSpacerItem( 900, 0, QSizePolicy::Maximum );
    m_buttonsLayout->addSpacerItem( item );*/
    m_buttonsLayout->addStretch( 4 );
    m_buttonsLayout->addWidget( m_closeButton );

    m_layout->addWidget( m_toolbar );
    m_layout->addWidget( m_physicsList, 4 );
    m_layout->addLayout( m_buttonsLayout );

    setLayout( m_layout );
}

void PhysicsPropertiesList::initEvents()
{
    connect( m_closeButton, SIGNAL( clicked() ), this, SLOT( accept() ) );
}

void PhysicsPropertiesList::createList()
{
    QList<StarPhysics> physicsList = m_assetsManager->physicsList();

    foreach( StarPhysics physics, physicsList ) {
        QList< QStandardItem* > standardList;
        standardList.append( new QStandardItem( physics.physicName() ) );
        QString mass = "";
        if( physics.mass() == -1 )
            mass = tr( "No mass defined" );
        else
            mass = QVariant( physics.mass() ).toString();
        standardList.append( new QStandardItem( mass ) );
        standardList.append( new QStandardItem( QVariant( physics.gravityMultiplier() ).toString() ) );
        standardList.append( new QStandardItem( QVariant( physics.bounceFactor() ).toString() ) );
        standardList.append( new QStandardItem( QVariant( physics.maxMovementPerStep() ).toString() ) );

        QString maximumCorrection = "";
        if( physics.maximumCorrection() == -1 )
            maximumCorrection = tr( "No maximum correction defined" );
        else
            maximumCorrection = QVariant( physics.maximumCorrection() ).toString();
        standardList.append( new QStandardItem( maximumCorrection ) );
        standardList.append( new QStandardItem( QVariant( physics.airFriction() ).toString() ) );
        standardList.append( new QStandardItem( QVariant( physics.liquidFriction() ).toString() ) );

        QString groundFriction = "";
        if( physics.groundFriction() == -1 )
            groundFriction = tr( "No ground friction defined" );
        else
            groundFriction = QVariant( physics.groundFriction() ).toString();

        standardList.append( new QStandardItem( groundFriction ) );
        standardList.append( new QStandardItem( "to define" ) );
        standardList.append( new QStandardItem( QVariant( physics.ignorePlatformCollision() ).toString() ) );
        standardList.append( new QStandardItem( QVariant( physics.groundSlideMovementEnabled() ).toString() ) );

        m_model->appendRow( standardList );
    }

}
