#include "buildmoddialog.h"
#include "ui_buildmoddialog.h"

BuildModDialog::BuildModDialog( Project *prj, QString starboundModPath, QWidget *parent ) :
    QDialog(parent),
    ui(new Ui::BuildModDialog),
    m_project( prj ),
    m_starboundModPath( starboundModPath )
{
    ui->setupUi(this);
    setWindowTitle( tr( "Project building" ) );
    setFixedSize( 400, 220 );

    QString buildPath = QFileInfo( prj->projectPath() ).path() + "/build";
    m_defaultBuildPath = buildPath;
    changeInfoModLabelPath( buildPath );
    ui->modLocationLineEdit->setText( buildPath );

    initEvents();
}

BuildModDialog::~BuildModDialog()
{
    delete ui;
}

void BuildModDialog::initEvents() {
    connect( ui->closeButton, SIGNAL( clicked() ), this, SLOT(accept() ) );
    connect( ui->validateButton, SIGNAL( clicked() ), this, SLOT( onValidate() ) );
    connect( ui->browseButton, SIGNAL( clicked() ), this, SLOT( onBrowse() ) );
    connect( ui->installModCheckBox, SIGNAL( toggled(bool) ), this, SLOT( onCheckBoxToggled( bool ) ) );
}

void BuildModDialog::onValidate() {
    QString buildPath = ui->modLocationLineEdit->text();
    if( buildPath != "" ) {
        QString zipName = m_project->projectName() + "-" + m_project->projectVersion() + ".zip";
        QFileInfo zipPath( QString( buildPath + "/" + zipName ) );
        qDebug() << zipPath.absoluteFilePath() << zipPath.exists() << endl;

        if( zipPath.exists() ) {
            if( QFile::remove( zipPath.absoluteFilePath() ) ) {
                qDebug() << "Removed existing zip file" << endl;
            }
        }

        ModManager mp( m_project->projectPath(), zipPath.absoluteFilePath() );
        if( mp.pack() ) {
            if( ui->installModCheckBox->isChecked() ) {
                if( ModManager::installMod( m_project->projectPath(), m_project->projectName(), m_starboundModPath ) ){
                    QMessageBox::information( this, tr( "Success" ), tr( "Your mod has successfully been packaged and installed" ) );
                    accept();
                } else {
                    QMessageBox::warning( this, tr( "Warning" ), tr( "Your mod has successfully been packaged" ) + "<strong>" + tr( "but" ) + "<strong>" + tr( " an error has occured during installation" ) );
                    accept();
                }
            } else {
                QMessageBox::information( this, tr( "Success" ), tr( "Your mod has successfully been packaged" ) );
                accept();
            }
        } else {
            QMessageBox::critical( this, tr( "Error" ), tr( "An error has occured, cannot zip your mod..." ) );
        }
    }
}

void BuildModDialog::onBrowse() {
    QString currentBuildLocation = ui->modLocationLineEdit->text();
    QString path = QFileDialog::getExistingDirectory( this, tr( "Zipped mod location" ), currentBuildLocation );

    if( path != "" ) {
        changeInfoModLabelPath( path );
        ui->modLocationLineEdit->setText( path );
    } else {
        QString cPath = ui->modLocationLineEdit->text();
        changeInfoModLabelPath( cPath );
        ui->modLocationLineEdit->setText( cPath );
    }
}

void BuildModDialog::onCheckBoxToggled( bool toggled ) {
    if( toggled ) {
        ui->warningMsgInstallLabel->setText( "<font color='orange'>" + tr( "If you run a test build from StarMod, the installed mod will be erased" ) + "</font>" );
    } else {
        ui->warningMsgInstallLabel->setText( "" );
    }
}

void BuildModDialog::changeInfoModLabelPath( QString newPath ) {
    ui->infoModLocationLabel->setText( tr( "The mod will be zipped at this location" ) + ": <br /><strong>" + newPath + "</strong>" );
}
