#include "quickeditor.h"
#include "ui_quickeditor.h"

QuickEditor::QuickEditor(QString modPath, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QuickEditor),
    m_saveFile( "" ),
    m_modPath( modPath )
{
    ui->setupUi(this);
    buildInterface();
    initEvents();
    setWindowTitle( tr( "Quick Editor" ) );
}

QuickEditor::~QuickEditor()
{
    delete ui;
}

void QuickEditor::buildInterface() {
    m_mainLayout = new QVBoxLayout;
    m_actionSave = new QAction( QIcon( QPixmap( "assets/images/save.png" ) ), tr( "Save" ), this );

    m_toolbar = new QToolBar( tr( "Main toolbar" ) );

    m_toolbar->addAction( m_actionSave );

    m_mainLayout->addWidget( m_toolbar, 1 );

    m_editor = new QsciScintilla( this );
    m_editor->setAutoIndent( true );
    m_editor->setMarginLineNumbers( 1, true );
    m_editor->setMarginWidth( 1, "-----" );

    m_mainLayout->addWidget( m_editor );

    m_closeButton = new QPushButton( tr( "Close" ), this );

    m_mainLayout->addWidget( m_closeButton );

    setLayout( m_mainLayout );
}

void QuickEditor::initEvents() {
    connect( m_closeButton, SIGNAL( clicked() ), this, SLOT( onClose() ) );
    connect( m_actionSave, SIGNAL( triggered() ), this, SLOT( onSaveClick() ) );
}

void QuickEditor::onSaveClick() {
    if( m_saveFile == "" ) {
        m_saveFile = QFileDialog::getSaveFileName( this, tr( "Save your file" ), m_modPath );
        save( m_saveFile );
    } else {
        save( m_saveFile );
    }
}

void QuickEditor::save( QString filepath ) {
    QString content = m_editor->text();

    if( QFile::exists( filepath ) )
        QFile::remove( filepath );

    QFile file( filepath );

    if( !file.open( QIODevice::WriteOnly ) ) {
        qWarning() << "Cannot open " << filepath << endl;
        return;
    }

    QTextStream stream( &file );
    stream.setCodec( "UTF-8" );

    stream << content;

    file.flush();
    qDebug() << "Successfully saved " << filepath << endl;
}

void QuickEditor::onClose() {
    if( m_saveFile == "" ) {
        int rep = QMessageBox::question( this, tr( "Confirm close" ), tr( "The editor is about to close and your work has not been saved. What do you want to do?" ), QMessageBox::Save | QMessageBox::Cancel | QMessageBox::Close );

        if( rep == QMessageBox::Save ) {
            onSaveClick();
        } else if ( rep == QMessageBox::Cancel ) {
            return;
        } else {
            accept();
        }
    }
    accept();
}

void QuickEditor::setSyntax( int syntax ) {
    QsciLexer *lexer = NULL;
    switch( syntax ) {
        case Json:
            lexer = new QsciLexerJavaScript( this );
            m_editor->setLexer( lexer );
        break;

        case Lua:
            lexer = new QsciLexerLua( this );
            m_editor->setLexer( lexer );
        break;
    }
}

QString QuickEditor::getSaveFile() const {
    return m_saveFile;
}
