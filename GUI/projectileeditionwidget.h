#ifndef PROJECTILEEDITIONWIDGET_H
#define PROJECTILEEDITIONWIDGET_H

#include <QWidget>
#include <QMenu>
#include <QDebug>
#include <QString>
#include <QDir>
#include <QFileInfo>
#include <QFileDialog>
#include <QColorDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QPalette>
#include <QColor>
#include <QColorDialog>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QVBoxLayout>
#include "../Core/assetsmanager.h"
#include "../Starbound/starprojectile.h"
#include "frameeditionwindow.h"
#include "actiononreapwindow.h"

namespace Ui {
class ProjectileEditionWidget;
}

class ProjectileEditionWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit ProjectileEditionWidget( AssetsManager *assetsManager, QString modPath, QWidget *parent = 0);
    ~ProjectileEditionWidget();

    bool validate();
    void createProjectile();

    QString getProjectileName() const;
    void setProjectileName( QString name );

    QString getFrameFile() const;
    void setFrameFile( QString file );

    int getFrameNumber() const;
    void setFrameNumber( int number );

    double getAnimationCycle() const;
    void setAnimationCycle( double cycle );

    QString getDamageKind() const;
    void setDamageKind( QString damage );

    QString getDamageKindImage() const;
    void setDamageKindImage( QString path );

    QString getDamageType() const;
    void setDamageType( QString path );

    bool hasPointLight() const;
    void setPointLight( bool pointLight );

    QColor getLightColor() const;
    void setLightColor( QColor lightColor );

    double getFallSpeed() const;
    void setFallSpeed( double fallSpeed );
    
    QString lastValidationError() const;


    QList<ActionOnReap> actionOnReapList() const;

public slots:
    void onFrameFileBrowse();
    void onDamageKindBrowse();
    void onPickupColor();
    void onCreateFrameClicked();
    void onAddActionOnReap();
    void onActionOnReapItemClicked( QModelIndex idx );
    void onActionOnReapContextRequested( QPoint pt );
    void onActionEditClicked();
    void onActionRemoveClicked();
    void onCopyActionClicked();
    void onNTimeCopyActionClicked();
private:
    void initEvents();
    //void buildInterface();
    void initDamageComboBox();
    void initPhysicsComboBox();
    QString getDamageKind( QString path );
    void resetModel();
    void updateActionOnReapList();
    ActionOnReap populateActionOnReap( ActionOnReapWidget *widget );
    void copyAction( QModelIndex idx, bool refreshView = true );
private:
    Ui::ProjectileEditionWidget *ui;
    QColor m_lightColor;
    AssetsManager *m_assetsManager;
    QString m_modPath;
    QString m_lastValidationError;
    QStandardItemModel *m_actionOnReapModel;

    QList<ActionOnReap> m_actionOnReapList;
};

#endif // PROJECTILEEDITIONWIDGET_H
