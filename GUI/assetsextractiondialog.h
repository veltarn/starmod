#ifndef ASSETSEXTRACTIONDIALOG_H
#define ASSETSEXTRACTIONDIALOG_H

#include <QCloseEvent>
#include <QDialog>
#include <QProcess>
#include <QMessageBox>

namespace Ui {
class AssetsExtractionDialog;
}

class AssetsExtractionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AssetsExtractionDialog( QString extractorPath, QStringList arguments, QWidget *parent = 0);
    ~AssetsExtractionDialog();
    void initEvents();

public slots:
    void onReadyRead();
    void onFinished( int code );
protected:
    void closeEvent(QCloseEvent *ce);
private:
    Ui::AssetsExtractionDialog *ui;
    QString m_extractorPath;
    QStringList m_arguments;
    QProcess *m_process;
    bool m_processActive;
};

#endif // ASSETSEXTRACTIONDIALOG_H
