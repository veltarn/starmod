#include "objecteditionwindow.h"
#include "ui_objecteditionwindow.h"

ObjectEditionWindow::ObjectEditionWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ObjectEditionWindow)
{
    ui->setupUi(this);
}

ObjectEditionWindow::~ObjectEditionWindow()
{
    delete ui;
}
