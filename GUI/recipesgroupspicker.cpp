#include "recipesgroupspicker.h"
#include "ui_recipesgroupspicker.h"

RecipesGroupsPicker::RecipesGroupsPicker( AssetsManager *assetsManager, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RecipesGroupsPicker),
    m_assetsManager( assetsManager )
{
    ui->setupUi(this);
    setWindowTitle( tr( "Pickup a group" ) );

    buildInterface();
    initEvents();
}

RecipesGroupsPicker::~RecipesGroupsPicker()
{
    delete ui;
}

QString RecipesGroupsPicker::getSelectedGroup()
{
    return ui->filterComboBox->currentData().toString();
}

void RecipesGroupsPicker::validate()
{
    if( ui->filterComboBox->currentText() != "-" ) {
        accept();
    }
}

void RecipesGroupsPicker::buildInterface()
{
    Cache *cache = Cache::getInstance();
    QStringList list;
    QStringList filterType;
    filterType << "materials" << "armors" << "weapons" << "consumables" << "tools" << "objects" << "other" << "all";
    filterType.sort();

    if( cache->exists( "crafting_filter" ) ) {
        list = cache->getFileContent( "crafting_filter" );
    } else {
        list = m_assetsManager->getRecipesFilters();
    }

    ui->filterComboBox->addItem( "-", "-" );
    foreach( QString elem, list ) {
        ui->filterComboBox->addItem( elem, elem );
    }

    ui->filterComboBox->insertSeparator( ui->filterComboBox->count() );

    foreach( QString elem, filterType ) {
        ui->filterComboBox->addItem( elem, elem );
    }
}

void RecipesGroupsPicker::initEvents()
{
    connect( ui->addButton, SIGNAL( clicked() ), this, SLOT( validate() ) );
    connect( ui->cancelButton, SIGNAL( clicked() ), this, SLOT( reject() ) );
}
