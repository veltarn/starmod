#include "progresssplashscreen.h"

ProgressSplashScreen::ProgressSplashScreen(const QPixmap &pixmap ) :
    QSplashScreen(pixmap),
    m_minimum( 0 ),
    m_maximum( 100 )
{
    m_progression = new QProgressBar( this );

    /*if( !QFile::exists( "assets/css/ProgressBar.css" ) )
        qDebug() << "ProgressBar.css doesn't exists" << endl;*/

    m_progression->setStyleSheet( StylesheetManager::getStylesheet( "ProgressBar.css" ) );
    m_progression->setMinimum( 0 );
    m_progression->setMaximum( 100 );
    setValue( 0 );

    m_progression->setGeometry( 0, 253, 830, 20 );
    m_progression->setTextVisible( true );
    m_progression->setAlignment( Qt::AlignCenter );
    m_progression->show();
    qDebug() << width() << endl;
}

ProgressSplashScreen::~ProgressSplashScreen()
{}

int ProgressSplashScreen::getMinimum() const
{
    return m_minimum;
}

void ProgressSplashScreen::setMinimum(int value)
{
    m_minimum = value;
    m_progression->setMinimum( value );
}
int ProgressSplashScreen::getMaximum() const
{
    return m_maximum;
}

void ProgressSplashScreen::setMaximum(int value)
{
    m_maximum = value;
    m_progression->setMaximum( value );
}

int ProgressSplashScreen::value() const
{
    return m_value;
}

void ProgressSplashScreen::setValue(int value )
{
    m_value = value;
    m_progression->setValue( value );
    int pCent = static_cast< int >( ( m_value * 100 ) / m_maximum );

    if( pCent == 100 ) {
        m_progression->setFormat( tr( "Starting" ) + "..." );
    } else if( pCent == 0 ) {
        m_progression->setFormat( tr( "Loading Starmod" ) + "..." );
    }else {
        m_progression->setFormat( QString::number( pCent ) + "%" );
    }
}

void ProgressSplashScreen::increaseValue(int step)
{
    if( step > 0 ) {
        m_value += step;
        m_progression->setValue( m_value );
    }
}


void ProgressSplashScreen::drawContents( QPainter *painter )
{
    QSplashScreen::drawContents( painter );/*

    QLinearGradient gradient;
    gradient.setColorAt( 0.0, QColor( 25, 141, 113 ) );
    gradient.setColorAt( 1.0, QColor( 255, 255, 255 ) );

    QBrush brush( gradient );

    QPalette palette;
    palette.setBrush( QPalette::Background, brush );

    QStyleOptionProgressBarV2 pbStyle;
    pbStyle.initFrom( this );
    pbStyle.palette = palette;
    pbStyle.textVisible = true;
    pbStyle.state = QStyle::State_Enabled;
    pbStyle.minimum = m_minimum;
    pbStyle.maximum = m_maximum;
    pbStyle.progress = m_value;
    pbStyle.invertedAppearance = false;
    pbStyle.rect = QRect( 0, 253, 830, 20 );

    style()->drawControl( QStyle::CE_ProgressBar, &pbStyle, painter, this );*/
}
