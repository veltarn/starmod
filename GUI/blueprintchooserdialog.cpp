#include "blueprintchooserdialog.h"
#include "ui_blueprintchooserdialog.h"

BlueprintChooserDialog::BlueprintChooserDialog( AssetsManager *assetsManager, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BlueprintChooserDialog),
    m_assetsManager( assetsManager )
{
    ui->setupUi(this);
    setWindowTitle( tr( "Blueprint Chooser" ) );

    initEvents();
    loadBlueprints();
}

BlueprintChooserDialog::~BlueprintChooserDialog()
{
    delete ui;
}

QString BlueprintChooserDialog::getSelectedItem()
{
    return ui->blueprintComboBox->currentText();
}

void BlueprintChooserDialog::validate()
{
    accept();
}

void BlueprintChooserDialog::initEvents()
{
    connect( ui->validateButton, SIGNAL( clicked() ), this, SLOT( validate() ) );
    connect( ui->cancelButton, SIGNAL( clicked() ), this, SLOT( reject() ) );
}

void BlueprintChooserDialog::loadBlueprints()
{
    Cache *cache = Cache::getInstance();
    QStringList filesList;
    if( !cache->exists( "items_blueprintlist" ) ) {
        QDir assets( m_assetsManager->assetsPath() );
        Utility::recurseAddDir( assets, filesList, QStringList( "recipe" ) );
        cache->setFileContent( "items_blueprintlist", filesList );
    } else {
        filesList = cache->getFileContent( "items_blueprintlist" );
    }

    foreach( QString file, filesList ) {
        QFile bFile( file );

        if( !bFile.open( QIODevice::ReadOnly ) ) {
            qDebug() << "Cannot open file" << bFile.fileName() << ", " << bFile.errorString() << endl;
            continue;
        }

        QJsonParseError error;
        QJsonDocument doc = QJsonDocument::fromJson( bFile.readAll(), &error );

        if( error.error != QJsonParseError::NoError ) {
            qDebug() << "Cannot parse" << bFile.fileName() << ", " << error.errorString() << endl;
            continue;
        }

        QJsonObject root = doc.object();
        QJsonValue output = root[ "output" ];
        if( !output.isNull() ) {
            QJsonObject objOutput = output.toObject();
            QJsonValue itemOutputValue = objOutput[ "item" ];
            if( !itemOutputValue.isNull() ) {
                QString itemName = itemOutputValue.toString();
                ui->blueprintComboBox->addItem( itemName );
            }
        } else {
            qDebug() << "Cannot find output in recipe file" << endl;
            continue;
        }
    }
}
