#ifndef PROJECTILELISTWINDOW_H
#define PROJECTILELISTWINDOW_H

#include <QDialog>
#include <QString>
#include <QPushButton>
#include <QTreeView>
#include <QStandardItemModel>
#include <QToolBar>
#include <QAction>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QColor>
#include <QBrush>
#include "../Core/assetsmanager.h"

namespace Ui {
class ProjectileListWindow;
}

class ProjectileListWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ProjectileListWindow( AssetsManager *assetsManager, QWidget *parent = 0);
    ~ProjectileListWindow();

private:
    void buildInterface();
    void initEvents();
    void createProjectilesList();
    QList<QStandardItem *> getPropertyPair( QStandardItem *title, QStandardItem *value );
private:
    Ui::ProjectileListWindow *ui;
    AssetsManager *m_assetsManager;

    QPushButton *m_closeButton;
    QTreeView *m_projectilesList;
    QStandardItemModel *m_model;
    QToolBar *m_toolbar;
    QAction *m_addAction;
    QAction *m_editAction;
    QAction *m_removeAction;

    QHBoxLayout *m_buttonLayout;
    QVBoxLayout *m_layout;
};

#endif // PROJECTILELISTWINDOW_H
