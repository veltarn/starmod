#ifndef PHYSICSPROPERTIESLIST_H
#define PHYSICSPROPERTIESLIST_H

#include <QString>
#include <QVariant>
#include <QDialog>
#include <QPushButton>
#include <QToolBar>
#include <QAction>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSpacerItem>
#include <QTreeView>
#include <QStandardItemModel>
#include <QList>
#include "../Starbound/starphysics.h"
#include "../Core/assetsmanager.h"
#include "../Core/utility.h"


namespace Ui {
class PhysicsPropertiesList;
}

class PhysicsPropertiesList : public QDialog
{
    Q_OBJECT

public:
    explicit PhysicsPropertiesList( AssetsManager *assetsManager, QWidget *parent = 0);
    ~PhysicsPropertiesList();
private:
    void buildInterface();
    void initEvents();
    void createList();
private:
    Ui::PhysicsPropertiesList *ui;

    QPushButton *m_closeButton;
    QToolBar *m_toolbar;
    QAction *m_addPhysicsPropAction;
    QAction *m_editPhysicsPropAction;
    QAction *m_removePhysicsPropAction;

    QTreeView *m_physicsList;
    QStandardItemModel *m_model;

    QVBoxLayout *m_layout;
    QHBoxLayout *m_buttonsLayout;

    AssetsManager *m_assetsManager;
};

#endif // PHYSICSPROPERTIESLIST_H
