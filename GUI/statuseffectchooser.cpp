#include "statuseffectchooser.h"
#include "ui_statuseffectchooser.h"

StatusEffectChooser::StatusEffectChooser( AssetsManager *assetsManager, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::StatusEffectChooser),
    m_assetsManager( assetsManager ),
    m_cancelled( false ),
    m_editStatus( false )
{
    m_log = Logger::getInstance();
    ui->setupUi(this);
    setWindowTitle( tr( "Status Effect Chooser" ) );
    loadStatusEffects();
    initEvents();
}

StatusEffectChooser::~StatusEffectChooser()
{
    delete ui;
}

QString StatusEffectChooser::getStatusEffect()
{
    return ui->statusEffectsComboBox->currentText();
}

void StatusEffectChooser::initEvents()
{
    connect( ui->validateButton, SIGNAL( clicked() ), this, SLOT( validate() ) );
    connect( ui->closeButton, SIGNAL( clicked() ), this, SLOT( close() ) );
    connect( ui->addAndEditButton, SIGNAL( clicked() ), this, SLOT( addAndEdit() ) );
}

void StatusEffectChooser::loadStatusEffects()
{
    QStringList filesList;
    Cache *cache = Cache::getInstance();
    if( !cache->exists( "item_statuseffect" ) ) {
        QDir assetsDir( m_assetsManager->assetsPath() );
        Utility::recurseAddDir( assetsDir, filesList, QStringList( "statuseffect" ) );
        cache->setFileContent( "item_statuseffect", filesList );
    } else {
        filesList = cache->getFileContent( "item_statuseffect" );
    }

    int nbErrors = 0;
    bool errors = false;
    foreach( QString file, filesList ) {
        QFile statusFile( file );

        if( !statusFile.exists() ) {
            m_log->addEntry( Warning, "Cannot open " + statusFile.fileName() + ", no such file or directory" );
            qDebug() << statusFile.fileName() << " no such file or directory" << endl;
            nbErrors += 1;
            errors = true;
            continue;
        }

        if( !statusFile.open( QIODevice::ReadOnly ) ) {
            m_log->addEntry( Warning, "Cannot open " + statusFile.fileName() + ", " + statusFile.errorString() );
            qDebug() << "Cannot open" << statusFile.fileName() << ", reason: " << statusFile.errorString() << endl;
            nbErrors += 1;
            errors = true;
            continue;
        }

        QJsonParseError error;
        QJsonDocument doc = QJsonDocument::fromJson( statusFile.readAll(), &error );

        if( error.error != QJsonParseError::NoError ) {
            m_log->addEntry( Error, "Cannot parse " + statusFile.fileName() + ", " + error.errorString() );
            qDebug() << "Cannot parse" << statusFile.fileName() << endl
                     << error.errorString() << endl;
            nbErrors += 1;
            errors = true;
            continue;
        }

        QJsonObject root = doc.object();
        StatusEffect *effect = StatusEffect::fromJsonObject( root );
        m_statusEffectList.append( effect );
        QString kind = effect->kind();

        if( kind != "") {
            //We add the kind + the last index added to the list
            ui->statusEffectsComboBox->addItem( kind, m_statusEffectList.size() - 1 );
        }
    }

    if( errors ) {
        ui->errorsLabel->setText( "<font color='red'>" + QString::number( nbErrors ) + " " + tr( "errors has been found during parsing, see log console to watch details" ) );
    }
}
bool StatusEffectChooser::editStatus() const
{
    return m_editStatus;
}



bool StatusEffectChooser::cancelled() const
{
    return m_cancelled;
}

void StatusEffectChooser::validate()
{
    m_cancelled = false;
    accept();
}

void StatusEffectChooser::addAndEdit()
{
    m_editStatus = true;
    accept();
}

void StatusEffectChooser::close()
{
    m_cancelled = true;
    accept();
}

