#include "texteditorwidget.h"
#include "ui_texteditorwidget.h"

TextEditorWidget::TextEditorWidget(QsciScintilla *editor, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TextEditorWidget),
    m_editor( editor )
{
    ui->setupUi(this);

    buildInterface();
    initEvents();
}

TextEditorWidget::~TextEditorWidget()
{
    delete ui;
}
QsciScintilla *TextEditorWidget::editor() const
{
    return m_editor;
}

void TextEditorWidget::setEditor(QsciScintilla *editor)
{
    m_editor = editor;
}
QTextEdit *TextEditorWidget::console() const
{
    return m_console;
}

void TextEditorWidget::toggleConsole()
{
    bool visible = m_console->isVisible();
    m_console->setVisible( !visible );
}

void TextEditorWidget::setConsoleEnabled(bool enabled)
{
    m_console->setVisible( enabled );
}

void TextEditorWidget::buildInterface()
{
    m_layout = new QVBoxLayout;

    m_widgetSplitter = new QSplitter( Qt::Vertical, this );

    m_console = new QTextBrowser( this );

    m_widgetSplitter->addWidget( m_editor );
    m_widgetSplitter->addWidget( m_console );

    m_widgetSplitter->setStretchFactor( 0, 3 );

    m_layout->setMargin( 0 );

    m_layout->addWidget( m_widgetSplitter );

    setLayout( m_layout );
}

void TextEditorWidget::initEvents()
{

}
