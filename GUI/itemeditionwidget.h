#ifndef ITEMEDITIONWIDGET_H
#define ITEMEDITIONWIDGET_H

#include <QWidget>
#include <QMenu>
#include <QMessageBox>
#include <QFileDialog>
#include <QStringList>
#include <QVector>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QVariant>
#include "../Core/assetsmanager.h"
#include "../Starbound/statuseffect.h"
#include "statuseffectchooser.h"
#include "jsonstatuseffecteditiondialog.h"
#include "blueprintchooserdialog.h"

namespace Ui {
class ItemEditionWidget;
}

class ItemEditionWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit ItemEditionWidget( AssetsManager *assetsManager, QWidget *parent = 0);
    ~ItemEditionWidget();

    QString getItemName() const;
    void setItemName( QString name );

    QString getItemId() const;
    void setItemId( QString id );

    QString getItemDescription() const;
    void setItemDescription( QString description );

    QString getFloranDescription() const;
    void setFloranDescription( QString description );

    QString getApexDescription() const;
    void setApexDescription( QString description );

    QString getAvianDescription() const;
    void setAvianDescription( QString description );

    QString getGlitchDescription() const;
    void setGlitchDescription( QString description );

    QString getHumanDescription() const;
    void setHumanDescription( QString description );

    QString getHylotlDescription() const;
    void setHylotlDescription( QString description );

    QString getItemRarity() const;
    void setItemRarity( QString rarity );

    int getMaxStack() const;
    void setMaxStack( const int stack );

    QString getInventoryIconPath() const;
    void setInventoryIconPath( QString path );

    int getFuelAmount() const;
    void setFuelAmount( int amount );

    QStringList getBlueprintsLearned();
    void setBlueprintsLearned( QStringList bp );

    QList< JsonStatusEffect > getStatusEffect();
    void setStatusEffect( QList <JsonStatusEffect> effects );

    QStringList verifyFields();

    void removeStatusEffect( QModelIndex index );

    bool effectExists( QString name );
    JsonStatusEffect getEffectByName( QString name );
    
public slots:
    void onNameChanged( QString name );
    void onBrowse();
    void onAddBlueprint();
    void onAddEffect();
    void onRowSelected( QModelIndex index );
    void onBlueprintRowSelected( QModelIndex index );
    void onRemove();
    void onRemoveBlueprint();
    void onStatusEffectClicked( QModelIndex idx );
    void onStatusContextMenu( QPoint pt );
    void onEditStatus();
    void onEditStatus( QModelIndex idx );
signals:
    void itemIdChanged( QString );
private:
    void initEvents();
    bool isSymbol( QChar ch );
    bool addStatusEffect( JsonStatusEffect eff );
private:
    Ui::ItemEditionWidget *ui;
    AssetsManager *m_assetsManager;
    QVector<QChar> m_symbols;
    QStandardItemModel *m_statusEffectModel;
    QList< JsonStatusEffect > m_statusEffects;
};

#endif // ITEMEDITIONWIDGET_H
