#ifndef SUBWINDOW_H
#define SUBWINDOW_H

#include <QMdiSubWindow>
#include <QCloseEvent>
#include <QActionGroup>


class SubWindow : public QMdiSubWindow
{
    Q_OBJECT
public:
    explicit SubWindow( QWidget * parent = 0, Qt::WindowFlags flags = 0);
    
    void setActionGroup( QActionGroup *group );
    QActionGroup *actionGroup() const;
signals:
    void aboutToClose( QCloseEvent *ce );
protected:
    void closeEvent(QCloseEvent *closeEvent);
private:
    QActionGroup *m_tools;
    
};

#endif // SUBWINDOW_H
