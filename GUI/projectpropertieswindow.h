#ifndef NEWPROJECTWINDOW_H
#define NEWPROJECTWINDOW_H

#include <QDialog>
#include <QMessageBox>
#include <QDir>
#include <QFileInfo>
#include <QDebug>
#include <QStandardPaths>

namespace Ui {
class ProjectPropertiesWindow;
}

class ProjectPropertiesWindow : public QDialog
{
    Q_OBJECT
    
public:
    explicit ProjectPropertiesWindow( QString title, QString workspace = "", QWidget *parent = 0, bool createProject = false );
    ~ProjectPropertiesWindow();

    void setProjectName( const QString name );
    QString getProjectName();
    void setProjectAuthor( const QString author );
    QString getProjectAuthor();
    void setProjectPath( const QString path );
    QString getProjectPath();
    void setProjectVersion( const QString version );
    QString getProjectVersion();

    void setStarboundVersion( const QString version );
    QString getStarboundVersion();

public slots:
    void onValidate();
    void onCancel();
    void onNameChanged( QString n );
    void onChange();
    //void onBrowseRequested();
private:
    void initEvents();
    bool verifyFields();
    bool projectExists( QString path );
private:
    Ui::ProjectPropertiesWindow *ui;
    bool m_modified;
    QString m_workspace;
    bool m_createProject;
};

#endif // NEWPROJECTWINDOW_H
