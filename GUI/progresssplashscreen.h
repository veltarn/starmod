#ifndef PROGRESSSPLASHSCREEN_H
#define PROGRESSSPLASHSCREEN_H

#include <cmath>
#include <QDebug>
#include <QFile>
#include <QSplashScreen>
#include <QPainter>
#include <QBrush>
#include <QPaintEvent>
#include <QStyleOptionProgressBarV2>
#include <QPalette>
#include <QLinearGradient>
#include <QPixmap>
#include <QProgressBar>
#include "../Core/stylesheetmanager.h"

class ProgressSplashScreen : public QSplashScreen
{
    Q_OBJECT
public:
    explicit ProgressSplashScreen( const QPixmap &pixmap );
    virtual ~ProgressSplashScreen();



    int getMinimum() const;
    void setMinimum(int value);

    int getMaximum() const;
    void setMaximum(int value);

    int value() const;
    void setValue(int value  );
    void increaseValue( int step = 1 );

signals:

public slots:

protected:
    void drawContents( QPainter *painter );

private:
    int m_minimum;
    int m_maximum;
    int m_value;
    QProgressBar *m_progression;
};

#endif // PROGRESSSPLASHSCREEN_H
