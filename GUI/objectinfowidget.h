#ifndef OBJECTINFOWIDGET_H
#define OBJECTINFOWIDGET_H

#include <QDialog>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QScrollArea>
#include <QGroupBox>
#include <QToolBox>
#include "colorpreviewwidget.h"
#include "../Starbound/Items/staritem.h"
#include "../Starbound/Items/starconsumable.h"
#include "../Starbound/Items/starweapon.h"
#include "../Starbound/Items/starminingtool.h"
#include "../Starbound/Items/starflashlight.h"
#include "../Starbound/Items/starbeamaxe.h"
#include "../Starbound/Items/starmatitem.h"
#include "../Starbound/Items/starinstrument.h"
#include "../Starbound/Items/starsword.h"
#include "../Starbound/Items/starthrown.h"
#include "../Starbound/Items/starworldobject.h"
#include "../Starbound/Items/starcoin.h"

class ObjectInfoWidget : public QDialog
{
    Q_OBJECT
public:
    explicit ObjectInfoWidget( StarItem *item, QWidget *parent = 0);
    
signals:
    
public slots:

private:
    QLabel *createLabel( QString labelName, QVariant value );
    /**
      * Convert a QPoint in QString with X and Y indications
      */
    QString getPointInfo( QPoint point );
    QString getPointInfo( QPointF point );

    QString getStatusEffects( QList<StatusEffect> &list );

    void createWidget();

    void createItemWidget( QToolBox *stackedWidget );
    void createConsumableWidget( QToolBox *stackedWidget );
    void createWeaponWidget( QToolBox *stackedWidget );
    void createMiningToolWidget( QToolBox *toolBox );
    void createFlashLightWidget( QToolBox *toolBox );
    void createBeamaxeWidget( QToolBox *toolBox );
    void createMatItemWidget( QToolBox *toolBox );
    void createInstrumentWidget( QToolBox *toolBox );
    void createSwordWidget( QToolBox *toolBox );
    void createPrimaryStancesWidget( QToolBox *toolBox );
    void createThrownWidget( QToolBox *toolBox );
    void createStarWorldObjectWidget( QToolBox *toolBox );
    void createStarCoinWidget( QToolBox *toolBox );
private:
    StarItem *m_item;
    
};

#endif // OBJECTINFOWIDGET_H
