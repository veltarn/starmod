#include "loggerdialog.h"
#include "ui_loggerdialog.h"

LoggerDialog::LoggerDialog( Logger *log, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoggerDialog)
{
    ui->setupUi(this);
    setWindowTitle( tr( "Console" ) );
    if( log != NULL ) {
        LogMessageList list = log->log();
        setLog( list );
    }

    initEvents();
}

LoggerDialog::~LoggerDialog()
{
    delete ui;
}

void LoggerDialog::initEvents() {
    connect( ui->closeButton, SIGNAL( clicked() ), this, SLOT( accept() ) );
}

void LoggerDialog::setLog( LogMessageList &logs ) {
    for( LogMessageList::iterator it = logs.begin(); it != logs.end(); ++it ) {
        LogMessage tmp = *it;

        QString message;

        switch( tmp.msgType ) {
            case Info:
                message += "<font color='lightblue'><em>[Info]</em> ";
            break;

            case Debug:
                message += "[Debug] ";
            break;

            case Warning:
                message += "<font color='orange'><strong>[Warning]</strong> ";
            break;

            case Error:
                message += "<font color='red'><strong>[Error]</strong> ";
            break;
        }

        message += tmp.message + "</font>";

        ui->log->append( message );
    }
}
