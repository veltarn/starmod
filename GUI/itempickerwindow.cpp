#include "itempickerwindow.h"
#include "ui_itempickerwindow.h"

ItemPickerWindow::ItemPickerWindow( AssetsManager *assetsManager, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ItemPickerWindow),
    m_assetsManager( assetsManager ),
    m_objectViewer( NULL ),
    m_cancelled( false )
{
    ui->setupUi(this);
    setWindowTitle( tr( "Pick an item" ) );

    buildInterface();
    initEvents();
}

ItemPickerWindow::~ItemPickerWindow()
{
    delete ui;
}

StarItem *ItemPickerWindow::getSelectedItem()
{
    if( m_objectViewer != NULL ) {
        return m_objectViewer->getSelectedItem();
    } else {
        return NULL;
    }
}

void ItemPickerWindow::validate()
{
    m_cancelled = false;
    accept();
}

void ItemPickerWindow::cancel()
{
    m_cancelled = true;
    accept();
}

void ItemPickerWindow::buildInterface()
{
    QApplication::setOverrideCursor( Qt::BusyCursor );

    m_mainLayout = new QHBoxLayout;
    m_sideLayout = new QVBoxLayout;

    m_objectViewer = new ObjectViewWidget( m_assetsManager, this );
    m_validateButton = new QPushButton( tr( "Ok" ) );
    m_cancelButton = new QPushButton( tr( "Cancel" ) );

    m_sideLayout->addWidget( m_validateButton );
    m_sideLayout->addWidget( m_cancelButton );
    //m_sideLayout->addSpacing( 300 );

    m_mainLayout->addWidget( m_objectViewer, 4 );
    m_mainLayout->addLayout( m_sideLayout, 1 );

    m_mainLayout->setMargin( 3 );

    setLayout( m_mainLayout );

    m_objectViewer->objectsView()->setModel( m_assetsManager->createObjectsModel() );

    QApplication::restoreOverrideCursor();
}

void ItemPickerWindow::initEvents()
{
    connect( m_validateButton, SIGNAL( clicked() ), this, SLOT( validate() ) );
    connect( m_cancelButton, SIGNAL( clicked() ), this, SLOT( cancel() ) );
}
bool ItemPickerWindow::cancelled() const
{
    return m_cancelled;
}

void ItemPickerWindow::setCancelled(bool cancelled)
{
    m_cancelled = cancelled;
}

void ItemPickerWindow::addWidgetToSide( QString textLabel, QWidget *widget)
{
    if( widget != NULL ) {
        QVBoxLayout *widgetLayout = new QVBoxLayout;

        QLabel *label = new QLabel( textLabel, this );

        widgetLayout->setMargin( 0 );
        widgetLayout->addWidget( label, 1 );
        widgetLayout->addWidget( widget, 4 );

        m_sideLayout->addLayout( widgetLayout, 1 );
    }
}

void ItemPickerWindow::selectItem(QString name)
{
    m_objectViewer->selectItem( name );
}

