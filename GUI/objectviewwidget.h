#ifndef OBJECTVIEWWIDGET_H
#define OBJECTVIEWWIDGET_H

#include <QDebug>
#include <QWidget>
#include <QLabel>
#include <QMenu>
#include <QAction>
#include <QString>
#include <QStringList>
#include <QSortFilterProxyModel>
#include <QListView>
#include <QPoint>
#include "../Starbound/Items/staritem.h"
#include "objectinfowidget.h"
#include "../Core/assetsmanager.h"

namespace Ui {
class ObjectViewWidget;
}

class ObjectViewWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ObjectViewWidget(AssetsManager *assetsManager, QWidget *parent = 0);
    ~ObjectViewWidget();

    QListView *objectsView() const;

    void updateObjectNumberLabel();
    void updateObjectNumberLabel( QSortFilterProxyModel *model );

    StarItem *getSelectedItem();
    void selectItem( QString name );
public slots:
    void onObjectSearch( QString text );
    void onObjectClicked( /*QModelIndex index*/ );
    void onObjectClicked( QModelIndex index );
    void onCategoryChange( int index );
    void customContextMenu( QPoint pt );
private:
    void initEvents();
    void buildInterface();
    void createInfoWindow( StarItem *item );

private:
    Ui::ObjectViewWidget *ui;
    ObjectInfoWidget *m_infosObject;
    AssetsManager *m_assetsManager;

    QModelIndex m_currentIndex;
};

#endif // OBJECTVIEWWIDGET_H
