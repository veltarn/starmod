#ifndef ACTIONONREAPWIDGET_H
#define ACTIONONREAPWIDGET_H

#include <QWidget>
#include <QFileDialog>
#include "../Core/assetsmanager.h"
#include "../Starbound/starprojectile.h"
#include "../Core/projectilesutility.h"

namespace Ui {
class ActionOnReapWidget;
}

class ActionOnReapWidget : public QWidget
{
    Q_OBJECT
public:


public:
    explicit ActionOnReapWidget( AssetsManager *assetsManager, QString projectPath, bool createAction = true, QWidget *parent = 0);
    ~ActionOnReapWidget();

    /**
     * @brief populateForm Used in case of edition
     * @param aor Existing actionOnReap
     */
    void populateForm( ActionOnReap &aor );

    int actionType(); // ActionType enum
    int delaySteps();

    //Projectiles
    QString projectileType(); //!< Name of the projectile
    double inheritDamageFactor();
    int angle();
    int adjustAngle();
    int fuzzAngle();

    //Liquid
    int liquidId(); // LiquidId enum
    int quantity();

    //Explosions
    int foregroundRadius();
    int backgroundRadius();
    int explosiveDamageAmount();

    //Light
    //QString color();

    //Sounds
    QStringList sounds();

    //Config
    QString config();

    bool validate();
    QStringList lastValidationErrorsList() const;

    bool createAction() const;

public slots:
    void onWidgetChanged( int index );
    void onActionTypeChanged( int idx );
    void onBrowseSound();
    void onSoundItemSelected( QModelIndex index );
    void onRemoveSound();
    void onBrowseConfig();
private:
    void buildInterface();
    void initEvents();
private:
    Ui::ActionOnReapWidget *ui;
    AssetsManager *m_assetsManager;
    QString m_projectPath;

    QStringList m_lastValidationErrorsList;
    bool m_createAction;
};

#endif // ACTIONONREAPWIDGET_H
