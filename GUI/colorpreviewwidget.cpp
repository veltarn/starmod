#include "colorpreviewwidget.h"

ColorPreviewWidget::ColorPreviewWidget(QColor color, QWidget *parent) :
    QWidget(parent),
    m_color( color )
{
    QString msg =  "R: " + QString::number( m_color.red() ) + " G: " + QString::number( m_color.green() ) + " B: " + QString::number( m_color.blue() );
    if( m_color.alpha() != 255 )
        msg += " A: " + QString::number( m_color.alpha() );

    setToolTip( msg );
}

void ColorPreviewWidget::paintEvent(QPaintEvent *pe)
{
    Q_UNUSED( pe );
    QPainter painter( this );

    painter.setBrush( QBrush( m_color ) );
    painter.setPen( Qt::NoPen );
    painter.drawRect( 0, 0, width(), height() );
}
