#include "projectileeditionwindow.h"
#include "ui_projectileeditionwindow.h"

ProjectileEditionWindow::ProjectileEditionWindow( AssetsManager *assetsManager, QString modPath, bool createProjectile, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProjectileEditionWindow),
    m_projectileEditionWidget( NULL ),
    m_assetsManager( assetsManager ),
    m_modPath( modPath ),
    m_createProjectile( createProjectile )
{
    ui->setupUi(this);
    setWindowIcon( QIcon( QPixmap( "assets/images/bullet.png" ) ) );
    buildInterface();
    initEvents();

    if( createProjectile )
        setWindowTitle( tr( "New projectile" ) );
    else
        setWindowTitle( tr( "Projectile edition" ) );
}

ProjectileEditionWindow::~ProjectileEditionWindow()
{
    delete ui;
}

void ProjectileEditionWindow::onValidate() {
    if( m_projectileEditionWidget->validate() ) {
        m_projectileEditionWidget->createProjectile();
        accept();
    } else {
        QMessageBox::warning( this, tr( "Validation error" ), m_projectileEditionWidget->lastValidationError() );
    }
}

void ProjectileEditionWindow::onClose() {
    accept();
}

void ProjectileEditionWindow::initEvents() {
    connect( ui->closeButton, SIGNAL( clicked() ), this, SLOT( onClose() ) );
    connect( ui->validateButton, SIGNAL( clicked() ), this, SLOT( onValidate() ) );
}

void ProjectileEditionWindow::buildInterface() {
    m_projectileEditionWidget = new ProjectileEditionWidget( m_assetsManager, m_modPath, this );

    ui->verticalLayout->insertWidget( 0, m_projectileEditionWidget, 4 );
}
