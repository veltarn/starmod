#include "projectilelistwindow.h"
#include "ui_projectilelistwindow.h"

ProjectileListWindow::ProjectileListWindow(AssetsManager *assetsManager, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProjectileListWindow),
    m_assetsManager( assetsManager )
{
    ui->setupUi(this);

    m_projectilesList = new QTreeView( this );
    m_model = new QStandardItemModel( this );

    QStringList headers;
    headers << tr( "Projectile name" )
            << tr( "Value" );

    m_model->setHorizontalHeaderLabels( headers );
    m_projectilesList->setModel( m_model );

    buildInterface();
    initEvents();
    createProjectilesList();
}

ProjectileListWindow::~ProjectileListWindow()
{
    delete ui;
}

void ProjectileListWindow::buildInterface()
{
    m_closeButton = new QPushButton( tr( "Close" ), this );
    m_toolbar = new QToolBar( tr( "Projectile menu" ), this );
    m_addAction = new QAction( QIcon( QPixmap( "assets/images/add.png" ) ), tr( "Add projectile" ), this );
    m_editAction = new QAction( QIcon( QPixmap( "assets/images/edit.png" ) ), tr( "Edit projectile" ), this );
    m_removeAction = new QAction( QIcon( QPixmap( "assets/images/remove.png" ) ), tr( "Remove projectile" ), this );

    m_editAction->setEnabled( false );
    m_removeAction->setEnabled( false );

    m_toolbar->addAction( m_addAction );
    m_toolbar->addAction( m_editAction );
    m_toolbar->addAction( m_removeAction );

    m_buttonLayout = new QHBoxLayout;
    m_layout = new QVBoxLayout;

    /*QSpacerItem *space = new QSpacerItem( 150, 0, QSizePolicy::Maximum );
    m_buttonLayout->addSpacerItem( space );*/
    m_buttonLayout->addStretch( 4 );
    m_buttonLayout->addWidget( m_closeButton );

    m_layout->addWidget( m_toolbar );
    m_layout->addWidget( m_projectilesList, 4 );
    m_layout->addLayout( m_buttonLayout );

    setLayout( m_layout );
}

void ProjectileListWindow::initEvents()
{
    connect( m_closeButton, SIGNAL( clicked() ), this, SLOT( accept() ) );
}

void ProjectileListWindow::createProjectilesList()
{
    foreach( AssetPtr ptr, m_assetsManager->assetsList() ) {
        if( ptr->fileType() == Projectile ) {
            StarProjectile *proj = ptr->asStarProjectile();

            QStandardItem *projName = new QStandardItem( proj->projectileName() );
            projName->setIcon( QIcon( QPixmap( proj->damageKindImage() ) ) );

            QStandardItem *mainProperties = new QStandardItem( tr( "Main properties" ) );
            QStandardItem *ttlTitle = new QStandardItem( tr( "Time To Live" ) );
            QStandardItem *timeToLive = new QStandardItem( QString::number( proj->timeToLive() ) );

            QStandardItem *levelTitle = new QStandardItem( tr( "Level" ) );
            QStandardItem *level = new QStandardItem( QString::number( proj->level() ) );

            QStandardItem *hydrophobicTitle = new QStandardItem( tr( "Hydrophobic" ) );
            QStandardItem *hydrophobic = new QStandardItem( QVariant( proj->hydrophobic() ).toString() );

            QList<QStandardItem*> itemList = getPropertyPair( ttlTitle, timeToLive );

            mainProperties->appendRow( itemList );

            itemList.clear();
            itemList = getPropertyPair( levelTitle, level );

            mainProperties->appendRow( itemList );

            itemList.clear();
            itemList = getPropertyPair( hydrophobicTitle, hydrophobic );
            mainProperties->appendRow( itemList );

            //Animation
            QStandardItem *animProperties = new QStandardItem( tr( "Animation" ) );

            QStandardItem *framePathTitle = new QStandardItem( tr( "Frame" ) );
            QStandardItem *frame = new QStandardItem( QFileInfo( proj->framePath() ).baseName() );

            QStandardItem *frameNumberTitle = new QStandardItem( tr( "Frame number" ) );
            QStandardItem *frameNumber = new QStandardItem( QString::number( proj->frameNumber() ) );

            QStandardItem *animationCycleTitle = new QStandardItem( tr( "Animation Cycle" ) );
            QStandardItem *animationCycle = new QStandardItem( QString::number( proj->animationCycle() ) );

            QStandardItem *animationLoopTitle = new QStandardItem( tr( "Animation Loop" ) );
            QStandardItem *animationLoop = new QStandardItem( QVariant( proj->animationLoop() ).toString() );

            itemList.clear();
            itemList = getPropertyPair( framePathTitle, frame );

            animProperties->appendRow( itemList );

            itemList.clear();
            itemList = getPropertyPair( frameNumberTitle, frameNumber );

            animProperties->appendRow( itemList );

            itemList.clear();
            itemList = getPropertyPair( animationCycleTitle, animationCycle );

            animProperties->appendRow( itemList );

            itemList.clear();
            itemList = getPropertyPair( animationLoopTitle, animationLoop );

            animProperties->appendRow( itemList );

            //Damage
            QStandardItem *damageProperties = new QStandardItem( tr( "Damage" ) );

            QStandardItem *damageTypeTitle = new QStandardItem( tr( "Damage Type" ) );
            QStandardItem *damageType = new QStandardItem( proj->damageType() );

            QStandardItem *damageKindTitle = new QStandardItem( tr( "Damage Kind" ) );
            QStandardItem *damageKind = new QStandardItem( proj->damageKind() );

            QStandardItem *powerTitle = new QStandardItem( tr( "Power" ) );
            QStandardItem *power = new QStandardItem( proj->power() );

            itemList.clear();
            itemList = getPropertyPair( damageTypeTitle, damageType );

            damageProperties->appendRow( itemList );

            itemList.clear();
            itemList = getPropertyPair( damageKindTitle, damageKind );

            damageProperties->appendRow( itemList );

            itemList.clear();
            itemList = getPropertyPair( powerTitle, power );

            damageProperties->appendColumn( itemList );

            //Physics
            QStandardItem *physicsProperties = new QStandardItem( tr( "Physics" ) );

            QStandardItem *physicsTitle = new QStandardItem( tr( "Physics Type" ) );
            QStandardItem *physics = new QStandardItem( proj->physics() );

            QStandardItem *bouncesTitle = new QStandardItem( tr( "Bounces" ) );
            QStandardItem *bounces = new QStandardItem( QString::number( proj->bounces() ) );

            QStandardItem *damagePolyTitle = new QStandardItem( tr( "Damage Poly" ) );
            QStandardItem *damagePoly = new QStandardItem( "to do" );

            QStandardItem *fallSpeedTitle = new QStandardItem( tr( "Fall Speed" ) );
            QStandardItem *fallSpeed = new QStandardItem( QString::number( proj->fallSpeed() ) );

            QStandardItem *knockbackPowerTitle = new QStandardItem( tr( "Knockback Power" ) );
            QStandardItem *knockbackPower = new QStandardItem( QString::number( proj->knockbackPower() ) );

            QStandardItem *speedTitle = new QStandardItem( tr( "Speed" ) );
            QStandardItem *speed = new QStandardItem( QString::number( proj->speed() ) );

            QStandardItem *initialVelocityTitle = new QStandardItem( tr( "Initial Velocity" ) );
            QStandardItem *initialVelocity = new QStandardItem( QString::number( proj->initialVelocity() ) );

            itemList.clear();
            itemList = getPropertyPair( physicsTitle, physics );

            physicsProperties->appendRow( itemList );

            itemList.clear();
            itemList = getPropertyPair( bouncesTitle, bounces );

            physicsProperties->appendRow( itemList );

            itemList.clear();
            itemList = getPropertyPair( damagePolyTitle, damagePoly );

            physicsProperties->appendRow( itemList );

            itemList.clear();
            itemList = getPropertyPair( fallSpeedTitle, fallSpeed );

            physicsProperties->appendRow( itemList );

            itemList.clear();
            itemList = getPropertyPair( knockbackPowerTitle, knockbackPower );

            physicsProperties->appendRow( itemList );

            itemList.clear();
            itemList = getPropertyPair( speedTitle, speed );

            physicsProperties->appendRow( itemList );

            itemList.clear();
            itemList = getPropertyPair( initialVelocityTitle, initialVelocity );

            physicsProperties->appendRow( itemList );

            //Light
            QStandardItem *lightProperties = new QStandardItem( tr( "Light" ) );

            QStandardItem *pointLightTitle = new QStandardItem( tr( "Point light" ) );
            QStandardItem *pointLight = new QStandardItem( QVariant( proj->pointLight() ).toString() );

            QStandardItem *lightColorTitle = new QStandardItem( tr( "Light Color" ) );
            QStandardItem *lightColor = new QStandardItem;
            lightColor->setBackground( QBrush( QColor( proj->lightColor() ) ) );
            lightColor->setToolTip( Utility::qColorToQString( proj->lightColor() ) );

            itemList.clear();
            itemList = getPropertyPair( pointLightTitle, pointLight );

            lightProperties->appendRow( itemList );

            itemList.clear();
            itemList = getPropertyPair( lightColorTitle, lightColor );

            lightProperties->appendRow( itemList );

            //StatusEffects
            QStandardItem *statusEffects = new QStandardItem( tr( "Status Effects" ) );

            foreach( JsonStatusEffect effect, proj->effects() ) {
                QStandardItem *effectTitle = new QStandardItem( effect.kind );

                statusEffects->appendRow( effectTitle );
            }

            //Emitters
            QStandardItem *emitters = new QStandardItem( tr( "Emitters" ) );

            foreach( QString emitter, proj->emitters() ) {
                QStandardItem *emitterItem = new QStandardItem( emitter );

                emitters->appendRow( emitterItem );
            }

            //ActionOnReap
            QStandardItem *actionOnReap = new QStandardItem( tr( "Action On Reap (not yet)" ) );


            projName->appendRow( mainProperties );
            projName->appendRow( animProperties );
            projName->appendRow( damageProperties);
            projName->appendRow( physicsProperties );
            projName->appendRow( lightProperties );
            projName->appendRow( statusEffects );
            projName->appendRow( emitters );
            projName->appendRow( actionOnReap );

            m_model->appendRow( projName );
        }
    }
}

QList<QStandardItem *> ProjectileListWindow::getPropertyPair(QStandardItem *title, QStandardItem *value)
{
    QList< QStandardItem *> list;
    list.append( title );
    list.append( value );
    return list;
}
