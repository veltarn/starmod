#ifndef RECIPESGROUPSPICKER_H
#define RECIPESGROUPSPICKER_H

#include <QDialog>
#include <QString>
#include <QStringList>
#include "../Core/assetsmanager.h"
#include "../Core/cache.h"

namespace Ui {
class RecipesGroupsPicker;
}

class RecipesGroupsPicker : public QDialog
{
    Q_OBJECT

public:
    explicit RecipesGroupsPicker( AssetsManager *assetsManager, QWidget *parent = 0);
    ~RecipesGroupsPicker();

    QString getSelectedGroup();
public slots:
    void validate();
private:
    void buildInterface();
    void initEvents();
private:
    Ui::RecipesGroupsPicker *ui;
    AssetsManager *m_assetsManager;
};

#endif // RECIPESGROUPSPICKER_H
