#include "swordeditionwindow.h"
#include "ui_swordeditionwindow.h"

SwordEditionWindow::SwordEditionWindow( AssetsManager *assetsManager, QString projectFilesPath, bool createSword, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SwordEditionWindow),
    m_assetsManager( assetsManager ),
    m_projectFilesPath( projectFilesPath ),
    m_createSword( createSword )
{
    ui->setupUi(this);

    if( m_createSword )
        setWindowTitle( tr( "Create New Sword" ) );
    else
        setWindowTitle( tr( "Editing Sword" ) );

    buildInterface();
    initEvents();
}

SwordEditionWindow::~SwordEditionWindow()
{
    delete ui;
}

void SwordEditionWindow::onClose()
{
    accept();
}

void SwordEditionWindow::onOpenWindupStatusEffectList()
{
    openStatusEffectList( m_windupStatusEffectsList );
    //Traitement
}

void SwordEditionWindow::onOpenCooldownStatusEffectList()
{
    openStatusEffectList( m_cooldownStatusEffectsList );
    //Traitement
}

void SwordEditionWindow::buildInterface()
{
    m_itemEditionWidget = new ItemEditionWidget( m_assetsManager, this );
    m_recipeWidget = new RecipesWidget( m_assetsManager, this );

    ui->mainProperties->layout()->addWidget( m_itemEditionWidget );
    ui->recipesTab->layout()->addWidget( m_recipeWidget );

    ProjectilesUtility::loadProjectilesList( ui->projectile_projectileTypeComboBox, m_assetsManager->assetsPath(), m_projectFilesPath );
}

void SwordEditionWindow::initEvents()
{
    connect( ui->closeButton, SIGNAL( clicked() ), this, SLOT( onClose() ) );
    connect( ui->windupOpenStatusEffectList, SIGNAL( clicked() ), this, SLOT( onOpenWindupStatusEffectList() ) );
    connect( ui->cooldownOpenStatusEffectList, SIGNAL( clicked() ), this, SLOT( onOpenCooldownStatusEffectList() ) );
}

void SwordEditionWindow::openStatusEffectList(QList<StatusEffect> &statusEffectList)
{
    StatusEffectsListWindow selw( statusEffectList, m_assetsManager, this );
    selw.exec();
}
