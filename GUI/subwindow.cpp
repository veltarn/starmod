#include "subwindow.h"

SubWindow::SubWindow( QWidget *parent, Qt::WindowFlags flags) :
    QMdiSubWindow(parent, flags),
    m_tools( NULL )
{
}

void SubWindow::closeEvent(QCloseEvent *closeEvent) {
    emit aboutToClose( closeEvent );
    closeEvent->accept();
}

void SubWindow::setActionGroup( QActionGroup *group ) {
    m_tools = group;
}

QActionGroup *SubWindow::actionGroup() const {
    return m_tools;
}
