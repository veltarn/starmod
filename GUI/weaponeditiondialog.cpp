#include "weaponeditiondialog.h"
#include "ui_weaponeditiondialog.h"

WeaponEditionDialog::WeaponEditionDialog( AssetsManager *assetsManager, QString projectPath, bool createWeapon, QWidget *parent ) :
    QDialog(parent),
    ui(new Ui::WeaponEditionDialog),
    m_itemEditionWidget( NULL ),
    m_createWeapon( createWeapon ),
    m_projectFilesPath( projectPath ),
    m_assetsManager( assetsManager )
{
    ui->setupUi(this);

    if( m_createWeapon ) {
        setWindowTitle( tr( "New Weapon" ) );
    } else {
        setWindowTitle( tr( "Weapon edition" ) );
    }
    setWindowIcon( QIcon( QPixmap( "assets/images/weapon.png" ) ) );
    //ui->projectileTab->setEnabled( false );
    buildInterface();
    initEvents();

    loadProjectilesList();
}

WeaponEditionDialog::~WeaponEditionDialog()
{
    delete ui;
}

void WeaponEditionDialog::buildInterface() {
    //QVBoxLayout *mainGeneralLayout = new QVBoxLayout;
    m_itemEditionWidget = new ItemEditionWidget( m_assetsManager, this );
    m_recipesWidget = new RecipesWidget( m_assetsManager, this );


    //mainGeneralLayout->addWidget( m_itemEditionWidget );

    ui->mainProperties->layout()->addWidget( m_itemEditionWidget );
    ui->recipesTab->layout()->addWidget( m_recipesWidget );
}


void WeaponEditionDialog::initEvents() {
    connect( ui->createButton, SIGNAL( clicked() ), this, SLOT( onValidate() ) );
    connect( ui->closeButton, SIGNAL( clicked() ), this, SLOT( onClose() ) );

    connect( ui->creatProjectileBtn, SIGNAL( clicked() ), this, SLOT( onCreateProjectileClicked() ) );
    connect( ui->editProjectile, SIGNAL( clicked() ), this, SLOT( onEditProjectileClicked() ) );

    connect( ui->browseSoundBtn, SIGNAL( clicked() ), this, SLOT( onFireSoundBrowse() ) );
    connect( ui->browseImageBtn, SIGNAL( clicked() ), this, SLOT( onImageBrowse() ) );    
    //connect( ui->hasProjectileCheckBox, SIGNAL( toggled(bool) ), this, SLOT( onProjectileChecked( bool ) ) );
    connect( ui->chooseColorButton, SIGNAL( clicked() ), this, SLOT( onProjectileColorChecked() ) );
    connect( m_itemEditionWidget, SIGNAL( itemIdChanged(QString) ), m_recipesWidget, SLOT( setOutputName(QString) ) );
}

void WeaponEditionDialog::loadProjectilesList() {
    QString gunProjectilesPath = m_assetsManager->assetsPath() + "/projectiles/guns";
    QString projectGunProjectilesPath = m_projectFilesPath + "/projectiles/guns";

    ui->projectileTypeComboBox->addItem( "-", "" );

    QStringList filesList;
    Utility::recurseAddDir( QDir( projectGunProjectilesPath ), filesList, QStringList( "projectile" ) );
    bool hasFiles = ( filesList.size() > 0 );

    foreach( QString file, filesList ) {
        if( !addToComboboxFromFile( file ) )
            continue;
    }

    if( hasFiles ) {
        ui->projectileTypeComboBox->insertSeparator( filesList.size() );
    }

    filesList.clear();
    Utility::recurseAddDir( QDir( gunProjectilesPath ), filesList, QStringList( "projectile" ) );
    foreach( QString file, filesList ) {
        if( !addToComboboxFromFile( file ) )
            continue;
    }
}

bool WeaponEditionDialog::addToComboboxFromFile( QString file ) {
    QFileInfo fifo( file );
    QFile projFile( file );
    if( !projFile.open( QIODevice::ReadOnly ) ) {
        qDebug() << "Cannot open " << projFile.fileName() << endl;
        return false;
    }

    QJsonDocument doc = QJsonDocument::fromJson( projFile.readAll() );
    QJsonObject root = doc.object();

    QJsonValue projName = root[ "projectileName" ];

    if( projName.isUndefined() ) {
        qWarning() << "projectileName property does not exists in file " << file;
        return false;
    }

    QJsonValue icon = root[ "damageKindImage" ];
    if( !icon.isUndefined() ) {
        QString iconPath = QFileInfo( file ).path() + "/" + icon.toString();
        ui->projectileTypeComboBox->addItem( QIcon( QPixmap( iconPath ) ), projName.toString(), file );
    } else {
        ui->projectileTypeComboBox->addItem( projName.toString(), file );
    }
    projFile.close();
    return true;
}

void WeaponEditionDialog::onCreateProjectileClicked() {
    ProjectileEditionWindow pew( m_assetsManager, m_projectFilesPath, true, this );
    pew.exec();

    ui->projectileTypeComboBox->clear();
    loadProjectilesList();
}

void WeaponEditionDialog::onEditProjectileClicked() {

}

void WeaponEditionDialog::onValidate() {
    if( validateFields() ) {
        StarWeapon sw;

        sw.setItemName( m_itemEditionWidget->getItemId() );
        sw.setShortDescription( m_itemEditionWidget->getItemName() );
        sw.setDescription( m_itemEditionWidget->getItemDescription() );
        sw.setRarity( m_itemEditionWidget->getItemRarity() );
        sw.setInventoryIcon( m_itemEditionWidget->getInventoryIconPath() );
        sw.setMaxStack( m_itemEditionWidget->getMaxStack() );

        sw.setIsMelee( ui->meleeWeaponCheckBox->isChecked() );
        sw.setFireTime( ui->fireTimeDoubleSpinBox->value() );
        QPoint handPos;
        handPos.setX( ui->xHandPos->value() );
        handPos.setY( ui->yHandPosition->value() );
        sw.setHandPosition( handPos );

        QPoint firePos;
        firePos.setX( ui->xFirePosition->value() );
        firePos.setY( ui->yFirePosition->value() );
        sw.setFirePosition( firePos );

        sw.setTwoHanded( ui->twoHandedCheckBox->isChecked() );
        sw.setLevel( ui->levelSpinBox->value() );
        sw.setEnergyCost( ui->energyCostSpinBox->value() );
        sw.setClassMultiplier( ui->classMultiplierSpinBox->value() );
        sw.setWeaponImagePath( ui->weaponImageLineEdit->text() );
        if( ui->hasProjectileGroupBox->isChecked() ) {
            sw.setProjectileName( ui->projectileTypeComboBox->currentText() );
            sw.setProjectileSpeed( ui->speedSpinBox->value() );
            sw.setProjectilePower( ui->powerDoubleSpinBox->value() );
            sw.setProjectileLifeTime( ui->lifeSpinBox->value() );
            sw.setProjectileColor( m_projectileColor );
        }
        sw.setHasProjectile( ui->hasProjectileGroupBox->isChecked() );

        m_recipesWidget->validate( &sw );

        if( !m_assetsManager->createItem( m_projectFilesPath, &sw ) ) {
            QMessageBox::critical( this, tr( "Error" ), tr( "Cannot create item" ) );
        } else {
            accept();
        }

        accept();
    }
}

void WeaponEditionDialog::onClose() {
    accept();
}

void WeaponEditionDialog::onProjectileChecked( bool checked ) {
    ui->projectileTab->setEnabled( checked );
}

void WeaponEditionDialog::onFireSoundBrowse() {
    QString cpath = ui->fireSoundLineEdit->text();
    QString path = QFileDialog::getOpenFileName( this, tr( "Select a sound" ), cpath, QString( "Wav File( *.wav )" ) );

    if( path != "" ) {
        ui->fireSoundLineEdit->setText( path );
    }
}

void WeaponEditionDialog::onImageBrowse() {
    QString cpath = ui->weaponImageLineEdit->text();
    QString path = QFileDialog::getOpenFileName( this, tr( "Selected a picture of your weapon" ), cpath, QString( "Png File( *.png )" ) );

    if( path != "" ) {
        ui->weaponImageLineEdit->setText( path );
    }
}


bool WeaponEditionDialog::isMelee() const {
    return ui->meleeWeaponCheckBox->isChecked();
}

void WeaponEditionDialog::setIsMelee( bool melee ) {
    ui->meleeWeaponCheckBox->setChecked( melee );
}


bool WeaponEditionDialog::isTwoHanded() const {
    return ui->twoHandedCheckBox->isChecked();
}

void WeaponEditionDialog::setTwoHanded( bool twoHanded ) {
    ui->twoHandedCheckBox->setChecked( twoHanded );
}

bool WeaponEditionDialog::hasProjectile() const {
    //return ui->hasProjectileCheckBox->isChecked();
    return ui->hasProjectileGroupBox->isChecked();
}

void WeaponEditionDialog::setHasProjectile( bool hasProjectile ) {
    //ui->hasProjectileCheckBox->setChecked( hasProjectile );
    ui->hasProjectileGroupBox->setChecked( hasProjectile );
}

int WeaponEditionDialog::getEnergyCost() const {
    return ui->energyCostSpinBox->value();
}

void WeaponEditionDialog::setEnergyCost( int energyCost ) {
    ui->energyCostSpinBox->setValue( energyCost );
}

int WeaponEditionDialog::getClassMultiplier() const {
    return ui->classMultiplierSpinBox->value();
}

void WeaponEditionDialog::setClassMultiplier( int classMultiplier ) {
    ui->classMultiplierSpinBox->setValue( classMultiplier );
}

double WeaponEditionDialog::getFireTime() const {
    return ui->fireTimeDoubleSpinBox->value();
}

void WeaponEditionDialog::setFireTime( double firetime ) {
    ui->fireTimeDoubleSpinBox->setValue( firetime );
}

QString WeaponEditionDialog::getFireSound() const {
    return ui->fireSoundLineEdit->text();
}

void WeaponEditionDialog::setFireSound( QString soundPath ) {
    ui->fireSoundLineEdit->setText( soundPath );
}

QString WeaponEditionDialog::getImagePath() const {
    return ui->weaponImageLineEdit->text();
}

void WeaponEditionDialog::setImagePath( QString path ) {
    ui->weaponImageLineEdit->setText( path );
}

QPointF WeaponEditionDialog::getHandPosition() {
    QPointF pos( ui->xHandPos->value(), ui->yHandPosition->value() );
    return pos;
}

void WeaponEditionDialog::setHandPosition( QPointF handPosition ) {
    ui->xHandPos->setValue( handPosition.x() );
    ui->yHandPosition->setValue( handPosition.y() );
}

QPointF WeaponEditionDialog::getFirePosition() {
    QPointF pos( ui->xFirePosition->value(), ui->yFirePosition->value() );
    return pos;
}

void WeaponEditionDialog::setFirePosition( QPointF firePosition ) {
    ui->xFirePosition->setValue( firePosition.x() );
    ui->yFirePosition->setValue( firePosition.y() );
}

bool WeaponEditionDialog::validateFields() {
    QStringList errorList;
    errorList = m_itemEditionWidget->verifyFields();

    if( ui->weaponImageLineEdit->text() == "" )
        errorList.append( tr( "Weapon image is empty" ) );

    if( ui->hasProjectileGroupBox->isChecked() && ( ui->projectileTypeComboBox->currentText() == "-" || ui->projectileTypeComboBox->currentText() == "" ) )
        errorList.append( tr( "Projectile type is empty" ) );

    if( errorList.size() > 0 ) {
        QString msg = tr( "Cannot create weapon because of the following errors" ) + " :<ul>";

        foreach( QString err, errorList ) {
            msg += "<li>" + err + "</li>";
        }
        msg += "</ul>";

        QMessageBox::warning( this, tr( "Cannot create weapon" ), msg );

        return false;
    } else {
        return true;
    }
    return false;
}

void WeaponEditionDialog::onProjectileColorChecked() {
    QColor color = QColorDialog::getColor();

    m_projectileColor = color;

    QPalette palette;
    palette.setColor( QPalette::ButtonText, color );
    ui->chooseColorButton->setPalette( palette );
}
