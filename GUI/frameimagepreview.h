#ifndef FRAMEIMAGEPREVIEW_H
#define FRAMEIMAGEPREVIEW_H

#include <cmath>
#include <QDebug>
#include <QDialog>
#include <QString>
#include <QBrush>
#include <QPen>
#include <QColor>
#include <QSize>
#include <QRect>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QGraphicsRectItem>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QSpacerItem>

namespace Ui {
class FrameImagePreview;
}

class FrameImagePreview : public QDialog
{
    Q_OBJECT
    
public:
    explicit FrameImagePreview(QWidget *parent = 0);
    ~FrameImagePreview();
    
    void setImage( QString path );
    void setRectangleSize( QSize size );
    void setRectangleSize( int w, int h );
    void setRectangleWidth( int w );
    void setRectangleHeight( int h );

    void moveToRight( int step = 1 );
    void moveToLeft( int step = 1 );

    int getFramesNumberX();
    int getFramesNumberY();
public slots:
    void onLeftButtonClicked();
    void onRightButtonClicked();
    void onClose();

private:
    void buildInterface();
    void initEvents();

private:
    Ui::FrameImagePreview *ui;
    QGraphicsScene *m_scene;
    QGraphicsView *m_view;

    QGraphicsRectItem *m_rectItem;
    QGraphicsPixmapItem *m_item;

    QPushButton *m_leftButton;
    QPushButton *m_rightButton;
    QPushButton *m_upButton;
    QPushButton *m_downButton;
    QPushButton *m_closeButton;

    QVBoxLayout *m_mainLayout;

    QRectF m_previewRectangleRect; // Position of the preview rectangle

};

#endif // FRAMEIMAGEPREVIEW_H
