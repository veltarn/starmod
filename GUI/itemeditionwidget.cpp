#include "itemeditionwidget.h"
#include "ui_itemeditionwidget.h"

ItemEditionWidget::ItemEditionWidget( AssetsManager *assetsManager, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ItemEditionWidget),
    m_assetsManager( assetsManager )
{
    ui->setupUi(this);

    m_symbols.append( '\'' );
    m_symbols.append( '"' );
    m_symbols.append( '(' );
    m_symbols.append( ')' );
    m_symbols.append( '{' );
    m_symbols.append( '}' );
    m_symbols.append( '=' );
    m_symbols.append( '+' );
    m_symbols.append( '*' );
    m_symbols.append( '/' );
    m_symbols.append( '|' );
    m_symbols.append( '-' );
    m_symbols.append( '_' );
    m_symbols.append( '~' );
    m_symbols.append( '&' );

    m_statusEffectModel = new QStandardItemModel( this );

    QStringList horizontalLabels;
    horizontalLabels << tr( "Effect Name" )
                     << tr( "Amount" )
                     << tr( "Range" )
                     << tr( "Percentage" )
                     << tr( "Color" );

    m_statusEffectModel->setHorizontalHeaderLabels( horizontalLabels );

    ui->statusEffectsList->setModel( m_statusEffectModel );
    ui->statusEffectsList->setContextMenuPolicy( Qt::CustomContextMenu );

    initEvents();
}

ItemEditionWidget::~ItemEditionWidget()
{
    delete ui;
}

void ItemEditionWidget::initEvents() {
    connect( ui->nameLineEdit, SIGNAL( textChanged(QString) ), this, SLOT( onNameChanged(QString) ) );
    connect( ui->browseInventoryIconBtn, SIGNAL( clicked() ), this, SLOT( onBrowse() ) );
    connect( ui->addStatusButton, SIGNAL( clicked() ), this, SLOT( onAddEffect() ) );
    connect( ui->statusEffectsList, SIGNAL( clicked(QModelIndex) ), this, SLOT( onRowSelected(QModelIndex) ) );
    connect( ui->removeStatusButton, SIGNAL( clicked() ), this, SLOT( onRemove() ) );
    connect( ui->blueprintAddButton, SIGNAL( clicked() ), this, SLOT( onAddBlueprint() ) );
    connect( ui->learnedBluprintsList, SIGNAL( clicked(QModelIndex) ), this, SLOT( onBlueprintRowSelected(QModelIndex)) );
    connect( ui->blueprintRemoveButton, SIGNAL( clicked() ), this, SLOT( onRemoveBlueprint() ) );
    connect( ui->statusEffectsList, SIGNAL( customContextMenuRequested(QPoint) ), this, SLOT( onStatusContextMenu( QPoint ) ) );
    connect( ui->editStatusButton, SIGNAL( clicked() ), this, SLOT( onEditStatus() ) );
    connect( ui->statusEffectsList, SIGNAL( clicked(QModelIndex) ), this, SLOT( onStatusEffectClicked(QModelIndex) ) );
}

void ItemEditionWidget::onNameChanged( QString name ) {
    QChar prevChar = QChar::Null;
    for( int i = 0; i < name.length(); i++ ) {
        QChar ch = name.at( i );
        if( prevChar == ' ' ) {
            name[i] = ch.toUpper();
        }
        prevChar = ch;
    }

    name = name.simplified();
    name.replace( " ", "" );

    //Replacing every symbols by nothing;
    foreach( QChar ch, m_symbols ) {
        name.replace( ch, "" );
    }

    ui->itemIdLineEdit->setText( name );
    itemIdChanged( name );
}

bool ItemEditionWidget::isSymbol( QChar ch ) {
    foreach( QChar chv, m_symbols ) {
        if( ch == chv )
            return true;
    }
    return false;
}

bool ItemEditionWidget::addStatusEffect(JsonStatusEffect eff)
{
    foreach( JsonStatusEffect effect, m_statusEffects ) {
        if( effect.kind == eff.kind ) {
            return false;
        }
    }

    m_statusEffects.append( eff );
    return true;
}

QString ItemEditionWidget::getItemName() const {
    return ui->nameLineEdit->text();
}

void ItemEditionWidget::setItemName( QString name ) {
    ui->nameLineEdit->setText( name );
}

QString ItemEditionWidget::getItemId() const {
    return ui->itemIdLineEdit->text();
}

void ItemEditionWidget::setItemId( QString id ) {
    ui->nameLineEdit->setText( id );
}

QString ItemEditionWidget::getItemDescription() const {
    return ui->generalDescriptionLineEdit->text();
}

void ItemEditionWidget::setItemDescription( QString description ) {
    ui->generalDescriptionLineEdit->setText( description );
}

QString ItemEditionWidget::getFloranDescription() const
{
    return ui->floranDescriptionLineEdit->text();
}

void ItemEditionWidget::setFloranDescription(QString description)
{
    ui->floranDescriptionLineEdit->setText( description );
}

QString ItemEditionWidget::getApexDescription() const
{
    return ui->apexDescriptionLineEdit->text();
}

void ItemEditionWidget::setApexDescription(QString description)
{
    ui->apexDescriptionLineEdit->setText( description );
}

QString ItemEditionWidget::getAvianDescription() const
{
    return ui->avianDescriptionLineEdit->text();
}

void ItemEditionWidget::setAvianDescription(QString description)
{
    ui->avianDescriptionLineEdit->setText( description );
}

QString ItemEditionWidget::getGlitchDescription() const
{
    return ui->glitchDescriptionLineEdit->text();
}

void ItemEditionWidget::setGlitchDescription(QString description)
{
    ui->glitchDescriptionLineEdit->setText( description );
}

QString ItemEditionWidget::getHumanDescription() const
{
    return ui->humanDescriptionLineEdit->text();
}

void ItemEditionWidget::setHumanDescription(QString description)
{
    ui->humanDescriptionLineEdit->setText( description );
}

QString ItemEditionWidget::getHylotlDescription() const
{
    return ui->hylotlDescriptionLineEdit->text();
}

void ItemEditionWidget::setHylotlDescription(QString description)
{
    ui->hylotlDescriptionLineEdit->setText( description );
}

QString ItemEditionWidget::getItemRarity() const {
    return ui->rarityComboBox->currentText();
}

void ItemEditionWidget::setItemRarity( QString rarity ) {
    int index = ui->rarityComboBox->findText( rarity );

    if( index != -1 )
        ui->rarityComboBox->setCurrentIndex( index );
}

int ItemEditionWidget::getMaxStack() const {
    return ui->maxStackSpinBox->value();
}

void ItemEditionWidget::setMaxStack( const int stack ) {
    ui->maxStackSpinBox->setValue( stack );
}

QString ItemEditionWidget::getInventoryIconPath() const {
    return ui->inventoryIconLineEdit->text();
}

void ItemEditionWidget::setInventoryIconPath( QString path ) {
    ui->inventoryIconLineEdit->setText( path );
}

int ItemEditionWidget::getFuelAmount() const
{
    return ui->fuelAmountSpinBox->value();
}

void ItemEditionWidget::setFuelAmount(int amount)
{
    ui->fuelAmountSpinBox->setValue( amount );
}

QStringList ItemEditionWidget::getBlueprintsLearned()
{
    QStringList bpList;
    for( int i = 0; i < ui->learnedBluprintsList->count(); ++i ) {
        QListWidgetItem *item = ui->learnedBluprintsList->item( i );
        bpList.append( item->text() );
    }
    return bpList;
}

void ItemEditionWidget::setBlueprintsLearned(QStringList bp)
{
    ui->learnedBluprintsList->clear();

    foreach( QString blueprint, bp ) {
        ui->learnedBluprintsList->addItem( blueprint );
    }
}

QList<JsonStatusEffect> ItemEditionWidget::getStatusEffect()
{
    return m_statusEffects;
}

void ItemEditionWidget::setStatusEffect(QList<JsonStatusEffect> effects)
{
    m_statusEffects = effects;
}

void ItemEditionWidget::onBrowse() {
    QString cPath = ui->inventoryIconLineEdit->text();

    QString path = QFileDialog::getOpenFileName( this, tr( "Get an inventory icon" ), cPath, QString( "Image files( *.png )" ) );

    if( path != "" ) {
        ui->inventoryIconLineEdit->setText( path );
    }
}

void ItemEditionWidget::onAddBlueprint()
{
    BlueprintChooserDialog blue( m_assetsManager, this );
    blue.exec();

    QString item = blue.getSelectedItem();

    QListWidgetItem *widgetItem = new QListWidgetItem( item );
    ui->learnedBluprintsList->addItem( widgetItem );
}

void ItemEditionWidget::onAddEffect()
{
    StatusEffectChooser sec( m_assetsManager, this );
    sec.exec();

    if( !sec.cancelled() ) {
        QString effect = sec.getStatusEffect();
        JsonStatusEffect statEffect;
        statEffect.kind = effect;
        statEffect.amount = 0;
        statEffect.percentage = 0;
        statEffect.range = 0;

        if( sec.editStatus() ) {
            JsonStatusEffectEditionDialog jsonedition( m_assetsManager, this );
            jsonedition.setKind( effect );
            jsonedition.exec();

            QString kind = jsonedition.getKind();
            double amountV = jsonedition.getAmount();
            int percentageV = jsonedition.getPercentage();
            int rangeV = jsonedition.getRange();
            //QString color = jsonedition.getColor();

            statEffect.kind = kind;
            statEffect.amount = amountV;
            statEffect.percentage = percentageV;
            statEffect.range = rangeV;

            if( addStatusEffect( statEffect ) ) {
                QStandardItem *item = new QStandardItem( kind );
                QStandardItem *amount = new QStandardItem( QVariant( amountV ).toString() );
                QStandardItem *percentage = new QStandardItem( QString::number( percentageV) );
                QStandardItem *range = new QStandardItem( QString::number( rangeV ) );
                QStandardItem *color = new QStandardItem( "Not defined yet" );

                /*item->appendRow( amount );
                item->appendRow( percentage );
                item->appendRow( range );
                item->appendRow( color );*/

                int row = m_statusEffectModel->rowCount();
                m_statusEffectModel->setItem( row, 0, item );
                m_statusEffectModel->setItem( row, 1, amount );
                m_statusEffectModel->setItem( row, 2, percentage );
                m_statusEffectModel->setItem( row, 3, range );
                m_statusEffectModel->setItem( row, 4, color );
            }

        } else {
            if( addStatusEffect( statEffect ) ) {
                QStandardItem *item = new QStandardItem( effect );
                QStandardItem *amount = new QStandardItem( QVariant( 0.0 ).toString() );
                QStandardItem *percentage = new QStandardItem( QString::number( 0 ) );
                QStandardItem *range = new QStandardItem( QString::number( 0 ) );
                QStandardItem *color = new QStandardItem( "Not defined yet" );

                /*item->appendRow( amount );
                item->appendRow( percentage );
                item->appendRow( range );
                item->appendRow( color );*/

                int row = m_statusEffectModel->rowCount();
                m_statusEffectModel->setItem( row, 0, item );
                m_statusEffectModel->setItem( row, 1, amount );
                m_statusEffectModel->setItem( row, 2, percentage );
                m_statusEffectModel->setItem( row, 3, range );
                m_statusEffectModel->setItem( row, 4, color );
            }
        }
    }
}

void ItemEditionWidget::onRowSelected(QModelIndex index)
{
    if( index.isValid() )
        ui->removeStatusButton->setEnabled( true );
    else
        ui->removeStatusButton->setDisabled( true );
}

void ItemEditionWidget::onBlueprintRowSelected(QModelIndex index)
{
    if( index.isValid() )
        ui->blueprintRemoveButton->setEnabled( true );
    else
        ui->blueprintRemoveButton->setDisabled( true );
}

void ItemEditionWidget::onRemove()
{
    QModelIndex index = ui->statusEffectsList->currentIndex();

    if( index.isValid() ) {
        int rep = QMessageBox::question( this, tr( "Confirmation" ), tr( "Do you really want to remove " ) + "<strong>" + index.data().toString() + "</strong>" );

        if( rep == QMessageBox::Yes ) {
            removeStatusEffect( index );
        }
    }
}

void ItemEditionWidget::onRemoveBlueprint()
{
    QModelIndex index = ui->learnedBluprintsList->currentIndex();

    if( index.isValid() ) {
        int rep = QMessageBox::question( this, tr( "Confirmation" ), tr( "Do you really want to remove " ) + "<strong>" + index.data().toString() + "</strong>" );

        if( rep == QMessageBox::Yes ) {
            QListWidgetItem *item = ui->learnedBluprintsList->takeItem( index.row() );
            delete item;
        }
    }
}

void ItemEditionWidget::onStatusEffectClicked(QModelIndex idx)
{
    if( idx.isValid() && !idx.parent().isValid() ) {
        ui->editStatusButton->setEnabled( true );
        ui->removeStatusButton->setEnabled( true );
    } else {
        ui->editStatusButton->setEnabled( false );
        ui->removeStatusButton->setEnabled( false );
    }
}

void ItemEditionWidget::onStatusContextMenu(QPoint pt)
{
    QModelIndex idx = ui->statusEffectsList->indexAt( pt );

    if( idx.isValid() && !idx.parent().isValid() ) {
        QMenu *contextMenu = new QMenu( this );

        QAction *editAction = new QAction( QIcon( QPixmap( "assets/images/edit.png" ) ), tr( "Edit Status Effect" ), this );
        QAction *removeAction = new QAction( QIcon( QPixmap( "assets/images/delete.png" ) ), tr( "Delete Status Effect" ), this );

        contextMenu->addAction( editAction );
        contextMenu->addAction( removeAction );

        connect( editAction, SIGNAL( triggered() ), this, SLOT( onEditStatus() ) );
        connect( removeAction, SIGNAL( triggered() ), this, SLOT( onRemove() ) );

        QPoint globalPoint = ui->statusEffectsList->mapToGlobal( pt );
        contextMenu->exec( globalPoint );
    }
}

void ItemEditionWidget::onEditStatus()
{
    QModelIndex idx = ui->statusEffectsList->currentIndex();

    if( idx.isValid() && !idx.parent().isValid() ) {
        onEditStatus( idx );
    }
}

void ItemEditionWidget::onEditStatus(QModelIndex idx)
{
    if( effectExists( idx.data().toString() ) ) {
        JsonStatusEffect effect = getEffectByName( idx.data().toString() );

        JsonStatusEffectEditionDialog jsonEdit( m_assetsManager, this );
        jsonEdit.setKind( effect.kind );
        jsonEdit.setAmount( effect.amount );
        jsonEdit.setPercentage( effect.percentage );
        jsonEdit.setRange( effect.range );

        if( jsonEdit.exec() == QDialog::Accepted ) {
            JsonStatusEffect editedEffect;
            editedEffect.kind = jsonEdit.getKind();
            editedEffect.amount = jsonEdit.getAmount();
            editedEffect.percentage = jsonEdit.getPercentage();
            editedEffect.range = jsonEdit.getRange();

            QModelIndex amountIdx = idx.child( 0, 0 );
            QModelIndex percentageIdx = idx.child( 1, 0 );
            QModelIndex rangeIdx = idx.child( 2, 0 );

            m_statusEffectModel->setData( idx, editedEffect.kind );
            m_statusEffectModel->setData( amountIdx, tr( "Amount" ) + ": " + QVariant( editedEffect.amount ).toString() );
            m_statusEffectModel->setData( percentageIdx, tr( "Percentage" ) + ": " + QString::number( editedEffect.percentage ) );
            m_statusEffectModel->setData( rangeIdx, tr( "Range" ) + ": " + QString::number( editedEffect.range ) );

            for( int i = 0; i < m_statusEffects.size(); i++ ) {
                //If we can find the OLD (non edited) effect, then we replace it
                if( effect.kind == m_statusEffects[i].kind ) {
                    m_statusEffects[i] = editedEffect;
                }
            }
        }
    }
}

QStringList ItemEditionWidget::verifyFields() {
    QStringList errorList;

    if( ui->nameLineEdit->text() == "" )
        errorList.append( tr( "Item name is empty" ) );

    if( ui->itemIdLineEdit->text() == "" )
        errorList.append( tr( "Item ID is empty" ) );

    if( ui->rarityComboBox->currentText() == "" )
        errorList.append( tr( "Rarity is empty" ) );

    return errorList;
}

void ItemEditionWidget::removeStatusEffect(QModelIndex index)
{
    if( index.isValid()) {
        m_statusEffectModel->removeRow( index.row() );

        for( QList<JsonStatusEffect>::iterator it = m_statusEffects.begin(); it != m_statusEffects.end(); ++it ) {
            JsonStatusEffect effect = *it;
            if( effect.kind == index.data().toString() ) {
                m_statusEffects.erase( it );
                break;
            }
        }
    }
}

bool ItemEditionWidget::effectExists(QString name)
{
    for( int i = 0; i < m_statusEffects.size(); i++ ) {
        if( m_statusEffects[i].kind == name ) {
            return true;
        }
    }
    return false;
}

JsonStatusEffect ItemEditionWidget::getEffectByName(QString name)
{
    for( int i = 0; i < m_statusEffects.size(); i++ ) {
        if( name == m_statusEffects[i].kind ) {
            return m_statusEffects[i];
        }
    }
    return JsonStatusEffect();
}
