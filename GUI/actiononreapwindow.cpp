#include "actiononreapwindow.h"
#include "ui_actiononreapwindow.h"

ActionOnReapWindow::ActionOnReapWindow(AssetsManager *assetsManager, QString projectPath, bool createAction, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ActionOnReapWindow)
{
    ui->setupUi(this);
    QString title = tr( "Action On Reap" );

    if( createAction )
        title += " - " + tr( "Creation" );
    else
        title += " - " + tr( "Edition" );

    setWindowTitle( title );

    buildInterface( assetsManager, projectPath, createAction );
    initEvents();
}

ActionOnReapWindow::~ActionOnReapWindow()
{
    delete ui;
}

void ActionOnReapWindow::buildInterface(AssetsManager *assetsManager, QString projectPath, bool createAction)
{
    m_actionOnReap = new ActionOnReapWidget( assetsManager, projectPath, createAction, this );
    m_validateButton = new QPushButton( tr( "Ok" ), this );
    m_cancelButton = new QPushButton( tr( "Close" ), this );

    m_buttonsLayout = new QHBoxLayout;
    m_mainLayout = new QVBoxLayout;

    QSpacerItem *space = new QSpacerItem( 250, 0, QSizePolicy::Maximum );
    m_buttonsLayout->addSpacerItem( space );
    m_buttonsLayout->addWidget( m_validateButton );
    m_buttonsLayout->addWidget( m_cancelButton );

    m_mainLayout->addWidget( m_actionOnReap );
    m_mainLayout->addLayout( m_buttonsLayout );

    m_mainLayout->setMargin( 3 );

    setLayout( m_mainLayout );
}

void ActionOnReapWindow::initEvents()
{
    connect( m_validateButton, SIGNAL( clicked() ), this, SLOT( validate() ) );
    connect( m_cancelButton, SIGNAL( clicked() ), this, SLOT( reject() ) );
}
ActionOnReapWidget *ActionOnReapWindow::actionOnReap() const
{
    return m_actionOnReap;
}

void ActionOnReapWindow::validate()
{
    if( !m_actionOnReap->validate() ) {
        QStringList errors = m_actionOnReap->lastValidationErrorsList();
        QString msg = tr( "You cannot create that action because of the following errors" ) + ": <br /><ul>";

        foreach( QString error, errors ) {
            msg += "<li><strong>" + error + "</strong></li>";
        }
        msg += "</ul>";
        QMessageBox::warning( this, tr( "Cannot create action" ), msg );
    } else {
        accept();
    }
}

