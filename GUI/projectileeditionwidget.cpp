#include "projectileeditionwidget.h"
#include "ui_projectileeditionwidget.h"

ProjectileEditionWidget::ProjectileEditionWidget( AssetsManager *assetsManager, QString modPath, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProjectileEditionWidget),
    m_assetsManager( assetsManager ),
    m_modPath( modPath ),
    m_lastValidationError( "" )
{
    ui->setupUi(this);

    ui->actionOnReapList->setContextMenuPolicy( Qt::CustomContextMenu );

    m_actionOnReapModel = new QStandardItemModel( this );
    resetModel();
    ui->actionOnReapList->setModel( m_actionOnReapModel );

    initEvents();
    initDamageComboBox();
    initPhysicsComboBox();
    ui->damageTypeComboBox->addItem( "default", "default" );
}

ProjectileEditionWidget::~ProjectileEditionWidget()
{
    delete ui;
}

void ProjectileEditionWidget::initEvents() {
    connect( ui->editActionButton, SIGNAL( clicked() ), this, SLOT( onActionEditClicked() ) );
    connect( ui->removeActionButton, SIGNAL( clicked() ), this, SLOT( onActionRemoveClicked() ) );
    connect( ui->frameBrowseButton, SIGNAL( clicked() ), this, SLOT( onFrameFileBrowse() ) );
    connect( ui->damageKindImageButton, SIGNAL( clicked() ), this, SLOT( onDamageKindBrowse() ) );
    connect( ui->colorChooser, SIGNAL( clicked() ), this, SLOT( onPickupColor() ) );
    connect( ui->createFrameFileButton, SIGNAL( clicked() ), this, SLOT( onCreateFrameClicked() ) );
    connect( ui->addActionButton, SIGNAL( clicked() ), this, SLOT( onAddActionOnReap() ) );
    connect( ui->actionOnReapList, SIGNAL( clicked(QModelIndex) ), this, SLOT( onActionOnReapItemClicked(QModelIndex) ) );
    connect( ui->actionOnReapList, SIGNAL( customContextMenuRequested(QPoint) ), this, SLOT( onActionOnReapContextRequested( QPoint ) ) );
}

void ProjectileEditionWidget::onCreateFrameClicked() {
    FrameEditionWindow few( m_modPath, this );

    few.exec();

    ui->frameBrowseLineEdit->setText( few.getFrameFile() );
}

void ProjectileEditionWidget::onAddActionOnReap()
{
    ActionOnReapWindow win( m_assetsManager, m_modPath, true, this );
    if( win.exec() == QDialog::Accepted ) {
        ActionOnReapWidget *widget = win.actionOnReap();

        if( widget->createAction() ) {
            ActionOnReap aor = populateActionOnReap( widget );
            m_actionOnReapList.append( aor );
            updateActionOnReapList();
        }
    }
}

void ProjectileEditionWidget::onActionOnReapItemClicked(QModelIndex idx)
{
    //If we are on root item
    if( idx.isValid() && !idx.parent().isValid() ) {
        ui->editActionButton->setEnabled( true );
        ui->removeActionButton->setEnabled( true );
    } else {
        ui->editActionButton->setEnabled( false );
        ui->removeActionButton->setEnabled( false );
    }
}

void ProjectileEditionWidget::onActionOnReapContextRequested(QPoint pt)
{
    QModelIndex idx = ui->actionOnReapList->indexAt( pt );

    if( idx.isValid() && !idx.parent().isValid() ) {
        QPoint globalPoint = ui->actionOnReapList->mapToGlobal( pt );

        QMenu *contextMenu = new QMenu( this );

        QAction *editAction = new QAction( QIcon( QPixmap( "assets/images/edit.png" ) ), tr( "Edit Action" ), this );
        QAction *copyAction = new QAction( QIcon( QPixmap( "assets/images/copy.png" ) ), tr( "Copy This Action" ), this );
        QAction *copyNTimesAction = new QAction( QIcon( QPixmap( "assets/images/copy.png" ) ), tr( "N Times Copy" ), this );
        QAction *removeAction = new QAction( QIcon( QPixmap( "assets/images/delete.png" ) ), tr( "Delete Action" ), this );

        contextMenu->addAction( editAction );
        contextMenu->addAction( copyAction );
        contextMenu->addAction( copyNTimesAction );
        contextMenu->addAction( removeAction );

        connect( editAction, SIGNAL( triggered() ), this, SLOT( onActionEditClicked() ) );
        connect( removeAction, SIGNAL( triggered() ), this, SLOT( onActionRemoveClicked() ) );
        connect( copyAction, SIGNAL( triggered() ), this, SLOT( onCopyActionClicked() ) );
        connect( copyNTimesAction, SIGNAL( triggered() ), this, SLOT( onNTimeCopyActionClicked() ) );

        contextMenu->exec( globalPoint );
    }
}

void ProjectileEditionWidget::onActionEditClicked()
{
    QModelIndex idx = ui->actionOnReapList->currentIndex();

    if( idx.isValid() && !idx.parent().isValid() ) {
        int row = idx.data( Qt::UserRole + 1 ).toInt();

        if( row >= 0 && row < m_actionOnReapList.size() ) {
            ActionOnReap aor = m_actionOnReapList[row];

            ActionOnReapWindow aorWin( m_assetsManager, m_modPath, false, this );
            aorWin.actionOnReap()->populateForm( aor );

            if( aorWin.exec() == QDialog::Accepted ) {
                ActionOnReap updatedAor = populateActionOnReap( aorWin.actionOnReap() );

                m_actionOnReapList[row] = updatedAor;
                updateActionOnReapList();
            }
        }
    }
}

void ProjectileEditionWidget::onActionRemoveClicked()
{
    QModelIndex idx = ui->actionOnReapList->currentIndex();

    if( idx.isValid() && !idx.parent().isValid() ) {
        int rep = QMessageBox::question( this, tr( "Confirmation" ), tr( "Do you really want to delete that action ?" ) );

        if( rep == QMessageBox::Yes ) {
            int arrayRow = idx.data( Qt::UserRole + 1 ).toInt();

            qDebug() << arrayRow << "/" << m_actionOnReapList.size() << endl;
            m_actionOnReapList.erase( m_actionOnReapList.begin() + arrayRow );
            updateActionOnReapList();
            qDebug() << m_actionOnReapList.size() << endl;
        }
    }
}

void ProjectileEditionWidget::onCopyActionClicked()
{
    QModelIndex idx = ui->actionOnReapList->currentIndex();

    if( idx.isValid() && !idx.parent().isValid() ) {
        copyAction( idx );
    }
}

void ProjectileEditionWidget::onNTimeCopyActionClicked()
{
    bool ok = false;
    int number = QInputDialog::getInt( this, tr( "Multiple copies" ), tr( "How many time you want to copy the action" ), 0, 0, 50, 1, &ok );

    if( ok ) {
        QModelIndex idx = ui->actionOnReapList->currentIndex();
        if( idx.isValid() && !idx.parent().isValid() ) {
            for( int i = 0; i < number; i++ ) {
                copyAction( idx, false );
            }
            updateActionOnReapList();
        }
    }

}

bool ProjectileEditionWidget::validate() {
    bool error = false;
    QStringList errorList;

    if( ui->projectileNameLineEdit->text() == "" ) {
        error = true;
        errorList.append( tr( "Projectile name cannot be empty" ) );
    }

    if( ui->frameBrowseLineEdit->text() == "" ) {
        error = true;
        errorList.append( tr( "You cannot create a projectile without frameset (Frame path is empty)" ) );
    }

    if( ui->damageKindComboBox->currentText() == "" ) {
        error = true;
        errorList.append( tr( "Cannot create projectile without specify a damage kind" ) );
    }

    if( ui->damageTypeComboBox->currentText() == "" ) {
        error = true;
        errorList.append( tr( "Cannot create projectile without specify a damage type" ) );
    }

    QString errorMsg = tr( "Cannot create projectile because of the following errors" ) + ": <br /><ul>";

    foreach( QString err, errorList ) {
        errorMsg += "<li><strong>" + err + "</strong></li>";
    }
    errorMsg += "</ul>";
    m_lastValidationError = errorMsg;

    return !error;
}

void ProjectileEditionWidget::createProjectile() {
    StarProjectile projectile;

    projectile.setProjectileName( ui->projectileNameLineEdit->text() );
    projectile.setTimeToLive( ui->timeToLiveSpinBox->value() );
    projectile.setLevel( ui->levelSpinBox->value() );
    projectile.setHydrophobic( ui->hydrophobicCheckBox->isChecked() );

    projectile.setFramePath( ui->frameBrowseLineEdit->text() );
    projectile.setFrameNumber( ui->frameNumberSpinBox->value() );
    projectile.setAnimationCycle( ui->animationCycleDoubleSpinBox->value() );
    projectile.setAnimationLoop( ui->animationLoopCheckBox->isChecked() );

    projectile.setDamageType( ui->damageTypeComboBox->itemData( ui->damageTypeComboBox->currentIndex() ).toString() );
    projectile.setDamageKind( ui->damageKindComboBox->itemData( ui->damageKindComboBox->currentIndex() ).toString() );
    projectile.setDamageKindImage( ui->damageKindImageLineEdit->text() );
    projectile.setPower( ui->powerDoubleSpinBox->value() );

    projectile.setPhysics( ui->physicsComboBox->itemData( ui->damageTypeComboBox->currentIndex() ).toString() );
    projectile.setBounces( ui->bouncesSpinBox->value() );
    projectile.setFallSpeed( ui->fallSpeedDoubleSpinBox->value() );
    projectile.setKnockbackPower( ui->knockbackPowerSpinBox->value() );
    projectile.setInitialVelocity( ui->initialVelocityDoubleSpinBox->value() );
    projectile.setSpeed( ui->speedDoubleSpinBox->value() );

    projectile.setPointLight( ui->pointLightCheckBox->isChecked() );
    projectile.setLightColor( m_lightColor );

    projectile.save( m_modPath );
}

QString ProjectileEditionWidget::lastValidationError() const
{
    return m_lastValidationError;
}
void ProjectileEditionWidget::initPhysicsComboBox() {
    QList<StarPhysics> list = m_assetsManager->physicsList();

    foreach( StarPhysics physics, list ) {
        ui->physicsComboBox->addItem( physics.physicName(), physics.physicName() );
    }
}


void ProjectileEditionWidget::onFrameFileBrowse() {
    QString cpath = ui->frameBrowseLineEdit->text();
    QString path = QFileDialog::getOpenFileName( this, tr( "Get a .frames file" ), cpath, QString( "Frames files( *.frames )" ) );

    if( path != "" ) {
        QFileInfo pathInfo( path );
        QDir dir( pathInfo.path() );

        if( !dir.exists( pathInfo.baseName() + ".png" ) ) {
            ui->frameWarningLabel->setText( "<font color='orange'>" + tr( "A frame picture file must exists next to the selected frame file" ) + "</font>" );
        } else {
            ui->frameWarningLabel->setText( "" );
        }

        ui->frameBrowseLineEdit->setText( path );
    }
}

void ProjectileEditionWidget::onDamageKindBrowse() {
    QString cpath = ui->damageKindImageLineEdit->text();
    QString path = QFileDialog::getOpenFileName( this, tr( "Get a image for Damage kind" ), QString(), QString( "PNG File( *.png )" ) );

    if( path != "" ) {
        ui->damageKindImageLineEdit->setText( path );
    }
}

void ProjectileEditionWidget::onPickupColor() {
    QColor color = QColorDialog::getColor();

    QPalette palette;
    palette.setColor( QPalette::ButtonText, color );
    ui->colorChooser->setPalette( palette );
    m_lightColor = color;
}

QString ProjectileEditionWidget::getProjectileName() const {
    return ui->projectileNameLineEdit->text();
}

void ProjectileEditionWidget::setProjectileName( QString name ) {
    ui->projectileNameLineEdit->setText( name );
}

QString ProjectileEditionWidget::getFrameFile() const {
    return ui->frameBrowseLineEdit->text();
}

void ProjectileEditionWidget::setFrameFile( QString file ) {
    ui->frameBrowseLineEdit->setText( file );
}

int ProjectileEditionWidget::getFrameNumber() const {
    return ui->frameNumberSpinBox->value();
}

void ProjectileEditionWidget::setFrameNumber( int number ) {
    ui->frameNumberSpinBox->setValue( number );
}

double ProjectileEditionWidget::getAnimationCycle() const {
    return ui->animationCycleDoubleSpinBox->value();
}

void ProjectileEditionWidget::setAnimationCycle( double cycle ) {
    ui->animationCycleDoubleSpinBox->setValue( cycle );
}

QString ProjectileEditionWidget::getDamageKind() const {
    return ui->damageKindComboBox->itemData( ui->damageKindComboBox->currentIndex() ).toString();
}

void ProjectileEditionWidget::setDamageKind( QString damage ) {
    int index = ui->damageKindComboBox->findData( damage );

    if( index != -1 )
        ui->damageKindComboBox->setCurrentIndex( index );
}

QString ProjectileEditionWidget::getDamageKindImage() const {
    return ui->damageKindImageLineEdit->text();
}

void ProjectileEditionWidget::setDamageKindImage( QString path ) {
    ui->damageKindImageLineEdit->setText( path );
}

QString ProjectileEditionWidget::getDamageType() const {
    return ui->damageTypeComboBox->itemData( ui->damageTypeComboBox->currentIndex() ).toString();
}

void ProjectileEditionWidget::setDamageType( QString path ) {
    int index = ui->damageTypeComboBox->currentIndex();

    if( index != -1 )
        ui->damageTypeComboBox->setCurrentIndex( index );
}

bool ProjectileEditionWidget::hasPointLight() const {
    return ui->pointLightCheckBox->isChecked();
}

void ProjectileEditionWidget::setPointLight( bool pointLight ) {
    ui->pointLightCheckBox->setChecked( pointLight );
}

QColor ProjectileEditionWidget::getLightColor() const {
    return m_lightColor;
}

void ProjectileEditionWidget::setLightColor( QColor lightColor ) {
    m_lightColor = lightColor;
}

double ProjectileEditionWidget::getFallSpeed() const {
    return ui->fallSpeedDoubleSpinBox->value();
}

void ProjectileEditionWidget::setFallSpeed( double fallSpeed ) {
    ui->fallSpeedDoubleSpinBox->setValue( fallSpeed );
}

void ProjectileEditionWidget::initDamageComboBox() {
    QDir damagePath = QString( m_assetsManager->assetsPath() + "/damage" );
    QFileInfoList entryList = damagePath.entryInfoList( QDir::NoDotAndDotDot | QDir::Files );

    foreach( QFileInfo fifo, entryList ) {
        if( fifo.suffix() == "damage" ) {
            QString damageKind = getDamageKind( fifo.absoluteFilePath() );
            ui->damageKindComboBox->addItem( damageKind, damageKind );
            if( damageKind == "default" ) {
                int index = ui->damageKindComboBox->findText( damageKind );
                if( index != -1 )
                    ui->damageKindComboBox->setCurrentIndex( index );
            }
        }
    }
}

QString ProjectileEditionWidget::getDamageKind( QString path ) {
    QFile file( path );
    if( !file.open( QIODevice::ReadOnly ) ) {
        qDebug() << "Cannot open " << file.fileName() << endl;
        return QString();
    }

    QJsonDocument doc = QJsonDocument::fromJson( file.readAll() );
    QJsonObject root = doc.object();

    QJsonObject::iterator kindIte = root.find( "kind" );

    if( kindIte != root.end() ) {
        return kindIte.value().toString();
    }

    return QString();
}

void ProjectileEditionWidget::resetModel()
{
    m_actionOnReapModel->clear();
    QStringList headers;
    headers << tr( "Action Type" ) << tr( "Value" );
    m_actionOnReapModel->setHorizontalHeaderLabels( headers );
}

void ProjectileEditionWidget::updateActionOnReapList()
{
    resetModel();
    int i = 0;
    foreach( ActionOnReap aor, m_actionOnReapList ) {
        QStandardItem *item = new QStandardItem;
        QString actionName = actionTypeToString( aor.action );

        item->setText( actionName );
        item->setData( i );

        if( aor.action == Projectile ) {
            QStandardItem *projectileTypeTitle = new QStandardItem( tr( "Projectile Type" ) );
            QStandardItem *projectileType = new QStandardItem( aor.projectileType );

            QStandardItem *inheritDamageFactorTitle = new QStandardItem( tr( "Inherit Damage Factor" ) );
            QStandardItem *inheritDamageFactor = new QStandardItem( QVariant( aor.inheritDamageFactor ).toString() );

            QStandardItem *angleTitle = new QStandardItem( tr( "Angle" ) );
            QStandardItem *angleItem = new QStandardItem;
            if( aor.angle == -1 ) {
                angleItem->setText( tr( "Angle Not Defined" ) );
            } else {
                angleItem->setText( QString::number( aor.angle ) );
            }

            QStandardItem *adjustAngleTitle = new QStandardItem( tr( "Adjust Angle" ) );
            QStandardItem *adjustAngle = new QStandardItem;
            if( aor.adjustAngle == -1 ) {
                adjustAngle->setText( tr( "Adjust Angle Not Defined" ) );
            } else {
                adjustAngle->setText( QString::number( aor.adjustAngle ) );
            }

            QStandardItem *fuzzAngleTitle = new QStandardItem( tr( "Fuzz Angle" ) );
            QStandardItem *fuzzAngle = new QStandardItem;
            if( aor.fuzzAngle == -1 ) {
                fuzzAngle->setText( tr( "Fuzz Angle Not Defined" ) );
            } else {
                fuzzAngle->setText( QString::number( aor.fuzzAngle ) );
            }

            item->setChild( 0, 0, projectileTypeTitle );
            item->setChild( 0, 1, projectileType );

            item->setChild( 1, 0, inheritDamageFactorTitle );
            item->setChild( 1, 1, inheritDamageFactor );

            item->setChild( 2, 0, angleTitle );
            item->setChild( 2, 1, angleItem );

            item->setChild( 3, 0, adjustAngleTitle );
            item->setChild( 3, 1, adjustAngle );

            item->setChild( 4, 0, fuzzAngleTitle );
            item->setChild( 4, 1, fuzzAngle );
        } else if( aor.action == Explosion ) {
            QStandardItem *foregroundRadiusTitle = new QStandardItem( tr( "Foreground Radius" ) );
            QStandardItem *foregroundRadius = new QStandardItem( QString::number( aor.foregroundRadius ) );

            QStandardItem *backgroundRadiusTitle = new QStandardItem( tr( "Background Radius" ) );
            QStandardItem *backgroundRadius = new QStandardItem( QString::number( aor.backgroundRadius ) );

            QStandardItem *explosiveDamageAmountTitle = new QStandardItem( tr( "Explosive Damage Amount" ) );
            QStandardItem *explosiveDamageAmount = new QStandardItem( QString::number( aor.explosiveDamageAmount ) );

            item->setChild( 0, 0, foregroundRadiusTitle );
            item->setChild( 0, 1, foregroundRadius );

            item->setChild( 1, 0, backgroundRadiusTitle );
            item->setChild( 1, 1, backgroundRadius );

            item->setChild( 2, 0, explosiveDamageAmountTitle );
            item->setChild( 2, 1, explosiveDamageAmount );
        } else if( aor.action == Liquid ) {
            QStandardItem *liquidIdTitle = new QStandardItem( tr( "Liquid Id" ) );
            QStandardItem *liquidId = new QStandardItem( QString::number( aor.liquidId ) );

            QStandardItem *quantityTitle = new QStandardItem( tr( "Quantity" ) );
            QStandardItem *quantity = new QStandardItem( QString::number( aor.quantity ) );

            item->setChild( 0, 0, liquidIdTitle );
            item->setChild( 0, 1, liquidId );

            item->setChild( 1, 0, quantityTitle );
            item->setChild( 1, 1, quantity );
        } else if( aor.action == Sounds ) {
            foreach( QString sound, aor.sounds ) {
                QStandardItem *soundItem = new QStandardItem( sound );

                item->appendRow( soundItem );
            }
        } else if( aor.action == Config ) {
            QStandardItem *configTitle = new QStandardItem( tr( "Config File" ) );
            QStandardItem *config = new QStandardItem( aor.configFile );

            item->setChild( 0, 0, configTitle );
            item->setChild( 0, 1, config );
        }

        m_actionOnReapModel->appendRow( item );

        i++;
    }
}

ActionOnReap ProjectileEditionWidget::populateActionOnReap(ActionOnReapWidget *widget)
{
    int actionType = widget->actionType();
    ActionOnReap aor;
    switch( actionType ) {
        case Projectile:
            aor.action = actionType;
            aor.projectileType = widget->projectileType();
            aor.angle = widget->angle();
            aor.adjustAngle = widget->adjustAngle();
            aor.fuzzAngle = widget->fuzzAngle();
            aor.inheritDamageFactor = widget->inheritDamageFactor();
        break;

        case Liquid:
            aor.action = actionType;
            aor.liquidId = widget->liquidId();
            aor.quantity = widget->quantity();
        break;

        case Explosion:
            aor.action = actionType;
            aor.foregroundRadius = widget->foregroundRadius();
            aor.backgroundRadius = widget->backgroundRadius();
            aor.explosiveDamageAmount = widget->explosiveDamageAmount();
        break;

        case Sounds:
            aor.action = actionType;
            aor.sounds = widget->sounds();
        break;

        case Config:
            aor.action = actionType;
            aor.configFile = widget->config();
        break;
    }
    return aor;
}

void ProjectileEditionWidget::copyAction(QModelIndex idx, bool refreshView)
{
    if( idx.isValid() && !idx.parent().isValid() ) {
        int row = idx.data( Qt::UserRole + 1 ).toInt();
        ActionOnReap aor = m_actionOnReapList[ row ];

        m_actionOnReapList.append( aor );

        if( refreshView )
            updateActionOnReapList();
    }
}
QList<ActionOnReap> ProjectileEditionWidget::actionOnReapList() const
{
    return m_actionOnReapList;
}
