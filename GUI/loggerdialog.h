#ifndef LOGGERDIALOG_H
#define LOGGERDIALOG_H

#include <QDialog>
#include "../Core/logger.h"

namespace Ui {
class LoggerDialog;
}

class LoggerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LoggerDialog( Logger *log, QWidget *parent = 0 );
    ~LoggerDialog();

    void setLog( LogMessageList &logs );
private:
    void initEvents();
private:
    Ui::LoggerDialog *ui;
};

#endif // LOGGERDIALOG_H
