#include "frameimagepreview.h"
#include "ui_frameimagepreview.h"

FrameImagePreview::FrameImagePreview(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FrameImagePreview),
    m_rectItem( NULL ),
    m_item( NULL )
{
    ui->setupUi(this);

    buildInterface();
    initEvents();
    setWindowTitle( tr( "Frames previewer" ) );
    setModal( false );
}

FrameImagePreview::~FrameImagePreview()
{
    delete ui;
}


void FrameImagePreview::buildInterface() {
    m_mainLayout = new QVBoxLayout;
    QGridLayout *toolbar = new QGridLayout;
    QHBoxLayout *toolbarLayout = new QHBoxLayout;

    m_leftButton = new QPushButton( "", this );
    m_rightButton = new QPushButton( "", this );
    m_upButton = new QPushButton( "", this );
    m_downButton = new QPushButton( "", this );
    m_closeButton = new QPushButton( tr( "Close" ), this );

    m_view = new QGraphicsView( this );
    m_scene = new QGraphicsScene( this );

    m_leftButton->setIcon( QIcon( QPixmap( "assets/images/left.png" ) ) );
    m_rightButton->setIcon( QIcon( QPixmap( "assets/images/right.png" ) ) );
    m_upButton->setIcon( QIcon( QPixmap( "assets/images/up.png" ) ) );
    m_downButton->setIcon( QIcon( QPixmap( "assets/images/down.png" ) ) );

    m_view->setScene( m_scene );
    m_view->scale( 2.0, 2.0 );

    toolbar->addWidget( m_leftButton, 1, 0);
    toolbar->addWidget( m_upButton, 0, 1 );
    toolbar->addWidget( m_downButton, 2, 1 );
    toolbar->addWidget( m_rightButton, 1, 2 );

    QSpacerItem *spacer = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Expanding );
    toolbarLayout->addLayout( toolbar );
    toolbarLayout->addItem( spacer );

    m_mainLayout->addLayout( toolbarLayout , 2 );
    m_mainLayout->addWidget( m_view, 4 );
    m_mainLayout->addWidget( m_closeButton, 1 );

    setLayout( m_mainLayout );
}

void FrameImagePreview::initEvents() {
    connect( m_closeButton, SIGNAL( clicked() ), this, SLOT( onClose() ) );
    connect( m_leftButton, SIGNAL( clicked() ), this, SLOT( onLeftButtonClicked() ) );
    connect( m_rightButton, SIGNAL( clicked() ), this, SLOT( onRightButtonClicked() ) );
}

void FrameImagePreview::onLeftButtonClicked() {
    moveToLeft();
}

void FrameImagePreview::onRightButtonClicked() {
    moveToRight();
}

void FrameImagePreview::onClose() {
    accept();
}

void FrameImagePreview::setImage( QString path ) {
    m_scene->clear();

    if( m_item != NULL ) {
        delete m_item;
        m_item = NULL;
    }

    m_item = new QGraphicsPixmapItem( QPixmap( path ) );

    m_scene->addItem( m_item );

    m_previewRectangleRect = m_item->boundingRect();
    m_rectItem = new QGraphicsRectItem( m_previewRectangleRect );

    m_rectItem->setPen( QPen( QColor( 255, 95, 0 ), 1 ) );
    m_scene->addItem( m_rectItem );
    //m_scene->setSceneRect( QRectF( ( item->x() - ( item->boundingRect().width() / 2 ) ), ( item->y() - ( item->boundingRect().height() / 2 ) ), item->boundingRect().width(), item->boundingRect().height() ) );
}


void FrameImagePreview::setRectangleSize( QSize size ) {
    m_previewRectangleRect.setSize( size );
    m_rectItem->setRect( m_previewRectangleRect );
}

void FrameImagePreview::setRectangleSize( int w, int h ) {
    setRectangleSize( QSize( w, h ) );
}

void FrameImagePreview::setRectangleWidth( int w ) {
    m_previewRectangleRect.setWidth( w );

    m_rectItem->setRect( m_previewRectangleRect );
}

void FrameImagePreview::setRectangleHeight( int h ) {
    m_previewRectangleRect.setHeight( h );

    m_rectItem->setRect( m_previewRectangleRect );
}

void FrameImagePreview::moveToRight( int step ) {
    QRectF boundingImage = m_item->boundingRect();

    int maxSteps = floor( boundingImage.width() / m_previewRectangleRect.width() );
    int currentStep = 0;
    if( m_previewRectangleRect.x() != 0 )
        currentStep = m_previewRectangleRect.x() / m_previewRectangleRect.width();

    int nextStep = currentStep + step;

    if( nextStep >= maxSteps ) {
        /*int width = m_previewRectangleRect.width();
        m_previewRectangleRect.setX( 0 );
        m_previewRectangleRect.setWidth( width );*/
        m_previewRectangleRect.moveLeft( 0 );
        m_rectItem->setRect( m_previewRectangleRect );
    } else {
        m_previewRectangleRect.moveLeft( m_previewRectangleRect.width() * nextStep );
        /*int newX = nextStep * m_previewRectangleRect.width();
        m_previewRectangleRect.setX( newX );*/
        //qDebug() << boundingImage << m_previewRectangleRect << maxSteps << currentStep << nextStep << newX << endl;
        m_rectItem->setRect( m_previewRectangleRect );
    }
}

void FrameImagePreview::moveToLeft( int step ) {
    QRectF boundingImage = m_item->boundingRect();

    int maxSteps = floor( boundingImage.width() / m_previewRectangleRect.width() );
    int currentStep = 0;

    if( m_previewRectangleRect.x() != 0 )
        currentStep = m_previewRectangleRect.x() / m_previewRectangleRect.width();

    int nextStep = currentStep - step;

    if( nextStep < 0 ) {
        m_previewRectangleRect.moveLeft( ( maxSteps - 1 ) * m_previewRectangleRect.width() );
        m_rectItem->setRect( m_previewRectangleRect );
    } else {
        m_previewRectangleRect.moveLeft( m_previewRectangleRect.width() * nextStep );
        m_rectItem->setRect( m_previewRectangleRect );
    }
}

int FrameImagePreview::getFramesNumberX() {
    QRectF bImage = m_item->boundingRect();
    return floor( bImage.width() / m_previewRectangleRect.width() );
}

int FrameImagePreview::getFramesNumberY() {
    QRectF bImage = m_item->boundingRect();

    return floor( bImage.height() / m_previewRectangleRect.height() );
}
