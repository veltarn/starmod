#ifndef ACTIONONREAPWINDOW_H
#define ACTIONONREAPWINDOW_H

#include <QDialog>
#include <QString>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include "../Core/assetsmanager.h"
#include "actiononreapwidget.h"

namespace Ui {
class ActionOnReapWindow;
}

class ActionOnReapWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ActionOnReapWindow( AssetsManager *assetsManager, QString projectPath, bool createAction = true, QWidget *parent = 0);
    ~ActionOnReapWindow();

    ActionOnReapWidget *actionOnReap() const;

public slots:
    void validate();
private:
    void buildInterface( AssetsManager *assetsManager, QString projectPath, bool createAction );
    void initEvents();

private:
    Ui::ActionOnReapWindow *ui;
    QHBoxLayout *m_buttonsLayout;
    QVBoxLayout *m_mainLayout;

    QPushButton *m_validateButton;
    QPushButton *m_cancelButton;

    ActionOnReapWidget *m_actionOnReap;
};

#endif // ACTIONONREAPWINDOW_H
