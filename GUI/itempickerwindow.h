#ifndef ITEMPICKERWINDOW_H
#define ITEMPICKERWINDOW_H

#include <QDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include "../Starbound/Items/staritem.h"
#include "../Core/assetsmanager.h"
#include "objectviewwidget.h"

namespace Ui {
class ItemPickerWindow;
}

class ItemPickerWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ItemPickerWindow( AssetsManager *assetsManager, QWidget *parent = 0);
    ~ItemPickerWindow();

    StarItem *getSelectedItem();
    bool cancelled() const;
    void setCancelled(bool cancelled);

    void addWidgetToSide( QString textLabel, QWidget *widget );
    void selectItem( QString name );
public slots:
    void validate();
    void cancel();
protected:
    AssetsManager *m_assetsManager;

private:
    void buildInterface();
    void initEvents();
private:
    Ui::ItemPickerWindow *ui;
    ObjectViewWidget *m_objectViewer;
    QPushButton *m_validateButton;
    QPushButton *m_cancelButton;

    QHBoxLayout *m_mainLayout;
    QVBoxLayout *m_sideLayout;


    bool m_cancelled;
};

#endif // ITEMPICKERWINDOW_H
