#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/*
 *TEST
 */
#include <QTimer>
#include "Starbound/Items/staritem.h"
/*
 *===============
 */

#include <QMainWindow>
#include <QMdiSubWindow>
#include <QMap>
#include <QMdiArea>
#include <QMenu>
#include <QSettings>
#include <QStandardPaths>
#include <QMessageBox>
#include <QFileDialog>
#include <QCloseEvent>
#include <QDir>
#include <QLabel>
#include <QString>
#include <QTreeView>
#include <QList>
#include <QModelIndex>
#include <QTextEdit>
#include <QDebug>
#include <QFileInfo>
#include <QActionGroup>
#include <QProgressDialog>
#include <QProcess>
#include <QStandardItemModel>
#include <QWizard>
#include <QWizardPage>
#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>
#include "Json/jsonvalidator.h"
#include "GUI/texteditorwidget.h"
#include "GUI/physicspropertieslist.h"
#include "GUI/progresssplashscreen.h"
#include <Qsci/qsciscintilla.h>
#include <Qsci/qscilexerlua.h>
#include <Qsci/qscilexerjavascript.h>
#include "GUI/projectpropertieswindow.h"
#include "GUI/subwindow.h"
#include "GUI/previewwidget.h"
#include "GUI/projectilelistwindow.h"
#include "Graphics/starboundcharacter.h"
#include "Core/utility.h"
#include "Core/projectmanager.h"
#include "Core/assetsmanager.h"
#include "Core/modmanager.h"
#include "Core/zip.h"
//#include "Core/logger.h"
#include "Models/filesystemmodel.h"
#include "Models/starbounditemmodel.h"
#include "Parser/luahighlighter.h"
#include "Parser/jsonhighlighter.h"
#include "GUI/itemeditionwindow.h"
#include "GUI/sidewidget.h"
#include "GUI/buildmoddialog.h"
#include "GUI/weaponeditiondialog.h"
#include "GUI/swordeditionwindow.h"
#include "GUI/projectileeditionwindow.h"
#include "GUI/Wizard/intropage.h"
#include "GUI/Wizard/starboundpathpage.h"
#include "GUI/Wizard/workspacepage.h"
#include "GUI/Wizard/endpage.h"
#include "GUI/assetsextractiondialog.h"
#include "GUI/loggerdialog.h"
#include "constants.h"

enum WizardPages {
    INTRO, PATH, WORKSPACE, FINISH
};

struct StarModOptions {
    QSize size;
    bool isMaximized;
    QMdiArea::ViewMode viewMode;

};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    void openSubWindow( QString windowTitle, QString filepath );
    ~MainWindow();

    void addProjectToRecents( const Project *prj );
    void refreshRecentProjectsList();
    void freeRecentsProjectsMenu();
    bool openProject( QString projectPath );
    //Save current file (not project! );
    bool saveFile(QString content, QString filepath );
    void saveAllSubWindows();
    void saveProject();
    bool shutdown();
    void fillEditor( QsciScintilla *editor, QString filepath );

public slots:
    void onNewProjectBtnClicked();
    void onOpenProjectBtnClicked();
    void onProjectPropertiesClicked();
    void onAddProjectileActionClicked();
    void onSaveBtnClicked();
    void onTestRunBtnClicked();
    void onBuildClicked();
    void onRowSelected( QModelIndex idx );
    void onRowDoubleClicked( QModelIndex idx );
    void onTabViewTriggered();
    void onSubWindowClose( QCloseEvent *ce );
    void onSubWindowActivated( QMdiSubWindow *s );
    void onSubWindowSelected();
    void onTextEditorChanged();
    void onRecentProjectClicked();
    void onProjectClosed();
    void copyAvailable( bool available );
    void onCopy();
    void onPaste();
    void onSaveFile();
    void onDirectoryLoaded( QString d );
    void onRelatedFileDbClicked( QModelIndex item );
    void onProcessStarted();
    void onProcessFinished( int exitCode, QProcess::ExitStatus exitStatus );
    void onCreateItemClicked();
    void onCreateWeaponClicked();
    void onCreateSwordClicked();
    /*void onWizardStarPathBrowseButtonClicked();
    void onWorkspaceBrowseButtonClicked();*/
    void onPageChanged( int pageId );
    //void onTimeout();
    void onAssetsUpdateClicked();
    void onErrorLogged( LogMessage msg );
    void openConsole();
    void openPhysicsProperties();
    void openProjectileList();
    void validateJson();
    void toggleSubWindowConsole();
    void clearTextEditorConsole();
    void createRawFile();
signals:

protected:
    void closeEvent( QCloseEvent *ce );

private:
    void buildInterface();
    void defineShortcuts();
    StarModOptions readOptions();
    void initEvents();
    void saveWindowOptions();
    void addSubWindowToWindowList( SubWindow *win, QString filepath );
    void removeSubWindowFromWindowList( SubWindow *win );
    void refreshSubWindowsList();
    SubWindow *getSubWindow( QString windowTitle );
    QActionGroup* addTextEditorTools( QsciScintilla *editor );
    QActionGroup *addJsonEditorTools( QsciScintilla *editor );
    void loadAssetsList();
    void loadProjectDirectory();

    void enableGui( bool enable );
    //Enable or disable features when a project opens (or closes)
    void enableProjectRelatedActions( bool enable );
    QWizard* createStarterWizard();
    QString getAssetsExtractorPath();
    /*QWizardPage *createIntroPage();
    QWizardPage *createStarboundLocationPage();
    QWizardPage *createWorkspacePage();
    QWizardPage *createEndPage();*/
private:
    Ui::MainWindow *ui;
    QMdiArea *m_centralWidget;
    QString m_starboundDir;
    QString m_workspaceDir;
    ProjectManager *m_projectManager;
    SideWidget *m_assetsView;
    //LuaHighlighter *m_hlt;
    AssetsManager *m_assetsManager;
    QMap<SubWindow*, QString> m_subwindowsList;
    //QStringList m_subwindowsList;
    QMenu *m_subWindowsMenu;
    QActionGroup *m_currentActionGroup;
    QProcess *m_starProcess;
    Logger *m_log;
};

#endif // MAINWINDOW_H
