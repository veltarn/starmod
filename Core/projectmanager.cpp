#include "projectmanager.h"

ProjectManager::ProjectManager(QString starPath, QString workspace, QObject *parent ) :
    QObject(parent),
    m_currentProject( NULL ),
    m_workspace( workspace ),
    m_starPath( starPath ),
    m_isOpen( false )
{
}


QDir ProjectManager::getWorkspace() const {
    return m_workspace;
}

bool ProjectManager::newProject() {
    ProjectPropertiesWindow npw( tr( "New Project" ), m_workspace.path(), 0, true );
    if( npw.exec() == QDialog::Accepted ) {
        QString name = npw.getProjectName();
        QString author = npw.getProjectAuthor();
        QString path = npw.getProjectPath();
        QString version = npw.getProjectVersion();
        QString starboundVersion = npw.getStarboundVersion();

        Project *p = new Project( name, author, path, version, starboundVersion );
        if( !createProjectDirectory( p ) ) {
            QMessageBox::critical( 0, tr( "Error" ), tr( "Cannot create project directory, unable to continue" ) );
            delete p;
            return false;
        }

        if( !p->save() ) {
            QMessageBox::warning( 0, tr( "Error" ), tr( "Cannot save project file" ) );
            delete p;
            return false;
        }
        m_currentProject = p;
        m_isOpen = true;

        return true;
    }
    return false;
}

bool ProjectManager::openProject( QString path ) {
    QFileInfo fifo( path );
    if( !fifo.exists() )
        return false;

    closeProject();

    m_currentProject = new Project( this );
    if( m_currentProject->load( path ) ) {
        m_isOpen = true;

        return true;
    }
    return false;
}

Project *ProjectManager::getProject() {
    return m_currentProject;
}

bool ProjectManager::createProjectDirectory( Project *prj ) {
    if( m_workspace.mkdir( prj->projectName() ) ) {
        QDir projectDir( m_workspace );
        projectDir.cd( prj->projectName() );
        projectDir.mkdir( "files" );
        projectDir.mkdir( "build" );
        return true;
    }
    return false;
}

bool ProjectManager::isOpen() {
    return m_isOpen;
}

void ProjectManager::save() {
    if( m_currentProject != NULL ) {
        m_currentProject->save();
    }
}

bool ProjectManager::isProjectOpen( QString projectPath ) {
    if( m_currentProject != NULL ) {
        if( m_currentProject->projectPath() == projectPath ) {
            return true;
        } else {
            return false;
        }
    }

    return false;
}

bool ProjectManager::closeProject() {
    if( m_currentProject != NULL ) {
        if( m_currentProject->isModified() ) {
            int rep = QMessageBox::question( 0, tr( "Project Closing" ), m_currentProject->projectName() + tr( " is about to close but some changes has not been modified.<br />Do you want to close without saving?" ), QMessageBox::Yes | QMessageBox::No );
            if( rep == QMessageBox::Yes ) {
                m_currentProject->close();
                delete m_currentProject;
                m_currentProject = NULL;
                emit projectClosed();

                return true;
            } else {
                return false;
            }
        } else {
            m_currentProject->close();
            delete m_currentProject;
            m_currentProject = NULL;
            emit projectClosed();
            return true;
        }
    }
    return false;
}

bool ProjectManager::saveFile(QString content, QString filepath, bool isStarboundFile ) {
    if( m_currentProject ) {
        //Splitting filepath string in order to build a local Starbound-like tree
        QString starboundAssetsPath;
        if( isStarboundFile ) {
            starboundAssetsPath = Utility::getCommonPath( filepath, "assets/" );

            //starboundAssetsPath = starboundAssetsPath.remove( 0, 1 );
            QString projectDirectory = QFileInfo( m_currentProject->projectPath() ).path();

            //Building project path
            QFileInfo fullPath = projectDirectory + "/files/" + starboundAssetsPath;

            //Creating subdirectories
            QDir projectDir( projectDirectory );
            projectDir.cd( "files" );

            QFileInfo projectAssetsPathInfo( starboundAssetsPath );
            if( projectDir.mkpath( projectAssetsPathInfo.path() ) ) {

                QFile file( fullPath.absoluteFilePath());
                if( !file.open( QIODevice::WriteOnly ) ) {
                    qDebug() << "Cannot open " << fullPath.absoluteFilePath() << ": " << file.errorString() << endl;
                    return false;
                }

                QTextStream stream( &file );
                stream.setCodec( "UTF-8" );
                stream << content;
                if( file.flush() ) {
                    //Adding file to edited files list
                    m_currentProject->addEditedFile( fullPath.absoluteFilePath() );
                    return true;
                }
                else
                    return false;

                //Notify current Project that there is a new file edited

                return true;
            } else {
                return false;
            }
        } else {
            //If it's not a starbound dir, then we will save an existing file
            if( QFile::exists( filepath ) )
                QFile::remove( filepath );

            QFile file( filepath );

            if( !file.open( QIODevice::WriteOnly ) ) {
                qDebug() << "Cannot open " << filepath << ": " << file.errorString() << endl;
                return false;
            }

            QTextStream stream( &file );
            stream.setCodec( "UTF-8" );
            stream << content;

            return file.flush();
        }
    }
    qDebug() << "Cannot save the current file, no project open" << endl;
    return false;
}

bool ProjectManager::isEditedFile( QString filepath ) {
    if( m_currentProject != NULL ) {
        QFileInfo fifo( m_currentProject->projectPath() );
        QString projectFilePath = Utility::convertAppPathToProject( filepath, fifo.path() );
        QStringList editedFiles = m_currentProject->getEditedFiles();

        for( QStringList::iterator it = editedFiles.begin(); it != editedFiles.end(); ++it ) {
            QString file = *it;
            if( file == projectFilePath || filepath == file ) {
                return true;
            }
        }
    }
    return false;
}

QString ProjectManager::getEditedFileFromStarBPath( QString starBFile ) {
    if( m_currentProject ) {
        QStringList relatedFiles = m_currentProject->getEditedFiles();

        foreach( QString file, relatedFiles ) {
            /*QString tmpRelated = Utility::getCommonPath( file, "files" + QString( QDir::separator() ) );
            QString tmpFile = Utility::getCommonPath( starBFile, "assets" + QString( QDir::separator() ) );*/
            QString tmpRelated = Utility::getCommonPath( file, "files/" );
            QString tmpFile = Utility::getCommonPath( starBFile, "assets/" );

            if( tmpFile == tmpRelated ) {
                return file;
            }
        }
    }

    return QString();
}

bool ProjectManager::writeModinfoFile() {
    if( m_currentProject != NULL ) {
        return m_currentProject->writeModinfoFile();
    }
    return false;
}
