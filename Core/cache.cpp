#include "cache.h"


void Cache::init()
{
    m_cachedFiles.clear();

    QFileInfo fifo( "cache" );
    if( !fifo.exists() ) {
        QDir dir;
        dir.mkdir( "cache" );
    }

    updateCachedFiles();
}

void Cache::setFileContent(QString filename, QStringList content)
{
    QString fullName = "cache/" + filename + ".cachefile";
    if( QFile::exists( fullName ) )
        QFile::remove( fullName );

    QFile file( fullName );

    if( !file.open( QIODevice::WriteOnly ) ) {
        qDebug() << "Cannot open" << file.fileName() << ", "  << file.errorString() << endl;
        return;
    }

    QTextStream stream( &file );

    foreach( QString text, content ) {
        stream << text << endl;
    }

    file.flush();
}

void Cache::updateCachedFiles()
{
    QFileInfo fileInfo( "cache/" );
    if( fileInfo.exists() ) {
        QDir cacheDir( "cache/" );
        QStringList files = cacheDir.entryList( QDir::Files | QDir::NoDotAndDotDot );

        foreach( QString file, files ) {
            m_cachedFiles.append( file );
        }
        qDebug() << "Cache directory read, " << m_cachedFiles.size() << " cached files found" << endl;
    }
}

QStringList Cache::getFileContent( QString fileName ) {
    QString fullName = "cache/" + fileName + ".cachefile";
    QFileInfo fifo( fullName );

    if( !fifo.exists() ) {
        qDebug() << fifo.filePath() << " doesn't exists" << endl;
        return QStringList();
    }

    QFile file( fullName );

    if( !file.open( QIODevice::ReadOnly ) ) {
        qDebug() << "Cannot open " << file.fileName() << ", " << file.errorString() << endl;
        return QStringList();
    }

    QTextStream stream( &file );
    stream.setCodec( "UTF-8" );

    QStringList content;
    while( !stream.atEnd() ) {
        content.append( stream.readLine() );
    }

    return content;
}

bool Cache::exists(QString fileName)
{
    QFileInfo fifo( "cache/" + fileName + ".cachefile" );

    return fifo.exists();
}

bool Cache::remove(QString filename)
{
    QString fullPath = "cache/" + filename + ".cachefile";
    if( !QFile::exists( fullPath ) )
        return false;

    QFile::remove( fullPath );
    return true;
}

void Cache::purgeCache() {
    foreach( QString cacheFile, m_cachedFiles ) {
        remove( cacheFile );
    }
    m_cachedFiles.clear();
}
