#include "starvariant.h"

/*StarVariant::StarVariant() : m_isNull( true ), m_fileType( INVALID )
{
}

StarVariant::StarVariant(const StarVariant &orig)
{
    m_isNull = orig.m_isNull;

    if( !m_isNull ) {
        m_fileType = orig.m_fileType;
        m_data = orig.m_data;
    } else {
        m_fileType = INVALID;
    }
}

StarVariant::StarVariant(StarItemPtr item)
{
    m_fileType = ITEM;
    m_data.item = item;
    m_isNull = false;
}

StarVariant::StarVariant( StarProjectilePtr proj )
{
    m_fileType = PROJECTILE;
    m_data.projectile = proj;
    m_isNull = false;
}

StarVariant::StarVariant( StatusEffectPtr statusEffect)
{
    m_fileType = STATUS_EFFECT;
    m_data.statusEffect = statusEffect;
    m_isNull = false;
}

StarVariant::StarVariant(StarRecipePtr recipe)
{
    m_fileType = RECIPE;
    m_data.recipe = recipe;
    m_isNull = false;
}

StarVariant::~StarVariant()
{

}

bool StarVariant::isNull() const
{
    return m_isNull;
}

StarItemPtr StarVariant::toStarItem()
{
    if( m_isNull ) {
        StarItemPtr ptr;
        return ptr;
    }

    if( m_fileType == ITEM ) {
        return m_data.item;
    } else {
        return StarItemPtr;
    }
}

StarProjectilePtr StarVariant::toStarProjectile()
{
    if( m_isNull ) {
        return StarProjectilePtr;
    }

    if( m_fileType == PROJECTILE ) {
        return m_data.projectile;
    } else {
        return StarProjectilePtr;
    }
}

StatusEffectPtr StarVariant::toStatusEffect()
{
    if( m_isNull ) {
        return StatusEffectPtr;
    }

    if( m_fileType == STATUS_EFFECT ) {
        return m_data.statusEffect;
    } else {
        return StatusEffectPtr;
    }
}

StarRecipePtr StarVariant::toStarRecipe()
{
    if( m_isNull ) {
        return StarRecipePtr;
    }

    if( m_fileType == RECIPE ) {
        return m_data.recipe;
    } else {
        return StarRecipePtr;
    }
}

bool StarVariant::operator==(const StarVariant &b)
{
    if( m_isNull && b.m_isNull )
        return true;

    if( m_fileType == b.m_fileType ) {
        switch( m_fileType ) {
            case ITEM:
                if( m_data.item == b.m_data.item )
                    return true;
                break;

            case PROJECTILE:
                if( m_data.projectile == b.m_data.projectile )
                    return true;
                break;

            case STATUS_EFFECT:
                if( m_data.statusEffect == b.m_data.statusEffect )
                    return true;
                break;

            case RECIPE:
                if( m_data.recipe == b.m_data.recipe )
                    return true;
                break;

            case INVALID:
                return false;
                break;

            default:
                return false;
                break;
        }

        return false;
    } else {
        return false;
    }
}

bool StarVariant::operator!=(const StarVariant &b)
{
    if( m_isNull && b.m_isNull )
        return false;

    if( m_fileType == b.m_fileType ) {
        switch( m_fileType ) {
            case ITEM:
                if( m_data.item == b.m_data.item )
                    return false;
                break;

            case PROJECTILE:
                if( m_data.projectile == b.m_data.projectile )
                    return false;
                break;

            case STATUS_EFFECT:
                if( m_data.statusEffect == b.m_data.statusEffect )
                    return false;
                break;

            case RECIPE:
                if( m_data.recipe == b.m_data.recipe )
                    return false;
                break;

            default:
                return true;
                break;
        }

        return true;
    } else {
        return true;
    }
}
*/
