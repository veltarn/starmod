#ifndef ZIP_H
#define ZIP_H

#include <iostream>
#include <map>
#include <cmath>
#include <string>
#include <QObject>
#include <QApplication>
#include <QString>
#include <QFile>
#include <QDir>
#include <QProgressDialog>
#include <QMap>
#include <QDebug>

#define BUFFER_SIZE static_cast<quint64>(1024)

typedef unsigned char uint8;

class Zip : public QObject
{
    Q_OBJECT
public:
    Zip( QObject *parent = 0 );
    void addFile( QString file, QString nameInArchive );
    void removeFile( QString file );
    QMap<QString, QString> filesList() const;
    bool createArchive( QString archiveName );
private:
    //      source,         destination
   QMap< QString, QString> m_filePaths;
};

#endif // ZIP_H
