#ifndef STYLESHEETMANAGER_H
#define STYLESHEETMANAGER_H

#define BASE_CSS_PATH QString( "assets/css" )

#include <QDebug>
#include <QFile>
#include <QString>

class StylesheetManager
{
public:
    StylesheetManager();

    static QString getStylesheet( QString filename );
};

#endif // STYLESHEETMANAGER_H
