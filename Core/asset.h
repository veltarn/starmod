#ifndef ASSET_H
#define ASSET_H

#include <QDebug>
#include <QVariant>
#include <QSharedPointer>

class StarItem;
class StarWeapon;
class StarConsumable;
class StarMiningTool;
class StarFlashlight;
class StarBeamaxe;
class StarMatItem;
class StarInstrument;
class StarSword;
class StarThrown;
class StarWorldObject;
class StarCoin;
class StarProjectile;
class StarRecipe;
class StatusEffect;

union StarVariant {
    StarItem *item;
    StarProjectile *projectile;
    StarRecipe *recipe;
    StatusEffect *statusEffect;
};

class Asset
{
public:
    enum FileType {
        Item, Projectile, Recipe, StatusEffectEnum, Invalid
    };

public:
    Asset();
    ~Asset();
    Asset( StarItem* item );
    Asset( StarWeapon* item );
    Asset( StarConsumable* item );
    Asset( StarMiningTool *item );
    Asset( StarFlashlight* item );
    Asset( StarBeamaxe* item );
    Asset( StarMatItem* item );
    Asset( StarInstrument *item );
    Asset( StarSword* item );
    Asset( StarThrown* item );
    Asset( StarCoin *item );
    Asset( StarWorldObject* item );
    Asset( StarProjectile *proj );
    Asset( StatusEffect *effect );

    StarItem* asStarItem();
    StarProjectile *asStarProjectile();

    bool isNull() const;
    /**
     * @brief clearAsset
     * Delete the pointer
     */
    void clearAsset();

    Asset &operator=( Asset const &copy );
    int fileType() const;

private:
    int m_fileType;
    //QVariant m_data;
    StarVariant m_data;
    bool m_isNull;
};

typedef QSharedPointer< Asset > AssetPtr;

#endif // ASSET_H
