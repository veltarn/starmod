#ifndef PROJECTMANAGER_H
#define PROJECTMANAGER_H

#include <QObject>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QStandardPaths>
#include <QString>
#include "../GUI/projectpropertieswindow.h"
#include "project.h"

class ProjectManager : public QObject
{
    Q_OBJECT
public:
    explicit ProjectManager( QString starPath, QString workspace, QObject *parent = 0 );
    
    QDir getWorkspace() const;

    bool newProject();
    bool openProject( QString path );
    void save();
    /**
     * @brief Save a single file and notify the opened project of the operation (add file to edited files list)
     * @param filepath Path of the file to save
     * @return true if the file has correctly been saved, false otherwise
     */
    bool saveFile( QString content, QString filepath, bool isStarboundFile = true );
    bool closeProject();

    Project *getProject();
    bool isProjectOpen( QString prj );

    bool isOpen();
    bool isEditedFile( QString filepath );

    /**
     * @brief Get (if exists) the edited file from the starbound's tree path
     * @param starBFile
     * @return Edited file
     */
    QString getEditedFileFromStarBPath( QString starBFile );

    bool writeModinfoFile();
signals:
    void projectClosed();
public slots:

private:
    bool createProjectDirectory( Project *prj );
private:
    Project *m_currentProject;

    QDir m_workspace;
    QDir m_starPath;

    bool m_isOpen;
    
};

#endif // PROJECTMANAGER_H
