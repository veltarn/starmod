#ifndef CACHE_H
#define CACHE_H

#include <QDebug>
#include <QDir>
#include <QString>
#include <QStringList>
#include <QTextStream>
#include <QFile>
#include <QTemporaryFile>
#include "singleton.hpp"

class Cache : public Singleton<Cache>
{
    friend class Singleton<Cache>;
public:
    void init();
    void setFileContent( QString filename, QStringList content );
    /**
     * @brief Read every line of the file and returns a list of those lines
     * @param fileName
     * @return
     */
    QStringList getFileContent( QString fileName );
    bool exists( QString fileName );

    bool remove( QString filename );
    //Clear all of files;
    void purgeCache();
private:
    void updateCachedFiles();
private:
  QStringList m_cachedFiles;
};

#endif // CACHE_H
