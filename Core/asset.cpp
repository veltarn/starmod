#include "asset.h"
#include "../Starbound/Items/staritem.h"
#include "../Starbound/Items/starweapon.h"
#include "../Starbound/Items/starconsumable.h"
#include "../Starbound/Items/starminingtool.h"
#include "../Starbound/Items/starflashlight.h"
#include "../Starbound/Items/starbeamaxe.h"
#include "../Starbound/Items/starmatitem.h"
#include "../Starbound/Items/starinstrument.h"
#include "../Starbound/Items/starsword.h"
#include "../Starbound/Items/starthrown.h"
#include "../Starbound/Items/starcoin.h"
#include "../Starbound/Items/starworldobject.h"
#include "../Starbound/starprojectile.h"
#include "../Starbound/starrecipe.h"
#include "../Starbound/statuseffect.h"


Asset::Asset() : m_fileType( Invalid ), m_isNull( true )
{
}

Asset::~Asset()
{
    clearAsset();
}

Asset::Asset(StarItem *item) : m_fileType( Item ), m_isNull( false )
{
    if( item == NULL ) {
        m_isNull = true;
    }
    m_data.item = item;
}

Asset::Asset(StarWeapon *item) : m_fileType( Item ), m_isNull( false )
{
    if( item == NULL ) {
        m_isNull = true;
    }
    m_data.item = item;
}

Asset::Asset(StarConsumable *item) : m_fileType( Item ), m_isNull( false )
{
    if( item == NULL ) {
        m_isNull = true;
    }
    m_data.item = item;
}

Asset::Asset(StarMiningTool* item) : m_fileType( Item ), m_isNull( false )
{
    if( item == NULL ) {
        m_isNull = true;
    }
    m_data.item = item;
}

Asset::Asset(StarFlashlight *item) : m_fileType( Item ), m_isNull( false )
{
    if( item == NULL ) {
        m_isNull = true;
    }

    m_data.item = item;
}

Asset::Asset(StarBeamaxe *item) : m_fileType( Item ), m_isNull( false )
{
    if( item == NULL ) {
        m_isNull = true;
    }

    m_data.item = item;
}

Asset::Asset(StarMatItem *item) : m_fileType( Item ), m_isNull( false )
{
    if( item == NULL ) {
        m_isNull = true;
    }
    m_data.item = item;
}

Asset::Asset(StarInstrument* item) : m_fileType( Item ), m_isNull( false )
{
   if( item == NULL ) {
       m_isNull = true;
   }
   m_data.item = item;
}

Asset::Asset(StarSword *item) : m_fileType( Item ), m_isNull( false )
{
    if( item == NULL ) {
        m_isNull = true;
    }
    m_data.item = item;
}

Asset::Asset(StarThrown *item) : m_fileType( Item ), m_isNull( false )
{
    if( item == NULL ) {
        m_isNull = true;
    }
    m_data.item = item;
}

Asset::Asset(StarCoin *item) : m_fileType( Item ), m_isNull( false )
{
    if( item == NULL )
        m_isNull = false;

    m_data.item = item;
}

Asset::Asset(StarWorldObject *item) : m_fileType( Item ), m_isNull( false )
{
    if( item == NULL ) {
        m_isNull = true;
    }
    m_data.item = item;
}

Asset::Asset(StarProjectile *proj) : m_fileType( Projectile ), m_isNull( false )
{
    if( proj == NULL )
        m_isNull = true;

    m_data.projectile = proj;
}

Asset::Asset( StatusEffect *effect) : m_fileType( StatusEffectEnum ), m_isNull( false )
{
    if( effect == NULL )
        m_isNull = true;

    m_data.statusEffect = effect;
}

StarItem* Asset::asStarItem()
{
    if( !m_isNull && m_fileType == Item ) {
        return m_data.item;
    } else {
        return NULL;
    }
}

StarProjectile *Asset::asStarProjectile()
{
    if( !m_isNull && m_fileType == Projectile ) {
        return m_data.projectile;
    } else {
        return NULL;
    }
}
bool Asset::isNull() const
{
    return m_isNull;
}

void Asset::clearAsset()
{
    int sum = 0;
    if( !m_isNull ) {
        switch( m_fileType ) {
            case Item:
                sum = sizeof( *m_data.item );
                delete m_data.item;
            break;

            case Projectile:
                sum = sizeof( *m_data.projectile );
                delete m_data.projectile;
            break;

            case Recipe:
                sum = sizeof( *m_data.recipe );
                delete m_data.recipe;
            break;

            case StatusEffectEnum:
                sum = sizeof( *m_data.statusEffect );
                delete m_data.statusEffect;
            break;

            default:
                qDebug() << "Cannot delete data of unknown type" << endl
                         << "Possibility of memory leak!" << endl;
            break;
        }
        //qDebug() << "Deleted " << sum << " bytes " << endl;
    }
}

Asset &Asset::operator=(const Asset &copy)
{
    m_fileType = copy.m_fileType;
    m_isNull = copy.m_isNull;
    m_data = copy.m_data;

    return *this;
}

int Asset::fileType() const
{
    return m_fileType;
}


