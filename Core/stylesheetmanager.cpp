#include "stylesheetmanager.h"

StylesheetManager::StylesheetManager()
{
}

QString StylesheetManager::getStylesheet(QString filename)
{
    QString fullPath = BASE_CSS_PATH + "/" + filename;
    QFile file( fullPath );

    if( !file.exists() ) {
        qWarning() << "File" << file.fileName() << " doesn't exists";
        return QString();
    }

    if( !file.open( QIODevice::ReadOnly ) ) {
        qWarning() << "File" << file.fileName() << " cannot be opened: " << file.errorString();
        return QString();
    }

    QString text = file.readAll();
    return text;
}
