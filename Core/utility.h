#ifndef UTILITY_H
#define UTILITY_H

#include <iostream>
#include <algorithm>
#include <QStringList>
#include <QDir>
#include <QFile>
#include <QDebug>
#include <QColor>
#include "../Starbound/starrecipe.h"

class Utility
{
public:
    Utility();
    static QString  qColorToQString( QColor color );
    static QStringList reverseArray( QStringList list );

    static bool exists( const QString &elmt, const QStringList &list ) {
        for( QStringList::const_iterator it = list.begin(); it != list.end(); ++it ) {
            if( *it == elmt ) {
                return true;
            }
        }
        return false;
    }

    static bool exists( const QString &input, const QList< Input > &list ) {
        for( QList< Input >::const_iterator it = list.begin(); it != list.end(); ++it ) {
            if( it->item == input ) {
                return true;
            }
        }
        return false;
    }

    static bool exists( const Input &input, const QList< Input > &list ) {
        for( QList< Input >::const_iterator it = list.begin(); it != list.end(); ++it ) {
            if( it->item == input.item ) {
                return true;
            }
        }
        return false;
    }

    /**
     * @brief This method converts a path inside Starbound assets into a project path
     * This method converts a path inside Starbound assets into a project path
     * eg: /home/Dikavon/Starbound/assets/assets.config will be converted in /home/Dikavon/StarModProjects/MyProject/files/assets.config
     * @param assetAppPath Starbound related file path
     * @param projectPath Path of the project
     * @return Converted path
     */
    static QString convertAppPathToProject( QString assetAppPath, QString projectPath );

    /**
     * @brief Get common path between Starbound tree and project tree
     * Get common path between Starbound tree and project tree
     * eg: /home/Dikavon/Starbound/assets/monsters/flocking.lua will be monsters/flocking.lua
     * @param path Path to "commonise"
     * @return Commonised path
     */
    static QString getCommonPath( QString path, QString separator );

    static void recurseAddDir( QDir d, QStringList &list, QStringList extensionFilter = QStringList(), QStringList excludeExtensions = QStringList() );


};

#endif // UTILITY_H
