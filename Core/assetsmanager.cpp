#include "assetsmanager.h"

AssetsManager::AssetsManager( QString starboundAssetsPath, QString starboundPath, QObject *parent) :
    QObject(parent),
  m_starboundAssetsPath( starboundAssetsPath ),
  m_starboundPath( starboundPath ),
  m_assetsExtensions( QStringList() ),
  m_itemsListModel( NULL )
{
    m_assetsExtensions << "item"
                       << "gun"
                       << "consumable"
                       << "miningtool"
                       << "flashlight"
                       << "beamaxe"
                       << "matitem"
                       << "instrument"
                       << "sword"
                       << "thrownitem"
                       << "object"
                       << "coinitem"
                       << "projectile"
                       << "statuseffect";

    m_jsonFileExtensions << "config"
                         << "item"
                         << "gun"
                         << "consumable"
                         << "miningtool"
                         << "flashlight"
                         << "beamaxe"
                         << "matitem"
                         << "instrument"
                         << "sword"
                         << "thrownitem"
                         << "object"
                         << "coinitem"
                         << "projectile"
                         << "statuseffect";

    m_assetsExtensions.sort();

}

AssetsManager::~AssetsManager() {
    clearAssetsList();
}

FileType AssetsManager::getFileType( QString filepath ) {
    QFileInfo fifo( filepath );

    if( fifo.suffix().toLower() == "png" || fifo.suffix().toLower() == "jpg" ) {
        return Image;
    } else if( fifo.suffix().toLower() == "lua" ) {
        return Lua;
    }

    if( Utility::exists( fifo.suffix().toLower(), m_jsonFileExtensions ) ) {
        return Json;
    } else {
        //If it's neither Image or Lua or known Json, then we try to determine if the file is a Json File
        QFile file( filepath );
        if( !file.open( QIODevice::ReadOnly ) ) {
            QString msg =  tr( "Cannot open" ) + " " + file.fileName() + ", " + file.errorString() ;
            Logger::getInstance()->addEntry( Warning, msg );
            QMessageBox::warning( 0, tr( "Cannot open file" ), msg );
            return Unkwown;
        }

        QTextStream stream( &file );
        QString begChar = stream.read( 1 );
        stream.seek( file.size() - 2 );
        QString endChar = stream.read( 1 );

        //Maybe too easy deduction
        if( begChar == "{" /*&& endChar == "}"*/ ) {
            return Json;
        }
    }

    return Text;
}


void AssetsManager::parseAssets( ProgressSplashScreen *splash, bool createProgressDialog ) {
    Cache *cache = Cache::getInstance();
    QStringList craftingFilter; //!< Used to create a cache file

    QStringList fileList;

    if( !cache->exists( "assets_location" ) ) {
        QDir d( m_starboundAssetsPath );
        Utility::recurseAddDir( d, fileList, m_assetsExtensions );
        cache->setFileContent( "assets_location", fileList );
    } else {
        fileList = cache->getFileContent( "assets_location" );
    }
    splash->setMinimum( 0 );
    splash->setMaximum( fileList.size() );
    //qDebug() << fileList << endl;
    QProgressDialog *dialog = NULL;
    if( createProgressDialog && splash == NULL ) {
        dialog = new QProgressDialog;
        dialog->setMinimum( 0 );
        dialog->setCancelButton( 0 );
        dialog->setMaximum( fileList.size() );
        dialog->setWindowTitle( tr( "Loading assets" ) );
        dialog->setMinimumDuration( 1000 );
        dialog->show();
    }

    AssetsParser parser;
    int sum = 0;
    int i = 0;
    foreach( QString file, fileList ) {        
        QFileInfo fifo( file );
        //QFile starFile( file );

        if( splash != NULL ) {
            /*splash->clearMessage();
            splash->showMessage( tr( "Loading" ) + ": " + fifo.fileName(), Qt::AlignBottom, Qt::white);*/
            QCoreApplication::processEvents();
        } else if( createProgressDialog ) {
            dialog->setValue( i + 1 );
            dialog->setLabelText( tr( "Loading" ) + ": " + fifo.fileName() );
            QCoreApplication::processEvents();
        }

        JsonValidator validator( file );
        if( !validator.validate() ) {
            qDebug() << "Cannot valid" << file << " error in line " << validator.lastError().line << ":" << validator.lastError().col << endl;
        }

        try {
            AssetPtr ressource = parser.parseAsset( file );

            m_data.append( ressource );


        } catch( AssetsParserException &e ) {
            LogType typeError;
            switch( e.errorType() ) {
                case AssetsParserException::MINOR: typeError = Warning;
                case AssetsParserException::WARNING: typeError = Warning;
                case AssetsParserException::ERROR: typeError = Error;
                default: typeError = Info;
            }

            Logger::getInstance()->addEntry( typeError, e.what() );
        }

        i++;
        splash->setValue( i );
    }

    QString physicsFile = m_starboundAssetsPath + "/projectiles/physics.config";

    try {
        m_physicsList = parser.parsePhysicsFile( physicsFile );
    } catch( FileException &e ) {
        QMessageBox::warning( 0, tr( "File parsing error" ), e.what() );
    } catch( AssetsParserException &e ) {
        switch( e.errorType() ) {
            case AssetsParserException::WARNING:
            case AssetsParserException::MINOR:
                qDebug() << QString( e.what() ) << endl;
            break;

            case AssetsParserException::ERROR:
                QMessageBox::critical( 0, tr( "Error" ), e.what() );
            break;

            default:
                QMessageBox::critical( 0, tr( "Error" ), e.what() );
            break;
        }
    }

    /*if( !cache->exists( "crafting_filter" ) ) {
        cache->setFileContent( "crafting_filter", craftingFilter );
    } else {
        QStringList cacheList = cache->getFileContent( "crafting_filter" );
        if( craftingFilter != cacheList ) {
            cache->remove( "crafting_filter" );
            cache->setFileContent( "crafting_filter", craftingFilter );
        }
    }*/

}

QString AssetsManager::assetsPath() const {
    return m_starboundAssetsPath;
}

QStringList AssetsManager::assetsExtensions() const {
    return m_assetsExtensions;
}

StarItem *AssetsManager::getItem(int row)
{
    if( row < m_data.size() ) {
        if( m_data[row]->fileType() == Asset::Item ) {
            return m_data[row]->asStarItem();
        } else {
            return NULL;
        }
    }

    return NULL;
}

StarItem *AssetsManager::getItem( QString itemName ) {
    for( QList< AssetPtr >::iterator it = m_data.begin(); it != m_data.end(); ++it ) {
        AssetPtr ptr = *it;
        if( !ptr.isNull() ) {
            if( ptr->fileType() == Asset::Item ) {
                StarItem *item = ptr->asStarItem();
                if( item->itemName() == itemName ) {
                    return item;
                }
            }
        }
    }

    return NULL;
}

int AssetsManager::itemsSize() {
    return m_data.size();
}

QSortFilterProxyModel *AssetsManager::createObjectsModel() {
    qDebug() << m_data.size() << endl;
    //if( m_itemsListModel == NULL ) {
        if( m_data.size() > 0 ) {
            QStandardItemModel *model = new QStandardItemModel;
            QSortFilterProxyModel *proxy = new QSortFilterProxyModel;
            proxy->setSourceModel( model );
            proxy->setFilterCaseSensitivity( Qt::CaseInsensitive );

            for( QList< AssetPtr >::iterator it = m_data.begin(); it != m_data.end(); ++it ) {
                AssetPtr ptr = *it;
                if( !ptr.isNull() ) {
                    if( ptr->fileType() == Asset::Item ) {
                        StarItem *starItem = ptr->asStarItem();
                        if( starItem->idObject() == "coinitem" )
                            int a = 42;
                        QFileInfo starInfo( starItem->itemLocation() );
                        //qDebug() << starItem << endl;
                        QString name = "";

                        if( starItem->shortDescription() != "" ) {
                            name = starItem->shortDescription();
                        } else {
                            name = starItem->itemName();
                        }

                        QIcon icon( QPixmap( starInfo.path() + "/" + starItem->inventoryIcon() ) );
                        QStandardItem *item = new QStandardItem( icon, name );
                        item->setData( starItem->itemName() );

                        model->appendRow( item );
                    }
                }
            }
            m_itemsListModel = proxy;
            return m_itemsListModel;
        } else {
            return NULL;
        }
   /* } else {
        return m_itemsListModel;
    }*/
}


QString AssetsManager::starboundPath() const
{
    return m_starboundPath;
}

void AssetsManager::setStarboundPath(const QString &starboundPath)
{
    m_starboundPath = starboundPath;
}

QStringList AssetsManager::getRecipesFilters()
{
    QStringList list;
    for( QList<AssetPtr>::iterator it = m_data.begin(); it != m_data.end(); ++it ) {
        AssetPtr ptr = *it;
        if( ptr.isNull() ) {
            if( ptr->fileType() == Asset::Item ) {
                StarItem *baseItem = ptr->asStarItem();

                if( baseItem->idObject() == "object" ) {
                    StarWorldObject *worldObject = dynamic_cast< StarWorldObject*>( baseItem );

                    if( worldObject->hasProperty( "interactData" ) ) {
                        QStringList filters = worldObject->interactData().filter;
                        foreach( QString filter, filters ) {
                            if( !Utility::exists( filter, list ) ) {
                                list.append( filter );
                            }
                        }
                    }
                }
            }
        }
    }
    Cache *cache = Cache::getInstance();
    if( !cache->exists( "crafting_filter" ) ) {
        cache->setFileContent( "crafting_filter", list );
    }

    return list;
}

bool AssetsManager::createItem(QString modPath, StarItem *item)
{
    QStringList files;
    // for example items/weapons/myWeapon, the s is automatically added
    QString itemPath = "items/" + item->idObject() + "s/" + item->itemName();

    QDir modDirectory( modPath );
    if( !modDirectory.mkpath( itemPath ) ) {
        qDebug() << "Cannot create item directory for" << item->itemName();
        qDebug() << "Unable to save" << endl;
        return false;
    }
    //Building item path
    QString absoluteItemPath = modPath + "/" + itemPath + "/" + item->itemName() + "." + item->filesType();

    QFile itemFile( absoluteItemPath );

    //Removing
    if( itemFile.exists() )
        itemFile.remove();

    if( !itemFile.open( QIODevice::WriteOnly ) ) {
        qDebug() << "Cannot open " << itemFile.fileName() << ", " << itemFile.errorString() << endl;
        return false;
    }
    QJsonDocument jsonFile;

    files = item->save( modPath, itemPath, jsonFile );

    QTextStream stream( &itemFile );
    stream.setCodec( "UTF-8" );
    stream << jsonFile.toJson();

    itemFile.flush();
    itemFile.close();

    files.append( itemFile.fileName() );

    emit newProjectFiles( files );

    return true;
}

void AssetsManager::parseFile(QFile &file)
{
}

QList<StarPhysics> AssetsManager::physicsList() const
{
    return m_physicsList;
}

QList<AssetPtr> AssetsManager::assetsList()
{
    return m_data;
}



QStringList AssetsManager::getItemsListByCateory( QString category ) {
    QStringList list;

    if( category == "" ) {
        return QStringList();
    }

    foreach( AssetPtr ptr, m_data ) {
        if( !ptr.isNull() ) {
            if( ptr->fileType() == Asset::Item ) {
                StarItem *item = ptr->asStarItem();
                QString extensionsItem = item->filesType();
                QStringList listExt = extensionsItem.split( "," );

                foreach( QString ext, listExt ) {
                    if( ext == category ) {
                        if( item->shortDescription() == "" ) {
                            list.append( item->itemName() );
                        } else {
                            list.append( item->shortDescription() );
                        }
                        break;
                    }
                }
            }
        }
    }

    return list;
}

void AssetsManager::clearAssetsList() {
    /*int sum = 0;
    foreach( AssetPtr ptr, m_data ) {
        sum += sizeof( *item );
        delete item;
    }*/

    m_data.clear();
    //qDebug() << "Cleared " << sum << " bytes" << endl;
}
