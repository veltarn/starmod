#include "assetsparser.h"

AssetsParser::AssetsParser()
{
}


AssetPtr AssetsParser::parseAsset(QString &file)
{
    QFileInfo fifo( file );
    QFile starFile( file );

    if( !starFile.open( QIODevice::ReadOnly ) ) {
        throw AssetsParserException( "Cannot open " + starFile.fileName() + ", " + starFile.errorString(), AssetsParserException::ERROR );
    }


    QJsonDocument doc;
    QJsonParseError error;

    JsonValidator validator( file );

    if( !validator.validate() ) {
        QByteArray data = validator.correctJson();
        doc = QJsonDocument::fromJson( data, &error );
    } else {
        doc = QJsonDocument::fromJson( starFile.readAll(), &error );
    }

    //doc = QJsonDocument::fromBinaryData( starFile.readAll(), QJsonDocument::BypassValidation );

    if( error.error == QJsonParseError::NoError ) {

        QJsonObject root = doc.object();

        if( fifo.suffix() == "item" ) {
            StarItem* item = new StarItem;
            item->setItemLocation( fifo.absoluteFilePath() );
            populateItem( item, root );
            //sum += sizeof( *item );

            AssetPtr asset( new Asset( item ) );
            //asset.setFile( );
            //m_data.append( item );
            //qDebug() << item  << endl;
            return asset;
        } else if( fifo.suffix() == "gun" ) {
            StarWeapon* item = new StarWeapon;
            item->setItemLocation( fifo.absoluteFilePath() );

            populateGun( item, root );
            /*sum += sizeof( *item );
            m_data.append( item );*/

            AssetPtr asset( new Asset( item ) );
            //asset.setFile( );
            //m_data.append( item );
            //qDebug() << item  << endl;
            return asset;
        } else if( fifo.suffix() == "consumable" ) {
            StarConsumable* item = new StarConsumable;
            item->setItemLocation( fifo.absoluteFilePath() );

            populateConsumable( item, root );
            /*sum += sizeof( *item );
            m_data.append( item );*/

            AssetPtr asset( new Asset( item ) );
            //asset.setFile( );
            //m_data.append( item );
            //qDebug() << item  << endl;
            return asset;
        } else if( fifo.suffix() == "miningtool" ) {
            StarMiningTool* tool = new StarMiningTool;
            tool->setItemLocation( fifo.absoluteFilePath() );

            populateMiningTool( tool, root );
            /*sum += sizeof( *tool );
            m_data.append( tool );*/

            AssetPtr asset( new Asset( tool ) );
            //asset.setFile( );
            //m_data.append( item );
            //qDebug() << item  << endl;
            return asset;
        } else if( fifo.suffix() == "flashlight" ) {
            StarFlashlight* item = new StarFlashlight;
            item->setItemLocation( fifo.absoluteFilePath() );

            populateFlashlight( item, root );
            /*sum += sizeof( *item );
            m_data.append( item );*/

            AssetPtr asset( new Asset( item ) );
            //asset.setFile( );
            //m_data.append( item );
            //qDebug() << item  << endl;
            return asset;
        } else if( fifo.suffix() == "beamaxe" ) {
            StarBeamaxe* item = new StarBeamaxe;
            item->setItemLocation( fifo.absoluteFilePath() );

            populateBeamaxe( item, root );
            /*sum += sizeof( *item );
            m_data.append( item );*/

            AssetPtr asset( new Asset( item ) );
            //asset.setFile( );
            //m_data.append( item );
            //qDebug() << item  << endl;
            return asset;
        } else if( fifo.suffix() == "matitem" ) {
            StarMatItem* matItem = new StarMatItem;
            matItem->setItemLocation( fifo.absoluteFilePath() );

            populateMatItem( matItem, root );

            /*sum += sizeof( *matItem );
            m_data.append( matItem );*/

            AssetPtr asset( new Asset( matItem ) );
            //asset.setFile( );
            //m_data.append( item );
            //qDebug() << item  << endl;
            return asset;
        } else if( fifo.suffix() == "instrument" ) {
            StarInstrument* instrument = new StarInstrument;
            instrument->setItemLocation( fifo.absoluteFilePath() );


            populateInstrument( instrument, root );

            /*sum += sizeof( *instrument );
            m_data.append( instrument );*/

            AssetPtr asset( new Asset( instrument ) );
            //asset.setFile( );
            //m_data.append( item );
            //qDebug() << item  << endl;
            return asset;
        } else if( fifo.suffix() == "sword" ) {
            /*
             * PARSE ERROR dans les fichiers Json! (Commentaires...)
             */
            StarSword* sword = new StarSword;
            sword->setItemLocation( fifo.absoluteFilePath() );

            populateSword( sword, root );

            /*sum += sizeof( *sword );
            m_data.append( sword );*/

            AssetPtr asset( new Asset( sword ) );
            //asset.setFile( );
            //m_data.append( item );
            //qDebug() << item  << endl;
            return asset;
        } else if( fifo.suffix() == "thrownitem" ) {
            StarThrown* item = new StarThrown;
            item->setItemLocation( fifo.absoluteFilePath() );

            populateThrown( item, root );

            /*sum += sizeof( *item );
            m_data.append( item );*/
            AssetPtr asset( new Asset( item ) );
            return asset;
        } else if( fifo.suffix() == "object" ) {
            StarWorldObject* item = new StarWorldObject;
            item->setItemLocation( fifo.absoluteFilePath() );

            populateWorldObject( item, root );

            /*sum += sizeof( *item );
            m_data.append( item );*/

            AssetPtr asset( new Asset( item ) );
            //asset.setFile( );
            //m_data.append( item );
            //qDebug() << item  << endl;
            return asset;
        } else if( fifo.suffix() == "projectile" ) {
            StarProjectile *proj = new StarProjectile;
            proj->setProjectileLocation( fifo.absoluteFilePath() );

            populateProjectile( proj, root );

            AssetPtr asset( new Asset( proj ) );
            return asset;
        } else if( fifo.suffix() == "coinitem" ) {
            StarCoin *coin = new StarCoin;
            coin->setItemLocation( fifo.absoluteFilePath() );

            populateStarCoin( coin, root );

            AssetPtr asset( new Asset( coin ) );
            return asset;
        } else if( fifo.suffix() == "statuseffect" ) {
            StatusEffect *effect = populateStatusEffect( root );

            AssetPtr asset( new Asset( effect ) );
            return asset;
        }

        QString msg = QObject::tr( "The parsed file is not a Starbound asset" );
        throw AssetsParserException( msg, AssetsParserException::MINOR );
    } else {
        QString msg = QObject::tr( "Cannot parse Json file" );
        throw AssetsParserException (  msg + ", " + error.errorString(), AssetsParserException::WARNING );
    }
}

QList<StarPhysics> AssetsParser::parsePhysicsFile(QString physicsFile)
{
    QList< StarPhysics > physList;
    if( !QFile::exists( physicsFile ) ) {
        throw FileException( QObject::tr( "Cannot open" ) + " " + physicsFile + ", " + QObject::tr( "no such file or directory" ) );
    }



    QJsonParseError error;
    QJsonDocument doc;
    JsonValidator validator( physicsFile );
    if( !validator.validate() ) {
        qDebug() << "Cannot validate physics file. " << validator.lastError().errorString << endl
                 << ", " << validator.lastError().line << ":" << validator.lastError().col << endl;
        qDebug() << "Trying to correct this file..." << endl;
        QByteArray arr = validator.correctJson();

        doc = QJsonDocument::fromJson( arr, &error );
    } else {
        QFile file( physicsFile );

        if( !file.open( QIODevice::ReadOnly ) ) {
            throw FileException( QObject::tr( "Cannot open" ) + " " + file.fileName() + ", " + file.errorString() );
        }
        doc = QJsonDocument::fromJson( file.readAll(), &error );
    }

    if( error.error == QJsonParseError::NoError ) {
        QJsonObject root = doc.object();

        for( QJsonObject::iterator it = root.begin(); it != root.end(); ++it ) {
            QJsonObject obj = it.value().toObject();
            StarPhysics phys;

            populatePhysics( phys, it.key(), obj );

            physList.append( phys );
        }

    } else {
        throw AssetsParserException( QObject::tr( "Cannot parse" ) + " " + physicsFile + ", " + error.errorString(), AssetsParserException::WARNING );
    }

    return physList;
}


void AssetsParser::populateItem(StarItem *item, QJsonObject &rootObject ) {
    QJsonValue jsonItemName =           rootObject[ "itemName" ];
    QJsonValue jsonShortDescription =   rootObject[ "shortdescription" ];
    QJsonValue description =            rootObject[ "description" ];
    QJsonValue rarity =                 rootObject[ "rarity" ];
    QJsonValue inventoryIcon =          rootObject[ "inventoryIcon" ];
    QJsonValue maxStack =               rootObject[ "maxStack" ];
    QJsonValue fuelAmount =             rootObject[ "fuelAmount" ];
    QJsonValue avianDescription =       rootObject[ "avianDescription" ];
    QJsonValue apexDescription =        rootObject[ "apexDescription" ];
    QJsonValue floranDescription =      rootObject[ "floranDescription" ];
    QJsonValue glitchDescription =      rootObject[ "glitchDescription" ];
    QJsonValue humanDescription =       rootObject[ "humanDescription" ];
    QJsonValue hylotlDescription =      rootObject[ "hylotlDescription" ];
    QJsonValue learnBlueprintOnPickup = rootObject[ "learnBlueprintOnPickup" ];
    QJsonValue effects =                rootObject[ "effects" ];

    if( !jsonItemName.isNull() )
        item->setItemName( jsonItemName.toString() );

    if( !jsonShortDescription.isNull() )
        item->setShortDescription( jsonShortDescription.toString() );

    if( !description.isNull() )
        item->setDescription( description.toString() );

    if( !rarity.isNull() )
        item->setRarity( rarity.toString() );

    if( !inventoryIcon.isNull() )
        item->setInventoryIcon( inventoryIcon.toString() );

    if( !maxStack.isNull() )
        item->setMaxStack( maxStack.toVariant().toInt() );

    if( !fuelAmount.isNull() )
        item->setFuelAmount( fuelAmount.toVariant().toInt() );

    if( !avianDescription.isNull() )
        item->setAvianDescription( avianDescription.toString() );

    if( !apexDescription.isNull() )
        item->setApexDescription( apexDescription.toString() );

    if( !floranDescription.isNull() )
        item->setFloranDescription( floranDescription.toString() );

    if( !glitchDescription.isNull() )
        item->setGlitchDescription( glitchDescription.toString() );

    if( !humanDescription.isNull() )
        item->setHumanDescription( humanDescription.toString() );

    if( !hylotlDescription.isNull() )
        item->setHylotlDescription( hylotlDescription.toString() );

    if( !learnBlueprintOnPickup.isNull() ) {
        QJsonArray arr = learnBlueprintOnPickup.toArray();
        for( QJsonArray::iterator it = arr.begin(); it != arr.end(); ++it ) {
            QString blueprint = (*it).toString();
            item->addLearnBlueprintOnPickup( blueprint );
        }
    }

    /*if( !effects.isNull() ) {
        QJsonArray arr = effects.toArray();
        for( int i = 0; i < arr.size(); i++ ) {
            Q
        }
    }*/
}



void AssetsParser::populateHandedItem(StarHandedItem *item, QJsonObject &rootObject ) {
    populateItem( item, rootObject );

    QJsonValue handPosition = rootObject[ "handPosition" ];

    if( !handPosition.isNull() ) {
        QJsonArray arr = handPosition.toArray();

        QPoint handPos;
        handPos.setX( arr.at( 0 ).toVariant().toInt() );
        handPos.setY( arr.at( 1 ).toVariant().toInt() );

        item->setHandPosition( handPos );
    }
}

void AssetsParser::populateEquipableItem(StarEquipableItem *item, QJsonObject &rootObject)
{
    populateHandedItem( item, rootObject );

    QJsonValue twoHanded = rootObject[ "twoHanded" ];

    if( !twoHanded.isNull() ) {
        item->setTwoHanded( twoHanded.toBool() );
    }
}

void AssetsParser::populateGun(StarWeapon *item, QJsonObject &rootObject ) {
    populateEquipableItem( item, rootObject );
    QJsonValue projectileObj = rootObject[ "projectile" ];

    QJsonValue fireTime =           rootObject[ "fireTime" ];
    QJsonValue firePosition =       rootObject[ "firePosition" ];
    QJsonValue level =              rootObject[ "level" ];
    QJsonValue energyCost =         rootObject[ "energyCost" ];
    QJsonValue classMultiplier =    rootObject[ "classMultiplier" ];
    QJsonValue image =              rootObject[ "image" ];

    QJsonValue projectileType = QJsonValue::Undefined;
    QJsonValue projectileSpeed = QJsonValue::Undefined;
    QJsonValue projectileColor = QJsonValue::Undefined;
    QJsonValue projectileLife = QJsonValue::Undefined;

    if( !projectileObj.isNull() ) {
        QJsonObject obj = projectileObj.toObject();
        projectileType =     rootObject[ "projectileType" ];
        projectileSpeed =    obj[ "projectileSpeed" ];
        projectileColor =    obj["color"];
        projectileLife =     obj[ "life" ];
    }

    if( !fireTime.isNull() )
        item->setFireTime( fireTime.toDouble( ) );

    if( !firePosition.isNull() ) {
        QJsonArray arr = firePosition.toArray();
        QPoint firepos;
        firepos.setX( arr.at( 0 ).toVariant().toInt() );
        firepos.setY( arr.at( 1 ).toVariant().toInt() );
        item->setFirePosition( firepos );
    }

    if( !level.isNull() )
        item->setLevel( level.toVariant().toInt() );

    if( !energyCost.isNull() )
        item->setEnergyCost( energyCost.toVariant().toInt() );

    if( !classMultiplier.isNull() )
        item->setClassMultiplier( classMultiplier.toVariant().toInt());

    if( !image.isNull() )
        item->setWeaponImagePath( image.toString() );

    if( !projectileType.isNull() ) {
        item->setHasProjectile( true );
        item->setProjectileName( projectileType.toString() );
    }

    if( !projectileSpeed.isNull() )
        item->setProjectileSpeed( projectileSpeed.toVariant().toInt() );

    if( !projectileColor.isNull() ) {
        QJsonArray arr = projectileColor.toArray();
        int r = arr.at( 0 ).toVariant().toInt();
        int g = arr.at( 1 ).toVariant().toInt();
        int b = arr.at( 2 ).toVariant().toInt();

        QColor c( r, g, b );
        item->setProjectileColor( c );
    }

    if( !projectileLife.isNull() )
        item->setProjectileLifeTime( projectileLife.toVariant().toInt() );
}

void AssetsParser::populateConsumable(StarConsumable *item, QJsonObject &rootObject) {
    populateHandedItem( item, rootObject );

    QJsonValue emote = rootObject[ "emote" ];

    if( !emote.isNull() ) {
        item->setEmote( emote.toString() );
    }

}

void AssetsParser::populateMiningTool(StarMiningTool *item, QJsonObject &rootObject)
{
    populateEquipableItem( item, rootObject );

    QJsonValue pointable = rootObject[ "pointable" ];
    QJsonValue strikeSound = rootObject[ "strikeSound" ];
    QJsonValue idleSound = rootObject[ "idleSound" ];
    QJsonValue swingStart = rootObject[ "swingStart" ];
    QJsonValue swingFinish = rootObject[ "swingFinish" ];
    QJsonValue largeImage = rootObject[ "largeImage" ];
    QJsonValue image = rootObject[ "image" ];
    QJsonValue inspectionKind = rootObject[ "inspectionKind" ];
    QJsonValue tileDamage = rootObject[ "tileDamage" ];
    QJsonValue tileDamageBlunted = rootObject[ "tileDamageBlunted" ];
    QJsonValue animationCycle = rootObject[ "animationCycle" ];
    QJsonValue durabilityRegen = rootObject[ "durabilityRegenChart" ];
    QJsonValue durability = rootObject[ "durability" ];
    QJsonValue durabilityPerUse = rootObject[ "durabilityPerUse" ];
    QJsonValue blockRadius = rootObject[ "blockRadius" ];
    QJsonValue altBlockRadius = rootObject[ "altBlockRadius" ];
    QJsonValue fireTime = rootObject[ "fireTime" ];
    QJsonValue frames = rootObject[ "frames" ];

    if( !pointable.isNull() )
        item->setPointable( pointable.toBool() );

    if( !strikeSound.isNull() )
        item->setStrikeSound( strikeSound.toString() );

    if( !idleSound.isNull() )
        item->setIdleSound( idleSound.toString() );

    if( !swingStart.isNull() )
        item->setSwingStart( swingStart.toInt() );

    if( !swingFinish.isNull() )
        item->setSwingFinish( swingFinish.toInt() );

    if( !largeImage.isNull() )
        item->setLargeImage( largeImage.toString() );

    if( !image.isNull() )
        item->setImage( image.toString() );

    if( !inspectionKind.isNull() )
        item->setInspectionKind( inspectionKind.toString() );

    if( !tileDamage.isNull() )
        item->setTileDamage( tileDamage.toDouble() );

    if( !tileDamageBlunted.isNull() )
        item->setTileDamageBlunted( tileDamageBlunted.toDouble() );

    if( !animationCycle.isNull() )
        item->setAnimationCycle( animationCycle.toDouble() );

    if( !durability.isNull() )
        item->setDurability( durability.toInt() );

    if( !durabilityPerUse.isNull() )
        item->setDurabilityPerUse( durabilityPerUse.toInt() );

    if( !blockRadius.isNull() )
        item->setBlockRadius( blockRadius.toInt() );

    if( !altBlockRadius.isNull() )
        item->setAltBlockRadius( altBlockRadius.toInt() );

    if( !fireTime.isNull() )
        item->setFireTime( fireTime.toDouble() );

    if( !frames.isNull() )
        item->setFrames( frames.toInt() );

    if( !durabilityRegen.isNull() ) {
        QVector<DurabilityRegen> chart;
        QJsonArray rootArr = durabilityRegen.toArray();

        for( int i = 0; i < rootArr.size(); i++ ) {
            DurabilityRegen dre;
            QJsonArray duraArr = rootArr[i].toArray();

            QJsonArray itemsList = duraArr[0].toArray();
            for( int j = 0; j < itemsList.size(); j++ ) {
                QString itemName = itemsList[j].toString();
                dre.regenItems.append( itemName );
            }

            dre.regenAmount = duraArr[1].toInt();

            chart.append( dre );
        }
    }
}

void AssetsParser::populateFlashlight(StarFlashlight *item, QJsonObject &rootObject ) {
    populateHandedItem( item, rootObject );

    QJsonValue largeImage =     rootObject[ "largeImage" ];
    QJsonValue inspectionKind = rootObject[ "inspectionKind" ];
    QJsonValue lightPosition =  rootObject[ "lightPosition" ];
    QJsonValue lightColor =     rootObject[ "lightColor" ];
    QJsonValue beamWidth =      rootObject[ "beamWidth" ];
    QJsonValue ambientFactor =  rootObject[ "ambientFactor" ];

    if( !largeImage.isNull() )
        item->setLargeImage( largeImage.toString() );
    else
        qDebug() << "Large image is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !inspectionKind.isNull() )
        item->setInspectionKind( inspectionKind.toString() );
    else
        qDebug() << "Inspection Kind is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !lightPosition.isNull() ) {
        QJsonArray arr = lightPosition.toArray();
        QPoint tmp;
        tmp.setX( arr[0].toInt() );
        tmp.setY( arr[1].toInt() );

        item->setLightPosition( tmp );
    } else {
        qDebug() << "Light position is undefined " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !lightColor.isNull() ) {
        QJsonArray arr = lightColor.toArray();
        QColor color;
        color.setRed( arr[0].toInt() );
        color.setGreen( arr[1].toInt() );
        color.setBlue( arr[2].toInt() );

        item->setLightColor( color );
    } else {
        qDebug() << "Light color is undefined " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !beamWidth.isNull() )
        item->setBeamWidth( beamWidth.toDouble() );
    else
        qDebug() << "Beam width is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !ambientFactor.isNull() )
        item->setAmbientFactor( ambientFactor.toDouble() );
    else
        qDebug() << "Ambient Factor is undefined " << __FILE__ << ":" << __LINE__ << endl;

}

void AssetsParser::populateBeamaxe(StarBeamaxe *item, QJsonObject &rootObject)
{
    populateEquipableItem( item, rootObject );

    QJsonValue category = rootObject[ "category" ];
    QJsonValue inspectionKind = rootObject[ "inspectionKind" ];
    QJsonValue fireTime = rootObject[ "fireTime" ];
    QJsonValue blockRadius = rootObject[ "blockRadius" ];
    QJsonValue altBlockRadius = rootObject[ "altBlockRadius" ];
    QJsonValue critical = rootObject[ "critical" ];
    QJsonValue strikeSound = rootObject[ "strikeSound" ];
    QJsonValue image = rootObject[ "image" ];
    QJsonValue endImage = rootObject[ "endImage" ];
    QJsonValue firePosition = rootObject[ "firePosition" ];
    QJsonValue segmentPerUnit = rootObject[ "segmentPerUnit" ];
    QJsonValue nearControlPointElasticity = rootObject[ "nearControlPointElasticity" ];
    QJsonValue farControlPointElasticity = rootObject[ "farControlPointElasticity" ];
    QJsonValue nearControlPointDistance = rootObject[ "nearControlPointDistance" ];
    QJsonValue targetSegmentRun = rootObject[ "targetSegmentRun" ];
    QJsonValue innerBrightnessScale = rootObject[ "innerBrightnessScale" ];
    QJsonValue firstStripeThickness = rootObject[ "firstStripeThickness" ];
    QJsonValue secondStripeThickness = rootObject[ "secondStripeThickness" ];
    QJsonValue minBeamWidth = rootObject[ "minBeamWidth" ];
    QJsonValue maxBeamWidth = rootObject[ "maxBeamWidth" ];
    QJsonValue maxBeamJitter = rootObject[ "maxBeamJitter" ];
    QJsonValue minBeamJitter = rootObject[ "minBeamJitter" ];
    QJsonValue minBeamTrans = rootObject[ "minBeamTrans" ];
    QJsonValue maxBeamTrans = rootObject[ "maxBeamTrans" ];
    QJsonValue minBeamLines = rootObject[ "minBeamLines" ];
    QJsonValue maxBeamLines = rootObject[ "maxBeamLines" ];

    if( !category.isNull() )
        item->setCategory( category.toString() );
    else
        qDebug() << "Category is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !inspectionKind.isNull() )
        item->setInspectionKind( inspectionKind.toString() );
    else
        qDebug() << "Inspection is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !fireTime.isNull() )
        item->setFireTime( fireTime.toDouble() );
    else
        qDebug() << "Fire Time is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !blockRadius.isNull() )
        item->setBlockRadius( blockRadius.toInt() );
    else
        qDebug() << "Block Radius is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !altBlockRadius.isNull() )
        item->setAltBlockRadius( altBlockRadius.toInt() );
    else
        qDebug() << "Alt Block Radius is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !critical.isNull() )
        item->setCritical( critical.toBool() );
    else
        qDebug() << "Critical is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !strikeSound.isNull() )
        item->setStrikeSound( strikeSound.toString() );
    else
        qDebug() << "Strike Sound is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !image.isNull() )
        item->setImage( image.toString() );
    else
        qDebug() << "Image is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !endImage.isNull() ) {
        QJsonArray arr = endImage.toArray();
        QStringList images;
        for( int i = 0; i < arr.size(); i++ ) {
            images.append( arr[i].toString() );
        }
        item->setEndImages( images );
    } else
        qDebug() << "End Images is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !firePosition.isNull() ) {
        QJsonArray arr = firePosition.toArray();
        QPoint pts;
        pts.setX( arr[0].toInt() );
        pts.setY( arr[1].toInt() );
        item->setFirePosition( pts );
    } else
        qDebug() << "Fire Position is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !segmentPerUnit.isNull() )
        item->setSegmentPerUnit( segmentPerUnit.toInt() );
    else
        qDebug() << "Segment Per Unit is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !nearControlPointElasticity.isNull() )
        item->setNearControlPointElasticity( nearControlPointElasticity.toDouble() );
    else
        qDebug() << "NearControlPointElasticity is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !farControlPointElasticity.isNull() )
        item->setFarControlPointElasticity( farControlPointElasticity.toDouble( ) );
    else
        qDebug() << "FarControlPointElasticity is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !nearControlPointDistance.isNull() )
        item->setNearControlPointDistance( nearControlPointDistance.toDouble() );
    else
        qDebug() << "NearControlPointDistance is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !targetSegmentRun.isNull() )
        item->setTargetSegmentRun( targetSegmentRun.toInt() );
    else
        qDebug() << "TargetSegmentRun is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !innerBrightnessScale.isNull() )
        item->setInnerBrightnessScale( innerBrightnessScale.toInt() );
    else
        qDebug() << "InnerBrightnessScale is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !firstStripeThickness.isNull() )
        item->setFirstStripeThickness( firstStripeThickness.toDouble() );
    else
        qDebug() << "FirstStripeThickness is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !secondStripeThickness.isNull() )
        item->setSecondStripeThickness( secondStripeThickness.toDouble() );
    else
        qDebug() << "SecondStripeThickness is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !minBeamJitter.isNull() )
        item->setMinBeamJitter( minBeamJitter.toDouble() );
    else
        qDebug() << "MinBeamJitter is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !maxBeamJitter.isNull() )
        item->setMaxBeamJitter( maxBeamJitter.toDouble() );
    else
        qDebug() << "MaxBeamJitter is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !minBeamLines.isNull() )
        item->setMinBeamLines( minBeamLines.toInt() );
    else
        qDebug() << "MinBeamLines is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !maxBeamLines.isNull() )
        item->setMaxBeamLines( maxBeamLines.toInt() );
    else
        qDebug() << "MaxBeamLines is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !minBeamTrans.isNull() )
        item->setMinBeamTrans( minBeamTrans.toDouble() );
    else
        qDebug() << "MinBeamTrans is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !maxBeamTrans.isNull() )
        item->setMaxBeamTrans( maxBeamTrans.toDouble() );
    else
        qDebug() << "MaxBeamTrans is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !minBeamWidth.isNull() )
        item->setMinBeamWidth( minBeamWidth.toInt() );
    else
        qDebug() << "MinBeamWidth is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !maxBeamWidth.isNull() )
        item->setMaxBeamWidth( maxBeamWidth.toInt() );
    else
        qDebug() << "MaxBeamWidth is undefined " << __FILE__ << ":" << __LINE__ << endl;
}

void AssetsParser::populateMatItem(StarMatItem *item, QJsonObject &rootObject)
{
    populateItem( item, rootObject );

    QJsonValue materialId = rootObject[ "materialId" ];

    if( !materialId.isNull() )
        item->setMaterialId( materialId.toInt() );
    else
        qDebug() << "MaterialId is undefined " << __FILE__ << ":" << __LINE__ << endl;
}

void AssetsParser::populateInstrument(StarInstrument *item, QJsonObject &rootObject)
{
    populateEquipableItem( item, rootObject );

    QJsonValue activeHandPosition = rootObject[ "activeHandPosition" ];
    QJsonValue largeImage = rootObject[ "largeImage" ];
    QJsonValue image = rootObject[ "image" ];
    QJsonValue activeImage = rootObject[ "activeImage" ];
    QJsonValue inspectionKind = rootObject[ "inspectionKind" ];
    QJsonValue kind = rootObject[ "kind" ];
    QJsonValue activeAngle = rootObject[ "activeAngle" ];

    if( !activeHandPosition.isNull() ) {
        QJsonArray arr = activeHandPosition.toArray();
        QPointF pt;
        pt.setX( arr[0].toDouble() );
        pt.setY( arr[1].toDouble() );
        item->setActiveHandPosition( pt );
    } else
        qDebug() << "ActiveHandPosition is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !largeImage.isNull() )
        item->setLargeImage( largeImage.toString() );
    else
        qDebug() << "LargeImage is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !image.isNull() )
        item->setImage( image.toString() );
    else
        qDebug() << "Image is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !activeImage.isNull() )
        item->setActiveImage( activeImage.toString() );
    else
        qDebug() << "ActiveImage is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !inspectionKind.isNull() )
        item->setInspectionKind( inspectionKind.toString() );
    else
        qDebug() << "InspectionKind is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !kind.isNull() )
        item->setKind( kind.toString() );
    else
        qDebug() << "Kind is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !activeAngle.isNull() )
        item->setActiveAngle( activeAngle.toInt() );
    else
        qDebug() << "ActiveAngle is undefined " << __FILE__ << ":" << __LINE__ << endl;
}

void AssetsParser::populateSword(StarSword *item, QJsonObject &rootObject)
{
    populateItem( item, rootObject );

    QJsonValue kind = rootObject[ "kind" ];
    QJsonValue firePosition = rootObject[ "firePosition" ];
    QJsonValue soundEffect = rootObject[ "soundEffect" ];
    QJsonValue fireAfterWindup = rootObject[ "fireAfterWindup" ];
    QJsonValue twoHanded = rootObject[ "twoHanded" ];
    QJsonValue colorOptions = rootObject[ "colorOptions" ];
    QJsonValue level = rootObject[ "level" ];
    QJsonValue fireTime = rootObject[ "fireTime" ];
    QJsonValue primaryStances = rootObject[ "primaryStances" ];

    if( !kind.isNull() ) {
        item->setKind( kind.toString() );
    } else {
        qDebug() << "Kind is undefined " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !firePosition.isNull() ) {
        QPointF pt;
        QJsonArray arr = firePosition.toArray();
        pt.setX( arr[0].toInt() );
        pt.setY( arr[1].toInt() );

        item->setFirePosition( pt );
    } else {
        qDebug() << "FirePosition is undefined " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !soundEffect.isNull() ) {
        QJsonObject obj = soundEffect.toObject();
        QJsonArray arr = obj[ "fireSound" ].toArray();
        SoundEffect effect;

        for( int i = 0; i < arr.size(); i++ ) {
            QJsonObject sound = arr[i].toObject();
            QMap<QString, QString> object;
            for( QJsonObject::iterator it = sound.begin(); it != sound.end(); ++it ) {
                QString key = it.key();
                QString val = it.value().toString();

                object[ key ] = val;
            }
            effect.fireSound.append( object );
        }

        item->setSoundEffect( effect );
    } else {
        qDebug() << "SoundEffect is undefined " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !fireAfterWindup.isNull() ) {
        item->setFireAfterWindup( fireAfterWindup.toBool() );
    } else {
        qDebug() << "FireAfterWindup is undefined " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !twoHanded.isNull() ) {
        item->setTwoHanded( twoHanded.toBool() );
    } else {
        qDebug() << "TwoHanded is undefined " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !colorOptions.isNull() ) {
        QJsonArray arr = colorOptions.toArray();
        ColorOptionsList list;

        for( int i = 0; i < arr.size(); i++ ) {
            QJsonObject obj = arr[i].toObject();
            QMap< QString, QString> object;
            for( QJsonObject::iterator it = obj.begin(); it != obj.end(); ++it ) {
                QString key = it.key();
                QString val = it.value().toString();

                object[ key ] = val;
            }
            list.append( object );
        }

        item->setColorOptions( list );
    } else {
        qDebug() << "ColorOptions is undefined " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !level.isNull() ) {
        item->setLevel( level.toInt() );
    } else {
        qDebug() << "Level is undefined " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !fireTime.isNull() ) {
        item->setFireTime( fireTime.toDouble() );
    } else {
        qDebug() << "FireTime is undefined " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !primaryStances.isNull() ) {
        PrimaryStances stance;
        QJsonObject obj = primaryStances.toObject();

        QJsonValue idle = obj[ "idle" ];
        QJsonValue windup = obj[ "windup" ];
        QJsonValue cooldown = obj[ "cooldown" ];
        QJsonValue projectile = obj[ "projectile" ];
        QJsonValue projectileType = obj[ "projectileType" ];

        if( !idle.isNull() ) {
            QJsonObject o = idle.toObject();

            QJsonValue twoHanded = o[ "twoHanded" ];
            QJsonValue armAngle = o[ "armAngle" ];
            QJsonValue swordAngle = o[ "swordAngle" ];
            QJsonValue handPosition = o[ "handPosition" ];
            QJsonValue armFrameOverride = o[ "armFrameOverride" ];
            QJsonValue duration = o[ "duration" ];

            if( !twoHanded.isNull() )
                stance.idleTwoHanded = twoHanded.toBool();

            if( !armAngle.isNull() )
                stance.idleArmAngle = armAngle.toInt();

            if( !swordAngle.isNull() )
                stance.idleSwordAngle = swordAngle.toInt();

            if( !handPosition.isNull() ) {
                QJsonArray arr = handPosition.toArray();
                QPoint pt;
                pt.setX( arr[0].toInt() );
                pt.setY( arr[1].toInt() );

                stance.idleHandPosition = pt;
            }

            if( !armFrameOverride.isNull() )
                stance.armFrameOverride = armFrameOverride.toString();

            if( !duration.isNull() )
                stance.idleDuration = duration.toInt();
        }

        if( !windup.isNull() ) {
            QJsonObject o = windup.toObject();

            QJsonValue twoHanded = o[ "twoHanded" ];
            QJsonValue armAngle = o[ "armAngle" ];
            QJsonValue statusEffect = o[ "statusEffects" ];
            QJsonValue duration = o[ "duration" ];
            QJsonValue swordAngle = o[ "swordAngle" ];
            QJsonValue handPosition = o[ "handPosition" ];

            if( !twoHanded.isNull() )
                stance.windupTwoHanded = twoHanded.toBool();

            if( !armAngle.isNull() )
                stance.windupArmAngle = armAngle.toInt();

            if( !statusEffect.isNull() ) {
                QJsonArray arr = statusEffect.toArray();
                QList<StatusEffect> statusEffects;
                for( int i = 0; i < arr.size(); i++ ) {
                    QJsonObject obj = arr[i].toObject();

                    StatusEffect eff;
                    eff.setKind( obj[ "kind" ].toString() );
                    eff.setDuration( obj[ "duration" ].toDouble() );
                    statusEffects.append( eff );
                }
                stance.windupStatusEffects = statusEffects;
            }

            if( !duration.isNull() )
                stance.windupDuration = duration.toDouble();

            if( !swordAngle.isNull() )
                stance.windupSwordAngle = swordAngle.toInt();

            if( !handPosition.isNull() ) {
                QJsonArray arr = handPosition.toArray();
                QPoint pt;
                pt.setX( arr[0].toInt() );
                pt.setY( arr[1].toInt() );

                stance.windupHandPosition = pt;
            }
        }

        if( !cooldown.isNull() ) {
            QJsonObject o = cooldown.toObject();

            QJsonValue duration = o[ "duration" ];
            QJsonValue statusEffects = o[ "statusEffects" ];
            QJsonValue armAngle = o[ "armAngle" ];
            QJsonValue twoHanded = o[ "twoHanded" ];
            QJsonValue swordAngle = o[ "swordAngle" ];
            QJsonValue handPosition = o[ "handPosition" ];

            if( !duration.isNull() )
                stance.cooldownDuration = duration.toDouble();

            if( !statusEffects.isNull() ) {
                QJsonArray arr = statusEffects.toArray();
                QList<StatusEffect> statusEffects;
                for( int i = 0; i < arr.size(); i++ ) {
                    QJsonObject obj = arr[i].toObject();

                    StatusEffect eff;
                    eff.setKind( obj[ "kind" ].toString() );
                    eff.setDuration( obj[ "duration"].toDouble() );
                    statusEffects.append( eff );
                }
                stance.cooldownStatusEffects = statusEffects;
            }

            if( !armAngle.isNull() )
                stance.cooldownArmAngle = armAngle.toInt();

            if( !twoHanded.isNull() )
                stance.cooldownTwoHanded = twoHanded.toBool();

            if( !swordAngle.isNull() )
                stance.cooldownSwordAngle = swordAngle.toInt();

            if( !handPosition.isNull() ) {
                QJsonArray arr = handPosition.toArray();
                QPoint pt;
                pt.setX( arr[0].toInt() );
                pt.setY( arr[1].toInt() );
            }
        }

        if( !projectile.isNull() ) {
            QJsonObject obj = projectile.toObject();

            QJsonValue speed = obj[ "speed" ];
            QJsonValue power = obj[ "power" ];

            if( !speed.isNull() ) {
                stance.projectileSpeed = speed.toDouble();
            }

            if( !power.isNull() ) {
                stance.projectilePower = power.toInt();
            }
            stance.projectileType = projectileType.toString();
        }

        item->setPrimaryStances( stance );
    } else {
        qDebug() << "PrimaryStances is undefined " << __FILE__ << ":" << __LINE__ << endl;
    }
}

void AssetsParser::populateThrown(StarThrown *item, QJsonObject &rootObject)
{
    populateItem( item, rootObject );

    QJsonValue ammoUsage = rootObject[ "ammoUsage" ];
    QJsonValue image = rootObject[ "image" ];
    QJsonValue cooldown = rootObject[ "windupTime" ];
    QJsonValue windupTime = rootObject[ "windupTime" ];
    QJsonValue edgeTrigger = rootObject[ "edgeTrigger" ];
    QJsonValue projectileType = rootObject[ "projectileType" ];
    QJsonValue projectileConfig = rootObject[ "projectileConfig" ];

    if( !ammoUsage.isNull() )
        item->setAmmoUsage( ammoUsage.toInt() );
    else
        qDebug() << "AmmoUsage is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !image.isNull() )
        item->setImage( image.toString() );
    else
        qDebug() << "Image is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !cooldown.isNull() )
        item->setCooldown( cooldown.toDouble() );
    else
        qDebug() << "Cooldown is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !windupTime.isNull() )
        item->setWindupTime( windupTime.toDouble() );
    else
        qDebug() << "WindupTime is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !edgeTrigger.isNull() )
        item->setEdgeTrigger( edgeTrigger.toBool() );
    else
        qDebug() << "EdgeTrigger is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !projectileType.isNull() )
        item->setProjectileType( projectileType.toString() );
    else
        qDebug() << "ProjectileType is undefined " << __FILE__ << ":" << __LINE__ << endl;

    if( !projectileConfig.isNull() ) {
        QJsonObject obj = projectileConfig.toObject();
        ProjectileConfig conf;

        QJsonValue armorPenetration = obj[ "armorPenetration" ];
        QJsonValue level = obj[ "level" ];
        QJsonValue speed = obj[ "speed" ];
        QJsonValue power = obj[ "power" ];

        if( !armorPenetration.isNull() )
            conf.armorPenetration = armorPenetration.toInt();

        if( !level.isNull() )
            conf.level = level.toInt();

        if( !speed.isNull() )
            conf.speed = speed.toInt();

        if( !power.isNull() )
            conf.power = power.toInt();

        item->setProjectileConfig( conf );
    } else {
        qDebug() << "ProjectileConfig is undefined " << __FILE__ << ":" << __LINE__ << endl;
    }
}

void AssetsParser::populateWorldObject(StarWorldObject *item, QJsonObject &rootObject )
{

    QJsonValue jsonItemName =           rootObject[ "objectName" ];
    QJsonValue jsonShortDescription =   rootObject[ "shortdescription" ];
    QJsonValue description =            rootObject[ "description" ];
    QJsonValue rarity =                 rootObject[ "rarity" ];
    QJsonValue inventoryIcon =          rootObject[ "inventoryIcon" ];
    QJsonValue maxStack =               rootObject[ "maxStack" ];
    QJsonValue fuelAmount =             rootObject[ "fuelAmount" ];
    QJsonValue avianDescription =       rootObject[ "avianDescription" ];
    QJsonValue apexDescription =        rootObject[ "apexDescription" ];
    QJsonValue floranDescription =      rootObject[ "floranDescription" ];
    QJsonValue glitchDescription =      rootObject[ "glitchDescription" ];
    QJsonValue humanDescription =       rootObject[ "humanDescription" ];
    QJsonValue hylotlDescription =      rootObject[ "hylotlDescription" ];
    QJsonValue learnBlueprintOnPickup = rootObject[ "learnBlueprintOnPickup" ];
    QJsonValue effects =                rootObject[ "effects" ];

    QJsonValue particleEmitters = rootObject[ "particleEmitters" ];
    QJsonValue sitAngle = rootObject[ "sitAngle" ];
    QJsonValue lightColor = rootObject[ "lightColor" ];
    QJsonValue lightColors = rootObject[ "lightColors" ];
    QJsonValue animationParts = rootObject[ "animationParts" ];
    QJsonValue scriptDelta = rootObject[ "scriptDelta" ];
    QJsonValue spawner = rootObject[ "spawner" ];
    QJsonValue statusEffect = rootObject[ "statusEffect" ];
    QJsonValue breakDropOptions = rootObject[ "breakDropOptions" ];
    QJsonValue interactionTransition = rootObject[ "interactionTransition" ];
    QJsonValue race = rootObject[ "race" ];
    QJsonValue scripts = rootObject[ "scripts" ];
    QJsonValue script = rootObject[ "script" ];
    QJsonValue outboundNodesLocation = rootObject[ "outboundNodesLocation" ];
    QJsonValue touchDamage = rootObject[ "touchDamage" ];
    QJsonValue closeSounds = rootObject[ "closeSounds" ];
    QJsonValue truthtable = rootObject[ "truthtable" ];
    QJsonValue openSounds = rootObject[ "openSounds" ];
    QJsonValue sitCoverImage = rootObject[ "sitCoverImage" ];
    QJsonValue offSounds = rootObject[ "offSounds" ];
    QJsonValue hasWindowIcon = rootObject[ "hasWindowIcon" ];
    QJsonValue smashSounds = rootObject[ "smashSounds" ];
    QJsonValue rotationRange = rootObject[ "rotationRange" ];
    QJsonValue frameCooldown = rootObject[ "frameCooldown" ];
    QJsonValue animation = rootObject[ "animation" ];
    QJsonValue animationPosition = rootObject[ "animationPosition" ];
    QJsonValue detectTickDuration = rootObject[ "detectTickDuration" ];
    QJsonValue autocloseCooldown = rootObject[ "autocloseCooldown" ];
    QJsonValue health = rootObject[ "health" ];
    QJsonValue category = rootObject[ "category" ];
    QJsonValue pointBeam = rootObject[ "pointBeam" ];
    QJsonValue price = rootObject[ "price" ];
    QJsonValue inboundNodes = rootObject[ "inboundNodes" ];
    QJsonValue flickerDistance = rootObject[ "flickerDistance" ];
    QJsonValue uiConfig = rootObject[ "uiConfig" ];
    QJsonValue fireCooldown = rootObject[ "fireCooldown" ];
    QJsonValue orientations = rootObject[ "orientation" ];
    QJsonValue sitEffectEmitters = rootObject[ "sitEffectEmitters" ];
    QJsonValue hydrophobic = rootObject[ "hydrophobic" ];
    QJsonValue interval = rootObject[ "interval" ];
    QJsonValue engineOnOrientations = rootObject[ "engineOnOrientations" ];
    QJsonValue state = rootObject[ "state" ];
    QJsonValue tipOffset = rootObject[ "tipOffset" ];
    QJsonValue outboundNodes = rootObject[ "outboundNodes" ];
    QJsonValue targetHoldTime = rootObject[ "targetHoldTime" ];
    QJsonValue sitPosition = rootObject[ "sitPosition" ];
    QJsonValue onSounds = rootObject[ "onSounds" ];
    QJsonValue slotCount = rootObject[ "slotCount" ];
    QJsonValue printable = rootObject[ "printable" ];
    QJsonValue genericDescription = rootObject[ "genericDescription" ];
    QJsonValue unlit = rootObject[ "unlit" ];
    QJsonValue fireOffsets = rootObject[ "fireOffsets" ];
    QJsonValue objectItem = rootObject[ "objectItem" ];
    QJsonValue rotationPauseTime = rootObject[ "rotationPauseTime" ];
    QJsonValue interactAction = rootObject[ "interactAction" ];
    QJsonValue sounds = rootObject[ "sounds" ];
    QJsonValue sitFlipDirection = rootObject[ "sitFlipDirection" ];
    QJsonValue soundEffect = rootObject[ "soundEffect" ];
    QJsonValue detectRadius = rootObject[ "detectRadius" ];
    QJsonValue stageAlts = rootObject[ "stageAlts" ];
    QJsonValue rotationTime = rootObject[ "rotationTime" ];
    QJsonValue flickerTiming = rootObject[ "flickerTiming" ];
    QJsonValue useSounds = rootObject[ "useSounds" ];
    QJsonValue growing = rootObject[ "growing" ];
    QJsonValue flickerStrength = rootObject[ "flickerStrength" ];
    QJsonValue gates = rootObject[ "gates" ];
    QJsonValue pointLight = rootObject[ "pointLight" ];
    QJsonValue projectileOptions = rootObject[ "projectileOptions" ];
    QJsonValue baseOffset = rootObject[ "baseOffset" ];
    QJsonValue damageTime = rootObject[ "damageTeam" ];
    QJsonValue statusEffects = rootObject[ "statusEffects" ];
    QJsonValue interactData = rootObject[ "interactData" ];
    QJsonValue soundEffectRadius = rootObject[ "soundEffectRadius" ];
    QJsonValue sitOrientation = rootObject[ "sitOrientation" ];
    QJsonValue subtitle = rootObject[ "subtitle" ];
    QJsonValue sitEmote = rootObject[ "sitEmote" ];
    QJsonValue maxLaserLength = rootObject[ "maxLaserLength" ];
    QJsonValue smashDropOptions = rootObject[ "smashDropOptions" ];
    QJsonValue recipeGroup = rootObject[ "recipeGroup" ];
    QJsonValue unbreakable = rootObject[ "unbreakable" ];
    QJsonValue objectType = rootObject[ "objectType" ];

    if( !jsonItemName.isNull() )
        item->setItemName( jsonItemName.toString() );


    if( !jsonShortDescription.isNull() )
        item->setShortDescription( jsonShortDescription.toString() );

    if( !description.isNull() )
        item->setDescription( description.toString() );

    if( !rarity.isNull() )
        item->setRarity( rarity.toString() );

    if( !inventoryIcon.isNull() )
        item->setInventoryIcon( inventoryIcon.toString() );

    if( !maxStack.isNull() )
        item->setMaxStack( maxStack.toVariant().toInt() );

    if( !fuelAmount.isNull() )
        item->setFuelAmount( fuelAmount.toVariant().toInt() );

    if( !avianDescription.isNull() )
        item->setAvianDescription( avianDescription.toString() );

    if( !apexDescription.isNull() )
        item->setApexDescription( apexDescription.toString() );

    if( !floranDescription.isNull() )
        item->setFloranDescription( floranDescription.toString() );

    if( !glitchDescription.isNull() )
        item->setGlitchDescription( glitchDescription.toString() );

    if( !humanDescription.isNull() )
        item->setHumanDescription( humanDescription.toString() );

    if( !hylotlDescription.isNull() )
        item->setHylotlDescription( hylotlDescription.toString() );

    if( !learnBlueprintOnPickup.isNull() ) {
        QJsonArray arr = learnBlueprintOnPickup.toArray();
        for( QJsonArray::iterator it = arr.begin(); it != arr.end(); ++it ) {
            QString blueprint = (*it).toString();
            item->addLearnBlueprintOnPickup( blueprint );
        }
    }

    // Particle emitters
    if( !particleEmitters.isNull() ) {
        QJsonArray arr = particleEmitters.toArray();
        QList<ParticleEmitter> pEList;

        for( int i = 0; i < arr.size(); i++ ) {
            QJsonObject obj = arr[i].toObject();
            ParticleEmitter pe;

            QJsonValue emissionRate = obj[ "emissionRate" ];
            QJsonValue emissionVariance = obj[ "emissionVariance" ];
            QJsonValue pixelOrigin = obj[ "pixelOrigin" ];
            QJsonValue particle = obj[ "particle" ];
            QJsonValue particleVariance = obj[ "particleVariance" ];

            if( !emissionRate.isNull() )
                pe.emissionRate = emissionRate.toDouble();

            if( !emissionVariance.isNull() )
                pe.emissionVariance = emissionVariance.toDouble();

            if( !pixelOrigin.isNull() ) {
                pe.pixelOrigin = StarItem::qJsonArrayToQPoint( pixelOrigin.toArray() );
            }

            if( !particle.isNull() ) {
                QJsonObject partObj = particle.toObject();
                Particle particle;

                QJsonValue fade = partObj[ "fade" ];
                QJsonValue initialVelocity = partObj[ "initialVelocity" ];
                QJsonValue destructionTime = partObj[ "destructionTime" ];
                QJsonValue layer = partObj[ "layer" ];
                QJsonValue image = partObj[ "image" ];
                QJsonValue type = partObj[ "type" ];
                QJsonValue timeToLive = partObj[ "timeToLive" ];
                QJsonValue size = partObj[ "size" ];
                QJsonValue color = partObj[ "color" ];
                QJsonValue approach = partObj[ "approach" ];
                QJsonValue finalVelocity = partObj[ "finalVelocity" ];
                QJsonValue light = partObj[ "light" ];
                QJsonValue destructionAction = partObj[ "destructionAction" ];

                if( !fade.isNull() )
                    particle.fade = fade.toDouble();

                if( !initialVelocity.isNull() )
                    particle.initialVelocity = StarItem::qJsonArrayToQPointF( initialVelocity.toArray() );

                if( !destructionTime.isNull() )
                    particle.destructionTime = destructionTime.toInt();

                if( !layer.isNull() )
                    particle.layer = layer.toString();

                if( !image.isNull() )
                    particle.image = image.toString();

                if( !type.isNull() )
                    particle.type = type.toString();

                if( !timeToLive.isNull() )
                    particle.timeToLive = timeToLive.toDouble();

                if( !size.isNull() )
                    particle.size = size.toDouble();

                if( !color.isNull() )
                    particle.color = StarItem::qJsonArrayToQColor( color.toArray() );

                if( !approach.isNull() )
                    particle.approach = StarItem::qJsonArrayToQPoint( approach.toArray() );

                if( !finalVelocity.isNull() )
                    particle.finalVelocity = StarItem::qJsonArrayToQPointF( finalVelocity.toArray() );

                if( !light.isNull() )
                    particle.light = StarItem::qJsonArrayToQColor( light.toArray() );

                if( !destructionAction.isNull() )
                    particle.destructionAction = destructionAction.toString();


                pe.particle = particle;
            }

            if( !particleVariance.isNull() ) {
                QJsonObject partObj = particleVariance.toObject();
                ParticleVariance var;

                QJsonValue position = partObj[ "position" ];
                QJsonValue initialVelocity = partObj[ "initialVelocity" ];
                QJsonValue finalVelocity = partObj[ "finalVelocity" ];

                if( !position.isNull() )
                    var.position = StarItem::qJsonArrayToQPoint( position.toArray() );

                if( !initialVelocity.isNull() )
                    var.initialVelocity = StarItem::qJsonArrayToQPointF( initialVelocity.toArray() );

                if( !finalVelocity.isNull() )
                    var.finalVelocity = StarItem::qJsonArrayToQPointF( finalVelocity.toArray() );

                pe.particleVariance = var;
            }
            pEList.append( pe );
        }
        item->setParticleEmitters( pEList );
    }

    if( !sitAngle.isNull() )
        item->setSitAngle( sitAngle.toInt() );

    if( !lightColor.isNull() )
        item->setLightColor( StarItem::qJsonArrayToQColor( lightColor.toArray() ) );

    if( !lightColors.isNull() ) {
        QJsonObject colorsObj = lightColors.toObject();
        LightColors colors;

        for( QJsonObject::iterator it = colorsObj.begin(); it != colorsObj.end(); ++it ) {
            colors[ it.key() ] = StarItem::qJsonArrayToQColor( it.value().toArray() );
        }

        item->setLightColors( colors );
    }

    if( !animationParts.isNull() ) {
        QJsonObject animObj = animationParts.toObject();
        AnimationParts parts;

        for( QJsonObject::iterator it = animObj.begin(); it != animObj.end(); ++it ) {
            parts[ it.key() ] = it.value().toString();
        }
        item->setAnimationParts( parts );
    }

    if( !scriptDelta.isNull() )
        item->setScriptDelta( scriptDelta.toInt() );

    if( !spawner.isNull() ) {
        QJsonObject spawnObj = spawner.toObject();
        Spawner spawn;

        QJsonValue npcParameterOptions = spawnObj[ "npcParameterOptions" ];
        QJsonValue npcTypeOptions = spawnObj[ "npcTypeOptions" ];
        QJsonValue searchRadius = spawnObj[ "searchRadius" ];
        QJsonValue npcSpeciesOptions = spawnObj[ "npcSpeciesOptions" ];

        if( !npcParameterOptions.isNull() ) {
            QJsonArray arr = npcParameterOptions.toArray();
            NpcParameterOptions npc;

            for( int i = 0; i < arr.size(); i++ ) {
                QJsonObject sObj = arr[i].toObject();
                QMap< QString, QJsonArray> map;
                for( QJsonObject::iterator it = sObj.begin(); it != sObj.end(); ++it ) {
                    map[ it.key() ] = it.value().toArray();
                }
                npc.dropPools.append( map );
            }
            spawn.npcParameterOptions = npc;
        }

        if( !searchRadius.isNull() )
            spawn.searchRadius = searchRadius.toDouble();

        if( !npcTypeOptions.isNull() )
            spawn.npcTypeOptions = StarItem::qJsonArrayToQStringList( npcTypeOptions.toArray() );

        if( !npcSpeciesOptions.isNull() )
            spawn.npcSpeciesOptions = StarItem::qJsonArrayToQStringList( npcSpeciesOptions.toArray() );

        item->setSpawner( spawn );
    }

    if( !statusEffect.isNull() ) {
        QJsonObject statusObj = statusEffect.toObject();
        SitStatusEffect effect = StatusEffect::qJsonObjectToJsonStatusEffect( statusObj );

        item->setStatusEffect( effect );
    }

    if( !breakDropOptions.isNull() ) {
        QJsonArray arr1 = breakDropOptions.toArray();
        QList< QList< BreakDropOptions > > dropOptions;

        for( int i = 0; i < arr1.size(); ++i ) {
            QJsonArray arr2 = arr1[i].toArray();
            QList< BreakDropOptions > dropOpt2;

            for( int j = 0; j < arr2.size(); ++j ) {
                QJsonArray arr3 = arr2[ j ].toArray();
                BreakDropOptions opt;

                opt.item = arr3[0].toString();
                opt.amount = arr3[1].toInt();
                opt.foo = arr3[2].toObject();

                dropOpt2.append( opt );
            }

            dropOptions.append( dropOpt2 );
        }

        item->setBreakDropOptions( dropOptions );
    }

    if( !smashDropOptions.isNull() ) {
        QJsonArray arr1 = smashDropOptions.toArray();
        QList< QList< SmashDropOptions > > dropOptions;

        for( int i = 0; i < arr1.size(); ++i ) {
            QJsonArray arr2 = arr1[i].toArray();
            QList< SmashDropOptions > dropOpt2;

            for( int j = 0; j < arr2.size(); ++j ) {
                QJsonArray arr3 = arr2[ j ].toArray();
                SmashDropOptions opt;

                opt.item = arr3[0].toString();
                opt.amount = arr3[1].toInt();
                opt.foo = arr3[2].toObject();

                dropOpt2.append( opt );
            }

            dropOptions.append( dropOpt2 );
        }

        item->setSmashDropOptions( dropOptions );
    }

    if( !interactionTransition.isNull() ) {
        QJsonObject transitionObj = interactionTransition.toObject();
        InteractionTransition interact;

        QJsonValue finalStep = transitionObj[ "2" ];
        QJsonValue growingStep = transitionObj[ "growing" ];
        QJsonValue stageAlts = transitionObj[ "stageAlts" ];

        if( !finalStep.isNull() ) {
            QJsonObject stepObj = finalStep.toObject();

            QJsonValue dropOptions = stepObj[ "dropOptions" ];
            QJsonValue command = stepObj[ "command" ];
            DropOptions dropOpt;

            if( !dropOptions.isNull() ) {
                QJsonArray arr = dropOptions.toArray();
                dropOpt.probability = arr[0].toDouble();

                QList< QList< DropItem > > items;
                for( int i = 1; i < arr.size(); ++i ) {
                    if( arr[i].isArray() ) {
                        QJsonArray sArr = arr[i].toArray();
                        QList< DropItem > dropItemsList;
                        for( int j = 0; j < sArr.size(); ++j ) {
                            QJsonObject obj = sArr[j].toObject();
                            DropItem dropItem;

                            for( QJsonObject::iterator it = obj.begin(); it != obj.end(); ++it ) {
                                if( it.key() == "name" )
                                    dropItem.name = it.value().toString();
                                else if( it.key() == "count" )
                                    dropItem.count = it.value().toInt();
                                else
                                    qDebug() << "Unknown DropItem property " << __FILE__ << ":" << __LINE__ << endl;
                            }
                            dropItemsList.append( dropItem );
                        }
                        items.append( dropItemsList );
                    } else {
                        qWarning() << "Warning, unexpected element in json file" << endl;
                    }
                }
                dropOpt.itemsPack = items;
            }

            if( !command.isNull() )
                dropOpt.command = command.toString();

            interact.finalStep = dropOpt;
        }

        if( !growingStep.isNull() ) {
            Growing gStep;
            QJsonObject growObj = growingStep.toObject();

            QJsonValue stepOne = growObj[ "1" ];
            QJsonValue stepTwo = growObj[ "2" ];

            if( !stepOne.isNull() ) {
                QJsonObject obj = stepOne.toObject();
                gStep.step1 = StarWorldObject::objectToGrowingStep( obj );
            }

            if( !stepTwo.isNull() ) {
                QJsonObject obj = stepTwo.toObject();
                gStep.step2 = StarWorldObject::objectToGrowingStep( obj );
            }
            interact.growingStep = gStep;
        }

        if( !stageAlts.isNull() ) {
            QJsonObject stage = stageAlts.toObject();
            StageAlts alts;

            QJsonValue count = stage[ "count" ];
            QJsonValue final = stage[ "2" ];

            if( !count.isNull() )
                alts.count = count.toInt();

            if( !final.isNull() )
                alts.finalStep = final.toInt();

            interact.stageAlts = alts;
        }
        item->setInteractionTransition( interact );
    }

    //continue!
    if( !race.isNull() )
        item->setRace( race.toString() );

    if( !script.isNull() )
        item->setScript( script.toString() );
    else if( !scripts.isNull() )
        item->setScripts( StarItem::qJsonArrayToQStringList( scripts.toArray() ) );

    if( !outboundNodesLocation.isNull() )
        item->setOutboundNodesLocation( StarItem::qJsonArrayToQPoint( outboundNodesLocation.toArray() ) );

    if( !touchDamage.isNull() ) {
        TouchDamage touch;
        QJsonObject obj = touchDamage.toObject();

        QJsonValue statusEffectV = obj[ "statusEffects" ];
        QJsonValue damageType = obj[ "damageType" ];
        QJsonValue damageSourceKind = obj[ "damageSourceKind" ];
        QJsonValue poly = obj[ "poly" ];
        QJsonValue damage = obj[ "damage" ];

        if( !statusEffectV.isNull() ) {
            QJsonArray arr = statusEffectV.toArray();
            QList< JsonStatusEffect > effs;
            for( int i = 0; i < arr.size(); ++i ) {
                QJsonObject effObj = arr[i].toObject();
                JsonStatusEffect eff = StatusEffect::qJsonObjectToJsonStatusEffect( effObj );

                effs.append( eff );
            }
            touch.statusEffects = effs;
        }

        if( !poly.isNull() ) {
            QJsonArray mainArray = poly.toArray();
            Polygon polygon;

            for( int i = 0; i < mainArray.size(); ++i ) {
                QJsonArray pointArr = mainArray[i].toArray();
                QPointF pt = StarItem::qJsonArrayToQPointF( pointArr );
                polygon.append( pt );
            }
            touch.poly = polygon;
        }

        if( !damageType.isNull() )
            touch.damageType = damageType.toString();

        if( !damageSourceKind.isNull() )
            touch.damageSourceKind = damageSourceKind.toString();

        if( !damage.isNull() )
            touch.damage = damage.toInt();

        item->setTouchDamage( touch );
    }

    if( !closeSounds.isNull() )
        item->setCloseSounds( StarItem::qJsonArrayToQStringList( closeSounds.toArray() ) );

    if( !truthtable.isNull() ) {
        Truthtable truth;
        QJsonArray truthArray = truthtable.toArray();

        if( truthArray.size() == 2 ) {
            QJsonValue firstRow = truthArray[0];
            QJsonValue secondRow = truthArray[1];

            //if we are in a 4 * 4 array ( 00, 01, 10, 11 )
            if( firstRow.isArray() && secondRow.isArray() ) {
                QJsonArray arr = firstRow.toArray();
                QJsonArray arr2 = secondRow.toArray();
                truth.type = FOUR;
                QVector< QVector< bool > > table;
                QVector< bool > fTable;

                fTable.append( arr[0].toBool() );
                fTable.append( arr[1].toBool() );
                table.append( fTable );

                fTable.clear();

                fTable.append( arr2[0].toBool() );
                fTable.append( arr2[1].toBool() );
                table.append( fTable );

                truth.fourTable = table;
            } else {
                QVector< bool > table;
                table.append( truthArray[0].toBool() );
                table.append( truthArray[1].toBool() );
                truth.twoTable = table;
            }
        }
       item->setTruthtable( truth );
    }

    if( !openSounds.isNull() )
        item->setOpenSounds( StarItem::qJsonArrayToQStringList( openSounds.toArray() ) );

    if( !sitCoverImage.isNull() )
        item->setSitCoverImage( sitCoverImage.toString() );

    if( !offSounds.isNull() )
        item->setOffSounds( StarItem::qJsonArrayToQStringList( offSounds.toArray() ) );

    if( !hasWindowIcon.isNull() )
        item->setHasWindowIcon( hasWindowIcon.toBool() );

    if( !smashSounds.isNull() )
        item->setSmashSounds( StarItem::qJsonArrayToQStringList( smashSounds.toArray() ) );

    if( !rotationRange.isNull() ) {
        QRange range;
        QJsonArray arr = rotationRange.toArray();

        if( arr.size() >= 2 ) {
            range.append( arr[0].toInt() );
            range.append( arr[1].toInt() );
        }

        item->setRotationRange( range );
    }

    if( !frameCooldown.isNull() )
        item->setFrameCooldown( frameCooldown.toInt() );

    if( !animation.isNull() )
        item->setAnimation( animation.toString() );

    if( !animationPosition.isNull() )
        item->setAnimationPosition( StarItem::qJsonArrayToQPoint( animationPosition.toArray() ) );

    if( !detectTickDuration.isNull() )
        item->setDetectTickDuration( detectTickDuration.toInt() );

    if( !autocloseCooldown.isNull() )
        item->setAutocloseCooldown( autocloseCooldown.toInt() );

    if( !health.isNull() )
        item->setHealth( health.toDouble() );

    if( !category.isNull() )
        item->setCategory( category.toString() );

    if( !pointBeam.isNull() )
        item->setPointBeam( pointBeam.toDouble() );

    if( !price.isNull() )
        item->setPrice( price.toInt() );

    if( !inboundNodes.isNull() ) {
        QJsonArray arr = inboundNodes.toArray();
        QVector< QPoint > inbound;

        for( int i = 0; i < arr.size(); ++i ) {
            QJsonArray pointArray = arr[i].toArray();
            QPoint point = StarItem::qJsonArrayToQPoint( pointArray );
            inbound.append( point );
        }
        item->setInboundNodes( inbound );
    }

    if( !flickerDistance.isNull() )
        item->setFlickerDistance( flickerDistance.toDouble() );

    if( !uiConfig.isNull() )
        item->setUiConfig( uiConfig.toString() );

    if( !fireCooldown.isNull() )
        item->setFireCooldown( fireCooldown.toDouble() );

    if( !orientations.isNull() ) {
        QJsonArray arr = orientations.toArray();
        QList< Orientations > orientationsList;

        for( int i = 0; i < arr.size(); ++i ) {
            QJsonObject obj = arr[i].toObject();
            Orientations ori = StarWorldObject::objectToOrientations( obj );
            orientationsList.append( ori );
        }
        item->setOrientations( orientationsList );
    }

    if( !sitEffectEmitters.isNull() )
        item->setSitEffectEmitters( StarItem::qJsonArrayToQStringList( sitEffectEmitters.toArray() ) );

    if( !hydrophobic.isNull() )
        item->setHydrophobic( hydrophobic.toBool() );

    if( !interval.isNull() )
        item->setInterval( interval.toInt() );

    if( !engineOnOrientations.isNull() ) {
        QJsonArray arr = engineOnOrientations.toArray();
        QList< Orientations > orientationsList;

        for( int i = 0; i < arr.size(); ++i ) {
            QJsonObject obj = arr[i].toObject();
            Orientations ori = StarWorldObject::objectToOrientations( obj );
            orientationsList.append( ori );
        }
        item->setEngineOnOrientations( orientationsList );
    }

    if( !state.isNull() ) {
        QJsonObject obj = state.toObject();
        State stateStruct;

        QJsonValue uiconfig = obj[ "uiconfig" ];
        QJsonValue openSoundsState = obj[ "openSounds" ];
        QJsonValue closeSoundsState = obj[ "closeSounds" ];
        QJsonValue kind = obj[ "kind" ];
        QJsonValue slotCound = obj[ "slotCount" ];

        if( !uiconfig.isNull() )
            stateStruct.uiconfig = uiconfig.toString();

        if( !openSoundsState.isNull() )
            stateStruct.openSounds = StarItem::qJsonArrayToQStringList( openSoundsState.toArray() );

        if( !closeSoundsState.isNull() )
            stateStruct.closeSounds = StarItem::qJsonArrayToQStringList( closeSoundsState.toArray() );

        if( !kind.isNull() )
            stateStruct.kind = kind.toString();

        if( !slotCount.isNull() )
            stateStruct.slotCount = slotCount.toInt();
        item->setState( stateStruct );
    }

    if( !tipOffset.isNull() )
        item->setTipOffset( StarItem::qJsonArrayToQPointF( tipOffset.toArray() ) );

    if( !outboundNodes.isNull() ) {
        QJsonArray arr = outboundNodes.toArray();
        QVector< QPoint > outbound;

        for( int i = 0; i < arr.size(); ++i ) {
            QJsonArray pointArray = arr[i].toArray();
            QPoint pt = StarItem::qJsonArrayToQPoint( pointArray );
            outbound.append( pt );
        }
        item->setOutboundNodes( outbound );
    }

    if( !targetHoldTime.isNull() )
        item->setTargetHoldTime( targetHoldTime.toInt() );

    if( !sitPosition.isNull() )
        item->setSitPosition( StarItem::qJsonArrayToQPoint( sitPosition.toArray() ) );

    if( !onSounds.isNull() )
        item->setOnSounds( StarItem::qJsonArrayToQStringList( onSounds.toArray() ) );

    if( !slotCount.isNull() )
        item->setSlotCount( slotCount.toInt() );

    if( !printable.isNull() )
        item->setPrintable( printable.toBool() );

    if( !genericDescription.isNull() )
        item->setGenericDescription( genericDescription.toString() );

    if( !unlit.isNull() )
        item->setUnlit( unlit.toBool() );

    if( !fireOffsets.isNull() ) {
        QJsonArray arr = fireOffsets.toArray();
        QVector< double > offsets;

        for( int i = 0; i < arr.size(); ++i ) {
            offsets.append( arr[i].toDouble() );
        }
        item->setFireOffsets( offsets );
    }

    if( !objectItem.isNull() )
        item->setObjectItem( objectItem.toString() );

    if( !rotationPauseTime.isNull() )
        item->setRotationPauseTime( rotationPauseTime.toDouble() );

    if( !interactAction.isNull() )
        item->setInteractAction( interactAction.toString() );

    if( !sounds.isNull() )
        item->setSounds( StarItem::qJsonArrayToQStringList( sounds.toArray() ) );

    if( !sitFlipDirection.isNull() )
        item->setSitFlipDirection( sitFlipDirection.toBool() );

    if( !soundEffect.isNull() )
        item->setSoundEffect( soundEffect.toString() );

    if( !detectRadius.isNull() )
        item->setDetectRadius( detectRadius.toInt() );

    if( !stageAlts.isNull() ) {
        QJsonObject stage = stageAlts.toObject();
        StageAlts alts;

        QJsonValue count = stage[ "count" ];
        QJsonValue final = stage[ "2" ];

        if( !count.isNull() )
            alts.count = count.toInt();

        if( !final.isNull() )
            alts.finalStep = final.toInt();

        item->setStageAlts( alts );
    }

    if( !rotationTime.isNull() )
        item->setRotationTime( rotationTime.toDouble() );

    if( !flickerTiming.isNull() )
        item->setFlickerTiming( flickerTiming.toDouble() );

    if( !useSounds.isNull() )
        item->setUseSounds( StarItem::qJsonArrayToQStringList( useSounds.toArray() ) );

    if( !growing.isNull() ) {
        Growing gStep;
        QJsonObject growObj = growing.toObject();

        QJsonValue stepOne = growObj[ "1" ];
        QJsonValue stepTwo = growObj[ "2" ];

        if( !stepOne.isNull() ) {
            QJsonObject obj = stepOne.toObject();
            gStep.step1 = StarWorldObject::objectToGrowingStep( obj );
        }

        if( !stepTwo.isNull() ) {
            QJsonObject obj = stepTwo.toObject();
            gStep.step2 = StarWorldObject::objectToGrowingStep( obj );
        }
        item->setGrowing( gStep );
    }

    if( !flickerStrength.isNull() )
        item->setFlickerStrength( flickerStrength.toDouble() );

    if( !gates.isNull() )
        item->setGates( gates.toInt() );

    if( !pointLight.isNull() )
        item->setPointLight( pointLight.toBool() );

    if( !projectileOptions.isNull() ) {
        QJsonObject obj = projectileOptions.toObject();
        ProjectileOptions opt;

        QJsonValue projectileType = obj[ "projectileType" ];
        QJsonValue projectileParams = obj[ "projectileParams" ];

        if( !projectileType.isNull() )
            opt.projectileType = projectileType.toString();

        if( !projectileParams.isNull() )
            opt.projectileParams = projectileParams.toObject();

        item->setProjectileOptions( opt );
    }

    if( !baseOffset.isNull() )
        item->setBaseOffset( StarItem::qJsonArrayToQPointF( baseOffset.toArray() ) );

    if( !damageTime.isNull() ) {
        QJsonObject obj = damageTime.toObject();

        QJsonValue type = obj[ "type" ];

        if( !type.isNull() )
            item->setDamageTime( type.toString() );
    }

    if( !statusEffects.isNull() ) {
        QJsonArray arr = statusEffects.toArray();
        QList< JsonStatusEffect > list;

        for( int i = 0; i < arr.size(); ++i ) {
            QJsonObject obj = arr[i].toObject();
            JsonStatusEffect effect = StatusEffect::qJsonObjectToJsonStatusEffect( obj );
            list.append( effect );
        }
        item->setStatusEffects( list );
    }

    if( !interactData.isNull() ) {
        QJsonObject obj = interactData.toObject();
        InteractData data;

        QJsonValue config = obj[ "config" ];
        QJsonValue filter = obj[ "filter" ];

        if( !config.isNull() )
            data.config = config.toString();

        if( !filter.isNull() ) {
            QStringList filters = StarItem::qJsonArrayToQStringList( filter.toArray() );

            /*foreach( QString filt, filters ) {
                //If the current filter doesn't exists, we add it to the list
                if( !Utility::exists( filt, craftingFilter ) ) {
                    craftingFilter.append( filt );
                }
            }*/

            data.filter = filters;
        }

        item->setInteractData( data );
    }

    if( !soundEffectRadius.isNull() )
        item->setSoundEffectRadius( soundEffectRadius.toInt() );

    if( !sitOrientation.isNull() )
        item->setSitOrientation( sitOrientation.toString() );

    if( !subtitle.isNull() )
        item->setSubtitle( subtitle.toString() );

    if( !sitEmote.isNull() )
        item->setSitEmote( sitEmote.toString() );

    if( !maxLaserLength.isNull() )
        item->setMaxLaserLength( maxLaserLength.toInt() );

    if( !recipeGroup.isNull() )
        item->setRecipeGroup( recipeGroup.toString() );

    if( !unbreakable.isNull() )
        item->setUnbreakable( unbreakable.toBool() );

    if( !objectType.isNull() )
        item->setObjectType( objectType.toString() );

}

void AssetsParser::populateStarCoin(StarCoin *item, QJsonObject &rootObject)
{
    populateItem( item, rootObject );

    QJsonValue value = rootObject[ "value" ];
    QJsonValue pickupSoundsSmall = rootObject[ "pickupSoundsSmall" ];
    QJsonValue pickupSoundsMedium = rootObject[ "pickupSoundsMedium" ];
    QJsonValue pickupSoundsLarge = rootObject[ "pickupSoundsLarge" ];
    QJsonValue smallStackLimit = rootObject[ "smallStackLimit" ];
    QJsonValue mediumStackLimit = rootObject[ "mediumStackLimit" ];

    if( !value.isNull() )
        item->setValue( value.toInt() );

    if( !pickupSoundsSmall.isNull() ) {
        QStringList list;
        QJsonArray arr = pickupSoundsSmall.toArray();

        for( int i = 0; i < arr.size(); ++i ) {
            list.append( arr[i].toString() );
        }
        item->setPickupSoundsSmall( list );
    }

    if( !pickupSoundsMedium.isNull() ) {
        QStringList list;
        QJsonArray arr = pickupSoundsMedium.toArray();

        for( int i = 0; i < arr.size(); ++i ) {
            list.append( arr[i].toString() );
        }
        item->setPickupSoundsMedium( list );
    }

    if( !pickupSoundsLarge.isNull() ) {
        QStringList list;
        QJsonArray arr = pickupSoundsLarge.toArray();

        for( int i = 0; i < arr.size(); ++i ) {
            list.append( arr[i].toString() );
        }
        item->setPickupSoundsLarge( list );
    }

    if( !smallStackLimit.isNull() )
        item->setSmallStackLimit( smallStackLimit.toInt() );

    if( !mediumStackLimit.isNull() )
        item->setMediumStackLimit( mediumStackLimit.toInt() );
}

void AssetsParser::populateProjectile(StarProjectile *proj, QJsonObject &rootObject)
{
    QJsonValue projectileName = rootObject[ "projectileName" ];
    QJsonValue timeToLive = rootObject[ "timeToLive" ];
    QJsonValue level = rootObject[ "level" ];
    QJsonValue hydrophobic = rootObject[ "hydrophobic" ];

    QJsonValue framePath = rootObject[ "frames" ]; // Not the frame path but the image (for now)
    QJsonValue frameNumber = rootObject[ "frameNumber" ];
    QJsonValue animationCycle = rootObject[ "animationCycle" ];
    QJsonValue animationLoop = rootObject[ "animationLoop" ];

    QJsonValue damageType = rootObject[ "damageType" ];
    QJsonValue damageKind = rootObject[ "damageKind" ];
    QJsonValue damageKindImage = rootObject[ "damageKindImage" ];
    QJsonValue power = rootObject[ "power" ];

    QJsonValue physics = rootObject[ "physics" ];
    QJsonValue bounces = rootObject[ "bounces" ];
    QJsonValue damagePoly = rootObject[ "damagePoly" ];
    QJsonValue fallSpeed = rootObject[ "fallSpeed" ];
    QJsonValue knockbackPower = rootObject[ "knockbackPower" ];
    QJsonValue speed = rootObject[ "speed" ];
    QJsonValue initialVelocity = rootObject[ "initialVelocity" ];

    QJsonValue pointLight = rootObject[ "pointLight" ];
    QJsonValue lightColor = rootObject[ "lightColor" ];

    QJsonValue effects = rootObject[ "statusEffects" ];
    QJsonValue emitters = rootObject[ "emitters" ];

    QJsonValue actionOnReap = rootObject[ "actionOnReap" ];

    if( !projectileName.isNull() )
        proj->setProjectileName( projectileName.toString() );

    if( !timeToLive.isNull() )
        proj->setTimeToLive( timeToLive.toInt() );

    if( !level.isNull() )
        proj->setLevel( level.toInt() );

    if( !hydrophobic.isNull() )
        proj->setHydrophobic( hydrophobic.toBool() );

    if( !framePath.isNull() ) {
        QString path = framePath.toString();
        QFileInfo fifo( path );
        QFileInfo fifoP = proj->projectileLocation();
        QString basename = fifo.baseName();
        QString framePath =  fifoP.path() + "/" + basename + ".frames";

        if( QFile::exists( framePath ) ) {
            proj->setFramePath( framePath );
        } else {
            qDebug() << "Cannot find frame file for " << proj->projectileLocation() << ", no such file or directory " << __FILE__ << ":" << __LINE__ << endl;
        }
    }

    if( !frameNumber.isNull() )
        proj->setFrameNumber( frameNumber.toInt() );

    if( !animationCycle.isNull() )
        proj->setAnimationCycle( animationCycle.toDouble() );

    if( !animationLoop.isNull() )
        proj->setAnimationLoop( animationLoop.toBool() );

    if( !damageType.isNull() )
        proj->setDamageType( damageType.toString() );

    if( !damageKind.isNull() )
        proj->setDamageKind( damageKind.toString() );

    if( !damageKindImage.isNull() ) {
        QFileInfo fifo( proj->projectileLocation() );
        QString fullPath = fifo.path() + "/" + damageKindImage.toString();
        proj->setDamageKindImage( fullPath );
    }

    if( !power.isNull() )
        proj->setPower( power.toDouble() );

    if( !physics.isNull() )
        proj->setPhysics( physics.toString() );

    if( !bounces.isNull() )
        proj->setBounces( bounces.toInt() );

    if( !damagePoly.isNull() ) {
        QJsonArray arr = damagePoly.toArray();
        PolygonI polygon;
        for( int i = 0; i < arr.size(); i++ ) {
            QJsonArray pt = arr[i].toArray();
            QPoint point;
            point.setX( pt[0].toInt() );
            point.setY( pt[1].toInt() );
            polygon.append( point );
        }
        proj->setDamagePoly( polygon );
    }

    if( !fallSpeed.isNull() )
        proj->setFallSpeed( fallSpeed.toDouble() );

    if( !knockbackPower.isNull() )
        proj->setKnockbackPower( knockbackPower.toInt() );

    if( !speed.isNull() )
        proj->setSpeed( speed.toDouble() );

    if( !initialVelocity.isNull() )
        proj->setInitialVelocity( initialVelocity.toDouble() );

    if( !pointLight.isNull() )
        proj->setPointLight( pointLight.toBool() );

    if( !lightColor.isNull() ) {
        QJsonArray arr = lightColor.toArray();
        QColor color;
        color.setRed( arr[0].toInt() );
        color.setGreen( arr[1].toInt() );
        color.setBlue( arr[2].toInt() );

        proj->setLightColor( color );
    }

    if( !effects.isNull() ) {
        QList<JsonStatusEffect> effectList;
        QJsonArray arr = effects.toArray();
        for( int i = 0; i < arr.size(); i++ ) {
            QJsonObject obj = arr[i].toObject();
            JsonStatusEffect eff;

            QJsonValue kind = obj[ "kind" ];
            QJsonValue amount = obj[ "amount" ];
            QJsonValue range = obj[ "range" ];
            QJsonValue percentage = obj[ "percentage" ];
            QJsonValue color = obj[ "color" ];

            if( !kind.isNull() )
                eff.kind = kind.toString();

            if( !amount.isNull() )
                eff.amount = amount.toDouble();

            if( !range.isNull() )
                eff.range = range.toInt();

            if( !percentage.isNull() )
                eff.percentage = percentage.toInt();

            if( !color.isNull() )
                eff.color = color.toString();

            effectList.append( eff );
        }
        proj->setEffects( effectList );
    }

    if( !emitters.isNull() ) {
        QJsonArray arr = emitters.toArray();
        QStringList emitters;

        for( int i = 0; i < arr.size(); i++ ) {
            QString value = arr[i].toString();
            emitters.append( value );
        }
        proj->setEmitters( emitters );
    }

    if( !actionOnReap.isNull() ) {
        QJsonArray arr = actionOnReap.toArray();

        QList<ActionOnReap> list = createActionOnReap( arr );

        proj->setActionOnReap( list );
    }

}

void AssetsParser::populatePhysics(StarPhysics &phys, QString physicName, QJsonObject &rootObject)
{
    phys.setPhysicName( physicName );

    QJsonValue mass =                       rootObject[ "mass" ];
    QJsonValue gravityMultiplier =          rootObject[ "gravityMultiplier" ];
    QJsonValue bounceFactor =               rootObject[ "bounceFactor" ];
    QJsonValue maxMovementPerStep =         rootObject[ "maxMovementPerStep" ];
    QJsonValue maximumCorrection =          rootObject[ "maximumCorrection" ];
    QJsonValue airFriction =                rootObject[ "airFriction" ];
    QJsonValue liquidFriction =             rootObject[ "liquidFriction" ];
    QJsonValue groundFriction =             rootObject[ "groundFriction" ];
    QJsonValue ignorePlatformCollision =    rootObject[ "ignorePlatformCollision" ];
    QJsonValue groundSlideMovementEnabled = rootObject[ "groundSlideMovementEnabled" ];
    QJsonValue collisionPoly =              rootObject[ "collisionPoly" ];

    if( !mass.isNull() )
        phys.setMass( mass.toDouble() );

    if( !gravityMultiplier.isNull() )
        phys.setGravityMultiplier( gravityMultiplier.toDouble() );

    if( !bounceFactor.isNull() )
        phys.setBounceFactor( bounceFactor.toDouble() );

    if( !maxMovementPerStep.isNull() )
        phys.setMaxMovementPerStep( maxMovementPerStep.toDouble() );

    if( !maximumCorrection.isNull() )
        phys.setMaximumCorrection( maximumCorrection.toDouble() );

    if( !airFriction.isNull() )
        phys.setAirFriction( airFriction.toDouble() );

    if( !liquidFriction.isNull() )
        phys.setLiquidFriction( liquidFriction.toDouble() );

    if( !groundFriction.isNull() )
        phys.setGroundFriction( groundFriction.toDouble() );

    if( !ignorePlatformCollision.isNull() )
        phys.setIgnorePlatformCollision( ignorePlatformCollision.toBool() );

    if( !groundSlideMovementEnabled.isNull() )
        phys.setGroundSlideMovementEnabled( groundSlideMovementEnabled.toBool() );

    if( !collisionPoly.isNull() ) {
        Polygon poly;
        QJsonArray arr = collisionPoly.toArray();

        for( int i = 0; i < arr.size(); i++ ) {
            QJsonArray pt = arr[i].toArray();
            QPointF point;
            point.setX( pt[0].toDouble() );
            point.setY( pt[1].toDouble() );

            poly.append( point );
        }
        phys.setCollisionPoly( poly );
    }
}

StatusEffect *AssetsParser::populateStatusEffect( QJsonObject &rootObject)
{
    return StatusEffect::fromJsonObject( rootObject );
}

QList<ActionOnReap> AssetsParser::createActionOnReap( QJsonArray &array )
{
    QList<ActionOnReap> aorList;
    for( int i = 0; i < array.size(); i++ ) {
        QJsonObject obj = array[i].toObject();
        ActionOnReap aor;

        QJsonValue action = obj[ "action" ];
        QJsonValue delaySteps = obj[ "delaySteps" ];

        int actionType = -1;
        if( !action.isNull() ) {
            actionType = stringToActionType( action.toString() );
            aor.action = actionType;
        }

        if( !delaySteps.isNull() )
            aor.delaySteps = delaySteps.toInt();

        if( actionType == Projectile ) {
            QJsonValue projectileType = obj[ "type" ];
            QJsonValue inheritDamageFactor = obj[ "inheritDamageFactor" ];
            QJsonValue angle = obj[ "angle" ];
            QJsonValue adjustAngle = obj[ "adjustAngle" ];
            QJsonValue fuzzAngle = obj[ "fuzzAngle" ];

            if( !projectileType.isNull() )
                aor.projectileType = projectileType.toString();

            if( !inheritDamageFactor.isNull() )
                aor.inheritDamageFactor = inheritDamageFactor.toDouble();

            if( !angle.isNull() )
                aor.angle = angle.toInt();
            else
                aor.angle = -1;

            if( !adjustAngle.isNull() )
                aor.adjustAngle = adjustAngle.toInt();
            else
                aor.angle = -1;

            if( !fuzzAngle.isNull() )
                aor.fuzzAngle = fuzzAngle.toInt();
            else
                aor.angle = -1;

        } else if( actionType == Explosion ) {
            QJsonValue foregroundRadius = obj[ "foregroundRadius" ];
            QJsonValue backgroundRadius = obj[ "backgroundRadius" ];
            QJsonValue explosiveDamageAmount = obj[ "explosiveDamageAmount" ];

            if( !foregroundRadius.isNull() )
                aor.foregroundRadius = foregroundRadius.toInt();

            if( !backgroundRadius.isNull() )
                aor.backgroundRadius = backgroundRadius.toInt();

            if( !explosiveDamageAmount.isNull() )
                aor.explosiveDamageAmount = explosiveDamageAmount.toInt();
        } else if( actionType == Liquid ) {
            QJsonValue liquidId = obj[ "liquidId" ];
            QJsonValue quantity = obj[ "quantity" ];

            if( !liquidId.isNull() )
                aor.liquidId = liquidId.toInt();

            if( !quantity.isNull() )
                aor.quantity = quantity.toInt();
        } else if( actionType == Sounds ) {
            QJsonValue option = obj[ "options" ];

            if( !option.isNull() ) {
                QJsonArray arr = option.toArray();
                QStringList list;
                for( int i = 0; i < arr.size(); i++ ) {
                    list.append( arr[i].toString() );
                }
                aor.sounds = list;
            }
        } else if( actionType == Config ) {
            QJsonValue file = obj[ "file" ];
            if( !file.isNull() ) {
                aor.configFile = file.toString();
            }
        }

        aorList.append( aor );

    }

    return aorList;
}
