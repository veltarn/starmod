#include "projectilesutility.h"

ProjectilesUtility::ProjectilesUtility()
{
}

void ProjectilesUtility::loadProjectilesList( QComboBox *box, QString assetsPath, QString projectFilePath ) {
    QString gunProjectilesPath = assetsPath + "/projectiles/guns";
    QString projectGunProjectilesPath = projectFilePath + "/projectiles/guns";

    box->addItem( "-", "-" );

    //Listing custom created projectiles
    QStringList filesList;
    Utility::recurseAddDir( QDir( projectGunProjectilesPath ), filesList, QStringList( "projectile" ) );
    bool hasFiles = ( filesList.size() > 0 );

    foreach( QString file, filesList ) {
        if( !ProjectilesUtility::addToComboboxFromFile( box, file ) )
            continue;
    }

    if( hasFiles ) {
        box->insertSeparator( filesList.size() );
    }

    filesList.clear();
    //Listing game projectiles
    Utility::recurseAddDir( QDir( gunProjectilesPath ), filesList, QStringList( "projectile" ) );
    foreach( QString file, filesList ) {
        if( !ProjectilesUtility::addToComboboxFromFile( box, file ) )
            continue;
    }
}

bool ProjectilesUtility::addToComboboxFromFile( QComboBox *box, QString file ) {
    QFileInfo fifo( file );
    QFile projFile( file );
    if( !projFile.open( QIODevice::ReadOnly ) ) {
        qDebug() << "Cannot open " << projFile.fileName() << endl;
        return false;
    }

    QJsonDocument doc = QJsonDocument::fromJson( projFile.readAll() );
    QJsonObject root = doc.object();

    QJsonValue projName = root[ "projectileName" ];

    if( projName.isUndefined() ) {
        qWarning() << "projectileName property does not exists in file " << file;
        return false;
    }

    QJsonValue icon = root[ "damageKindImage" ];
    if( !icon.isUndefined() ) {
        QString iconPath = QFileInfo( file ).path() + "/" + icon.toString();
        box->addItem( QIcon( QPixmap( iconPath ) ), projName.toString(), file );
    } else {
        box->addItem( projName.toString(), file );
    }
    projFile.close();
    return true;
}
