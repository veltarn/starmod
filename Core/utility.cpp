#include "utility.h"

Utility::Utility()
{
}

QString Utility::qColorToQString(QColor color)
{
    QString colorStr;
    colorStr += "R: " + QString::number( color.red() ) + " ";
    colorStr += "G: " + QString::number( color.green() ) + " ";
    colorStr += "B: " + QString::number( color.blue() );

    return colorStr;
}

QStringList Utility::reverseArray( QStringList list ) {
    QStringList reversedList;
    reversedList.reserve( list.size() );
    std::reverse_copy( list.begin(), list.end(), std::back_inserter( reversedList ) );
    return reversedList;
}

QString Utility::convertAppPathToProject( QString assetAppPath, QString projectPath ) {
    QStringList splitted = assetAppPath.split( "Starbound/" );

    if( splitted.size() > 0 ) {
        QString relativePath = splitted.at( splitted.size() - 1 );
        QString fullPath = projectPath + "/files/" + relativePath;

        return fullPath;
    }

    return QString();
}

QString Utility::getCommonPath( QString path, QString separator ) {
    QStringList cutPath = path.split( separator );

    if( cutPath.size() > 0 ) {
        return cutPath.at( cutPath.size() - 1 );
    }
    return QString();
}

void Utility::recurseAddDir( QDir d, QStringList &list, QStringList extensionFilter, QStringList excludeExtensions ) {
    QStringList entries = d.entryList( QDir::NoDotAndDotDot | QDir::Dirs | QDir::Files );

    foreach( QString file, entries ) {
        QFileInfo fifo( QString( "%1/%2" ).arg( d.path() ).arg( file ) );

        if( fifo.isSymLink() )
            return;

        if( fifo.isDir() ) {
            QString dirname = fifo.fileName();
            QDir sd( fifo.filePath() );

            recurseAddDir( sd, list, extensionFilter, excludeExtensions );
        } else {
            if( extensionFilter.size() > 0 ) {
                if( Utility::exists( fifo.suffix(), extensionFilter ) )
                    list << QDir::toNativeSeparators( fifo.filePath() );
            } else if( excludeExtensions.size() > 0 ) {
                if( !Utility::exists( fifo.suffix(), excludeExtensions ) ) {
                    list << QDir::toNativeSeparators( fifo.filePath() );
                }
            } else {
                list << QDir::toNativeSeparators( fifo.filePath() );
            }
        }
    }
}
