#ifndef ASSETSPARSER_H
#define ASSETSPARSER_H

#include <iostream>
#include <QObject>
#include <QString>
#include "../Json/jsonvalidator.h"
#include "asset.h"
#include "Exceptions/fileexception.h"
#include "Exceptions/assetsparserexception.h"
#include "../Starbound/Items/staritem.h"
#include "../Starbound/Items/starweapon.h"
#include "../Starbound/Items/starconsumable.h"
#include "../Starbound/Items/starminingtool.h"
#include "../Starbound/Items/starflashlight.h"
#include "../Starbound/Items/starbeamaxe.h"
#include "../Starbound/Items/starmatitem.h"
#include "../Starbound/Items/starinstrument.h"
#include "../Starbound/Items/starsword.h"
#include "../Starbound/Items/starthrown.h"
#include "../Starbound/Items/starworldobject.h"
#include "../Starbound/starphysics.h"
#include "../Starbound/Items/starcoin.h"
#include "../Starbound/statuseffect.h"

class AssetsParser
{
public:
    AssetsParser();

    AssetPtr parseAsset( QString &file );
    QList<StarPhysics> parsePhysicsFile( QString physicsFile );

private:
    void populateItem( StarItem* item, QJsonObject &rootObject );
    void populateHandedItem( StarHandedItem* item, QJsonObject &rootObject );
    void populateEquipableItem( StarEquipableItem* item, QJsonObject &rootObject );
    void populateGun( StarWeapon* item, QJsonObject &rootObject );
    void populateConsumable( StarConsumable* item, QJsonObject &rootObject );
    void populateMiningTool( StarMiningTool* item, QJsonObject &rootObject );
    void populateFlashlight( StarFlashlight* item, QJsonObject &rootObject );
    void populateBeamaxe( StarBeamaxe* item, QJsonObject &rootObject );
    void populateMatItem( StarMatItem* item, QJsonObject &rootObject );
    void populateInstrument( StarInstrument* item, QJsonObject &rootObject );
    void populateSword( StarSword* item, QJsonObject &rootObject );
    void populateThrown( StarThrown* item, QJsonObject &rootObject );
    void populateWorldObject( StarWorldObject* item, QJsonObject &rootObject );
    void populateStarCoin( StarCoin *item, QJsonObject &rootObject );
    void populateProjectile( StarProjectile *proj, QJsonObject &rootObject );
    void populatePhysics( StarPhysics &phys, QString physicName, QJsonObject &rootObject );
    StatusEffect* populateStatusEffect( QJsonObject &rootObject );

    QList<ActionOnReap> createActionOnReap( QJsonArray &array );
};

#endif // ASSETSPARSER_H
