#include "zip.h"
#include "miniz.c"

using namespace std;

Zip::Zip( QObject *parent ) : QObject( parent )  {
}

void Zip::addFile( QString file, QString nameInArchive ) {
    m_filePaths[file] = nameInArchive;
    qDebug() << "Added file " << file << " as " << nameInArchive << endl;
}

void Zip::removeFile( QString file ) {
    for( QMap<QString, QString>::iterator it = m_filePaths.begin(); it != m_filePaths.end(); ++it ) {
        if( it.key() == file )
        {
            m_filePaths.erase( it );
            break;
        }
    }
}

QMap<QString, QString> Zip::filesList() const {
    return m_filePaths;
}

bool Zip::createArchive( QString archiveName ) {
    qDebug() << "Creating archive \"" << archiveName << "\"" << endl;
    qDebug() << m_filePaths.size() << " file(s) to archive" << endl;
    QProgressDialog progress( tr( "Creating archive" ), tr( "Cancel" ), 0, m_filePaths.size() );
    progress.show();

    bool opOk = true;

    int i = 0;
    for( QMap<QString, QString>::iterator it = m_filePaths.begin(); it != m_filePaths.end(); ++it ) {
        progress.setValue( i );

        QApplication::processEvents( QEventLoop::ExcludeUserInputEvents );

        if( progress.wasCanceled() ) {
            QFile::remove( archiveName );
            qDebug() << "Cancelled zipping operation, removed zip file" << endl;
            return false;
        }

        QString source = it.key();
        QString dest = it.value();
        dest = QDir::fromNativeSeparators( dest );
        QFile file( source );
        file.open( QIODevice::ReadOnly );
        quint64 size = file.size();

        QByteArray buffer = file.readAll();

        qDebug() << "Adding " << file.fileName() << "(" << size << ")" << "to " << dest << endl;

        mz_bool ok = mz_zip_add_mem_to_archive_file_in_place( archiveName.toStdString().c_str(), dest.toStdString().c_str(), buffer.data(), buffer.size(), "NC", (size_t)strlen("NC"), MZ_DEFAULT_COMPRESSION );

        if ( !ok ) {
            opOk = false;
        }
        i++;
    }
    return opOk;
}
