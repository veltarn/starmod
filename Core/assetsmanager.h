#ifndef ASSETSMANAGER_H
#define ASSETSMANAGER_H

#include <QCoreApplication>
#include <QObject>
#include <QMap>
#include <QFile>
#include <QString>
#include <QVariant>
#include <QFileInfo>
#include <QDebug>
#include <QProgressDialog>
#include <QMessageBox>
#include <QList>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QSortFilterProxyModel>
#include <QIcon>
#include <QVariant>
#include "../Json/jsonvalidator.h"
#include "Exceptions/assetsexception.h"
#include "assetsparser.h"
#include "utility.h"
#include "cache.h"
#include "../GUI/progresssplashscreen.h"
#include "logger.h"
#include "../Starbound/Items/staritem.h"
#include "../Starbound/Items/starweapon.h"
#include "../Starbound/Items/starconsumable.h"
#include "../Starbound/Items/starminingtool.h"
#include "../Starbound/Items/starflashlight.h"
#include "../Starbound/Items/starbeamaxe.h"
#include "../Starbound/Items/starmatitem.h"
#include "../Starbound/Items/starinstrument.h"
#include "../Starbound/Items/starsword.h"
#include "../Starbound/Items/starthrown.h"
#include "../Starbound/Items/starworldobject.h"
#include "../Starbound/Items/starcoin.h"
#include "../Starbound/starphysics.h"
#include "../Starbound/statuseffect.h"

enum FileType {
    Image, Text, Lua, Json, Unkwown
};

class AssetsManager : public QObject
{
    Q_OBJECT
public:
    explicit AssetsManager( QString starboundAssetsPath, QString starboundPath, QObject *parent = 0);
    ~AssetsManager();

    FileType getFileType( QString filepath );
    void parseAssets( ProgressSplashScreen *splash = NULL, bool createProgressDialog = false );

    StarItem *getItem( int row );
    StarItem *getItem( QString itemName );
    int itemsSize();

    QSortFilterProxyModel *createObjectsModel();

    QString assetsPath() const;

    QStringList assetsExtensions() const;
    QStringList getItemsListByCateory( QString category );

    void clearAssetsList();

    QString starboundPath() const;
    void setStarboundPath(const QString &starboundPath);

    QStringList getRecipesFilters();

    /**
     * @brief This method creates the item "item" by calling it's save function and add it to the custom items list
     * @param modPath
     * @return true if the item has been created, false otherwise
     */
    bool createItem( QString modPath, StarItem *item );

    QList<StarPhysics> physicsList() const;
    QList<AssetPtr> assetsList();

signals:
    void newProjectFiles( QStringList );
public slots:

private:
    void parseFile( QFile &file );

    void populateItem( StarItem *item, QJsonObject &rootObject );
    void populateHandedItem( StarHandedItem *item, QJsonObject &rootObject );
    void populateEquipableItem( StarEquipableItem *item, QJsonObject &rootObject );
    void populateGun( StarWeapon *item, QJsonObject &rootObject );
    void populateConsumable( StarConsumable *item, QJsonObject &rootObject );
    void populateMiningTool( StarMiningTool *item, QJsonObject &rootObject );
    void populateFlashlight( StarFlashlight *item, QJsonObject &rootObject );
    void populateBeamaxe( StarBeamaxe *item, QJsonObject &rootObject );
    void populateMatItem( StarMatItem *item, QJsonObject &rootObject );
    void populateInstrument( StarInstrument *item, QJsonObject &rootObject );
    void populateSword( StarSword *item, QJsonObject &rootObject );
    void populateThrown( StarThrown *item, QJsonObject &rootObject );
    void populateWorldObject( StarWorldObject *item, QJsonObject &rootObject, QStringList &craftingFilter );
private:
    // Extracted path
    QString m_starboundAssetsPath;
    //Game path
    QString m_starboundPath;
    QStringList m_assetsExtensions; //Assets parsed by the paser
    QStringList m_jsonFileExtensions; //Json file extensions (used to recognise json files)
    //QList< StarItem* > m_data;
    QList< AssetPtr > m_data;
    QList< StarPhysics > m_physicsList; //!< List of physics properties in physics.config
    QSortFilterProxyModel *m_itemsListModel; //!< Item list for the objectView
};

#endif // ASSETSMANAGER_H
