#include "logger.h"


void Logger::addEntry(LogType type, QString msg)
{
    LogMessage log;
    log.msgType = type;
    log.message = msg;

    m_log.append( log );

    qDebug() << msg << endl;

    if( type == Error )
        emit errorMessage( log );
}

LogMessage Logger::lastErrorMessage() {
    for( LogMessageList::iterator it = m_log.end(); it != m_log.begin(); --it ) {
        LogMessage tmp = *it;
        if( tmp.msgType == Error ) {
            return tmp;
        }
    }
    LogMessage tmp;
    tmp.msgType = Invalid;

    return tmp;
}

QList<LogMessage> Logger::log() const
{
    return m_log;
}

