#ifndef PROJECT_H
#define PROJECT_H

#include <QObject>
#include <QDebug>
#include <QString>
#include <QMap>
#include <QtXml>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QStringList>
#include <QTextStream>
#include <QMessageBox>
#include "utility.h"


class Project : public QObject
{
    Q_OBJECT
public:
    explicit Project(QObject *parent = 0);
    Project( QString name, QString author, QString projectPath, QString version, QString starboundVersion, QObject *parent = 0 );

    void setProjectName( const QString name );
    QString projectName() const;
    void setProjectAuthor( const QString author );
    QString projectAuthor() const;
    void setProjectPath( const QString projectPath );
    QString projectPath() const;
    void setProjectVersion( const QString version );
    QString projectVersion() const;

    void setStarboundVersion( const QString version );
    QString starboundVersion() const;

    QStringList getOpenFilesList() const;
    void addOpenFileToQueue( const QString filepath );
    bool removeOpenFile( QString filepath );

    QStringList getEditedFiles() const;
    void addEditedFile( const QString file );
    //void removeEditedFile( const QString file );

    bool save();
    void close();
    bool load( QString path );

    void setModified( bool modified );
    bool isModified() const;


    QString getLastError() const;

    /**
     * @brief This method writes the .modinfo file used by Starbound to recognize a mod
     * @return true if the file has successfully been write, false otherwise
     */
    bool writeModinfoFile();
    
signals:
    
public slots:

private:
    QString m_projectName;
    QString m_projectAuthor;
    QString m_projectPath;
    QString m_projectVersion;
    QString m_starboundVersion;
    QStringList m_editedFiles;
    QStringList m_openFiles;
    bool m_isModified;

    QString m_lastError;
};

QDebug operator << ( QDebug dbg, const Project &prj );
QDebug operator << ( QDebug dbg, const Project *prj );

#endif // PROJECT_H
