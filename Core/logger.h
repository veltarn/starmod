#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include <QDebug>
#include <QList>
#include <QString>
#include "singleton.hpp"

enum LogType {
    Invalid, Info, Debug, Warning, Error
};

struct LogMessage {
    LogType msgType;
    QString message;
};

typedef QList<LogMessage> LogMessageList;

class Logger : public QObject, public Singleton<Logger>
{
    Q_OBJECT
    friend class Singleton<Logger>;
public:
    void addEntry( LogType type, QString msg );
    LogMessage lastErrorMessage();
    QList<LogMessage> log() const;

signals:
    void errorMessage( LogMessage );
private:
    QList<LogMessage> m_log;

};

#endif // LOGGER_H
