#ifndef STARVARIANT_H
#define STARVARIANT_H

#include <QSharedPointer>
#include "../Starbound/Items/staritem.h"
#include "../Starbound/Items/starbeamaxe.h"
#include "../Starbound/Items/starconsumable.h"
#include "../Starbound/Items/starequipableitem.h"
#include "../Starbound/Items/starflashlight.h"
#include "../Starbound/Items/starhandeditem.h"
#include "../Starbound/Items/starinstrument.h"
#include "../Starbound/Items/starmatitem.h"
#include "../Starbound/Items/starminingtool.h"
#include "../Starbound/Items/starsword.h"
#include "../Starbound/Items/starthrown.h"
#include "../Starbound/Items/starweapon.h"
#include "../Starbound/Items/starworldobject.h"
#include "../Starbound/starprojectile.h"
#include "../Starbound/starrecipe.h"
#include "../Starbound/statuseffect.h"

/*union StarVariant_data {
    StarItemPtr item;
    StarProjectilePtr projectile;
    StatusEffectPtr statusEffect;
    StarRecipePtr recipe;
};

class StarVariant
{
public:
    enum FileType {
        ITEM, PROJECTILE, STATUS_EFFECT, RECIPE, INVALID
    };

public:
    StarVariant();
    StarVariant( StarVariant const &orig );
    StarVariant( StarItemPtr item );
    StarVariant( StarProjectilePtr proj );
    StarVariant( StatusEffectPtr statusEffect );
    StarVariant( StarRecipePtr recipe );
    ~StarVariant();

    bool isNull() const;

    StarItemPtr toStarItem();
    StarProjectilePtr toStarProjectile();
    StatusEffectPtr toStatusEffect();
    StarRecipePtr toStarRecipe();

    bool operator==( StarVariant const &b );
    bool operator!=( StarVariant const &b );
private:
    bool m_isNull;
    int m_fileType;
    StarVariant_data m_data;
};
*/
#endif // STARVARIANT_H
