#include "project.h"

Project::Project(QObject *parent) :
    QObject(parent),
    m_projectName( "" ),
    m_projectAuthor( "" ),
    m_projectPath( "" ),
    m_projectVersion( "" ),
    m_isModified( false )
{
}

Project::Project( QString name, QString author, QString projectPath, QString version, QString starboundVersion, QObject *parent ) :
    QObject( parent ),
    m_projectName( name ),
    m_projectAuthor( author ),
    m_projectPath( projectPath ),
    m_projectVersion( version ),
    m_starboundVersion( starboundVersion ),
    m_isModified( false )
{

}

void Project::setProjectName( const QString name ) {
    m_projectName = name;
}

QString Project::projectName() const {
    return m_projectName;
}

void Project::setProjectAuthor( const QString author ) {
    m_projectAuthor = author;
}

QString Project::projectAuthor() const {
    return m_projectAuthor;
}

void Project::setProjectPath( const QString projectPath ) {
    m_projectPath = projectPath;
}

QString Project::projectPath() const {
    return m_projectPath;
}

void Project::setProjectVersion( const QString version ) {
    m_projectVersion = version;
}

QString Project::projectVersion() const {
    return m_projectVersion;
}

void Project::setStarboundVersion( const QString version ) {
    m_starboundVersion = version;
}

QString Project::starboundVersion() const {
    return m_starboundVersion;
}

bool Project::save() {
    QDomDocument starSave( "StarModSave" );
    QDomProcessingInstruction pri = starSave.createProcessingInstruction("xml", "version='1.0' encoding='UTF-8' standalone='no'");
    starSave.appendChild( pri );
    QDomNode root = starSave.createElement( "StarSave" );

    starSave.appendChild( root );

    QDomElement nameElmt = starSave.createElement( "ProjectName" );
    QDomText nameText = starSave.createTextNode( "ProjectNameText" );
    nameText.setData( m_projectName );
    nameElmt.appendChild( nameText );
    root.appendChild( nameElmt );

    QDomElement authorElmt = starSave.createElement( "ProjectAuthor" );
    QDomText authorText = starSave.createTextNode( "AuthorText" );
    authorText.setData( m_projectAuthor );
    authorElmt.appendChild( authorText );
    root.appendChild( authorElmt );

    QDomElement versionElmt = starSave.createElement( "ProjectVersion" );
    QDomText versionText = starSave.createTextNode( "VersionText" );
    versionText.setData( m_projectVersion );
    versionElmt.appendChild( versionText );
    root.appendChild( versionElmt );

    QDomElement starboundVersionElmt = starSave.createElement( "StarboundVersion" );
    QDomText starBText = starSave.createTextNode( "StarBText" );
    starBText.setData( m_starboundVersion );
    starboundVersionElmt.appendChild( starBText );
    root.appendChild( starboundVersionElmt );

    QDomElement pathElmt = starSave.createElement( "ProjectPath" );
    QDomText pathText = starSave.createTextNode( "PathText" );
    pathText.setData( m_projectPath );
    pathElmt.appendChild( pathText );
    root.appendChild( pathElmt );

    QDomElement openedFileElmt = starSave.createElement( "OpenFiles" );
    root.appendChild( openedFileElmt );

    for( QStringList::iterator it = m_openFiles.begin(); it != m_openFiles.end(); ++it ) {
        QString file = *it;

        QDomElement fileElmt = starSave.createElement( "File" );
        QDomText fileText = starSave.createTextNode( "FileText" );
        fileText.setData( file );

        fileElmt.appendChild( fileText );
        openedFileElmt.appendChild( fileElmt );
    }

    QDomElement editedFileElmt = starSave.createElement( "RelatedFiles" );
    root.appendChild( editedFileElmt );

    for( QStringList::iterator it = m_editedFiles.begin(); it != m_editedFiles.end(); ++it ) {

        QDomElement fileElmt = starSave.createElement( "File" );
        QDomText filenameText = starSave.createTextNode( "FilenameText" );
        filenameText.setData( *it );

        fileElmt.appendChild( filenameText  );

        editedFileElmt.appendChild( fileElmt );

    }

    QFile file( m_projectPath );
    if( !file.open( QIODevice::WriteOnly ) ) {
        return false;
    }

    QTextStream stream( &file );
    stream.setCodec( "UTF-8" );
    stream << starSave.toString();

    if( file.flush() )
        return true;
    else {
        m_lastError = tr( "Unable to write into XML file" );
        return false;
    }
}

bool Project::load( QString path ) {
    QFile file( path );
    m_openFiles.clear();
    if( !file.open( QIODevice::ReadOnly ) ) {
        return false;
    }

    QDomDocument starOpen( "StarModSave" );
    starOpen.setContent( &file );

    QDomElement root = starOpen.firstChildElement();

    QDomElement elmt = root.firstChildElement();
    while( !elmt.isNull() ) {
        QDomNode valueElmt = elmt.firstChild();

        if( elmt.nodeName() == "ProjectName" )
            m_projectName = valueElmt.nodeValue();

        if( elmt.nodeName() == "ProjectAuthor" )
            m_projectAuthor = valueElmt.nodeValue();

        if( elmt.nodeName() == "ProjectPath" )
            m_projectPath = valueElmt.nodeValue();

        if( elmt.nodeName() == "ProjectVersion" )
            m_projectVersion = valueElmt.nodeValue();

        if( elmt.nodeName() == "StarboundVersion" )
            m_starboundVersion = valueElmt.nodeValue();

        if( elmt.nodeName() == "OpenFiles" ) {
            QDomElement fileElmt = elmt.firstChildElement();

            while( !fileElmt.isNull() ) {
                QDomNode valueFile = fileElmt.firstChild();

                m_openFiles.append( valueFile.nodeValue() );

                fileElmt = fileElmt.nextSiblingElement();
            }
        }

        if( elmt.nodeName() == "RelatedFiles" ) {
            QDomElement fileElmt = elmt.firstChildElement();

            while( !fileElmt.isNull() ) {
                QDomNode valueFile = fileElmt.firstChild();
                m_editedFiles.append( valueFile.nodeValue() );
                fileElmt = fileElmt.nextSiblingElement();
            }
            qDebug() << m_editedFiles << endl;
        }

        elmt = elmt.nextSiblingElement();
    }

    return true;
}

void Project::close() {

}

void Project::setModified( bool modified ) {
    m_isModified = modified;
}

bool Project::isModified() const {
    return m_isModified;
}

QString Project::getLastError() const {
    return m_lastError;
}

QStringList Project::getOpenFilesList() const {
    return m_openFiles;
}

void Project::addOpenFileToQueue( const QString filepath ) {
    if( !Utility::exists( filepath, m_openFiles ) ) {
        m_openFiles.append( filepath );
    }
}

bool Project::removeOpenFile( QString filepath ) {
    return m_openFiles.removeOne( filepath );
}


QStringList Project::getEditedFiles() const {
    return m_editedFiles;
}

void Project::addEditedFile( const QString file ) {
    if( !Utility::exists( file, m_editedFiles ) ) {
        m_editedFiles.append( file );
    }
}

bool Project::writeModinfoFile() {
    QJsonDocument doc;
    QJsonObject root = doc.object();

    root.insert( "name", m_projectName );
    root.insert( "version", m_starboundVersion );
    root.insert( "path", QString( "." ) );
    root.insert( "dependencies", QJsonArray() );

    QFileInfo fifo( m_projectPath );
    QString jsonPath = fifo.path() + "/files/" + m_projectName + ".modinfo";
    QFile jsonFile( jsonPath );

    if( !jsonFile.open( QIODevice::WriteOnly ) ) {
        qDebug() << "Cannot open \"" << jsonPath << "\": " << jsonFile.errorString() << endl;
        return false;
    }

    doc.setObject( root );
    QTextStream stream( &jsonFile );

    stream.setCodec( "UTF-8" );
    stream << doc.toJson();

    if( jsonFile.flush() )
        return true;
    else
        return false;
}

QDebug operator << ( QDebug dbg, const Project &prj ) {
    dbg.nospace() << "( Project " << prj.projectName() << " ) { " << endl
                  << "\tAuthor " << prj.projectAuthor() << endl
                  << "\tVersion " << prj.projectVersion() <<  endl
                  << "\tStarbound version " << prj.starboundVersion() << endl
                  << "\tPath " << prj.projectPath() << endl
                  << "}" << endl;

    return dbg.space();
}


QDebug operator << ( QDebug dbg, const Project *prj ) {
    if( prj != NULL ) {
        dbg.nospace() << "( Project " << prj->projectName() << " ) { " << endl
                      << "\tAuthor " << prj->projectAuthor() << endl
                      << "\tVersion " << prj->projectVersion() <<  endl
                      << "\tStarbound version " << prj->starboundVersion() << endl
                      << "\tPath " << prj->projectPath() << endl
                      << "}" << endl;

        return dbg.space();
    } else {
        return dbg;
    }
}
