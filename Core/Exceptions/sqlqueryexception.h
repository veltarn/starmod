#ifndef SQLQUERYEXCEPTION_H
#define SQLQUERYEXCEPTION_H

#include <exception>
#include <QString>

class SqlQueryException : public std::exception
{
public:
    SqlQueryException(QString text) throw();
    ~SqlQueryException() throw();

    virtual const char *what() const throw();
    QString message() throw();

private:
    QString m_message;
};

#endif // SQLQUERYEXCEPTION_H
