#include "sqlqueryexception.h"

SqlQueryException::SqlQueryException(QString text) throw() : m_message(text)
{
}

SqlQueryException::~SqlQueryException() throw() {

}

const char *SqlQueryException::what() const throw() {
    return m_message.toStdString().c_str();
}


QString SqlQueryException::message() throw() {
    return m_message;
}
