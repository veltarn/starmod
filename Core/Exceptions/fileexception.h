#ifndef FILEEXCEPTION_H
#define FILEEXCEPTION_H

#include <QString>
#include <exception>
#include "../../constants.h"

class FileException : public std::exception
{
public:
    FileException( QString text ) throw();
    virtual ~FileException() throw();

    virtual const char *what() const throw();
private:
    QString m_message;
};

#endif // FILEEXCEPTION_H
