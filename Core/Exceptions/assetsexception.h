#ifndef ASSETSEXCEPTION_H
#define ASSETSEXCEPTION_H

#include <QString>
#include <exception>
#include "../../constants.h"

class AssetsException : public std::exception
{
public:
    enum ErrorType {
        MINOR, WARNING, ERROR, FATAL
    };

public:
    AssetsException( QString text, int errorType ) throw();
    int errorType() const throw();

    virtual const char *what() const throw();

    virtual ~AssetsException() throw();
private:
    QString m_message;
    int m_errorType;
};

#endif // ASSETSEXCEPTION_H
