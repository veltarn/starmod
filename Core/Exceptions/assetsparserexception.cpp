#include "assetsparserexception.h"

AssetsParserException::AssetsParserException(QString text, int errorType) throw() : m_message( text ), m_errorType( errorType )
{
}

AssetsParserException::~AssetsParserException() throw()
{

}

int AssetsParserException::errorType() const throw()
{
    return m_errorType;
}

const char *AssetsParserException::what() const throw()
{
    return m_message.toStdString().c_str();
}


