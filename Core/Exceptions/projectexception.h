#ifndef PROJECTEXCEPTION_H
#define PROJECTEXCEPTION_H

#include <QString>
#include <exception>

class ProjectException : std::exception
{
public:
    ProjectException( QString text ) throw();
    virtual const char *what() const throw();

    virtual ~ProjectException() throw();
private:
    QString m_errorMessage;
};

#endif // PROJECTEXCEPTION_H
