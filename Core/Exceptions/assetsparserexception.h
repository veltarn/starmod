#ifndef ASSETSPARSEREXCEPTION_H
#define ASSETSPARSEREXCEPTION_H

#include <exception>
#include <QString>
#include "../../constants.h"

class AssetsParserException : public std::exception
{
public:
    enum ErrorType {
        MINOR, WARNING, ERROR
    };

public:
    AssetsParserException( QString text, int errorType ) throw();
    virtual ~AssetsParserException() throw();

    int errorType() const throw();
    virtual const char *what() const throw();


private:
    QString m_message;
    int m_errorType;
};

#endif // ASSETSPARSEREXCEPTION_H
