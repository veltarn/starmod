#include "projectexception.h"

ProjectException::ProjectException( QString text ) throw() : exception(), m_errorMessage( text )
{
}

ProjectException::~ProjectException() throw() {
}

const char *ProjectException::what() const throw() {
    return m_errorMessage.toStdString().c_str();
}
