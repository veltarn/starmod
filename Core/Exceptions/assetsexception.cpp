#include "assetsexception.h"

AssetsException::AssetsException(QString text, int errorType) throw() : m_message( text ), m_errorType( errorType )
{

}

int AssetsException::errorType() const throw()
{
    return m_errorType;
}

const char *AssetsException::what() const throw()
{
    return m_message.toStdString().c_str();
}

AssetsException::~AssetsException() throw()
{

}
