#include "fileexception.h"

FileException::FileException( QString text ) throw() : m_message( text )
{
}

FileException::~FileException() throw()
{

}

const char *FileException::what() const throw()
{
    return m_message.toStdString().c_str();
}
