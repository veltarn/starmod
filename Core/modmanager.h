#ifndef MODMANAGER_H
#define MODMANAGER_H

#include <QObject>
#include <QApplication>
#include <QStringList>
#include <QString>
#include <QFileInfo>
#include <QDebug>
#include "utility.h"
#include "zip.h"

class ModManager : public QObject
{
    Q_OBJECT
public:
    explicit ModManager( QString projectPath, QString zipDestination, QObject *parent = 0);
    
    static bool installMod( QString projectPath, QString projectName, QString starboundModPath );
    bool installMod( QString projectName, QString starboundModPath );
    static bool uninstallMod( QString modPath );

    bool pack();

    QString zipDestination() const;
    void setZipDestination( QString dest );
private:
    void createFileList( QString filesDirectory );
private:
    QString m_projectPath;
    QString m_filesDirectory;
    QString m_zipDestination;
    Zip m_zipHandler;
};

#endif // MODMANAGER_H
