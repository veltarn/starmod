#ifndef PROJECTILESUTILITY_H
#define PROJECTILESUTILITY_H

#include <QString>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonParseError>
#include <QComboBox>
#include <QFile>
#include <QFileInfo>
#include "utility.h"

class ProjectilesUtility
{
public:
    ProjectilesUtility();
    /**
     * @brief loadProjectilesList
     * @param box
     * @param assetsPath
     * @param projectFilePath
     * @todo Remplacer la recherche dans les fichiers par l'utilisation des classes contenues dans le assets manager
     */
    static void loadProjectilesList( QComboBox *box, QString assetsPath, QString projectFilePath );
    static bool addToComboboxFromFile( QComboBox *box, QString file );
};

#endif // PROJECTILESUTILITY_H
