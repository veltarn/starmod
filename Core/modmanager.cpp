#include "modmanager.h"

ModManager::ModManager( QString projectPath, QString zipDestination, QObject *parent) :
    QObject(parent),
    m_projectPath( projectPath ),
    m_zipDestination( zipDestination )
{
    QFileInfo fileInfo( projectPath );
    QString filesPath = fileInfo.path() + "/files";

    createFileList( filesPath );
}

void ModManager::createFileList( QString filesDirectory ) {
    QStringList filesToZip;

    Utility::recurseAddDir( QDir( filesDirectory ), filesToZip );

    foreach( QString file, filesToZip ) {
        QString commonPath = Utility::getCommonPath( file, "files" + QString( QDir::separator() ) );

        m_zipHandler.addFile( file, commonPath );
    }
}

bool ModManager::pack() {
    qDebug() << "Creating zip archive..." << endl;
    bool ok = m_zipHandler.createArchive( m_zipDestination );

    qDebug() << "Done";

    if( !ok )
        qDebug() << " with errors...";

    qDebug() << endl;

    return ok;
}

QString ModManager::zipDestination() const {
    return m_zipDestination;
}

void ModManager::setZipDestination( QString dest ) {
    m_zipDestination = dest;
}

bool ModManager::installMod( QString projectPath, QString projectName, QString starboundModPath ) {
    bool error = false;
    QString projectFiles = QFileInfo( projectPath ).path() + "/files";
    QDir modRoot( starboundModPath );
    modRoot.cdUp(); // Locating modRoot on the root folder to create mod's directory

    if( modRoot.mkpath( projectName ) ) {
        modRoot.cd( projectName );

        //Creating file list to copy
        QStringList filesList;
        QDir projectDir( projectFiles );
        Utility::recurseAddDir( projectDir, filesList );

        QProgressDialog progress( tr( "Copying mod files to Starbound location" ), tr( "Cancel" ), 0, filesList.size() );
        progress.setWindowTitle( tr( "Copying" ) );
        progress.show();

        for( int i = 0; i < filesList.size(); i++ ) {
            progress.setValue( i );

            QApplication::processEvents( QEventLoop::ExcludeUserInputEvents );

            if( progress.wasCanceled() ) {
                progress.setLabelText( tr( "Cancelled, removing files..." ) );
                uninstallMod( starboundModPath );
                return false;
            }

            QString relativePath = Utility::getCommonPath( filesList.at( i ), "files" + QString( QDir::separator() ) );
            QString modFilePath = starboundModPath + "/" + relativePath;

            modFilePath = QDir::toNativeSeparators( modFilePath );
            modRoot.mkpath( QFileInfo( modFilePath ).path() );

            QFile file( filesList.at( i ) );
            if( QFile::exists( modFilePath ) )
                QFile::remove( modFilePath );

            if( !file.copy( modFilePath ) ) {
                error = true;
                qDebug() << "Cannot copy \"" << filesList.at( i ) << "\" to \"" << modFilePath << "\"" << ": " << file.errorString() << endl;
            }
        }
    } else {
        error = true;
    }

    return !error;
}

bool ModManager::installMod( QString projectName, QString starboundModPath ) {
    return ModManager::installMod( m_projectPath, projectName, starboundModPath );
}

bool ModManager::uninstallMod( QString modPath ) {
    QDir modDir( modPath );

    return modDir.removeRecursively();
}
