#include "database.h"

namespace Database {
    Database::Database() :
        m_host("localhost"),
        m_hostname("root"),
        m_hostpass(""),
        m_databaseName(DATABASE_NAME)
    {
        initDb();
    }

    Database::~Database()
    {
        if(m_db.isOpen())
        {
            m_db.close();
        }
    }


    void Database::initDb()
    {
        QFile file(m_databaseName);

        if(!file.exists())
        {
            this->open();
            QSqlQuery query(m_db);

            QStringList tablesList;
            tablesList.push_back("CREATE TABLE options ( option_name  VARCHAR( 255 )  PRIMARY KEY NOT NULL, option_value VARCHAR( 255 ) );");
            tablesList.push_back("CREATE TABLE projects ( id INTEGER PRIMARY KEY ASC AUTOINCREMENT NOT NULL, projectName VARCHAR( 250 ) NOT NULL, projectPath TEXT NOT NULL, lastTimeOpened DATETIME, numberTimesOpened INTEGER DEFAULT ( 0 ) );");

            for(int i = 0; i < tablesList.size(); i++)
            {
                if(!query.exec(tablesList.at(i)))
                {
                    QMessageBox::critical(0, QObject::tr("Error creating tables"), QString(QObject::tr("An error as occured during the creation of the table") + QString(" #") + QString::number(i + 1)));
                    return;
                }
            }
            this->close();
        }
    }

    void Database::open()
    {
        m_db = QSqlDatabase::addDatabase("QSQLITE");
        m_db.setHostName(m_host);
        m_db.setUserName(m_hostname);
        m_db.setPassword(m_hostpass);
        m_db.setDatabaseName(m_databaseName);

        if(!m_db.open())
        {
            QMessageBox::critical(0, QObject::tr("Error"), QString(QObject::tr("An error has occured during the connection to the database. Unable to continue") + "<br />" + m_db.lastError().text()));
            exit(1);
        }
    }

    void Database::close()
    {
        m_db.close();
    }

    QSqlDatabase &Database::db()
    {
        return m_db;
    }
}
