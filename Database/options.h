#ifndef OPTIONS_H
#define OPTIONS_H

#include <iostream>
#include <QVariant>
#include <QSqlQuery>
#include <QSqlRecord>
#include "../Core/Exceptions/sqlqueryexception.h"
#include "database.h"

namespace Database {
    class Options
    {
    public:
        Options();
        ~Options();

        QVariant getOption(QString optionName, QVariant defaultValue = QVariant::Invalid);
        /**
         *  Add the value optionName to the options table.
         * If the options already exists, it will update the record
         * @brief setOption
         * @param optionName
         * @param value
         * @return
         */
        void setOption(QString optionName, QVariant value);
        bool optionExists(QString optionName);
    private:
        std::string m_tableName;
        Database m_db;

    };
}

#endif // OPTIONS_H
