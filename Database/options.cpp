#include "options.h"

namespace Database {
    Options::Options() : m_tableName("options")
    {
    }

    Options::~Options()
    {
        if( m_db.db().isOpen() ) {
            m_db.close();
        }
    }

    QVariant Options::getOption(QString optionName, QVariant defaultValue)
    {
         m_db.open();
         QString sql = "SELECT * FROM options WHERE option_name=?";

         QSqlQuery cQuery(sql);

         cQuery.addBindValue(optionName);
         QVariant value = QVariant::Invalid;

         if(cQuery.exec()) {
             while(cQuery.next()) {
                 value = cQuery.record().value("option_value");
             }
         }
         m_db.close();
         return value;
    }

    void Options::setOption(QString optionName, QVariant value)
    {
        if(!optionExists(optionName)) {
            m_db.open();

            QString sql = "INSERT INTO options VALUES(?, ?)";
            QSqlQuery query(sql);

            query.addBindValue(optionName);
            query.addBindValue(value);

            if(!query.exec()) {
                throw SqlQueryException("Unable to set the option.\n" + query.lastError().text());
            }

            m_db.close();
        } else {
            m_db.open();

            QString sql = "UPDATE options SET option_value=? WHERE option_name=?";

            QSqlQuery query(sql);

            query.addBindValue(value);
            query.addBindValue(optionName);

            if(!query.exec()) {
                throw SqlQueryException("Unable to set the option.\n" + query.lastError().text());
            }

            m_db.close();
        }
    }

    bool Options::optionExists(QString optionName)
    {
        m_db.open();

        QString sql = "SELECT COUNT(*) AS nb FROM options WHERE option_name = ?";

        QSqlQuery query(sql);

        query.addBindValue(optionName);

        if(query.exec()) {
            //Should iterate once..
            while(query.next()) {
                int count = query.record().value("nb").toInt();

                m_db.close();
                if(count == 0)
                    return false;
                else
                    return true;
            }
        }

        m_db.close();
    }
}
