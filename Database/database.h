#ifndef DATABASE_H
#define DATABASE_H

#include <iostream>
#include <cstdlib>
#include <QString>
#include <QStringList>
#include <QFile>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QMessageBox>
#include "../constants.h"

namespace Database {
    class Database
    {
    public:
        Database();
        ~Database();

        void open();
        void close();
        //Get a reference of the database
        QSqlDatabase &db();

    private:
        /**
         * Verify if this is the first start of Lunar
         *if it is, it will create the tables
         */
        void initDb();
        void createTables();
    private:
        QSqlDatabase m_db;
        QString m_host;
        QString m_hostname;
        QString m_hostpass;
        QString m_databaseName;
    };
}
#endif // DATABASE_H
