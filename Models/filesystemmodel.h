#ifndef FILESYSTEMMODEL_H
#define FILESYSTEMMODEL_H

#include <QFileSystemModel>
#include <QVariant>
#include <QBrush>
#include <QColor>
#include <QDebug>

class FileSystemModel : public QFileSystemModel
{
    Q_OBJECT
public:
    FileSystemModel( QObject *parent = 0 );
    virtual QVariant data( const QModelIndex &index, int role ) const;

};

#endif // FILESYSTEMMODEL_H
