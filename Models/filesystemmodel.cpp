#include "filesystemmodel.h"

FileSystemModel::FileSystemModel( QObject *parent ) : QFileSystemModel( parent ) {
}

QVariant FileSystemModel::data( const QModelIndex &index, int role ) const {
    /*qDebug() << "#" << role << (role == Qt::BackgroundRole) << endl;
    if( role == Qt::BackgroundRole ) {


        return QColor( 255, 255, 0 );
    }*/
    return QFileSystemModel::data( index, role );
}
