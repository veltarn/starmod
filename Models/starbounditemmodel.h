#ifndef STARBOUNDITEMMODEL_H
#define STARBOUNDITEMMODEL_H

#include <QDebug>
#include <QAbstractItemModel>
#include <QModelIndex>
#include <QFile>
#include <QTextStream>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QFileInfo>
#include <QList>
#include <QVariant>
#include "../Core/assetsmanager.h"
#include "../Starbound/Items/staritem.h"
#include "../Core/utility.h"

class StarboundItemModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit StarboundItemModel( AssetsManager *assets, QObject *parent = 0 );
    virtual int columnCount( const QModelIndex &parent ) const;
    virtual QVariant data( const QModelIndex &index, int role = Qt::DisplayRole ) const;
    virtual Qt::ItemFlags flags( const QModelIndex &index ) const;
    virtual QModelIndex index( int row, int column, const QModelIndex &parent = QModelIndex() ) const;
    virtual QModelIndex parent( const QModelIndex &child ) const;
    virtual int rowCount( const QModelIndex &parent = QModelIndex() ) const;
    virtual bool setData( const QModelIndex &index, const QVariant &value, int role = Qt::DisplayRole );

signals:
    
public slots:

/*protected:
    virtual QModelIndex createIndex(int row, int column, void *data) const;*/

private:
    void parseAssets();
private:

 QString m_assetsPath;
 AssetsManager *m_assetsManager;
    
};

#endif // STARBOUNDITEMMODEL_H
