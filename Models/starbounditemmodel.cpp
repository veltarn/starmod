#include "starbounditemmodel.h"

StarboundItemModel::StarboundItemModel( AssetsManager *assets, QObject *parent ) :
    QAbstractItemModel(parent),
    m_assetsManager( assets )
{
    //m_assetsManager->parseAssets();
}


int StarboundItemModel::columnCount( const QModelIndex &parent ) const {
    return 1;
}

QVariant StarboundItemModel::data( const QModelIndex &index, int role) const {

    StarItem *item = reinterpret_cast<StarItem*>( index.data().value<void*>() );

    if( item != NULL )
        return item->itemName();
    else
        return QVariant();
}

Qt::ItemFlags StarboundItemModel::flags( const QModelIndex &index ) const {
    return Qt::ItemIsEditable;
}

/*QModelIndex StarboundItemModel::createIndex(int row, int column, void *data) const {
    StarItem *item = m_assetsManager;
    return QModelIndex;
}*/

QModelIndex StarboundItemModel::index( int row, int column, const QModelIndex &parent ) const {
    StarItem *item = m_assetsManager->getItem( row );
    qDebug() << sizeof( *item ) << endl;
    QModelIndex index = createIndex( row, 0, (void*)item );

    qDebug() << index.row() << endl;
    return index;
}

QModelIndex StarboundItemModel::parent( const QModelIndex &child ) const {
    return QModelIndex();
}

int StarboundItemModel::rowCount( const QModelIndex &parent ) const {
    return m_assetsManager->itemsSize();
}

bool StarboundItemModel::setData( const QModelIndex &index, const QVariant &value, int role ) {
    //StarItem item = value.value<StarItem>();
    return true;
}
