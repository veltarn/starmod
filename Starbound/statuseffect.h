#ifndef STATUSEFFECT_H
#define STATUSEFFECT_H

#include <QString>
#include <QList>
#include <QStringList>
#include <QVariant>
#include <QMap>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include "../Core/utility.h"

struct Primitive {
    Primitive() : name( "" ), mode( "" ), percentage( -1 ), amount( -1 ) {}

    QString name;
    QString mode;
    int percentage;
    int amount;
};

typedef QList< Primitive >  PrimitivesList;

/**
 * @brief The JsonStatusEffect struct
 * StatusEffect specific to JsonFiles which call one or more effects
 */
struct JsonStatusEffect {
    JsonStatusEffect() : kind( QString() ), amount( 0.0 ), range( 0 ), percentage( 0 ), color( QString() ) {}

    QString kind; //!< Relation to the StatusEffect class
    double amount;
    int range;
    int percentage;
    QString color;
};

class StatusEffect;
struct Tick {
    QList< StatusEffect* > statusEffects;
};

class StatusEffect
{
public:
    StatusEffect();
    StatusEffect( StatusEffect const& orig );
    ~StatusEffect();

    QString kind() const;
    void setKind(const QString &kind);

    double interval() const;
    void setInterval(const double &interval);

    bool percentage() const;
    void setPercentage(bool percentage);

    bool suppressTooltip() const;
    void setSuppressTooltip(bool suppressTooltip);

    QString tooltipLabel() const;
    void setTooltipLabel(const QString &tooltipLabel);

    Tick tick() const;
    void setTick(const Tick &tick);

    QString damageKind() const;
    void setDamageKind(const QString &damageKind);

    bool initialTick() const;
    void setInitialTick(bool initialTick);

    int duration() const;
    void setDuration(int duration);

    QString mainInterfaceIcon() const;
    void setMainInterfaceIcon(const QString &mainInterfaceIcon);

    QString tooltipAmountLabel() const;
    void setTooltipAmountLabel(const QString &tooltipAmountLabel);

    QStringList effectSources() const;
    void addEffectSource( QString effect );
    void clearEffectSources();

    QJsonObject toJsonObject();
    static StatusEffect *fromJsonObject(QJsonObject &rootObject );
    static QJsonObject jsonStatusEffectToJsonObject( JsonStatusEffect effect );
    static JsonStatusEffect qJsonObjectToJsonStatusEffect( QJsonObject obj );

    StatusEffect &operator=( StatusEffect const& orig );
    PrimitivesList primitives() const;
    void setPrimitives(const PrimitivesList &primitives);

private:
    QString m_kind;
    double m_interval;
    bool m_percentage;
    bool m_suppressTooltip;
    PrimitivesList m_primitives;
    QString m_tooltipLabel;
    Tick m_tick;
    QString m_damageKind;
    bool m_initialTick;
    int m_duration;
    QStringList m_effectSources;
    QString m_mainInterfaceIcon;
    QString m_tooltipAmountLabel;
};

typedef QSharedPointer< StatusEffect > StatusEffectPtr;

Q_DECLARE_METATYPE( StatusEffect )
Q_DECLARE_METATYPE( QSharedPointer< StatusEffect > )

#endif // STATUSEFFECT_H
