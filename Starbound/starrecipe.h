#ifndef STARRECIPE_H
#define STARRECIPE_H

#include <QList>
#include <QString>
#include <QStringList>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include "StarTypes.h"

struct Input {
    Input() : item( QString() ), count( 0 ) {}

    QString item;
    int count;
};

typedef Input Output;


class StarRecipe
{
public:
    StarRecipe();
    StarRecipe( StarRecipe const &orig );
    ~StarRecipe();

    void save(QString modPath, QJsonDocument &jsonFile );

    QList<Input> input() const;
    void setInput(const QList<Input> &input);

    Output output() const;
    void setOutput(const Output &output);

    QStringList groups() const;
    void setGroups(const QStringList &groups);


    static void removeInput( QString input, QList< Input > &list );
protected:
    ItemMap serialize();
private:
    QList< Input > m_input;
    Output m_output;
    QStringList m_groups;
};

typedef QSharedPointer< StarRecipe > StarRecipePtr;

Q_DECLARE_METATYPE( StarRecipe )
Q_DECLARE_METATYPE( QSharedPointer< StarRecipe> )

#endif // STARRECIPE_H
