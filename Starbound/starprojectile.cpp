#include "starprojectile.h"

StarProjectile::StarProjectile() :
    m_projectileName( "" ),
    m_timeToLive( 0 ),
    m_level( 1 ),
    m_hydrophobic( false ),
    m_framePath( "" ),
    m_frameNumber( 0 ),
    m_animationCycle( 0.0 ),
    m_animationLoop( true ),
    m_damageType( "default" ),
    m_damageKind( "default" ),
    m_damageKindImage( "" ),
    m_power( 0.0 ),
    m_physics( "" ),
    m_bounces( 0 ),
    m_fallSpeed( 0.0 ),
    m_knockbackPower( 0 ),
    m_speed( 0.0 ),
    m_initialVelocity( 0.0 ),
    m_pointLight( false )
{
}

QStringList StarProjectile::save( QString modPath ) {
    QStringList filesList;
    QString projectilesPath = modPath + "/projectiles/guns";
    QString projectilePath = projectilesPath + "/" + m_projectileName;

    QDir dTmp( projectilesPath );
    dTmp.mkpath( m_projectileName );

    QJsonDocument doc;
    QJsonObject root = doc.object();

    root.insert( "projectileName", m_projectileName );

    if( m_timeToLive > 0 )
        root.insert( "timeToLive", m_timeToLive );

    root.insert( "level", m_level );

    if( m_hydrophobic )
        root.insert( "hydrophobic", m_hydrophobic );

    if( m_framePath != "" ) {
        QString commonPath = Utility::getCommonPath( m_framePath, "files/" );
        root.insert( "frames", "/" + commonPath );
        /*QDir projDir( projectilePath );
        QFileInfo frameInfo( m_framePath );
        if( projDir.exists( frameInfo.fileName() ) ) {
            root.insert( "frames", frameInfo.fileName() );
            filesList.append( m_framePath );
        } else {
            //frame.png
            QFile::copy( m_framePath, projDir.path() + "/" + frameInfo.fileName() );
            filesList.append( projDir.path() + "/" + frameInfo.fileName() );
            root.insert( "frames", frameInfo.fileName() );
        }*/

        root.insert( "frameNumber", QVariant( m_frameNumber ).toInt() );
        root.insert( "animationCycle", m_animationCycle );
        if( m_animationLoop )
            root.insert( "animationLoop", m_animationLoop );
    }

    root.insert( "damageKind", m_damageKind );
    if( m_damageKindImage != "" ) {
        QFileInfo imageInfo( m_damageKindImage );
        QDir projDir( projectilePath );

        if( projDir.exists( imageInfo.fileName() ) ) {
            root.insert( "damageKindImage", imageInfo.fileName() );            
            filesList.append( m_damageKindImage );
        } else {
            QFile::copy( m_damageKindImage, projectilePath + "/" + imageInfo.fileName() );
            root.insert( "damageKindImage", imageInfo.fileName() );
            filesList.append( projectilePath + "/" + imageInfo.fileName() );
        }
    }
    root.insert( "damageType", m_damageType );
    root.insert( "power", m_power );

    if( m_physics != "" )
        root.insert( "physics", m_physics );

    if( m_bounces != 0 )
        root.insert( "bounces", m_bounces );    


    if( m_damagePoly.size() > 0 ) {
        QJsonArray polygon;
        for( PolygonI::iterator it = m_damagePoly.begin(); it != m_damagePoly.end(); ++it ) {
            QPoint vPoint = *it;
            QJsonArray point;
            point.append( vPoint.x() );
            point.append( vPoint.y() );

            polygon.append( point );
        }
        root.insert( "damagePoly", polygon );
    }

    root.insert( "fallSpeed", m_fallSpeed );

    if( m_knockbackPower != 0 )
        root.insert( "knockbackPower", m_knockbackPower );

    if( m_speed != 0 )
        root.insert( "speed", m_speed );

    if( m_initialVelocity != 0 )
        root.insert( "initialVelocity", m_initialVelocity );



    if( m_pointLight ) {
        root.insert( "pointLight", m_pointLight );
        QJsonArray colors;
        colors.append( m_lightColor.red() );
        colors.append( m_lightColor.green() );
        colors.append( m_lightColor.blue() );

        root.insert( "lightColor", colors );
    }

    if( m_emitters.size() > 0 ) {
        QJsonArray array;

        foreach( QString emitter, m_emitters ) {
            array.append( emitter );
        }

        root.insert( "emitters", array );
    }

    if( m_effects.size() > 0 ) {
        QJsonArray array;
        for( QList< JsonStatusEffect >::iterator it = m_effects.begin(); it != m_effects.end(); ++it ) {
            JsonStatusEffect jse = *it;
            QJsonObject statusEffect = StatusEffect::jsonStatusEffectToJsonObject( jse );

            array.append( statusEffect );
        }
        root.insert( "statusEffects", array );
    }

    if( m_actionOnReap.size() > 0 ) {
        QJsonArray arr = saveActionOnReap( m_actionOnReap );
        root.insert( "actionOnReap", arr );
    }

    doc.setObject( root );
    QString projectileFile = projectilePath + "/" + m_projectileName + ".projectile";

    //Verifying if the file already exists
    if( QFile::exists( projectileFile ) ) {
        QFile::remove( projectileFile );
    }

    QFile file( projectileFile );
    if( !file.open( QIODevice::WriteOnly ) ) {
        qWarning() << "Cannot open " << file.fileName() << endl;
        return QStringList();
    }

    QTextStream stream( &file );
    stream << doc.toJson();

    file.flush();
    file.close();
    filesList.append( projectileFile );

    return filesList;
}

QJsonArray StarProjectile::saveActionOnReap(QList<ActionOnReap> &actions)
{
    QJsonArray actionsOnReap;

    foreach( ActionOnReap aor, actions ) {
        QJsonObject obj;
        QJsonArray soundsArray;
        obj.insert( "delaySteps", aor.delaySteps );
        switch( aor.action ) {
            case Projectile:
                obj.insert( "action", QString( "projectile" ) );
                obj.insert( "type", aor.projectileType );
                obj.insert( "inheritDamageFactor", aor.inheritDamageFactor );
                if( aor.angle >= 0 )
                    obj.insert( "angle", aor.angle );

                if( aor.adjustAngle >= 0 )
                    obj.insert( "adjustAngle", aor.adjustAngle );

                if( aor.fuzzAngle >= 0 )
                    obj.insert( "fuzzAngle", aor.fuzzAngle );
            break;

            case Liquid:
                obj.insert( "action", QString( "liquid" ) );
                obj.insert( "liquidId", aor.liquidId );
                obj.insert( "quantity", aor.quantity );
            break;

            case Explosion:
                obj.insert( "action", QString( "explosion" ) );
                obj.insert( "foregroundRadius", aor.foregroundRadius );
                obj.insert( "backgroundRadius", aor.backgroundRadius );
                obj.insert( "explosiveDamageAmount", aor.explosiveDamageAmount );
            break;

            case Sounds:
                foreach( QString sound, aor.sounds ) {
                    soundsArray.append( sound );
                }
                obj.insert( "action", QString( "sounds" ) );
                obj.insert( "options", soundsArray );
            break;

            case Config:
                obj.insert( "action", QString( "config" ) );
                obj.insert( "file", aor.configFile );
            break;

            default:
                obj = QJsonObject(); // Clearing object;
            break;
        }
        actionsOnReap.append( obj );
    }

    return actionsOnReap;
}

bool StarProjectile::load( QString projPath ) {
    return true;
}

QString StarProjectile::projectileName() const
{
    return m_projectileName;
}

QStringList StarProjectile::emitters() const
{
    return m_emitters;
}

void StarProjectile::setEmitters(const QStringList &emitters)
{
    m_emitters = emitters;
}
QString StarProjectile::projectileLocation() const
{
    return m_projectileLocation;
}

void StarProjectile::setProjectileLocation(const QString &projectileLocation)
{
    m_projectileLocation = projectileLocation;
}
QList<ActionOnReap> StarProjectile::actionOnReap() const
{
    return m_actionOnReap;
}

void StarProjectile::addActionOnReap(ActionOnReap actionOnReap)
{
    m_actionOnReap.append( actionOnReap );
}

void StarProjectile::setActionOnReap(const QList<ActionOnReap> &actionOnReap)
{
    m_actionOnReap = actionOnReap;
}



QList<JsonStatusEffect> StarProjectile::effects() const
{
    return m_effects;
}

void StarProjectile::setEffects(const QList< JsonStatusEffect > &effects)
{
    m_effects = effects;
}

QColor StarProjectile::lightColor() const
{
    return m_lightColor;
}

void StarProjectile::setLightColor(const QColor &lightColor)
{
    m_lightColor = lightColor;
}

bool StarProjectile::pointLight() const
{
    return m_pointLight;
}

void StarProjectile::setPointLight(bool pointLight)
{
    m_pointLight = pointLight;
}

double StarProjectile::initialVelocity() const
{
    return m_initialVelocity;
}

void StarProjectile::setInitialVelocity(double initialVelocity)
{
    m_initialVelocity = initialVelocity;
}

double StarProjectile::speed() const
{
    return m_speed;
}

void StarProjectile::setSpeed(double speed)
{
    m_speed = speed;
}

int StarProjectile::knockbackPower() const
{
    return m_knockbackPower;
}

void StarProjectile::setKnockbackPower(int knockbackPower)
{
    m_knockbackPower = knockbackPower;
}

double StarProjectile::fallSpeed() const
{
    return m_fallSpeed;
}

void StarProjectile::setFallSpeed(double fallSpeed)
{
    m_fallSpeed = fallSpeed;
}

PolygonI StarProjectile::damagePoly() const
{
    return m_damagePoly;
}

void StarProjectile::setDamagePoly(const PolygonI &damagePoly)
{
    m_damagePoly = damagePoly;
}

int StarProjectile::bounces() const
{
    return m_bounces;
}

void StarProjectile::setBounces(int bounces)
{
    m_bounces = bounces;
}

QString StarProjectile::physics() const
{
    return m_physics;
}

void StarProjectile::setPhysics(const QString &physics)
{
    m_physics = physics;
}

double StarProjectile::power() const
{
    return m_power;
}

void StarProjectile::setPower(double power)
{
    m_power = power;
}

QString StarProjectile::damageKindImage() const
{
    return m_damageKindImage;
}

void StarProjectile::setDamageKindImage(const QString &damageKindImage)
{
    m_damageKindImage = damageKindImage;
}

QString StarProjectile::damageKind() const
{
    return m_damageKind;
}

void StarProjectile::setDamageKind(const QString &damageKind)
{
    m_damageKind = damageKind;
}

QString StarProjectile::damageType() const
{
    return m_damageType;
}

void StarProjectile::setDamageType(const QString &damageType)
{
    m_damageType = damageType;
}

bool StarProjectile::animationLoop() const
{
    return m_animationLoop;
}

void StarProjectile::setAnimationLoop(bool animationLoop)
{
    m_animationLoop = animationLoop;
}

double StarProjectile::animationCycle() const
{
    return m_animationCycle;
}

void StarProjectile::setAnimationCycle(double animationCycle)
{
    m_animationCycle = animationCycle;
}

uint StarProjectile::frameNumber() const
{
    return m_frameNumber;
}

void StarProjectile::setFrameNumber(const uint &frameNumber)
{
    m_frameNumber = frameNumber;
}

QString StarProjectile::framePath() const
{
    return m_framePath;
}

void StarProjectile::setFramePath(const QString &framePath)
{
    m_framePath = framePath;
}

bool StarProjectile::hydrophobic() const
{
    return m_hydrophobic;
}

void StarProjectile::setHydrophobic(bool hydrophobic)
{
    m_hydrophobic = hydrophobic;
}

int StarProjectile::level() const
{
    return m_level;
}

void StarProjectile::setLevel(int level)
{
    m_level = level;
}

int StarProjectile::timeToLive() const
{
    return m_timeToLive;
}

void StarProjectile::setTimeToLive(int timeToLive)
{
    m_timeToLive = timeToLive;
}

void StarProjectile::setProjectileName(const QString &projectileName)
{
    m_projectileName = projectileName;
}


QString actionTypeToString(int actionType)
{
    switch( actionType ) {
        case Projectile:
            return QString( "Projectile" );
        break;

        case Explosion:
            return QString( "Explosion" );
        break;

        case Liquid:
            return QString( "Liquid" );
        break;

        case Sounds:
            return QString( "Sounds" );
        break;

        case Config:
            return QString( "Config" );
        break;

        default:
            return QString();
        break;
    }
}


int stringToActionType(QString action)
{
    if( action == "projectile" ) {
        return Projectile;
    } else if( action == "explosion" ) {
        return Explosion;
    } else if( action == "liquid" ) {
        return Liquid;
    } else if( action == "sounds" ) {
        return Sounds;
    } else if( action == "config" ) {
        return Config;
    } else {
        return NoAction;
    }
}
