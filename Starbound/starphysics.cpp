#include "starphysics.h"

StarPhysics::StarPhysics() :
    m_physicName( QString() ),
    m_mass( -1.0 ),
    m_gravityMultiplier( -1.0 ),
    m_bounceFactor( 0.0 ),
    m_maxMovementPerStep( 0.0 ),
    m_maximumCorrection( -1.0 ),
    m_airFriction( 0.0 ),
    m_liquidFriction( 0.0 ),
    m_groundFriction( -1.0 ),
    m_ignorePlatformCollision( false ),
    m_groundSlideMovementEnabled( true )
{
}

StarPhysics::StarPhysics(const StarPhysics &orig)
{
    m_physicName =                  orig.m_physicName;
    m_mass =                        orig.m_mass;
    m_gravityMultiplier =           orig.m_gravityMultiplier;
    m_bounceFactor =                orig.m_bounceFactor;
    m_maxMovementPerStep =          orig.m_maxMovementPerStep;
    m_maximumCorrection =           orig.m_maximumCorrection;
    m_airFriction =                 orig.m_airFriction;
    m_liquidFriction =              orig.m_liquidFriction;
    m_groundFriction =              orig.m_groundFriction;
    m_ignorePlatformCollision =     orig.m_ignorePlatformCollision;
    m_groundSlideMovementEnabled =  orig.m_groundSlideMovementEnabled;
    m_collisionPoly =               orig.m_collisionPoly;
}

StarPhysics::~StarPhysics()
{

}

AssetMap StarPhysics::serialize()
{
    if( m_physicName != "" ) {
        AssetMap map;
        QJsonObject rootObject;

        if( m_mass >= 0 )
            rootObject[ "mass" ] = m_mass;


        if( m_bounceFactor >= 0 )
            rootObject[ "bounceFactor" ] = m_bounceFactor;

        if( m_maximumCorrection >= 0 )
            rootObject[ "maximumCorrection" ] = m_maximumCorrection;

        if( m_groundFriction >= 0 )
            rootObject[ "groundFriction" ] = m_groundFriction;

        if( !m_groundSlideMovementEnabled )
            rootObject[ "groundSlideMovementEnabled" ] = m_groundSlideMovementEnabled;


        rootObject[ "gravityMultiplier" ] = m_gravityMultiplier;
        rootObject[ "maxMovementPerStep" ] = m_maxMovementPerStep;
        rootObject[ "airFriction" ] = m_airFriction;
        rootObject[ "liquidFriction" ] = m_liquidFriction;
        rootObject[ "ignorePlatformCollision" ] = m_ignorePlatformCollision;

        QJsonArray arr;
        for( int i = 0; i < m_collisionPoly.size(); i++ ) {
            QJsonArray pt;
            pt.append( m_collisionPoly[i].x() );
            pt.append( m_collisionPoly[i].y() );

            arr.append( pt );
        }

        rootObject[ "collisionPoly" ] = arr;

        map[ m_physicName ] = rootObject;

        return map;
    } else {
        throw AssetsException( QObject::tr( "Cannot save physics data with physics name empty" ), AssetsException::ERROR );
    }
}

QStringList StarPhysics::save(QString physicsFile)
{
    AssetMap map;
    try {
        map = serialize();
    } catch( AssetsException &e ) {
        throw;
    }

    QFile file( physicsFile );


    QJsonDocument doc;
    QJsonObject root;
    if( file.exists() ) {
        if( !file.open( QIODevice::ReadOnly ) ) {
            throw FileException( QObject::tr( "Cannot open" ) + " " + file.fileName() + ", " + file.errorString() );
        }
        QJsonParseError jsonError;
        doc = QJsonDocument::fromJson( file.readAll(), &jsonError );

        if( jsonError.error != QJsonParseError::NoError ) {
            throw AssetsParserException( QObject::tr( "Cannot parse" ) + " " + file.fileName() + ", " + jsonError.errorString(), AssetsParserException::ERROR );
        }

        root = doc.object();
        file.close();
    } else {
        root = doc.object();

        //Creating basic __merge structure
        root.insert( "__merge", QJsonArray() );
    }

    //Searching for an existing property with that name
    if( root.find( m_physicName ) != root.end() ) {
        root.remove( m_physicName ); //Removing it
    }

    //Writing serialized properties
    for( AssetMap::iterator it = map.begin(); it != map.end(); ++it ) {
        root.insert( it.key(), it.value() );
    }

    doc.setObject( root );

    if( !file.open( QIODevice::WriteOnly ) ) {
        throw FileException( QObject::tr( "Cannot open" ) + " " + file.fileName() + ", " + file.errorString() );
    }

    QTextStream stream( &file );
    stream << doc.toJson();

    file.flush();
    file.close();

    QStringList list;
    list << physicsFile;

    return list;
}

QStringList StarPhysics::getPhysicsNameList(QString assetsPath)
{
    /*QString physicsFile = assetsPath + "/projectiles/physics.config";
    QFile file( physicsFile );

    if( !file.open( QIODevice::ReadOnly ) ) {
        throw AssetsException( tr( "Cannot open" ) + physicsFile + ", " + file.errorString(), AssetsException::WARNING );
    }

    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson( file.readAll(), &error );

    if( error == QJsonParseError::NoError ) {
        QJsonObject root = doc.object();
    } else {
        throw AssetsParserException( tr( "Cannot parse" ) + physicsFile + ", " + error.errorString(), AssetsParserException::ERROR );
    }
    return QStringList();*/
}
QString StarPhysics::physicName() const
{
    return m_physicName;
}

void StarPhysics::setPhysicName(const QString &physicName)
{
    m_physicName = physicName;
}
double StarPhysics::mass() const
{
    return m_mass;
}

void StarPhysics::setMass(double mass)
{
    m_mass = mass;
}
double StarPhysics::gravityMultiplier() const
{
    return m_gravityMultiplier;
}

void StarPhysics::setGravityMultiplier(double gravityMultiplier)
{
    m_gravityMultiplier = gravityMultiplier;
}
double StarPhysics::bounceFactor() const
{
    return m_bounceFactor;
}

void StarPhysics::setBounceFactor(double bounceFactor)
{
    m_bounceFactor = bounceFactor;
}
double StarPhysics::maxMovementPerStep() const
{
    return m_maxMovementPerStep;
}

void StarPhysics::setMaxMovementPerStep(double maxMovementStep)
{
    m_maxMovementPerStep = maxMovementStep;
}
double StarPhysics::maximumCorrection() const
{
    return m_maximumCorrection;
}

void StarPhysics::setMaximumCorrection(double maximumCorrection)
{
    m_maximumCorrection = maximumCorrection;
}
double StarPhysics::airFriction() const
{
    return m_airFriction;
}

void StarPhysics::setAirFriction(double airFriction)
{
    m_airFriction = airFriction;
}
double StarPhysics::liquidFriction() const
{
    return m_liquidFriction;
}

void StarPhysics::setLiquidFriction(double liquidFriction)
{
    m_liquidFriction = liquidFriction;
}
double StarPhysics::groundFriction() const
{
    return m_groundFriction;
}

void StarPhysics::setGroundFriction(double groundFriction)
{
    m_groundFriction = groundFriction;
}
bool StarPhysics::ignorePlatformCollision() const
{
    return m_ignorePlatformCollision;
}

void StarPhysics::setIgnorePlatformCollision(bool ignorePlatformCollision)
{
    m_ignorePlatformCollision = ignorePlatformCollision;
}
bool StarPhysics::groundSlideMovementEnabled() const
{
    return m_groundSlideMovementEnabled;
}

void StarPhysics::setGroundSlideMovementEnabled(bool groundSlideMovementEnabled)
{
    m_groundSlideMovementEnabled = groundSlideMovementEnabled;
}
Polygon StarPhysics::collisionPoly() const
{
    return m_collisionPoly;
}

void StarPhysics::setCollisionPoly(const Polygon &collisionPoly)
{
    m_collisionPoly = collisionPoly;
}

StarPhysics &StarPhysics::operator=(const StarPhysics &orig)
{
    m_physicName =                  orig.m_physicName;
    m_mass =                        orig.m_mass;
    m_gravityMultiplier =           orig.m_gravityMultiplier;
    m_bounceFactor =                orig.m_bounceFactor;
    m_maxMovementPerStep =          orig.m_maxMovementPerStep;
    m_maximumCorrection =           orig.m_maximumCorrection;
    m_airFriction =                 orig.m_airFriction;
    m_liquidFriction =              orig.m_liquidFriction;
    m_groundFriction =              orig.m_groundFriction;
    m_ignorePlatformCollision =     orig.m_ignorePlatformCollision;
    m_groundSlideMovementEnabled =  orig.m_groundSlideMovementEnabled;
    m_collisionPoly =               orig.m_collisionPoly;

    return *this;
}












