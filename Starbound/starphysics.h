#ifndef STARPHYSICS_H
#define STARPHYSICS_H

#include <QString>
#include <QFile>
#include <QTextStream>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include "StarTypes.h"
#include "../Core/Exceptions/fileexception.h"
#include "../Core/Exceptions/assetsexception.h"
#include "../Core/Exceptions/assetsparserexception.h"
#include "../Core/cache.h"

class StarPhysics
{
public:
    StarPhysics();
    StarPhysics( StarPhysics const &orig );
    ~StarPhysics();

    AssetMap serialize();
    QStringList save( QString physicsFile );
    /**
     * @brief Get the list of physics effect in the physics.config file
     * @return
     */
    static QStringList getPhysicsNameList( QString assetsPath );

    QString physicName() const;
    void setPhysicName(const QString &physicName);

    double mass() const;
    void setMass(double mass);

    double gravityMultiplier() const;
    void setGravityMultiplier(double gravityMultiplier);

    double bounceFactor() const;
    void setBounceFactor(double bounceFactor);

    double maxMovementPerStep() const;
    void setMaxMovementPerStep(double maxMovementStep);

    double maximumCorrection() const;
    void setMaximumCorrection(double maximumCorrection);

    double airFriction() const;
    void setAirFriction(double airFriction);

    double liquidFriction() const;
    void setLiquidFriction(double liquidFriction);

    double groundFriction() const;
    void setGroundFriction(double groundFriction);

    bool ignorePlatformCollision() const;
    void setIgnorePlatformCollision(bool ignorePlatformCollision);

    bool groundSlideMovementEnabled() const;
    void setGroundSlideMovementEnabled(bool groundSlideMovementEnabled);

    Polygon collisionPoly() const;
    void setCollisionPoly(const Polygon &collisionPoly);

    StarPhysics &operator=( StarPhysics const &orig );

private:
    QString m_physicName; //!< name of the node (default, weather, laser etc)
    double m_mass;
    double m_gravityMultiplier;
    double m_bounceFactor;
    double m_maxMovementPerStep; //!< How much the projectile can move per physic step
    double m_maximumCorrection;
    double m_airFriction;
    double m_liquidFriction;
    double m_groundFriction;
    bool m_ignorePlatformCollision;
    bool m_groundSlideMovementEnabled;
    Polygon m_collisionPoly;
    //int m_speedLimit;

};

#endif // STARPHYSICS_H
