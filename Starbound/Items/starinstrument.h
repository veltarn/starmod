#ifndef STARINSTRUMENT_H
#define STARINSTRUMENT_H

#include "starequipableitem.h"

#define STARINSTRUMENT_DEFAULT_ACTIVE_HAND_POSITION QPointF( 0.0, 0.0 )
#define STARINSTRUMENT_DEFAULT_LARGE_IMAGE QString()
#define STARINSTRUMENT_DEFAULT_IMAGE QString()
#define STARINSTRUMENT_DEFAULT_ACTIVE_IMAGE QString()
#define STARINSTRUMENT_DEFAULT_INSPECTION_KIND QString()
#define STARINSTRUMENT_DEFAULT_KIND QString()
#define STARINSTRUMENT_DEFAULT_ACTIVE_ANGLE 0

class StarInstrument : public StarEquipableItem
{
public:
    StarInstrument();
    StarInstrument( StarInstrument const &orig );
    ~StarInstrument();

    virtual QStringList save( QString modPath );

    QPointF activeHandPosition() const;
    void setActiveHandPosition(const QPointF &activeHandPosition);

    QString largeImage() const;
    void setLargeImage(const QString &largeImage);

    QString image() const;
    void setImage(const QString &image);

    QString activeImage() const;
    void setActiveImage(const QString &activeImage);

    QString inspectionKind() const;
    void setInspectionKind(const QString &inspectionKind);

    QString kind() const;
    void setKind(const QString &kind);

    int activeAngle() const;
    void setActiveAngle(int activeAngle);

protected:
    virtual ItemMap serialize();
protected:
    QPointF m_activeHandPosition;
    QString m_largeImage;
    QString m_image;
    QString m_activeImage;
    QString m_inspectionKind;
    QString m_kind;
    int m_activeAngle;
};

typedef QSharedPointer< StarInstrument > StarInstrumentPtr;

Q_DECLARE_METATYPE( StarInstrument )
Q_DECLARE_METATYPE( QSharedPointer< StarInstrument > )

#endif // STARINSTRUMENT_H
