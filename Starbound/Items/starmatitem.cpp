#include "starmatitem.h"

StarMatItem::StarMatItem() : StarItem(),
    m_materialId( -1 )
{
    m_filesType = "matitem";
    m_idObject = "matitem";
}

StarMatItem::StarMatItem( StarMatItem const &orig ) : StarItem( orig ) {
    m_materialId =              orig.m_materialId;
}

StarMatItem::~StarMatItem() {

}

QStringList StarMatItem::save( QString modPath ) {
    return QStringList();
}

ItemMap StarMatItem::serialize() {
    ItemMap map = StarItem::serialize();

    map[ "materialId" ] = m_materialId;
}
int StarMatItem::materialId() const
{
    return m_materialId;
}

void StarMatItem::setMaterialId(int materialId)
{
    m_materialId = materialId;
}

