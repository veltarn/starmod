#ifndef STARTHROWN_H
#define STARTHROWN_H

#include "staritem.h"

struct ProjectileConfig {
    int armorPenetration;
    int level;
    int speed;
    int power;
};

#define STARTHROWN_DEFAULT_AMMO_USAGE 0
#define STARTHROWN_DEFAULT_IMAGE QString()
#define STARTHROWN_DEFAULT_COOLDOWN 0.0
#define STARTHROWN_DEFAULT_WINDUP_TIME 0.0
#define STARTHROWN_DEFAULT_EDGE_TRIGGER false
#define STARTHROWN_DEFAULT_PROJECTILE_TYPE QString()

class StarThrown : public StarItem
{
public:
    StarThrown();
    StarThrown( StarThrown const &orig );
    ~StarThrown();

    virtual QStringList save( QString modPath );

    int ammoUsage() const;
    void setAmmoUsage(int ammoUsage);

    QString image() const;
    void setImage(const QString &image);

    double cooldown() const;
    void setCooldown(double cooldown);

    double windupTime() const;
    void setWindupTime(double windupTime);

    bool edgeTrigger() const;
    void setEdgeTrigger(bool edgeTrigger);

    QString projectileType() const;
    void setProjectileType(const QString &projectileType);

    ProjectileConfig *projectileConfig();
    void setProjectileConfig(const ProjectileConfig &projectileConfig);

protected:
    virtual ItemMap serialize();
protected:
    int m_ammoUsage;
    QString m_image;
    double m_cooldown;
    double m_windupTime;
    bool m_edgeTrigger;
    QString m_projectileType;
    ProjectileConfig m_projectileConfig;
};

typedef QSharedPointer< StarThrown > StarThrownPtr;

Q_DECLARE_METATYPE( StarThrown )
Q_DECLARE_METATYPE( QSharedPointer< StarThrown > )

#endif // STARTHROWN_H
