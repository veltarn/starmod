#ifndef STARMATITEM_H
#define STARMATITEM_H

#include "staritem.h"

class StarMatItem : public StarItem
{
public:
    StarMatItem();
    StarMatItem( StarMatItem const &orig );
    virtual ~StarMatItem();

    virtual QStringList save( QString modPath );

    int materialId() const;
    void setMaterialId(int materialId);

protected:
    virtual ItemMap serialize();
protected:
    int m_materialId;
};

typedef QSharedPointer< StarMatItem > StarMatItemPtr;

Q_DECLARE_METATYPE( StarMatItem )
Q_DECLARE_METATYPE( QSharedPointer< StarMatItem > )

#endif // STARMATITEM_H
