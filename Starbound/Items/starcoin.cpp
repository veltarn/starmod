#include "starcoin.h"

StarCoin::StarCoin() : StarItem(), m_value( 0 ),
    m_smallStackLimit( 0 ),
    m_mediumStackLimit( 0 )
{
    m_idObject = "coinitem";
    m_filesType = "coinitem";
}

StarCoin::StarCoin(StarCoin &orig) : StarItem( orig )
{
    m_value = orig.m_value;
    m_pickupSoundsSmall = orig.m_pickupSoundsSmall;
    m_pickupSoundsMedium = orig.m_pickupSoundsMedium;
    m_pickupSoundsLarge = orig.m_pickupSoundsLarge;
    m_smallStackLimit = orig.m_smallStackLimit;
    m_mediumStackLimit = orig.m_mediumStackLimit;
}

StarCoin::~StarCoin()
{

}

ItemMap StarCoin::serialize()
{
    ItemMap map = StarItem::serialize();

    if( hasProperty( "value" ) )
        map[ "value" ] = m_value;

    if( hasProperty( "pickupSoundsSmall" ) ) {
        QJsonArray arr;
        foreach( QString sound, m_pickupSoundsSmall )
            arr.append( sound );

        map[ "pickupSoundsSmall" ] = arr;
    }

    if( hasProperty( "pickupSoundsMedium" ) ) {
        QJsonArray arr;
        foreach( QString sound, m_pickupSoundsMedium )
            arr.append( sound );
        map[ "pickupSoundsMedium" ] = arr;
    }

    if( hasProperty( "pickupSoundsLarge" ) ){
        QJsonArray arr;
        foreach( QString sound, m_pickupSoundsLarge )
            arr.append( sound );
        map[ "pickupSoundsLarge" ] = arr;
    }

    if( hasProperty( "smallStackLimit" ) )
        map[ "smallStackLimit" ] = m_smallStackLimit;

    if( hasProperty( "mediumStackLimit" ) )
        map[ "mediumStackLimit" ] = m_mediumStackLimit;

    return map;
}

QStringList StarCoin::save(QString modPath, QString itemPath, QJsonDocument &jsonFile)
{
    return QStringList();
}
int StarCoin::value() const
{
    return m_value;
}

void StarCoin::setValue(int value)
{
    m_value = value;
    m_properties.insert( "value" );
}
QStringList StarCoin::pickupSoundsSmall() const
{
    return m_pickupSoundsSmall;
}

void StarCoin::setPickupSoundsSmall(const QStringList &pickupSoundsSmall)
{
    m_pickupSoundsSmall = pickupSoundsSmall;
    m_properties.insert( "pickupSoundsSmall" );
}
QStringList StarCoin::pickupSoundsMedium() const
{
    return m_pickupSoundsMedium;
}

void StarCoin::setPickupSoundsMedium(const QStringList &pickupSoundsMedium)
{
    m_pickupSoundsMedium = pickupSoundsMedium;
    m_properties.insert( "pickupSoundsMedium" );
}
QStringList StarCoin::pickupSoundsLarge() const
{
    return m_pickupSoundsLarge;
}

void StarCoin::setPickupSoundsLarge(const QStringList &pickupSoundsLarge)
{
    m_pickupSoundsLarge = pickupSoundsLarge;
    m_properties.insert( "pickupSoundsLarge" );
}
int StarCoin::smallStackLimit() const
{
    return m_smallStackLimit;
}

void StarCoin::setSmallStackLimit(int smallStackLimit)
{
    m_smallStackLimit = smallStackLimit;
    m_properties.insert( "smallStackLimit" );
}
int StarCoin::mediumStackLimit() const
{
    return m_mediumStackLimit;
}

void StarCoin::setMediumStackLimit(int mediumStackLimit)
{
    m_mediumStackLimit = mediumStackLimit;
    m_properties.insert( "mediumStackLimit" );
}






