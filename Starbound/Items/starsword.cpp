#include "starsword.h"

StarSword::StarSword() : StarItem(),
    m_kind( STARSWORD_DEFAULT_KIND ),
    m_firePosition( STARSWORD_DEFAULT_FIRE_POSITION ),
    m_fireAfterWindup( STARSWORD_DEFAULT_FIRE_AFTER_WINDUP ),
    m_twoHanded( STARSWORD_DEFAULT_TWO_HANDED ),
    m_level( STARSWORD_DEFAULT_LEVEL ),
    m_fireTime( STARSWORD_DEFAULT_FIRE_TIME )
{
    m_filesType = "sword";
    m_idObject = "sword";

    PrimaryStances ps;

    ps.projectileType = "";
    ps.projectileSpeed = 0.0;
    ps.projectilePower = 0;

    ps.idleDuration = 0;
    ps.idleArmAngle = 0;
    ps.armFrameOverride = "";
    ps.idleTwoHanded = false;
    ps.idleSwordAngle = 0;
    ps.idleHandPosition = QPoint( 0, 0 );

    ps.windupDuration = 0.0;
    ps.windupArmAngle = 0;
    ps.windupSwordAngle = 0;
    ps.windupTwoHanded = false;
    ps.windupHandPosition = QPoint( 0, 0 );

    ps.cooldownDuration = 0;
    ps.cooldownArmAngle = 0;
    ps.cooldownTwoHanded = false;
    ps.cooldownSwordAngle = 0;
    ps.cooldownHandPosition = QPoint( 0, 0 );

    m_primaryStances = ps;
}

StarSword::StarSword(const StarSword &orig) : StarItem( orig )
{
    m_kind =                    orig.m_kind;
    m_firePosition =            orig.m_firePosition;
    m_soundEffect =             orig.m_soundEffect;
    m_fireAfterWindup =         orig.m_fireAfterWindup;
    m_twoHanded =               orig.m_twoHanded;
    m_colorOptions =            orig.m_colorOptions;
    m_level =                   orig.m_level;
    m_fireTime =                orig.m_fireTime;
    m_primaryStances =          orig.m_primaryStances;
}

StarSword::~StarSword()
{

}

QStringList StarSword::save(QString modPath)
{
    return QStringList();
}

ItemMap StarSword::serialize()
{
    ItemMap map = StarItem::serialize();

    map[ "kind" ] = m_kind;

    QJsonArray arr;
    arr.append( m_firePosition.x() );
    arr.append( m_firePosition.y() );

    map[ "firePosition" ] = arr;

    QJsonObject fireSound;
    QJsonArray fireSounds;

    for( QList<QMap< QString, QString> >::iterator it = m_soundEffect.fireSound.begin(); it != m_soundEffect.fireSound.end(); ++it ) {
        QMap< QString, QString> sound = *it;
        QJsonObject obj;
        for( QMap<QString, QString>::iterator it = sound.begin(); it != sound.end(); ++it ) {
            QString key = it.key();
            QString val = it.value();

            obj[ key ] = val;
        }
        fireSounds.append( obj );
    }

   /* foreach( QMap< QString, QString> sound, m_soundEffect.fireSound ) {
        QJsonObject obj;
        for( QMap<QString, QString>::iterator it = sound.begin(); it != sound.end(); ++it ) {
            QString key = it.key();
            QString val = it.value();

            obj[ key ] = val;
        }
        fireSounds.append( obj );
    }*/
    fireSound[ "fireSound" ] = fireSounds;
    map[ "soundEffect" ] = fireSound;

    map[ "fireAfterWindup" ] = m_fireAfterWindup;
    map[ "twoHanded" ] = m_twoHanded;

    QJsonArray colorOptions;

    for( ColorOptionsList::iterator it = m_colorOptions.begin(); it != m_colorOptions.end(); ++it ) {
        QMap< QString, QString > colorsMap = *it;

        QJsonObject obj;
        for( QMap<QString, QString>::iterator it = colorsMap.begin(); it != colorsMap.end(); ++it ) {
            QString key = it.key();
            QString val = it.value();

            obj[ key ] = val;
        }
        colorOptions.append( obj );
    }
    /*foreach( QMap< QString, QString > colorsMap, m_colorOptions ) {
        QJsonObject obj;
        for( QMap<QString, QString>::iterator it = colorsMap.begin(); it != colorsMap.end(); ++it ) {
            QString key = it.key();
            QString val = it.value();

            obj[ key ] = val;
        }
        colorOptions.append( obj );
    }*/

    map[ "colorOptions" ] = colorOptions;

    map[ "level" ] = m_level;
    map[ "fireTime" ] = m_fireTime;

    QJsonObject primaryStancesObj;

    QJsonObject idleObj;
    QJsonObject windupObj;
    QJsonObject cooldownObj;
    QJsonObject projectileObj;
    //Idle
    idleObj[ "twoHanded" ] = m_primaryStances.idleTwoHanded;
    idleObj[ "duration" ] = m_primaryStances.idleDuration;
    idleObj[ "armAngle" ] = m_primaryStances.idleArmAngle;
    idleObj[ "armFrameOverride" ] = m_primaryStances.armFrameOverride;
    idleObj[ "swordAngle" ] = m_primaryStances.idleSwordAngle;

    QJsonArray handPos;
    handPos.append( m_primaryStances.idleHandPosition.x() );
    handPos.append( m_primaryStances.idleHandPosition.y() );

    idleObj[ "handPosition" ] = handPos;

    //Windup
    windupObj[ "duration" ] = m_primaryStances.windupDuration;

    QJsonArray statuses;
    foreach( StatusEffect effect, m_primaryStances.windupStatusEffects ) {
        QJsonObject obj = effect.toJsonObject();

        statuses.append( obj );
    }

    windupObj[ "statusEffects" ] = statuses;
    windupObj[ "armAngle" ] = m_primaryStances.windupArmAngle;
    windupObj[ "twoHanded" ] = m_primaryStances.windupTwoHanded;
    windupObj[ "swordAngle" ] = m_primaryStances.windupSwordAngle;

    handPos = QJsonArray();
    handPos.append( m_primaryStances.windupHandPosition.x() );
    handPos.append( m_primaryStances.windupHandPosition.y() );

    windupObj[ "handPosition" ] = handPos;

    //Cooldown
    cooldownObj[ "duration" ] = m_primaryStances.cooldownDuration;

    statuses = QJsonArray();
    foreach( StatusEffect effect, m_primaryStances.cooldownStatusEffects ) {
        QJsonObject obj = effect.toJsonObject();
        statuses.append( obj );
    }

    cooldownObj[ "statusEffects" ] = statuses;
    cooldownObj[ "armAngle" ] = m_primaryStances.cooldownArmAngle;
    cooldownObj[ "twoHanded" ] = m_primaryStances.cooldownTwoHanded;
    cooldownObj[ "swordAngle" ] = m_primaryStances.cooldownSwordAngle;

    handPos = QJsonArray();
    handPos.append( m_primaryStances.cooldownHandPosition.x() );
    handPos.append( m_primaryStances.cooldownHandPosition.y() );
    cooldownObj[ "handPosition" ] = handPos;

    primaryStancesObj[ "projectileType" ] = m_primaryStances.projectileType;
    projectileObj[ "speed" ] = m_primaryStances.projectileSpeed;
    projectileObj[ "power" ] = m_primaryStances.projectilePower;

    primaryStancesObj[ "projectile" ] = projectileObj;
    primaryStancesObj[ "idle" ] = idleObj;
    primaryStancesObj[ "windup" ] = windupObj;
    primaryStancesObj[ "cooldown" ] = cooldownObj;

    map[ "primaryStances" ] = primaryStancesObj;

    return map;
}


PrimaryStances *StarSword::primaryStances()
{
    return &m_primaryStances;
}

void StarSword::setPrimaryStances(const PrimaryStances &primaryStances)
{
    m_primaryStances = primaryStances;
}

double StarSword::fireTime() const
{
    return m_fireTime;
}

void StarSword::setFireTime(double fireTime)
{
    m_fireTime = fireTime;
}

int StarSword::level() const
{
    return m_level;
}

void StarSword::setLevel(int level)
{
    m_level = level;
}

ColorOptionsList StarSword::colorOptions() const
{
    return m_colorOptions;
}

void StarSword::setColorOptions(const ColorOptionsList &colorOptions)
{
    m_colorOptions = colorOptions;
}

bool StarSword::twoHanded() const
{
    return m_twoHanded;
}

void StarSword::setTwoHanded(bool twoHanded)
{
    m_twoHanded = twoHanded;
}

bool StarSword::fireAfterWindup() const
{
    return m_fireAfterWindup;
}

void StarSword::setFireAfterWindup(bool fireAfterWindup)
{
    m_fireAfterWindup = fireAfterWindup;
}

SoundEffect StarSword::soundEffect() const
{
    return m_soundEffect;
}

void StarSword::setSoundEffect(const SoundEffect &soundEffect)
{
    m_soundEffect = soundEffect;
}

QPointF StarSword::firePosition() const
{
    return m_firePosition;
}

void StarSword::setFirePosition(const QPointF &firePosition)
{
    m_firePosition = firePosition;
}

QString StarSword::kind() const
{
    return m_kind;
}

void StarSword::setKind(const QString &kind)
{
    m_kind = kind;
}

