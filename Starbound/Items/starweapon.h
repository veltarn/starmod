#ifndef STARWEAPON_H
#define STARWEAPON_H

#include <QMetaType>
#include <QString>
#include <QPoint>
#include <QRectF>
#include "starequipableitem.h"
#include "../starprojectile.h"

#define STARWEAPON_DEFAULT_IS_MELEE false
#define STARWEAPON_DEFAULT_FIRE_TIME 0.00
#define STARWEAPON_DEFAULT_FIRE_POSITION QPoint( 0, 0 )
#define STARWEAPON_DEFAULT_LEVEL 1
#define STARWEAPON_DEFAULT_ENERGY_COST 0
#define STARWEAPON_DEFAULT_CLASS_MULTIPLIER 0
#define STARWEAPON_DEFAULT_WEAPON_IMAGE_PATH QString( "" )
#define STARWEAPON_DEFAULT_HAS_PROJECTILE false
#define STARWEAPON_DEFAULT_PROJECTILE_NAME QString( "" )
#define STARWEAPON_DEFAULT_PROJECTILE_SPEED 0
#define STARWEAPON_DEFAULT_PROJECTILE_POWER 0.0
#define STARWEAPON_DEFAULT_PROJECTILE_COLOR QColor( 255, 255, 255 );
#define STARWEAPON_DEFAULT_LIFE_TIME 0

class StarWeapon : public StarEquipableItem
{
public:
    StarWeapon();

    StarWeapon( StarWeapon const &orig );
    ~StarWeapon();

    virtual QStringList save( QString modPath, QString itemPath, QJsonDocument &jsonFile );

    bool isMelee() const;
    void setIsMelee( bool melee );

    double fireTime() const;
    void setFireTime(double fireTime);


    QPoint firePosition() const;
    void setFirePosition(const QPoint &firePosition);

    int level() const;
    void setLevel(int level);

    int energyCost() const;
    void setEnergyCost(int energyCost);

    int classMultiplier() const;
    void setClassMultiplier(int classMultiplier);

    QString weaponImagePath() const;
    void setWeaponImagePath(const QString &weaponImagePath);

    QString projectileName() const;
    void setProjectileName(QString projectile);

    int projectileSpeed() const;
    void setProjectileSpeed(int projectileSpeed);

    double projectilePower() const;
    void setProjectilePower(double projectilePower);

    QColor projectileColor() const;
    void setProjectileColor(const QColor &projectileColor);

    int projectileLifeTime() const;
    void setProjectileLifeTime(int projectileLifeTime);

    bool hasProjectile() const;
    void setHasProjectile(bool hasProjectile);

protected:
    virtual ItemMap serialize();
protected:
    bool m_isMelee;
    double m_fireTime;
    QPoint m_firePosition;
    int m_level;
    int m_energyCost;
    int m_classMultiplier;
    QString m_weaponImagePath;
    bool m_hasProjectile;
    //StarProjectile *m_projectile;
    QString m_projectileName;
    int m_projectileSpeed;
    double m_projectilePower;
    QColor m_projectileColor;
    int m_projectileLifeTime;
};

typedef QSharedPointer< StarWeapon > StarWeaponPtr;

Q_DECLARE_METATYPE( StarWeapon )
Q_DECLARE_METATYPE( QSharedPointer< StarWeapon > )

#endif // STARWEAPON_H
