#ifndef STAREQUIPABLEITEM_H
#define STAREQUIPABLEITEM_H

#include "starhandeditem.h"

#define STAREQUIPABLEITEM_DEFAULT_TWO_HANDED false

class StarEquipableItem : public StarHandedItem
{
public:
    StarEquipableItem();
    StarEquipableItem( StarEquipableItem const& orig );
    virtual ~StarEquipableItem();

    bool twoHanded() const;
    void setTwoHanded(bool twoHanded);

    virtual QStringList save( QString modPath, QString itemPath, QJsonDocument &jsonFile );
protected:
    virtual ItemMap serialize();
protected:
    bool m_twoHanded;

};

typedef QSharedPointer< StarEquipableItem > StarEquipableItemPtr;

Q_DECLARE_METATYPE( StarEquipableItem )
Q_DECLARE_METATYPE( QSharedPointer< StarEquipableItem > )

#endif // STAREQUIPABLEITEM_H
