#include "starbeamaxe.h"

StarBeamaxe::StarBeamaxe() : StarEquipableItem(),
    m_category( STARBEAMAXE_DEFAULT_CATEGORY ),
    m_inspectionKind( STARBEAMAXE_DEFAULT_INSPECTION_KIND ),
    m_fireTime( STARBEAMAXE_DEFAULT_FIRE_TIME ),
    m_blockRadius( STARBEAMAXE_DEFAULT_BLOCK_RADIUS ),
    m_altBlockRadius( STARBEAMAXE_DEFAULT_ALT_BLOCK_RADIUS ),
    m_critical( STARBEAMAXE_DEFAULT_CRITICAL ),
    m_strikeSound( STARBEAMAXE_DEFAULT_STRIKE_SOUND ),
    m_image( STARBEAMAXE_DEFAULT_IMAGE ),
    m_endImages( STARBEAMAXE_DEFAULT_END_IMAGES ),
    m_firePosition( STARBEAMAXE_DEFAULT_FIRE_POSITION ),
    m_segmentPerUnit( STARBEAMAXE_DEFAULT_SEGMENT_PER_UNIT ),
    m_nearControlPointElasticity( STARBEAMAXE_DEFAULT_NEAR_CONTROL_POINT_ELASTICITY ),
    m_farControlPointElasticity( STARBEAMAXE_DEFAULT_FAR_CONTROL_POINT_ELASTICITY ),
    m_nearControlPointDistance( STARBEAMAXE_DEFAULT_NEAR_CONTROL_POINT_DISTANCE ),
    m_targetSegmentRun( STARBEAMAXE_DEFAULT_TARGET_SEGMENT_RUN ),
    m_innerBrightnessScale( STARBEAMAXE_DEFAULT_INNER_BRIGHTNESS_SCALE ),
    m_firstStripeThickness( STARBEAMAXE_DEFAULT_FIRST_STRIPE_THICKNESS ),
    m_secondStripeThickness( STARBEAMAXE_DEFAULT_SECOND_STRIPE_THICKNESS ),
    m_minBeamWidth( STARBEAMAXE_DEFAULT_MIN_BEAM_WIDTH ),
    m_maxBeamWidth( STARBEAMAXE_DEFAULT_MAX_BEAM_WIDTH ),
    m_minBeamJitter( STARBEAMAXE_DEFAULT_MIN_BEAM_JITTER ),
    m_maxBeamJitter( STARBEAMAXE_DEFAULT_MAX_BEAM_JITTER ),
    m_minBeamTrans( STARBEAMAXE_DEFAULT_MIN_BEAM_TRANS ),
    m_maxBeamTrans( STARBEAMAXE_DEFAULT_MAX_BEAM_TRANS ),
    m_minBeamLines( STARBEAMAXE_DEFAULT_MIN_BEAM_LINES ),
    m_maxBeamLines( STARBEAMAXE_DEFAULT_MAX_BEAM_LINES )
{
    m_idObject = "beamaxe";
    m_filesType = "beamaxe";
}

StarBeamaxe::StarBeamaxe( const StarBeamaxe &orig ) : StarEquipableItem( orig ) {
    m_category =                orig.m_category;
    m_fireTime =                orig.m_fireTime;
    m_blockRadius =             orig.m_blockRadius;
    m_altBlockRadius =          orig.m_altBlockRadius;
    m_critical =                orig.m_critical;
    m_strikeSound =             orig.m_strikeSound;
    m_image =                   orig.m_image;
    m_endImages =               orig.m_endImages;
    m_segmentPerUnit =          orig.m_segmentPerUnit;
    m_nearControlPointElasticity = orig.m_nearControlPointElasticity;
    m_farControlPointElasticity = orig.m_farControlPointElasticity;
    m_nearControlPointDistance = orig.m_nearControlPointDistance;
    m_targetSegmentRun =        orig.m_targetSegmentRun;
    m_innerBrightnessScale =    orig.m_innerBrightnessScale;
    m_firstStripeThickness =    orig.m_firstStripeThickness;
    m_secondStripeThickness =   orig.m_secondStripeThickness;
    m_minBeamWidth =            orig.m_minBeamWidth;
    m_maxBeamWidth =            orig.m_maxBeamWidth;
    m_maxBeamJitter =           orig.m_maxBeamJitter;
    m_minBeamJitter =           orig.m_minBeamJitter;
    m_minBeamTrans =            orig.m_minBeamTrans;
    m_maxBeamTrans =            orig.m_maxBeamTrans;
    m_minBeamLines =            orig.m_minBeamLines;
    m_maxBeamLines =            orig.m_maxBeamLines;
}

StarBeamaxe::~StarBeamaxe() {

}

QStringList StarBeamaxe::save(QString modPath)
{
    return QStringList();
}

QString StarBeamaxe::category() const
{
    return m_category;
}

void StarBeamaxe::setCategory(const QString &category)
{
    m_category = category;
}
double StarBeamaxe::fireTime() const
{
    return m_fireTime;
}

void StarBeamaxe::setFireTime(double fireTime)
{
    m_fireTime = fireTime;
}
int StarBeamaxe::blockRadius() const
{
    return m_blockRadius;
}

void StarBeamaxe::setBlockRadius(int blockRadius)
{
    m_blockRadius = blockRadius;
}
int StarBeamaxe::altBlockRadius() const
{
    return m_altBlockRadius;
}

void StarBeamaxe::setAltBlockRadius(int altBlockRadius)
{
    m_altBlockRadius = altBlockRadius;
}
bool StarBeamaxe::critical() const
{
    return m_critical;
}

void StarBeamaxe::setCritical(bool critical)
{
    m_critical = critical;
}
QString StarBeamaxe::strikeSound() const
{
    return m_strikeSound;
}

void StarBeamaxe::setStrikeSound(const QString &strikeSound)
{
    m_strikeSound = strikeSound;
}
QString StarBeamaxe::image() const
{
    return m_image;
}

void StarBeamaxe::setImage(const QString &image)
{
    m_image = image;
}
QStringList StarBeamaxe::endImages() const
{
    return m_endImages;
}

void StarBeamaxe::setEndImages(const QStringList &endImages)
{
    m_endImages = endImages;
}
QPoint StarBeamaxe::firePosition() const
{
    return m_firePosition;
}

void StarBeamaxe::setFirePosition(const QPoint &firePosition)
{
    m_firePosition = firePosition;
}
int StarBeamaxe::segmentPerUnit() const
{
    return m_segmentPerUnit;
}

void StarBeamaxe::setSegmentPerUnit(int segmentPerUnit)
{
    m_segmentPerUnit = segmentPerUnit;
}
double StarBeamaxe::nearControlPointElasticity() const
{
    return m_nearControlPointElasticity;
}

void StarBeamaxe::setNearControlPointElasticity(double nearControlPointElasticity)
{
    m_nearControlPointElasticity = nearControlPointElasticity;
}
double StarBeamaxe::farControlPointElasticity() const
{
    return m_farControlPointElasticity;
}

void StarBeamaxe::setFarControlPointElasticity(double farControlPointElasticity)
{
    m_farControlPointElasticity = farControlPointElasticity;
}
double StarBeamaxe::nearControlPointDistance() const
{
    return m_nearControlPointDistance;
}

void StarBeamaxe::setNearControlPointDistance(double nearControlPointDistance)
{
    m_nearControlPointDistance = nearControlPointDistance;
}
int StarBeamaxe::targetSegmentRun() const
{
    return m_targetSegmentRun;
}

void StarBeamaxe::setTargetSegmentRun(int targetSegmentRun)
{
    m_targetSegmentRun = targetSegmentRun;
}
int StarBeamaxe::innerBrightnessScale() const
{
    return m_innerBrightnessScale;
}

void StarBeamaxe::setInnerBrightnessScale(int innerBrightnessScale)
{
    m_innerBrightnessScale = innerBrightnessScale;
}
double StarBeamaxe::firstStripeThickness() const
{
    return m_firstStripeThickness;
}

void StarBeamaxe::setFirstStripeThickness(double firstStripeThickness)
{
    m_firstStripeThickness = firstStripeThickness;
}
double StarBeamaxe::secondStripeThickness() const
{
    return m_secondStripeThickness;
}

void StarBeamaxe::setSecondStripeThickness(double secondStripeThickness)
{
    m_secondStripeThickness = secondStripeThickness;
}
int StarBeamaxe::minBeamWidth() const
{
    return m_minBeamWidth;
}

void StarBeamaxe::setMinBeamWidth(int minBeamWidth)
{
    m_minBeamWidth = minBeamWidth;
}
int StarBeamaxe::maxBeamWidth() const
{
    return m_maxBeamWidth;
}

void StarBeamaxe::setMaxBeamWidth(int maxBeamWidth)
{
    m_maxBeamWidth = maxBeamWidth;
}
double StarBeamaxe::maxBeamJitter() const
{
    return m_maxBeamJitter;
}

void StarBeamaxe::setMaxBeamJitter(double maxBeamJitter)
{
    m_maxBeamJitter = maxBeamJitter;
}
double StarBeamaxe::minBeamJitter() const
{
    return m_minBeamJitter;
}

void StarBeamaxe::setMinBeamJitter(double minBeamJitter)
{
    m_minBeamJitter = minBeamJitter;
}
double StarBeamaxe::minBeamTrans() const
{
    return m_minBeamTrans;
}

void StarBeamaxe::setMinBeamTrans(double minBeamTrans)
{
    m_minBeamTrans = minBeamTrans;
}
double StarBeamaxe::maxBeamTrans() const
{
    return m_maxBeamTrans;
}

void StarBeamaxe::setMaxBeamTrans(double maxBeamTrans)
{
    m_maxBeamTrans = maxBeamTrans;
}

int StarBeamaxe::minBeamLines() const
{
    return m_minBeamLines;
}

void StarBeamaxe::setMinBeamLines(int minBeamLines)
{
    m_minBeamLines = minBeamLines;
}
int StarBeamaxe::maxBeamLines() const
{
    return m_maxBeamLines;
}

void StarBeamaxe::setMaxBeamLines(int maxBeamLines)
{
    m_maxBeamLines = maxBeamLines;
}

ItemMap StarBeamaxe::serialize() {
    ItemMap map = StarEquipableItem::serialize();

    map[ "category" ] = m_category;
    map[ "inspectionKind" ] = m_inspectionKind;
    map[ "fireTime" ] = m_fireTime;
    map[ "blockRadius" ] = m_blockRadius;
    map[ "altBlockRadius" ] = m_altBlockRadius;
    map[ "critical" ] = m_critical;
    map[ "strikeSound" ] = m_strikeSound;
    map[ "image" ] = m_image;

    QJsonArray endImages;
    foreach( QString img, m_endImages ) {
        endImages.append( img );
    }

    map[ "endImages" ] = endImages;

    QJsonArray arr;
    arr.append( m_firePosition.x() );
    arr.append( m_firePosition.y() );

    map[ "firePosition" ] = arr;
    map[ "segmentPerUnit" ] = m_segmentPerUnit;
    map[ "nearControlPointElasticity" ] = m_nearControlPointElasticity;
    map[ "farControlPointElasticity" ] = m_farControlPointElasticity;
    map[ "nearControlPointDistance" ] = m_nearControlPointDistance;
    map[ "targetSegmentRun" ] = m_targetSegmentRun;
    map[ "innerBrightnessScale" ] = m_innerBrightnessScale;
    map[ "firstStripeThickness" ] = m_firstStripeThickness;
    map[ "secondStripeThickness" ] = m_secondStripeThickness;
    map[ "minBeamWidth" ] = m_minBeamWidth;
    map[ "maxBeamWidth" ] = m_maxBeamWidth;
    map[ "maxBeamJitter" ] = m_maxBeamJitter;
    map[ "minBeamJitter" ] = m_minBeamJitter;
    map[ "minBeamTrans" ] = m_minBeamTrans;
    map[ "maxBeamTrans" ] = m_maxBeamTrans;
    map[ "minBeamLines" ] = m_minBeamLines;
    map[ "maxBeamLines" ] = m_maxBeamLines;

    return map;
}
QString StarBeamaxe::inspectionKind() const
{
    return m_inspectionKind;
}

void StarBeamaxe::setInspectionKind(const QString &inspectionKind)
{
    m_inspectionKind = inspectionKind;
}

