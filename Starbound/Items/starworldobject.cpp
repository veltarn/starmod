#include "starworldobject.h"

StarWorldObject::StarWorldObject() : StarItem(),
    m_sitAngle( STARWORLDOBJECT_DEFAULT_SIT_ANGLE ),
    m_lightColor( STARWORLDOBJECT_DEFAULT_LIGHT_COLOR ),
    m_scriptDelta( STARWORLDOBJECT_DEFAULT_SCRIPT_DELTA ),
    m_race( STARWORLDOBJECT_DEFAULT_RACE ),
    m_sitCoverImage( STARWORLDOBJECT_DEFAULT_SIT_COVER_IMAGE ),
    m_hasWindowIcon( STARWORLDOBJECT_DEFAULT_HAS_WINDOW_ICON ),
    m_frameCooldown( STARWORLDOBJECT_DEFAULT_FRAME_COOLDOWN ),
    m_animation( STARWORLDOBJECT_DEFAULT_ANIMATION ),
    m_detectTickDuration( STARWORLDOBJECT_DEFAULT_DETECT_TICK_DURATION ),
    m_autocloseCooldown( STARWORLDOBJECT_DEFAULT_AUTOCLOSE_COOLDOWN ),
    m_health( STARWORLDOBJECT_DEFAULT_HEALTH ),
    m_script( STARWORLDOBJECT_DEFAULT_SCRIPT ),
    m_category( STARWORLDOBJECT_DEFAULT_CATEGORY ),
    m_pointBeam( STARWORLDOBJECT_DEFAULT_POINT_BEAM ),
    m_price( STARWORLDOBJECT_DEFAULT_PRICE ),
    m_flickerDistance( STARWORLDOBJECT_DEFAULT_FLICKER_DISTANCE ),
    m_uiConfig( STARWORLDOBJECT_DEFAULT_UI_CONFIG ),
    m_fireCooldown( STARWORLDOBJECT_DEFAULT_FIRE_COOLDOWN ),
    m_hydrophobic( STARWORLDOBJECT_DEFAULT_HYDROPHOBIC ),
    m_hasAnimation( STARWORLDOBJECT_DEFAULT_HAS_ANIMATION ),
    m_animationPosition( STARWORLDOBJECT_DEFAULT_ANIMATION_POSITION ),
    m_interval( STARWORLDOBJECT_DEFAULT_INTERVAL ),
    m_tipOffset( STARWORLDOBJECT_DEFAULT_TIP_OFFSET ),
    m_targetHoldTime( STARWORLDOBJECT_DEFAULT_TARGET_HOLD_TIME ),
    m_isSittable( STARWORLDOBJECT_DEFAULT_IS_SITTABLE ),
    m_sitPosition( STARWORLDOBJECT_DEFAULT_SIT_POSITION ),
    m_slotCount( STARWORLDOBJECT_DEFAULT_SLOT_COUNT ),
    m_printable( STARWORLDOBJECT_DEFAULT_PRINTABLE ),
    m_genericDescription( STARWORLDOBJECT_DEFAULT_GENERIC_DESCRIPTION ),
    m_unlit( STARWORLDOBJECT_DEFAULT_UNLIT ),
    m_objectItem( STARWORLDOBJECT_DEFAULT_OBJECT_ITEM ),
    m_rotationPauseTime( STARWORLDOBJECT_DEFAULT_ROTATION_PAUSE_TIME ),
    m_interactAction( STARWORLDOBJECT_DEFAULT_INTERACT_ACTION ),
    m_sitFlipDirection( STARWORLDOBJECT_DEFAULT_SIT_FLIP_DIRECTION ),
    m_soundEffect( STARWORLDOBJECT_DEFAULT_SOUND_EFFECT ),
    m_detectRadius( STARWORLDOBJECT_DEFAULT_DETECT_RADIUS ),
    m_rotationTime( STARWORLDOBJECT_DEFAULT_ROTATION_TIME ),
    m_flickerTiming( STARWORLDOBJECT_DEFAULT_FLICKER_TIMING ),
    m_flickerStrength( STARWORLDOBJECT_DEFAULT_FLICKER_STRENGTH ),
    m_gates( STARWORLDOBJECT_DEFAULT_GATES ),
    m_pointLight( STARWORLDOBJECT_DEFAULT_POINT_LIGHT ),
    m_baseOffset( STARWORLDOBJECT_DEFAULT_BASE_OFFSET ),
    m_damageTime( STARWORLDOBJECT_DEFAULT_DAMAGE_TIME ),
    m_soundEffectRadius( STARWORLDOBJECT_DEFAULT_SOUND_EFFECT_RADIUS ),
    m_sitOrientation( STARWORLDOBJECT_DEFAULT_SIT_ORIENTATION ),
    m_subtitle( STARWORLDOBJECT_DEFAULT_SUBTITLE ),
    m_sitEmote( STARWORLDOBJECT_DEFAULT_SIT_EMOTE ),
    m_maxLaserLength( STARWORLDOBJECT_DEFAULT_MAX_LASER_LENGTH ),
    m_recipeGroup( STARWORLDOBJECT_DEFAULT_RECIPE_GROUP ),
    m_unbreakable( STARWORLDOBJECT_DEFAULT_UNBREAKABLE ),
    m_objectType( STARWORLDOBJECT_DEFAULT_OBJECT_TYPE )
{
    m_filesType = "object";
    m_idObject = "object";
}

StarWorldObject::StarWorldObject(const StarWorldObject &orig) : StarItem( orig )
{
    m_particleEmitters =        orig.m_particleEmitters;
    m_sitAngle =                orig.m_sitAngle;
    m_lightColor =              orig.m_lightColor;
    m_lightColors =             orig.m_lightColors;
    m_animationParts =          orig.m_animationParts;
    m_scriptDelta =             orig.m_scriptDelta;
    m_spawner =                 orig.m_spawner;
    m_statusEffect =            orig.m_statusEffect;
    m_breakDropOptions =        orig.m_breakDropOptions;
    m_interactionTransition =   orig.m_interactionTransition;
    m_race =                    orig.m_race;
    m_scripts =                 orig.m_scripts;
    m_outboundNodesLocation =   orig.m_outboundNodesLocation;
    m_touchDamage =             orig.m_touchDamage;
    m_closeSounds =             orig.m_closeSounds;
    m_truthtable =              orig.m_truthtable;
    m_openSounds =              orig.m_openSounds;
    m_sitCoverImage =           orig.m_sitCoverImage;
    m_offSounds =               orig.m_offSounds;
    m_hasWindowIcon =           orig.m_hasWindowIcon;
    m_smashSounds =             orig.m_smashSounds;
    m_rotationRange =           orig.m_rotationRange;
    m_frameCooldown =           orig.m_frameCooldown;
    m_hasAnimation =            orig.m_hasAnimation;
    m_animationPosition =       orig.m_animationPosition;
    m_detectTickDuration =      orig.m_detectTickDuration;
    m_autocloseCooldown =       orig.m_autocloseCooldown;
    m_health =                  orig.m_health;
    m_script =                  orig.m_script;
    m_category =                orig.m_category;
    m_pointBeam =               orig.m_pointBeam;
    m_price =                   orig.m_price;
    m_inboundNodes =            orig.m_inboundNodes;
    m_flickerDistance =         orig.m_flickerDistance;
    m_uiConfig =                orig.m_uiConfig;
    m_fireCooldown =            orig.m_fireCooldown;
    m_orientations =            orig.m_orientations;
    m_sitEffectEmitters =       orig.m_sitEffectEmitters;
    m_hydrophobic =             orig.m_hydrophobic;
    m_interval =                orig.m_interval;
    m_engineOnOrientations =    orig.m_engineOnOrientations;
    m_state =                   orig.m_state;
    m_tipOffset =               orig.m_tipOffset;
    m_outboundNodes =           orig.m_outboundNodes;
    m_targetHoldTime =          orig.m_targetHoldTime;
    m_isSittable =              orig.m_isSittable;
    m_sitPosition =             orig.m_sitPosition;
    m_onSounds =                orig.m_onSounds;
    m_slotCount =               orig.m_slotCount;
    m_printable =               orig.m_printable;
    m_genericDescription =      orig.m_genericDescription;
    m_unlit =                   orig.m_unlit;
    m_fireOffsets =             orig.m_fireOffsets;
    m_objectItem =              orig.m_objectItem;
    m_rotationPauseTime =       orig.m_rotationPauseTime;
    m_interactAction =          orig.m_interactAction;
    m_sounds =                  orig.m_sounds;
    m_sitFlipDirection =        orig.m_sitFlipDirection;
    m_soundEffect =             orig.m_soundEffect;
    m_detectRadius =            orig.m_detectRadius;
    m_stageAlts =               orig.m_stageAlts;
    m_rotationTime =            orig.m_rotationTime;
    m_flickerTiming =           orig.m_flickerTiming;
    m_useSounds =               orig.m_useSounds;
    m_growing =                 orig.m_growing;
    m_flickerStrength =         orig.m_flickerStrength;
    m_gates =                   orig.m_gates;
    m_pointLight =              orig.m_pointLight;
    m_projectileOptions =       orig.m_projectileOptions;
    m_baseOffset =              orig.m_baseOffset;
    m_damageTime =              orig.m_damageTime;
    m_statusEffects =           orig.m_statusEffects;
    m_interactData =            orig.m_interactData;
    m_soundEffectRadius =       orig.m_soundEffectRadius;
    m_sitOrientation =          orig.m_sitOrientation;
    m_subtitle =                orig.m_subtitle;
    m_sitEmote =                orig.m_sitEmote;
    m_maxLaserLength =          orig.m_maxLaserLength;
    m_smashDropOptions =        orig.m_smashDropOptions;
    m_recipeGroup =             orig.m_recipeGroup;
    m_unbreakable =             orig.m_unbreakable;
    m_objectType =              orig.m_objectType;
}

StarWorldObject::~StarWorldObject()
{
    //qDebug() << "Deleted " << sizeof( *this ) << " bytes" << endl;
}

QStringList StarWorldObject::save(QString modPath)
{
    return QStringList();
}

ItemMap StarWorldObject::serialize()
{
    ItemMap map = StarItem::serialize();

    if( m_particleEmitters.size() > 0 ) {
        QJsonArray particleEmitters;

        foreach( ParticleEmitter particleEmitter, m_particleEmitters ) {
            QJsonObject jsonParticleEmitter;
            //==========================
            //  Particle Emitters Structure
            //==========================

            jsonParticleEmitter[ "emissionRate" ] = particleEmitter.emissionRate;
            jsonParticleEmitter[ "emissionVariance" ] = particleEmitter.emissionVariance;
            QJsonArray pixelOrigin = qPointToJsonArray( particleEmitter.pixelOrigin );
            jsonParticleEmitter[ "pixelOrigin" ] = pixelOrigin;

            QJsonObject particle;

            particle[ "fade" ] = particleEmitter.particle.fade;
            particle[ "initialVelocity" ] = qPointToJsonArray( particleEmitter.particle.initialVelocity );
            if( particleEmitter.particle.destructionTime != -1 )
                particle[ "destructionTime" ] = particleEmitter.particle.destructionTime;

            particle[ "layer" ] = particleEmitter.particle.layer;
            if( particleEmitter.particle.image != "" )
                particle[ "image" ] = particleEmitter.particle.image;

            if( particleEmitter.particle.type != "" )
                particle[ "type" ] = particleEmitter.particle.type;

            particle[ "timeToLive" ] = particleEmitter.particle.timeToLive;
            particle[ "size" ] = particleEmitter.particle.size;
            particle[ "color" ] = qColorToJsonArray( particleEmitter.particle.color );
            particle[ "approach" ] = qPointToJsonArray( particleEmitter.particle.approach );
            particle[ "finalVelocity" ] = qPointToJsonArray( particleEmitter.particle.finalVelocity );

            if( particleEmitter.particle.light != QColor( 0, 0, 0 ) )
                particle[ "light" ] = qColorToJsonArray( particleEmitter.particle.light );

            if( particleEmitter.particle.destructionAction != "" )
                particle[ "destructionAction" ] = particleEmitter.particle.destructionAction;

            jsonParticleEmitter[ "particle" ] = particle;

            QJsonObject particleVariance;

            particleVariance[ "position" ] = qPointToJsonArray( particleEmitter.particleVariance.position );
            particleVariance[ "initialVelocity" ] = qPointToJsonArray( particleEmitter.particleVariance.initialVelocity );
            particleVariance[ "finalVelocity" ] = qPointToJsonArray( particleEmitter.particleVariance.finalVelocity );

            jsonParticleEmitter[ "particleVariance" ] = particleVariance;

            //==========================
            //  End of particle Emitters
            //==========================
        }

        map[ "particleEmitters" ] = particleEmitters;
    }

    map[ "sitAngle" ] = m_sitAngle;

    if( m_lightColor != QColor( 0, 0, 0 ) )
        map[ "lightColor" ] = qColorToJsonArray( m_lightColor );
    if( m_lightColors.size() > 0 ) {
        QJsonObject lightColors;

        for( LightColors::iterator it = m_lightColors.begin(); it != m_lightColors.end(); ++it ) {
            QString key = it.key();
            QColor c = it.value();

            lightColors[ key ] = qColorToJsonArray( c );
        }
        map[ "lightColors" ] = lightColors;
    }

    if( m_animationParts.size() > 0 ) {
        QJsonObject animationParts = animationPartsToObject( m_animationParts );
        map[ "animationParts" ] = animationParts;
    }

    if( m_scriptDelta != -1 )
        map[ "scriptDelta" ] = m_scriptDelta;

    if( hasProperty( "spawner" ) ) {
        QJsonObject spawner;
        spawner[ "searchRadius" ] = m_spawner.searchRadius;
        spawner[ "npcTypeOptions" ] = qStringListToJsonArray( m_spawner.npcTypeOptions );
        spawner[ "npcSpeciesOptions" ] = qStringListToJsonArray( m_spawner.npcSpeciesOptions );

        QJsonArray npcParameterOptions;

        for( QList< QMap< QString, QJsonArray > >::iterator it = m_spawner.npcParameterOptions.dropPools.begin(); it != m_spawner.npcParameterOptions.dropPools.end(); ++it ) {
            QMap< QString, QJsonArray > dropPool = *it;
            QJsonObject obj;
            for( QMap< QString, QJsonArray>::iterator it2 = dropPool.begin(); it2 != dropPool.end(); ++it2 ) {
                obj[ it2.key() ] = it2.value();
            }
            npcParameterOptions.append( obj );
        }

        spawner[ "npcParameterOptions" ] = npcParameterOptions;

        map[ "spawner" ] = spawner;
    }

    //Continue!
    if( hasProperty( "statusEffect" ) ) {
        QJsonObject statusEffect = StatusEffect::jsonStatusEffectToJsonObject( m_statusEffect );

        map[ "statusEffect" ] = statusEffect;
    }

    if( m_breakDropOptions.size() > 0 ) {
        QJsonArray arr1;

        foreach( QList<BreakDropOptions> list, m_breakDropOptions ) {
            QJsonArray arr2;
            foreach( BreakDropOptions opt, list ) {
                QJsonArray optArray;
                optArray.append( opt.item );
                optArray.append( opt.amount );
                optArray.append( opt.foo );
                arr2.append( optArray );
            }
            arr1.append( arr2 );
        }
        map[ "breakDropOptions" ] = arr1;
    }

    if( hasProperty( "interactionTransition" ) ) {
        QJsonObject interactionTransition;
        QJsonObject growing;

        growing[ "0" ] = growingStepToObject( m_interactionTransition.growingStep.step1 );
        growing[ "1" ] = growingStepToObject( m_interactionTransition.growingStep.step2 );

        interactionTransition[ "growing" ] = growing;
        QJsonObject finalStep;
        QJsonArray dropOptions;

        dropOptions.append( m_interactionTransition.finalStep.probability );

        for( QList< QList < DropItem > >::iterator it = m_interactionTransition.finalStep.itemsPack.begin(); it != m_interactionTransition.finalStep.itemsPack.end(); ++it ) {
            QJsonArray itemPackArr;
            QList< DropItem > listItem = *it;
            foreach( DropItem item, listItem ) {
                QJsonObject obj;
                obj[ "name" ] = item.name;
                obj[ "count" ] = item.count;

                itemPackArr.append( obj );
            }
            dropOptions.append( itemPackArr );
        }

        finalStep[ "dropOptions" ] = dropOptions;
        finalStep[ "command" ] = m_interactionTransition.finalStep.command;

        interactionTransition[ "2" ] = finalStep;

        QJsonObject stageAlts;

        stageAlts[ "count" ] = m_interactionTransition.stageAlts.count;
        stageAlts[ "2" ] = m_interactionTransition.stageAlts.finalStep;

        interactionTransition[ "stageAlts" ] = stageAlts;

        map[ "interactionTransition" ] = interactionTransition;
    }

    if( m_race != "" )
        map[ "race" ] = m_race;

    if( m_script != "" ) {
        map[ "script" ] = m_script;
    } else if( m_scripts.size() > 0 ) {
        QJsonArray scripts;
        foreach( QString script, m_scripts ) {
            scripts.append( script );
        }
        map[ "scripts" ] = scripts;
    }

    if( hasProperty( "outboundNodesLocation" ) ) {
        QJsonArray onl;
        onl.append( m_outboundNodesLocation.x() );
        onl.append( m_outboundNodesLocation.y() );

        map[ "outboundNodesLocation" ] = onl;
    }

    if( hasProperty( "touchDamage" ) ) {
        QJsonObject touchDamageObj;

        QJsonArray statusArray;
        foreach( JsonStatusEffect eff, m_touchDamage.statusEffects ) {
            QJsonObject statusObj;

            statusObj[ "kind" ] = eff.kind;

            if( eff.amount != 0.0 )
                statusObj[ "amount" ] = eff.amount;

            if( eff.percentage != 0 )
                statusObj[ "percentage" ] = eff.percentage;

            if( eff.range != 0 )
                statusObj[ "range" ] = eff.range;


            statusArray.append( statusObj );
        }
        touchDamageObj[ "statusEffects" ] = statusArray;

        touchDamageObj[ "damageType" ] = m_touchDamage.damageType;
        touchDamageObj[ "damageSourceKind" ] = m_touchDamage.damageSourceKind;
        touchDamageObj[ "damage" ] = m_touchDamage.damage;

        map[ "touchDamage" ] = touchDamageObj;
    }

    if( hasProperty( "closeSounds" ) )
        map[ "closeSounds" ] = qStringListToJsonArray( m_closeSounds );

    if( hasProperty( "truthtable" ) ) {
        QJsonArray arr1;

        if( m_truthtable.type == TWO ) {
            for( int i = 0; i < m_truthtable.twoTable.size(); i++ ) {
                arr1.append( m_truthtable.twoTable[i] );
            }
        } else if( m_truthtable.type == FOUR ) {
            for( int i = 0; i < m_truthtable.fourTable.size(); i++ ) {
                QJsonArray arr2;
                for( int j = 0; j < m_truthtable.fourTable[i].size(); j++ ) {
                    arr2.append( m_truthtable.fourTable[i][j] );
                }
                arr1.append( arr2 );
            }
        }

        map[ "truthtable" ] = arr1;

    }

    if( hasProperty( "openSounds" ) )
        map[ "openSounds" ] = qStringListToJsonArray( m_openSounds );

    if( hasProperty( "sitCoverImage" ) )
        map[ "sitCoverImage" ] = m_sitCoverImage;

    if( hasProperty( "offSounds" ) )
        map[ "offSounds" ] = qStringListToJsonArray( m_offSounds );

    if( hasProperty( "hasWindowIcon" ) )
        map[ "hasWindowIcon" ] = m_hasWindowIcon;

    if( hasProperty( "smashSounds" ) )
        map[ "smashSounds" ] = qStringListToJsonArray( m_smashSounds );

    if( hasProperty( "rotationRange" ) ) {
        QJsonArray arr;
        arr.append( m_rotationRange[0] );
        arr.append( m_rotationRange[1] );

        map[ "rotationRange" ] = arr;
    }

    if( hasProperty( "frameCooldown" ) )
        map[ "frameCooldown" ] = m_frameCooldown;

    if( hasProperty( "animation" ) )
        map[ "animation" ] = m_animation;

    if( hasProperty( "animationPosition" ) ) {
        QJsonArray arr = qPointToJsonArray( m_animationPosition );

        map[ "animationPosition" ] = arr;
    }

    if( hasProperty( "detectTickDuration" ) )
        map[ "detectTickDuration" ] = m_detectTickDuration;

    if( hasProperty( "autocloseCooldown" ) )
        map[ "autocloseCooldown" ] = m_autocloseCooldown;

    if( hasProperty( "health" ) )
        map[ "health" ] = m_health;

    if( hasProperty( "category" ) )
        map[ "category" ] = m_category;

    if( hasProperty( "pointBeam" ) )
        map[ "pointBeam" ] = m_pointBeam;

    if( hasProperty( "price" ) )
        map[ "price" ] = m_price;

    if( hasProperty( "inboundNodes" ) ) {
        QJsonArray arr1;

        for( int i = 0; i < m_inboundNodes.size(); i++ ) {
            QJsonArray point = qPointToJsonArray( m_inboundNodes[i] );

            arr1.append( point );
        }
        map[ "inboundNodes" ] = arr1;
    }

    if( hasProperty( "flickerDistance" ) )
        map[ "flickerDistance" ] = m_flickerDistance;

    if( hasProperty( "uiConfig" ) )
        map[ "uiConfig" ] = m_uiConfig;

    if( hasProperty( "fireCooldown" ) )
        map[ "fireCooldown" ] = m_fireCooldown;

    if( hasProperty( "orientations" ) ) {
        QJsonArray orientations;

        foreach( Orientations ori, m_orientations ) {
            QJsonObject orientation = orientationsToObject( ori );

            orientations.append( orientation );
        }

        map[ "orientations" ] = orientations;
    }

    if( hasProperty( "sitEffectEmitters" ) )
        map[ "sitEffectEmitters" ] = qStringListToJsonArray( m_sitEffectEmitters );

    if( hasProperty( "hydrophobic" ) )
        map[ "hydrophobic" ] = m_hydrophobic;

    if( hasProperty( "interval" ) )
        map[ "interval" ] = m_interval;

    if( hasProperty( "engineOnOrientations" ) ) {
        QJsonArray orientations;

        foreach( Orientations ori, m_engineOnOrientations ) {
            QJsonObject orientation = orientationsToObject( ori );
            orientations.append( orientation );
        }
        map[ "engineOnOrientation" ] = orientations;
    }

    if( hasProperty( "state" ) ) {
        QJsonObject state;

        if( m_state.uiconfig != "" ) {
            state[ "uiconfig" ] = m_state.uiconfig;
        }

        if( m_state.openSounds.size() > 0 ) {
            state[ "openSounds" ] = qStringListToJsonArray( m_state.openSounds );
        }

        if( m_state.closeSounds.size() > 0 ) {
            state[ "closeSounds" ] = qStringListToJsonArray( m_state.closeSounds );
        }

        if( m_state.kind != "" ) {
            state[ "kind" ] = m_state.kind;
        }

        if( m_state.slotCount > 0 ) {
            state[ "slotCount" ] = m_state.slotCount;
        }

        map[ "state" ] = state;
    }

    if( hasProperty( "tipOffset" ) )
        map[ "tipOffset" ] = qPointToJsonArray( m_tipOffset );

    if( hasProperty( "outboundNodes" ) ) {
        QJsonArray arr;
        for( int i = 0; i < m_outboundNodes.size(); i++ ) {
            QJsonArray pt = qPointToJsonArray( m_outboundNodes[i] );
            arr.append( pt );
        }
        map[ "outboundNodes" ] = arr;
    }

    if( hasProperty( "targetHoldTime" ) )
        map[ "targetHoldTime" ] = m_targetHoldTime;

    if( hasProperty( "sitPosition" ) )
        map[ "sitPosition" ] = qPointToJsonArray( m_sitPosition );

    if( hasProperty( "onSounds" ) )
        map[ "onSounds" ] = qStringListToJsonArray( m_onSounds );

    if( hasProperty( "slotCount" ) )
        map[ "slotCount" ] = m_slotCount;

    if( hasProperty( "printable" ) )
        map[ "printable" ] = m_printable;

    if( hasProperty( "genericDescription" ) )
        map[ "genericDescription" ] = m_genericDescription;

    if( hasProperty( "unlit" ) )
        map[ "unlit" ] = m_unlit;

    if( hasProperty( "fireOffets" ) ) {
        QJsonArray arr;
        for( int i = 0; i < m_fireOffsets.size(); i++ ) {
            double off = m_fireOffsets[i];
            arr.append( off );
        }
        map[ "fireOffsets" ] = arr;
    }

    if( hasProperty( "objectItem" ) )
        map[ "objectItem" ] = m_objectItem;

    if( hasProperty( "rotationPauseTime" ) )
        map[ "rotationPauseTime" ] = m_rotationPauseTime;

    if( hasProperty( "interactAction" ) )
        map[ "interactAction" ] = m_interactAction;

    if( hasProperty( "sounds" ) )
        map[ "sounds" ] = qStringListToJsonArray( m_sounds );

    if( hasProperty( "sitFlipDirection" ) )
        map[ "sitFlipDirection" ] = m_sitFlipDirection;

    if( hasProperty( "soundEffect" ) )
        map[ "soundEffect" ] = m_soundEffect;

    if( hasProperty( "detectRadius" ) )
        map[ "detectRadius" ] = m_detectRadius;

    if( hasProperty( "stageAlts" ) ) {
        QJsonObject stageAlts;

        stageAlts[ "count" ] = m_stageAlts.count;
        stageAlts[ "2" ] = m_stageAlts.finalStep;

        map[ "stageAlts" ] = stageAlts;
    }

    if( hasProperty( "rotationTime" ) )
        map[ "rotationTime" ] = m_rotationTime;

    if( hasProperty( "flickerTiming" ) )
        map[ "flickerTiming" ] = m_flickerTiming;

    if( hasProperty( "useSounds" ) )
        map[ "useSounds" ] = qStringListToJsonArray( m_useSounds );

    if( hasProperty( "growing" ) ) {
        QJsonObject growing;

        growing[ "0" ] = growingStepToObject( m_growing.step1 );
        growing[ "1" ] = growingStepToObject( m_growing.step2 );

        map[ "growing" ] = growing;
    }

    if( hasProperty( "flickerStrength" ) )
        map[ "flickerStrength" ] = m_flickerStrength;

    if( hasProperty( "gates" ) )
        map[ "gates" ] = m_gates;

    if( hasProperty( "pointLight" ) )
        map[ "pointLight" ] = m_pointLight;

    if( hasProperty( "projectileOptions" ) ) {
        QJsonObject projectileOptions;
        if( m_projectileOptions.projectileType != "" )
            projectileOptions[ "projectileType" ] = m_projectileOptions.projectileType;

        projectileOptions[ "projectileParams" ] = m_projectileOptions.projectileParams;

        map[ "projectileOptions" ] = projectileOptions;
    }

    if( hasProperty( "baseOffset" ) )
        map[ "baseOffset" ] = qPointToJsonArray( m_baseOffset );

    if( hasProperty( "damageTime" ) ) {
        QJsonObject damageTeam;

        damageTeam[ "type" ] = m_damageTime;

        map[ "damageTeam" ] = damageTeam;
    }

    if( hasProperty( "statusEffects" ) ) {
        QJsonArray arr;
        foreach( JsonStatusEffect eff, m_statusEffects ) {
            arr.append( StatusEffect::jsonStatusEffectToJsonObject( eff ) );
        }
        map[ "statusEffects" ] = arr;
    }

    if( hasProperty( "interactData" ) ) {
        QJsonObject inter;

        if( m_interactData.config != "" )
            inter[ "config" ] = m_interactData.config;

        if( m_interactData.filter.size() > 0 )
            inter[ "filter" ] = qStringListToJsonArray( m_interactData.filter );

        map[ "interactData" ] = inter;
    }

    if( hasProperty( "soundEffectRadius" ) )
        map[ "soundEffectRadius" ] = m_soundEffectRadius;

    if( hasProperty( "sitOrientation" ) )
        map[ "sitOrientation" ] = m_sitOrientation;

    if( hasProperty( "subtitle" ) )
        map[ "subtitle" ] = m_subtitle;

    if( hasProperty( "sitEmote" ) )
        map[ "sitEmote" ] = m_sitEmote;

    if( hasProperty( "maxLaserLength" ) )
        map[ "maxLaserLength" ] = m_maxLaserLength;

    if( hasProperty( "smashDropOptions" ) ) {
        QJsonArray arr1;

        foreach( QList<SmashDropOptions> list, m_smashDropOptions ) {
            QJsonArray arr2;
            foreach( SmashDropOptions opt, list ) {
                QJsonArray optArray;
                optArray.append( opt.item );
                optArray.append( opt.amount );
                optArray.append( opt.foo );
                arr2.append( optArray );
            }
            arr1.append( arr2 );
        }
        map[ "smashDropOptions" ] = arr1;
    }

    if( hasProperty( "recipeGroup" ) )
        map[ "recipeGroup" ] = m_recipeGroup;

    if( hasProperty( "unbreakable" ) )
        map[ "unbreakable" ] = m_unbreakable;

    if( hasProperty( "objectType" ) )
        map[ "objectType" ] = m_objectType;

    return map;
}

QJsonObject StarWorldObject::orientationsToObject( Orientations &ori ) {
    QJsonObject orientation;

    if( ori.image != "" ) {
        orientation[ "image" ] = ori.image;
    } else if( ori.dualImage != "" ) {
        orientation[ "dualImage" ] = ori.dualImage;
    }

    orientation[ "imagePosition" ] = qPointToJsonArray( ori.imagePosition );

    if( ori.frames > 0 )
        orientation[ "frames" ] = ori.frames;

    orientation[ "animationCycle" ] = ori.animationCycle;

    if( ori.spaces.size() > 0 ) {
        QJsonArray spaces;
        foreach( QPoint point, ori.spaces ) {
            QJsonArray pt = qPointToJsonArray( point );
            spaces.append( pt );
        }
        orientation[ "spaces" ] = spaces;
    }

    if( ori.imageLayers.size() > 0 ) {
        QJsonArray arr;

        foreach( ImageLayer layer, ori.imageLayers ) {
            QJsonObject obj;
            obj[ "image" ] = layer.image;
            if( layer.unlit != false )
                obj[ "unlit" ] = layer.unlit;

            arr.append( obj );
        }
        orientation[ "imageLayers" ] = arr;
    }

    if( ori.spaceScan != 0.0 )
        orientation[ "spaceScan" ] = ori.spaceScan;

    if( ori.anchors.size() > 0 ) {
        QJsonArray anchors;
        foreach( QString anchor, ori.anchors ) {
            anchors.append( anchor );
        }
        orientation[ "anchors" ] = anchors;
    }

    if( ori.collision != "" )
        orientation[ "collision" ] = ori.collision;

    if( ori.direction != "" )
        orientation[ "direction" ] = ori.direction;

    if( ori.animationParts.size() > 0 ) {
        QJsonObject anim = animationPartsToObject( ori.animationParts );
        orientation[ "animationParts" ] = anim;
    }

    if( ori.fgAnchors.size() > 0 ) {
        QJsonArray arr;

        foreach( QPoint anchor, ori.fgAnchors ) {
            QJsonArray pt = qPointToJsonArray( anchor );
            arr.append( pt );
        }
        orientation[ "fgAnchors" ] = arr;
    }

    if( ori.bgAnchors.size() > 0 ) {
        QJsonArray arr;

        foreach( QPoint anchor, ori.bgAnchors ) {
            QJsonArray pt = qPointToJsonArray( anchor );
            arr.append( pt );
        }
        orientation[ "bgAnchors" ] = arr;
    }

    return orientation;
}

Orientations StarWorldObject::objectToOrientations(QJsonObject obj)
{
    Orientations ori;

    QJsonValue image = obj[ "image" ];
    QJsonValue dualImage = obj[ "dualImage" ];
    QJsonValue imageLayers = obj[ "imageLayers" ];
    QJsonValue imagePosition = obj[ "imagePosition" ];
    QJsonValue frame = obj[ "frame" ];
    QJsonValue animationCycle = obj[ "animationCycle" ];
    QJsonValue spaces = obj[ "spaces" ];
    QJsonValue spaceScan = obj[ "spaceScan" ];
    QJsonValue anchors = obj[ "anchors" ];
    QJsonValue collision = obj[ "collision" ];
    QJsonValue direction = obj[ "direction" ];
    QJsonValue animationParts = obj[ "animationParts" ];
    QJsonValue fgAnchors = obj[ "fgAnchors" ];
    QJsonValue bgAnchors = obj[ "bgAnchors" ];

    if( !image.isUndefined() )
        ori.image = image.toString();
    else if( !dualImage.isUndefined() )
        ori.dualImage = dualImage.toString();

    if( !imageLayers.isUndefined() ) {
        QJsonArray arr = imageLayers.toArray();
        QList< ImageLayer > imgLayerList;

        for( int i = 0; i < arr.size(); ++i ) {
            QJsonObject obj = arr[i].toObject();
            ImageLayer img;
            QJsonValue imageL = obj[ "image" ];
            QJsonValue unlitL = obj[ "unlit" ];

            if( !imageL.isUndefined() )
                img.image = imageL.toString();

            if( !unlitL.isUndefined() )
                img.unlit = unlitL.toBool();

            imgLayerList.append( img );
        }

        ori.imageLayers = imgLayerList;
    }

    if( !imagePosition.isUndefined() )
        ori.imagePosition = StarItem::qJsonArrayToQPoint( imagePosition.toArray() );

    if( !frame.isUndefined() )
        ori.frames = frame.toInt();

    if( !animationCycle.isUndefined() )
        ori.animationCycle = animationCycle.toDouble();

    if( !spaces.isUndefined() ) {
        QList< QPoint > spacesP;
        QJsonArray arr = spaces.toArray();

        for( int i = 0; i < arr.size(); ++i ) {
            QJsonArray pointArray = arr[i].toArray();
            QPoint pt = StarItem::qJsonArrayToQPoint( pointArray );
            spacesP.append( pt );
        }
        ori.spaces = spacesP;
    }

    if( !spaceScan.isUndefined() )
        ori.spaceScan = spaceScan.toDouble();

    if( !anchors.isUndefined() )
        ori.anchors = StarItem::qJsonArrayToQStringList( anchors.toArray() );

    if( !collision.isUndefined() )
        ori.collision = collision.toString();

    if( !direction.isUndefined() )
        ori.direction = direction.toString();

    if( !animationParts.isUndefined() ) {
        QJsonObject animObj = animationParts.toObject();
        AnimationParts parts;

        for( QJsonObject::iterator it = animObj.begin(); it != animObj.end(); ++it ) {
            parts[ it.key() ] = it.value().toString();
        }
        ori.animationParts = parts;
    }

    if( !fgAnchors.isUndefined() ) {
        QList< QPoint > anchorsP;
        QJsonArray arr = fgAnchors.toArray();

        for( int i = 0; i < arr.size(); i++ ) {
            QJsonArray pointArray = arr[i].toArray();
            QPoint pt = StarItem::qJsonArrayToQPoint( pointArray );
            anchorsP.append( pt );
        }
        ori.fgAnchors = anchorsP;
    }

    if( !bgAnchors.isUndefined() ) {
        QList< QPoint > anchorsP;
        QJsonArray arr = bgAnchors.toArray();

        for( int i = 0; i < arr.size(); i++ ) {
            QJsonArray pointArray = arr[i].toArray();
            QPoint pt = StarItem::qJsonArrayToQPoint( pointArray );
            anchorsP.append( pt );
        }
        ori.bgAnchors = anchorsP;
    }

    return ori;

}

QJsonObject StarWorldObject::growingStepToObject(GrowingStep &step)
{
    QJsonObject obj;
    QJsonArray duration;
    duration.append( step.duration[0] );
    duration.append( step.duration[1] );

    obj[ "duration" ] = duration;
    obj[ "success" ] = step.success;
    obj[ "failure" ] = step.failure;
    
    return obj;
}

GrowingStep StarWorldObject::objectToGrowingStep(QJsonObject obj)
{
    GrowingStep step;
    QJsonValue duration = obj[ "duration" ];
    QJsonValue success = obj[ "success" ];
    QJsonValue failure = obj[ "failure" ];

    if( !duration.isUndefined() ) {
        QJsonArray arr = duration.toArray();
        if( arr.size() > 0 ) {
            QRange range;
            range.append( arr[0].toInt() );
            range.append( arr[1].toInt() );
            step.duration = range;
        }
    }

    if( !success.isUndefined() )
        step.success = success.toInt();

    if( !failure.isUndefined() )
        step.failure = failure.toInt();

    return step;
}

bool StarWorldObject::isSittable() const
{
    return m_isSittable;
}

void StarWorldObject::setSittable(bool sittable)
{
    m_isSittable = sittable;
}

bool StarWorldObject::hasAnimation() const
{
    return m_hasAnimation;
}

void StarWorldObject::setHasAnimation(bool hasAnimation)
{
    m_hasAnimation = hasAnimation;
}

QString StarWorldObject::objectType() const
{
    return m_objectType;
}

void StarWorldObject::setObjectType(const QString &objectType)
{
    m_objectType = objectType;
}

bool StarWorldObject::unbreakable() const
{
    return m_unbreakable;
}

void StarWorldObject::setUnbreakable(bool unbreakable)
{
    m_properties.insert( "unbreakable" );
    m_unbreakable = unbreakable;
}

QString StarWorldObject::recipeGroup() const
{
    return m_recipeGroup;
}

void StarWorldObject::setRecipeGroup(const QString &recipeGroup)
{
    m_properties.insert( "recipeGroup" );
    m_recipeGroup = recipeGroup;
}

QList<QList<SmashDropOptions> > StarWorldObject::smashDropOptions() const
{
    return m_smashDropOptions;
}

void StarWorldObject::setSmashDropOptions(const QList<QList<SmashDropOptions> > &smashDropOptions)
{
    m_properties.insert( "smashDropOptions" );
    m_smashDropOptions = smashDropOptions;
}

int StarWorldObject::maxLaserLength() const
{
    return m_maxLaserLength;
}

void StarWorldObject::setMaxLaserLength(int maxLaserLength)
{
    m_properties.insert( "maxLaserLength" );
    m_maxLaserLength = maxLaserLength;
}

QString StarWorldObject::sitEmote() const
{
    return m_sitEmote;
}

void StarWorldObject::setSitEmote(const QString &sitEmote)
{
    m_properties.insert( "sitEmote" );
    m_sitEmote = sitEmote;
}

QString StarWorldObject::subtitle() const
{
    return m_subtitle;
}

void StarWorldObject::setSubtitle(const QString &subtitle)
{
    m_properties.insert( "subtitle" );
    m_subtitle = subtitle;
}

QString StarWorldObject::sitOrientation() const
{
    return m_sitOrientation;
}

void StarWorldObject::setSitOrientation(const QString &sitOrientation)
{
    m_properties.insert( "sitOrientation" );
    m_sitOrientation = sitOrientation;
}

int StarWorldObject::soundEffectRadius() const
{
    return m_soundEffectRadius;
}

void StarWorldObject::setSoundEffectRadius(int soundEffectRadius)
{
    m_properties.insert( "soundEffectRadius" );
    m_soundEffectRadius = soundEffectRadius;
}

InteractData StarWorldObject::interactData() const
{
    return m_interactData;
}

void StarWorldObject::setInteractData(InteractData interactData)
{
    m_properties.insert( "interactData" );
    m_interactData = interactData;
}

QList<JsonStatusEffect> StarWorldObject::statusEffects() const
{
    return m_statusEffects;
}

void StarWorldObject::setStatusEffects(const QList<JsonStatusEffect> &statusEffects)
{
    m_properties.insert( "statusEffects" );
    m_statusEffects = statusEffects;
}

QString StarWorldObject::damageTime() const
{
    return m_damageTime;
}

void StarWorldObject::setDamageTime(const QString &damageTime)
{
    m_properties.insert( "damageTime" );
    m_damageTime = damageTime;
}

QPointF StarWorldObject::baseOffset() const
{
    return m_baseOffset;
}

void StarWorldObject::setBaseOffset(const QPointF &baseOffset)
{
    m_properties.insert( "baseOffset" );
    m_baseOffset = baseOffset;
}

ProjectileOptions StarWorldObject::projectileOptions() const
{
    return m_projectileOptions;
}

void StarWorldObject::setProjectileOptions(ProjectileOptions projectileOptions)
{
    m_properties.insert( "projectileOptions" );
    m_projectileOptions = projectileOptions;
}

bool StarWorldObject::pointLight() const
{
    return m_pointLight;
}

void StarWorldObject::setPointLight(bool pointLight)
{
    m_properties.insert( "pointLight" );
    m_pointLight = pointLight;
}

int StarWorldObject::gates() const
{
    return m_gates;
}

void StarWorldObject::setGates(int gates)
{
    m_properties.insert( "gates" );
    m_gates = gates;
}

double StarWorldObject::flickerStrength() const
{
    return m_flickerStrength;
}

void StarWorldObject::setFlickerStrength(double flickerStrength)
{
    m_properties.insert( "flickerStrength" );
    m_flickerStrength = flickerStrength;
}

Growing StarWorldObject::growing() const
{
    return m_growing;
}

void StarWorldObject::setGrowing(Growing growing)
{
    m_properties.insert( "growing" );
    m_growing = growing;
}

QStringList StarWorldObject::useSounds() const
{
    return m_useSounds;
}

void StarWorldObject::setUseSounds(const QStringList &useSounds)
{
    m_properties.insert( "useSounds" );
    m_useSounds = useSounds;
}

double StarWorldObject::flickerTiming() const
{
    return m_flickerTiming;
}

void StarWorldObject::setFlickerTiming(double flickerTiming)
{
    m_properties.insert( "flickerTiming" );
    m_flickerTiming = flickerTiming;
}

double StarWorldObject::rotationTime() const
{
    return m_rotationTime;
}

void StarWorldObject::setRotationTime(double rotationTime)
{
    m_properties.insert( "rotationTime" );
    m_rotationTime = rotationTime;
}

StageAlts StarWorldObject::stageAlts() const
{
    return m_stageAlts;
}

void StarWorldObject::setStageAlts(StageAlts stageAlts)
{
    m_properties.insert( "stageAlts" );
    m_stageAlts = stageAlts;
}

int StarWorldObject::detectRadius() const
{
    return m_detectRadius;
}

void StarWorldObject::setDetectRadius(int detectRadius)
{
    m_properties.insert( "detectRadius" );
    m_detectRadius = detectRadius;
}

QString StarWorldObject::soundEffect() const
{
    return m_soundEffect;
}

void StarWorldObject::setSoundEffect(const QString &soundEffect)
{
    m_properties.insert( "soundEffect" );
    m_soundEffect = soundEffect;
}

bool StarWorldObject::sitFlipDirection() const
{
    return m_sitFlipDirection;
}

void StarWorldObject::setSitFlipDirection(bool sitFlipDirection)
{
    m_properties.insert( "sitFlipDirection" );
    m_sitFlipDirection = sitFlipDirection;
}

QStringList StarWorldObject::sounds() const
{
    return m_sounds;
}

void StarWorldObject::setSounds(const QStringList &sounds)
{
    m_properties.insert( "sounds" );
    m_sounds = sounds;
}

QString StarWorldObject::interactAction() const
{
    return m_interactAction;
}

void StarWorldObject::setInteractAction(const QString &interactAction)
{
    m_properties.insert( "interactAction" );
    m_interactAction = interactAction;
}

double StarWorldObject::rotationPauseTime() const
{
    return m_rotationPauseTime;
}

void StarWorldObject::setRotationPauseTime(double rotationPauseTime)
{
    m_properties.insert( "rotationPauseTime" );
    m_rotationPauseTime = rotationPauseTime;
}

QString StarWorldObject::objectItem() const
{
    return m_objectItem;
}

void StarWorldObject::setObjectItem(const QString &objectItem)
{
    m_properties.insert( "objectItem" );
    m_objectItem = objectItem;
}

QVector<double> StarWorldObject::fireOffsets() const
{
    return m_fireOffsets;
}

void StarWorldObject::setFireOffsets(const QVector<double> &fireOffsets)
{
    m_properties.insert( "fireOffsets" );
    m_fireOffsets = fireOffsets;
}

bool StarWorldObject::unlit() const
{
    return m_unlit;
}

void StarWorldObject::setUnlit(bool unlit)
{
    m_properties.insert( "unlit" );
    m_unlit = unlit;
}

QString StarWorldObject::genericDescription() const
{
    return m_genericDescription;
}

void StarWorldObject::setGenericDescription(const QString &genericDescription)
{
    m_properties.insert( "genericDescription" );
    m_genericDescription = genericDescription;
}

bool StarWorldObject::printable() const
{
    return m_printable;
}

void StarWorldObject::setPrintable(bool printable)
{
    m_properties.insert( "printable" );
    m_printable = printable;
}

int StarWorldObject::slotCount() const
{
    return m_slotCount;
}

void StarWorldObject::setSlotCount(int slotCount)
{
    m_properties.insert( "slotCount" );
    m_slotCount = slotCount;
}

QStringList StarWorldObject::onSounds() const
{
    return m_onSounds;
}

void StarWorldObject::setOnSounds(const QStringList &onSounds)
{
    m_properties.insert( "onSounds" );
    m_onSounds = onSounds;
}

QPoint StarWorldObject::sitPosition() const
{
    return m_sitPosition;
}

void StarWorldObject::setSitPosition(const QPoint &sitPosition)
{
    m_properties.insert( "sitPosition" );
    m_sitPosition = sitPosition;
}

int StarWorldObject::targetHoldTime() const
{
    return m_targetHoldTime;
}

void StarWorldObject::setTargetHoldTime(int targetHoldTime)
{
    m_properties.insert( "targetHoldTime" );
    m_targetHoldTime = targetHoldTime;
}

QVector< QPoint > StarWorldObject::outboundNodes() const
{
    return m_outboundNodes;
}

void StarWorldObject::setOutboundNodes(const QVector< QPoint > &outboundNodes)
{
    m_properties.insert( "outboundNodes" );
    m_outboundNodes = outboundNodes;
}

QPointF StarWorldObject::tipOffset() const
{
    return m_tipOffset;
}

void StarWorldObject::setTipOffset(const QPointF &tipOffset)
{
    m_properties.insert( "tipOffset" );
    m_tipOffset = tipOffset;
}

State StarWorldObject::state() const
{
    return m_state;
}

void StarWorldObject::setState(State state)
{
    m_properties.insert( "state" );
    m_state = state;
}

QList<Orientations> StarWorldObject::engineOnOrientations() const
{
    return m_engineOnOrientations;
}

void StarWorldObject::setEngineOnOrientations(const QList<Orientations> &engineOnOrientations)
{
    m_properties.insert( "engineOnOrientations" );
    m_engineOnOrientations = engineOnOrientations;
}

int StarWorldObject::interval() const
{
    return m_interval;
}

void StarWorldObject::setInterval(int interval)
{
    m_properties.insert( "interval" );
    m_interval = interval;
}

QPoint StarWorldObject::animationPosition() const
{
    return m_animationPosition;
}

void StarWorldObject::setAnimationPosition(const QPoint &animationPosition)
{
    m_properties.insert( "animationPosition" );
    m_animationPosition = animationPosition;
}

bool StarWorldObject::hydrophobic() const
{
    return m_hydrophobic;
}

void StarWorldObject::setHydrophobic(bool hydrophobic)
{
    m_properties.insert( "hydrophobic" );
    m_hydrophobic = hydrophobic;
}

QStringList StarWorldObject::sitEffectEmitters() const
{
    return m_sitEffectEmitters;
}

void StarWorldObject::setSitEffectEmitters(const QStringList &sitEffectEmitters)
{
    m_properties.insert( "sitEffectEmitters" );
    m_sitEffectEmitters = sitEffectEmitters;
}

QList<Orientations> StarWorldObject::orientations() const
{
    return m_orientations;
}

void StarWorldObject::setOrientations(const QList<Orientations> &orientations)
{
    m_properties.insert( "orientations" );
    m_orientations = orientations;
}

double StarWorldObject::fireCooldown() const
{
    return m_fireCooldown;
}

void StarWorldObject::setFireCooldown(double fireCooldown)
{
    m_properties.insert( "fireCooldown" );
    m_fireCooldown = fireCooldown;
}

QString StarWorldObject::uiConfig() const
{
    return m_uiConfig;
}

void StarWorldObject::setUiConfig(const QString &uiConfig)
{
    m_properties.insert( "uiConfig" );
    m_uiConfig = uiConfig;
}

double StarWorldObject::flickerDistance() const
{
    return m_flickerDistance;
}

void StarWorldObject::setFlickerDistance(double flickerDistance)
{
    m_properties.insert( "flickerDistance" );
    m_flickerDistance = flickerDistance;
}

QVector< QPoint > StarWorldObject::inboundNodes() const
{
    return m_inboundNodes;
}

void StarWorldObject::setInboundNodes(const QVector< QPoint > &inboundNodes)
{
    m_properties.insert( "inboundNodes" );
    m_inboundNodes = inboundNodes;
}

int StarWorldObject::price() const
{
    return m_price;
}

void StarWorldObject::setPrice(int price)
{
    m_properties.insert( "price" );
    m_price = price;
}

double StarWorldObject::pointBeam() const
{
    return m_pointBeam;
}

void StarWorldObject::setPointBeam(double pointBeam)
{
    m_properties.insert( "pointBeam" );
    m_pointBeam = pointBeam;
}

QString StarWorldObject::category() const
{
    return m_category;
}

void StarWorldObject::setCategory(const QString &category)
{
    m_properties.insert( "category" );
    m_category = category;
}

QString StarWorldObject::script() const
{
    return m_script;
}

void StarWorldObject::setScript(const QString &script)
{
    m_properties.insert( "script" );
    m_script = script;
}

double StarWorldObject::health() const
{
    return m_health;
}

void StarWorldObject::setHealth(double health)
{
    m_properties.insert( "health" );
    m_health = health;
}

int StarWorldObject::autocloseCooldown() const
{
    return m_autocloseCooldown;
}

void StarWorldObject::setAutocloseCooldown(int autocloseCooldown)
{
    m_properties.insert( "autocloseCooldown" );
    m_autocloseCooldown = autocloseCooldown;
}

int StarWorldObject::detectTickDuration() const
{
    return m_detectTickDuration;
}

void StarWorldObject::setDetectTickDuration(int detectTickDuration)
{
    m_properties.insert( "detectTickDuration" );
    m_detectTickDuration = detectTickDuration;
}

QString StarWorldObject::animation() const
{
    return m_animation;
}

void StarWorldObject::setAnimation(const QString &animation)
{
    m_properties.insert( "animation" );
    m_animation = animation;
}

int StarWorldObject::frameCooldown() const
{
    return m_frameCooldown;
}

void StarWorldObject::setFrameCooldown(int frameCooldown)
{
    m_properties.insert( "frameCooldown" );
    m_frameCooldown = frameCooldown;
}

QRange StarWorldObject::rotationRange() const
{
    return m_rotationRange;
}

void StarWorldObject::setRotationRange(const QRange &rotationRange)
{
    m_properties.insert( "rotationRange" );
    m_rotationRange = rotationRange;
}

QStringList StarWorldObject::smashSounds() const
{
    return m_smashSounds;
}

void StarWorldObject::setSmashSounds(const QStringList &smashSounds)
{
    m_properties.insert( "smashSounds" );
    m_smashSounds = smashSounds;
}

bool StarWorldObject::hasWindowIcon() const
{
    return m_hasWindowIcon;
}

void StarWorldObject::setHasWindowIcon(bool hasWindowIcon)
{
    m_properties.insert( "hasWindowIcon" );
    m_hasWindowIcon = hasWindowIcon;
}

QStringList StarWorldObject::offSounds() const
{
    return m_offSounds;
}

void StarWorldObject::setOffSounds(const QStringList &offSounds)
{
    m_properties.insert( "offSounds" );
    m_offSounds = offSounds;
}

QString StarWorldObject::sitCoverImage() const
{
    return m_sitCoverImage;
}

void StarWorldObject::setSitCoverImage(const QString &sitCoverImage)
{
    m_properties.insert( "sitCoverImage" );
    m_sitCoverImage = sitCoverImage;
}

QStringList StarWorldObject::openSounds() const
{
    return m_openSounds;
}

void StarWorldObject::setOpenSounds(const QStringList &openSounds)
{
    m_properties.insert( "openSounds" );
    m_openSounds = openSounds;
}

Truthtable StarWorldObject::truthtable() const
{
    return m_truthtable;
}

void StarWorldObject::setTruthtable(const Truthtable &truthtable)
{
    m_properties.insert( "truthtable" );
    m_truthtable = truthtable;
}

QStringList StarWorldObject::closeSounds() const
{
    return m_closeSounds;
}

void StarWorldObject::setCloseSounds(const QStringList &closeSounds)
{
    m_properties.insert( "closeSounds" );
    m_closeSounds = closeSounds;
}

TouchDamage StarWorldObject::touchDamage() const
{
    return m_touchDamage;
}

void StarWorldObject::setTouchDamage(TouchDamage touchDamage)
{
    m_properties.insert( "touchDamage" );
    m_touchDamage = touchDamage;
}

QPoint StarWorldObject::outboundNodesLocation() const
{
    return m_outboundNodesLocation;
}

void StarWorldObject::setOutboundNodesLocation(const QPoint &outboundNodesLocation)
{
    m_properties.insert( "outboundNodesLocation" );
    m_outboundNodesLocation = outboundNodesLocation;
}

QStringList StarWorldObject::scripts() const
{
    return m_scripts;
}

void StarWorldObject::setScripts(const QStringList &scripts)
{
    m_properties.insert( "scripts" );
    m_scripts = scripts;
}

QString StarWorldObject::race() const
{
    return m_race;
}

void StarWorldObject::setRace(const QString &race)
{
    m_properties.insert( "race" );
    m_race = race;
}

InteractionTransition StarWorldObject::interactionTransition() const
{
    return m_interactionTransition;
}

void StarWorldObject::setInteractionTransition(InteractionTransition interactionTransition)
{
    m_properties.insert( "interactionTransition" );
    m_interactionTransition = interactionTransition;
}

QList<QList<BreakDropOptions> > StarWorldObject::breakDropOptions() const
{
    return m_breakDropOptions;
}

void StarWorldObject::setBreakDropOptions(const QList<QList<BreakDropOptions> > &breakDropOptions)
{
    m_properties.insert( "breakDropOptions" );
    m_breakDropOptions = breakDropOptions;
}

SitStatusEffect StarWorldObject::statusEffect() const
{
    return m_statusEffect;
}

void StarWorldObject::setStatusEffect(SitStatusEffect statusEffect)
{
    m_properties.insert( "statusEffect" );
    m_statusEffect = statusEffect;
}

Spawner StarWorldObject::spawner() const
{
    return m_spawner;
}

void StarWorldObject::setSpawner(Spawner spawner)
{
    m_properties.insert( "spawner" );
    m_spawner = spawner;
}

int StarWorldObject::scriptDelta() const
{
    return m_scriptDelta;
}

void StarWorldObject::setScriptDelta(int scriptDelta)
{
    m_properties.insert( "scriptDelta" );
    m_scriptDelta = scriptDelta;
}

AnimationParts StarWorldObject::animationParts() const
{
    return m_animationParts;
}

void StarWorldObject::setAnimationParts(const AnimationParts &animationParts)
{
    m_properties.insert( "animationParts" );
    m_animationParts = animationParts;
}

LightColors StarWorldObject::lightColors() const
{
    return m_lightColors;
}

void StarWorldObject::setLightColors(const LightColors &lightColors)
{
    m_properties.insert( "lightColors" );
    m_lightColors = lightColors;
}

QColor StarWorldObject::lightColor() const
{
    return m_lightColor;
}

void StarWorldObject::setLightColor(const QColor &lightColor)
{
    m_properties.insert( "lightColor" );
    m_lightColor = lightColor;
}

int StarWorldObject::sitAngle() const
{
    return m_sitAngle;
}

void StarWorldObject::setSitAngle(int sitAngle)
{
    m_properties.insert( "sitAngle" );
    m_sitAngle = sitAngle;
}

QList<ParticleEmitter> StarWorldObject::particleEmitters() const
{
    return m_particleEmitters;
}

void StarWorldObject::setParticleEmitters(const QList<ParticleEmitter> &particleEmitters)
{
    m_properties.insert( "particleEmitters" );
    m_particleEmitters = particleEmitters;
}

QJsonObject StarWorldObject::animationPartsToObject( AnimationParts &animationParts ) {
    QJsonObject animationPart;

    for( AnimationParts::iterator it = animationParts.begin(); it != animationParts.end(); ++it ) {
        QString key = it.key();
        QString value = it.value();

        animationPart[ key ] = value;
    }
    return animationPart;
}
