#ifndef STARWORLDOBJECT_H
#define STARWORLDOBJECT_H

#include "staritem.h"

#define STARWORLDOBJECT_DEFAULT_SIT_ANGLE 0
#define STARWORLDOBJECT_DEFAULT_LIGHT_COLOR QColor( 0, 0, 0 ) // This color must NOT be displayed on the save
#define STARWORLDOBJECT_DEFAULT_SCRIPT_DELTA -1
#define STARWORLDOBJECT_DEFAULT_RACE QString()
#define STARWORLDOBJECT_DEFAULT_SIT_COVER_IMAGE QString()
#define STARWORLDOBJECT_DEFAULT_HAS_WINDOW_ICON true // False is only dispayed apparently
#define STARWORLDOBJECT_DEFAULT_FRAME_COOLDOWN 0
#define STARWORLDOBJECT_DEFAULT_ANIMATION QString()
#define STARWORLDOBJECT_DEFAULT_DETECT_TICK_DURATION -1 // Not displayed in that case
#define STARWORLDOBJECT_DEFAULT_AUTOCLOSE_COOLDOWN -1 // idem
#define STARWORLDOBJECT_DEFAULT_HEALTH -1.0 //idem
#define STARWORLDOBJECT_DEFAULT_SCRIPT QString()
#define STARWORLDOBJECT_DEFAULT_CATEGORY QString()
#define STARWORLDOBJECT_DEFAULT_POINT_BEAM 0.0
#define STARWORLDOBJECT_DEFAULT_PRICE 0
#define STARWORLDOBJECT_DEFAULT_FLICKER_DISTANCE 0.0
#define STARWORLDOBJECT_DEFAULT_UI_CONFIG QString()
#define STARWORLDOBJECT_DEFAULT_FIRE_COOLDOWN 0.0
#define STARWORLDOBJECT_DEFAULT_HYDROPHOBIC false
#define STARWORLDOBJECT_DEFAULT_HAS_ANIMATION false
#define STARWORLDOBJECT_DEFAULT_ANIMATION_POSITION QPoint( 0, 0 )
#define STARWORLDOBJECT_DEFAULT_INTERVAL -1
#define STARWORLDOBJECT_DEFAULT_TIP_OFFSET QPointF( 0.0, 0.0 ) //An offset is usually different from 0 so this will not be displayed
#define STARWORLDOBJECT_DEFAULT_TARGET_HOLD_TIME -1
#define STARWORLDOBJECT_DEFAULT_IS_SITTABLE false
#define STARWORLDOBJECT_DEFAULT_SIT_POSITION QPoint( 0, 0 )
#define STARWORLDOBJECT_DEFAULT_SLOT_COUNT -1
#define STARWORLDOBJECT_DEFAULT_PRINTABLE false
#define STARWORLDOBJECT_DEFAULT_GENERIC_DESCRIPTION QString()
#define STARWORLDOBJECT_DEFAULT_UNLIT false
#define STARWORLDOBJECT_DEFAULT_OBJECT_ITEM QString()
#define STARWORLDOBJECT_DEFAULT_ROTATION_PAUSE_TIME -1.0
#define STARWORLDOBJECT_DEFAULT_INTERACT_ACTION QString()
#define STARWORLDOBJECT_DEFAULT_SIT_FLIP_DIRECTION false
#define STARWORLDOBJECT_DEFAULT_SOUND_EFFECT QString()
#define STARWORLDOBJECT_DEFAULT_DETECT_RADIUS 0
#define STARWORLDOBJECT_DEFAULT_ROTATION_TIME -1
#define STARWORLDOBJECT_DEFAULT_FLICKER_TIMING -1
#define STARWORLDOBJECT_DEFAULT_FLICKER_STRENGTH 0
#define STARWORLDOBJECT_DEFAULT_GATES 0
#define STARWORLDOBJECT_DEFAULT_POINT_LIGHT false
#define STARWORLDOBJECT_DEFAULT_BASE_OFFSET QPointF( 0.0, 0.0 )
#define STARWORLDOBJECT_DEFAULT_DAMAGE_TIME QString()
#define STARWORLDOBJECT_DEFAULT_SOUND_EFFECT_RADIUS 0
#define STARWORLDOBJECT_DEFAULT_SIT_ORIENTATION QString()
#define STARWORLDOBJECT_DEFAULT_SUBTITLE QString()
#define STARWORLDOBJECT_DEFAULT_SIT_EMOTE QString()
#define STARWORLDOBJECT_DEFAULT_MAX_LASER_LENGTH 0
#define STARWORLDOBJECT_DEFAULT_RECIPE_GROUP QString()
#define STARWORLDOBJECT_DEFAULT_UNBREAKABLE false
#define STARWORLDOBJECT_DEFAULT_OBJECT_TYPE QString()


typedef QPoint QVector2;
typedef QPointF QVector2F;
typedef QMap< QString, QColor > LightColors;
typedef QMap< QString, QString > AnimationParts;
typedef QVector<int> QRange;
typedef JsonStatusEffect SitStatusEffect;

struct Particle {
    Particle() : fade( 0.0 ),
                initialVelocity( 0.0, 0.0 ),
                destructionTime( -1 ),
                layer( "middle" ),
                image( QString() ),
                type( QString() ),
                timeToLive( 0.0 ),
                size( 0.0 ),
                color( QColor( 0, 0, 0 ) ),
                approach( QVector2( 0, 0 ) ),
                finalVelocity( QVector2F( 0.0, 0.0 ) ),
                light( QColor( 0, 0, 0 ) ),
                destructionAction( QString() ) {}

    double fade;
    QVector2F initialVelocity;
    int destructionTime;
    QString layer;
    QString image;
    QString type;
    double timeToLive;
    double size;
    QColor color;
    QVector2 approach;
    QVector2F finalVelocity;
    QColor light;
    QString destructionAction;
};

struct ParticleVariance {
    ParticleVariance() : position( QPoint( 0, 0 ) ), initialVelocity( QVector2F( 0.0, 0.0 ) ), finalVelocity( QVector2F( 0.0, 0.0 ) ) {}

    QPoint position;
    QVector2F initialVelocity;
    QVector2F finalVelocity;
};

struct ParticleEmitter {
    ParticleEmitter() : emissionRate( 0.0 ), emissionVariance( 0.0 ), pixelOrigin( QPoint( 0, 0 ) ) {}

    double emissionRate;
    double emissionVariance;
    QPoint pixelOrigin;
    Particle particle;
    ParticleVariance particleVariance;
};

struct NpcParameterOptions {
    QList< QMap< QString, QJsonArray > > dropPools; //A changer quand la vraie nature de ça sera connue
};

struct Spawner {
    Spawner() : searchRadius( 0.0 ) {}

    NpcParameterOptions npcParameterOptions;
    QStringList npcTypeOptions;
    double searchRadius;
    QStringList npcSpeciesOptions;
};

/**
 * @brief The BreakDropOptions struct
 * Represents an array and NOT an object, item is the first case, amount the second and foo the third!
 */
struct BreakDropOptions {
    BreakDropOptions() : item( QString() ), amount( 0 ) {}

    QString item;
    int amount;
    QJsonObject foo; // Inutilisé dans le json, il s'agit d'un objet { } vide...
};

typedef BreakDropOptions SmashDropOptions;

struct GrowingStep {
    GrowingStep() : success( 0 ), failure( 0 ) {}

    QRange duration; //!< Interval during the plant CAN grow
    int success;
    int failure;
};

struct Growing {
    GrowingStep step1; //!< AKA "0"
    GrowingStep step2; //!< AKA "1"
};

struct StageAlts {
    StageAlts() : count( 0 ), finalStep( 0 ) {}

    int count;
    int finalStep; //!< Unknown Property
};

struct DropItem {
    DropItem() : name( QString() ), count( 0 ) {}

    QString name;
    int count;
};

struct DropOptions {
    DropOptions() : probability( 0.0 ), command( QString() ) {}
    
    double probability; // [ 0, 1 ]
    QList< QList< DropItem > > itemsPack;

    //Out of "dropOptions object, in "2" property"
    QString command;
};

struct InteractionTransition {
    DropOptions finalStep; //!< Aka "2" property, it's a raw QJsonObject because the internal values are very variables...
    Growing growingStep; //!< Contains 0 and 1 properties
    StageAlts stageAlts;
};

struct TouchDamage {
    TouchDamage() : damageType( QString() ), damageSourceKind( QString() ), damage( 0 ) {}

    QList< JsonStatusEffect > statusEffects;
    QString damageType;
    QString damageSourceKind;
    Polygon poly;
    int damage;
};

struct ImageLayer {
    ImageLayer() : image( QString() ), unlit( false ) {}

    QString image;
    bool unlit;
};

/**
 * @brief The Orientations struct
 */
struct Orientations {
    Orientations() : image( QString() ),
                     dualImage( QString() ),
                     imagePosition( QPoint( 0, 0 ) ),
                     frames( 0 ),
                     animationCycle( 0.0 ),
                     spaceScan( 0.0 ),
                     collision( QString() ),
                     direction( QString() ) {}

    QString image; //!< Image of the orientable object
    QString dualImage; //!< Used in some cases when there is two image in one file (doors for example )
    QList< ImageLayer > imageLayers;
    QPoint imagePosition; //!< Position of the image
    int frames; //!< Number of frames (facultatif)
    double animationCycle;

    QList< QPoint > spaces; //!< ???
    double spaceScan; //!< ???
    QStringList anchors; //!< ???
    QString collision; //!< Type of collision if it's different
    QString direction;
    AnimationParts animationParts;

    QList< QPoint > fgAnchors; //!< Used when the sprite is on the foreground
    QList< QPoint > bgAnchors; //!< Used when the sprite is on the background
};

struct State {
    State() : uiconfig( QString() ), kind( QString() ), slotCount( 0 ) {}

    QString uiconfig;
    QStringList openSounds;
    QStringList closeSounds;
    QString kind;
    int slotCount;
};

struct InteractData {
    InteractData() : config( QString() ) {}

    QString config;
    QStringList filter;
};

struct ProjectileOptions {
    ProjectileOptions() : projectileType( QString() ) {}

    QString projectileType; //!< Reference to the .projectile file
    QJsonObject projectileParams; //!< ??? Unknown
};

enum TruthtableType {
    INVALID, TWO /* 0, 1 */, FOUR /* 00, 01, 10, 11 */
};

struct Truthtable {
    Truthtable() : type( INVALID ) {}

    TruthtableType type;
    QVector< bool > twoTable;
    QVector< QVector< bool > > fourTable;
};

/**
 * @brief The StarWorldObject class
 * This class inherits from StarItem but don't have the same
 * JSON properties (objectName instead of itemName)
 * The save method will take care of that difference
 */
class StarWorldObject : public StarItem
{
public:
    StarWorldObject();
    StarWorldObject( StarWorldObject const &orig );
    ~StarWorldObject();

    virtual QStringList save( QString modPath );

    QList<ParticleEmitter> particleEmitters() const;
    void setParticleEmitters(const QList<ParticleEmitter> &particleEmitters);

    int sitAngle() const;
    void setSitAngle(int sitAngle);

    QColor lightColor() const;
    void setLightColor(const QColor &lightColor);

    LightColors lightColors() const;
    void setLightColors(const LightColors &lightColors);

    AnimationParts animationParts() const;
    void setAnimationParts(const AnimationParts &animationParts);

    int scriptDelta() const;
    void setScriptDelta(int scriptDelta);

    Spawner spawner() const;
    void setSpawner(Spawner spawner);

    SitStatusEffect statusEffect() const;
    void setStatusEffect(SitStatusEffect statusEffect);

    QList<QList<BreakDropOptions> > breakDropOptions() const;
    void setBreakDropOptions(const QList<QList<BreakDropOptions> > &breakDropOptions);

    InteractionTransition interactionTransition() const;
    void setInteractionTransition(InteractionTransition interactionTransition);

    QString race() const;
    void setRace(const QString &race);

    QStringList scripts() const;
    void setScripts(const QStringList &scripts);

    QPoint outboundNodesLocation() const;
    void setOutboundNodesLocation(const QPoint &outboundNodesLocation);

    TouchDamage touchDamage() const;
    void setTouchDamage(TouchDamage touchDamage);

    QStringList closeSounds() const;
    void setCloseSounds(const QStringList &closeSounds);

    Truthtable truthtable() const;
    void setTruthtable(const Truthtable &truthtable);

    QStringList openSounds() const;
    void setOpenSounds(const QStringList &openSounds);

    QString sitCoverImage() const;
    void setSitCoverImage(const QString &sitCoverImage);

    QStringList offSounds() const;
    void setOffSounds(const QStringList &offSounds);

    bool hasWindowIcon() const;
    void setHasWindowIcon(bool hasWindowIcon);

    QStringList smashSounds() const;
    void setSmashSounds(const QStringList &smashSounds);

    QRange rotationRange() const;
    void setRotationRange(const QRange &rotationRange);

    int frameCooldown() const;
    void setFrameCooldown(int frameCooldown);

    QString animation() const;
    void setAnimation(const QString &animation);

    int detectTickDuration() const;
    void setDetectTickDuration(int detectTickDuration);

    int autocloseCooldown() const;
    void setAutocloseCooldown(int autocloseCooldown);

    double health() const;
    void setHealth(double health);

    QString script() const;
    void setScript(const QString &script);

    QString category() const;
    void setCategory(const QString &category);

    double pointBeam() const;
    void setPointBeam(double pointBeam);

    int price() const;
    void setPrice(int price);

    QVector< QPoint > inboundNodes() const;
    void setInboundNodes(const QVector< QPoint > &inboundNodes);

    double flickerDistance() const;
    void setFlickerDistance(double flickerDistance);

    QString uiConfig() const;
    void setUiConfig(const QString &uiConfig);

    double fireCooldown() const;
    void setFireCooldown(double fireCooldown);

    QList<Orientations> orientations() const;
    void setOrientations(const QList<Orientations> &orientations);

    QStringList sitEffectEmitters() const;
    void setSitEffectEmitters(const QStringList &sitEffectEmitters);

    bool hydrophobic() const;
    void setHydrophobic(bool hydrophobic);

    QPoint animationPosition() const;
    void setAnimationPosition(const QPoint &animationPosition);

    int interval() const;
    void setInterval(int interval);

    QList<Orientations> engineOnOrientations() const;
    void setEngineOnOrientations(const QList<Orientations> &engineOnOrientations);

    State state() const;
    void setState(State state);

    QPointF tipOffset() const;
    void setTipOffset(const QPointF &tipOffset);

    QVector< QPoint > outboundNodes() const;
    void setOutboundNodes(const QVector< QPoint > &outboundNodes);

    int targetHoldTime() const;
    void setTargetHoldTime(int targetHoldTime);

    QPoint sitPosition() const;
    void setSitPosition(const QPoint &sitPosition);

    QStringList onSounds() const;
    void setOnSounds(const QStringList &onSounds);

    int slotCount() const;
    void setSlotCount(int slotCount);

    bool printable() const;
    void setPrintable(bool printable);

    QString genericDescription() const;
    void setGenericDescription(const QString &genericDescription);

    bool unlit() const;
    void setUnlit(bool unlit);

    QVector<double> fireOffsets() const;
    void setFireOffsets(const QVector<double> &fireOffsets);

    QString objectItem() const;
    void setObjectItem(const QString &objectItem);

    double rotationPauseTime() const;
    void setRotationPauseTime(double rotationPauseTime);

    QString interactAction() const;
    void setInteractAction(const QString &interactAction);

    QStringList sounds() const;
    void setSounds(const QStringList &sounds);

    bool sitFlipDirection() const;
    void setSitFlipDirection(bool sitFlipDirection);

    QString soundEffect() const;
    void setSoundEffect(const QString &soundEffect);

    int detectRadius() const;
    void setDetectRadius(int detectRadius);

    StageAlts stageAlts() const;
    void setStageAlts(StageAlts stageAlts);

    double rotationTime() const;
    void setRotationTime(double rotationTime);

    double flickerTiming() const;
    void setFlickerTiming(double flickerTiming);

    QStringList useSounds() const;
    void setUseSounds(const QStringList &useSounds);

    Growing growing() const;
    void setGrowing(Growing growing);

    double flickerStrength() const;
    void setFlickerStrength(double flickerStrength);

    int gates() const;
    void setGates(int gates);

    bool pointLight() const;
    void setPointLight(bool pointLight);

    ProjectileOptions projectileOptions() const;
    void setProjectileOptions(ProjectileOptions projectileOptions);

    QPointF baseOffset() const;
    void setBaseOffset(const QPointF &baseOffset);

    QString damageTime() const;
    void setDamageTime(const QString &damageTime);

    QList<JsonStatusEffect> statusEffects() const;
    void setStatusEffects(const QList<JsonStatusEffect> &statusEffects);

    InteractData interactData() const;
    void setInteractData(InteractData interactData);

    int soundEffectRadius() const;
    void setSoundEffectRadius(int soundEffectRadius);

    QString sitOrientation() const;
    void setSitOrientation(const QString &sitOrientation);

    QString subtitle() const;
    void setSubtitle(const QString &subtitle);

    QString sitEmote() const;
    void setSitEmote(const QString &sitEmote);

    int maxLaserLength() const;
    void setMaxLaserLength(int maxLaserLength);

    QList<QList<SmashDropOptions> > smashDropOptions() const;
    void setSmashDropOptions(const QList<QList<SmashDropOptions> > &smashDropOptions);

    QString recipeGroup() const;
    void setRecipeGroup(const QString &recipeGroup);

    bool unbreakable() const;
    void setUnbreakable(bool unbreakable);

    QString objectType() const;
    void setObjectType(const QString &objectType);

    bool hasAnimation() const;
    void setHasAnimation(bool hasAnimation);

    bool isSittable() const;
    void setSittable(bool sittable);

    static QJsonObject growingStepToObject( GrowingStep &step );
    static GrowingStep objectToGrowingStep( QJsonObject obj );
    static QJsonObject animationPartsToObject( AnimationParts &animationParts );
    static QJsonObject orientationsToObject( Orientations &ori );
    static Orientations objectToOrientations( QJsonObject obj );

protected:
    virtual ItemMap serialize();
protected:
    QList<ParticleEmitter> m_particleEmitters; //!< Used when there is more than one particle emitter
    int m_sitAngle;
    QColor m_lightColor;
    LightColors m_lightColors; //!< Used if there is more than one lightColor
    AnimationParts m_animationParts;
    int m_scriptDelta;
    Spawner m_spawner;
    SitStatusEffect m_statusEffect;
    QList< QList < BreakDropOptions > > m_breakDropOptions; //!< [ Propably ] Items dropped when the object is broken
    QList< QList< SmashDropOptions > > m_smashDropOptions;
    InteractionTransition m_interactionTransition; //!< Represents the transition of the plants and what they will drop when they are mature
    QString m_race; //!< If empty, available for all races
    QStringList m_scripts; //!< Used if there is more than one script
    QString m_script; //!< Path to the script
    QPoint m_outboundNodesLocation; //!< ???
    TouchDamage m_touchDamage; //!< Damages the living entities recieve if they touch the object
    QStringList m_closeSounds; //!< Sounds emitted when the player is close from the object
    Truthtable m_truthtable; //!< Truthtable of the object (used for wiring)
    QStringList m_openSounds;
    QString m_sitCoverImage; //!< Image used when the object have a cover and the character is sitting on it (eg: a bed)
    QStringList m_offSounds; //!< [ Probably ] Sounds used when the object is turned off
    bool m_hasWindowIcon; //!< ???
    QStringList m_smashSounds;
    QRange m_rotationRange; //!< Used to specify a range for the rotation of an object (eg: Security Camera )
    int m_frameCooldown; //!< ???
    bool m_hasAnimation; //!< Used to programmatically detect if there is an animation
    QString m_animation; //!< Link to the animation file
    QPoint m_animationPosition;
    int m_detectTickDuration; //!< [ Probably ] delay before the object detect something (eg: motion sensor )
    int m_autocloseCooldown; //!< Delay before auto closing (for the chests for example )
    double m_health; //!< Health of the object before it breaks;
    QString m_category;
    double m_pointBeam;
    int m_price;
    QVector< QPoint > m_inboundNodes;
    double m_flickerDistance;
    QString m_uiConfig; //!< Config path used to display a UI
    double m_fireCooldown;
    QList< Orientations > m_orientations;
    QStringList m_sitEffectEmitters;
    bool m_hydrophobic;
    int m_interval; //!< Interval where the related script is called
    QList< Orientations > m_engineOnOrientations;
    State m_state;
    QPointF m_tipOffset;
    QVector< QPoint > m_outboundNodes;
    int m_targetHoldTime;
    bool m_isSittable; //!< Used to programmatically determine if the object can be sitted
    QPoint m_sitPosition;
    QStringList m_onSounds;
    int m_slotCount;
    bool m_printable;
    QString m_genericDescription;
    bool m_unlit;
    QVector< double > m_fireOffsets;
    QString m_objectItem; //!< Redondance possible
    double m_rotationPauseTime;
    QString m_interactAction; //!< Name of the action (script maybe) used when there is an action
    QStringList m_sounds;
    bool m_sitFlipDirection;
    QString m_soundEffect;
    int m_detectRadius;
    StageAlts m_stageAlts; //!< Qu'est ce que ça fout ici?
    double m_rotationTime;
    double m_flickerTiming;
    QStringList m_useSounds;
    Growing m_growing; //!< Même chose que stageAlts!
    double m_flickerStrength;
    int m_gates;
    bool m_pointLight;
    ProjectileOptions m_projectileOptions;
    QPointF m_baseOffset;
    /**
     * @brief m_damageTime
     * It's an object!
     * "damageTeam": {
     *  "type": "stuff"
     * }
     */
    QString m_damageTime;
    QList< JsonStatusEffect > m_statusEffects;
    InteractData m_interactData;
    int m_soundEffectRadius;
    QString m_sitOrientation;
    QString m_subtitle;
    QString m_sitEmote;
    int m_maxLaserLength;
    QString m_recipeGroup;
    bool m_unbreakable;
    QString m_objectType;
};

typedef QSharedPointer< StarWorldObject > StarWorldObjectPtr;

Q_DECLARE_METATYPE( StarWorldObject )
Q_DECLARE_METATYPE( QSharedPointer< StarWorldObject > )

#endif // STARWORLDOBJECT_H
