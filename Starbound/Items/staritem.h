#ifndef STARITEM_H
#define STARITEM_H

#include <QMetaType>
#include <QDebug>
#include <QString>
#include <QVector>
#include <QStringList>
#include <QFile>
#include <QColor>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QSet>
#include <QCursor>

#include "../StarTypes.h"
#include "../starrecipe.h"
#include "../statuseffect.h"
#include "../../Core/utility.h"

#define STARITEM_DEFAULT_ITEM_NAME QString( "" )
#define STARITEM_DEFAULT_SHORT_DESCRIPTION QString( "" )
#define STARITEM_DEFAULT_DESCRIPTION QString( "" )
#define STARITEM_DEFAULT_RARITY QString( "Common" )
#define STARITEM_DEFAULT_INVENTORY_ICON QString( "" )
#define STARITEM_DEFAULT_MAX_STACK -1
#define STARITEM_DEFAULT_FUEL_AMOUNT 0
#define STARITEM_DEFAULT_AVIAN_DESCRIPTION QString( "" )
#define STARITEM_DEFAULT_APEX_DESCRIPTION QString( "" )
#define STARITEM_DEFAULT_FLORAN_DESCRIPTION QString( "" )
#define STARITEM_DEFAULT_GLITCH_DESCRIPTION QString( "" )
#define STARITEM_DEFAULT_HUMAN_DESCRIPTION QString( "" )
#define STARITEM_DEFAULT_HYLOTL_DESCRIPTION QString( "" )

class StarItem
{
public:
    StarItem();
    StarItem( StarItem const &orig );
    virtual ~StarItem();
    /**
     * @brief This method saves the item into the current mod path
     * @param modPath Path of the mod
     * @return List of wrote files
     */
    virtual QStringList save( QString modPath, QString itemPath, QJsonDocument &jsonFile );

    //virtual bool load( QJsonObject *rootObject );


    QString itemName() const;
    void setItemName(const QString &itemName);

    QString shortDescription() const;
    void setShortDescription(const QString &shortDescription);

    QString description() const;
    void setDescription(const QString &description);

    QString rarity() const;
    void setRarity(const QString &rarity);

    QString inventoryIcon() const;
    void setInventoryIcon(const QString &inventoryIcon);

    int maxStack() const;
    void setMaxStack(int maxStack);

    int fuelAmount() const;
    void setFuelAmount(int fuelAmount);

    QString avianDescription() const;
    void setAvianDescription(const QString &avianDescription);

    QString apexDescription() const;
    void setApexDescription(const QString &apexDescription);

    QString floranDescription() const;
    void setFloranDescription(const QString &floranDescription);

    QString glitchDescription() const;
    void setGlitchDescription(const QString &glitchDescription);

    QString humanDescription() const;
    void setHumanDescription(const QString &humanDescription);

    QString hylotlDescription() const;
    void setHylotlDescription(const QString &hylotlDescription);

    QStringList learnBlueprintsOnPickup() const;
    void setLearnBlueprintsOnPickup( QStringList &blueprints );
    void addLearnBlueprintOnPickup( QString blueprint );
    void clearBlueprintsList();

    QList< QList< JsonStatusEffect > > effects();
    void setEffects( QList< JsonStatusEffect > effects );

    virtual QString dump();

    QString itemLocation() const;
    void setItemLocation(const QString &itemLocation);

    QString filesType() const;
    void setFilesType(const QString &filesType);

    QString idObject() const;

    bool hasProperty( QString property );


    static QJsonArray qPointToJsonArray( QPoint point );
    static QJsonArray qPointToJsonArray( QPointF point );
    static QJsonArray qColorToJsonArray( QColor color );
    static QColor qJsonArrayToQColor( QJsonArray arr );
    static QJsonArray qStringListToJsonArray( QStringList list );
    static QStringList qJsonArrayToQStringList( QJsonArray arr );
    static QPoint qJsonArrayToQPoint( QJsonArray arr );
    static QPointF qJsonArrayToQPointF( QJsonArray arr );

    StarRecipe recipe() const;
    void setRecipe(const StarRecipe &recipe);

protected:
    virtual ItemMap serialize();
protected:
    QString m_itemName; ///!< Internal name of the item (must be without spaces)
    QString m_shortDescription; ///!< Name of the item in the game
    QString m_description; ///!< General description of the item
    QString m_rarity; ///!< Rarity of the item (Common, uncommon, Rare or Legendary)
    QString m_inventoryIcon; ///!< Path to the inventory icon of the item
    int m_maxStack; ///!< Maximum number of items in one stack
    int m_fuelAmount; ///!< Amount of fuel that the item can provide to the ship

    QString m_avianDescription; ///!< Description of the item by an Avian
    QString m_apexDescription; ///!< Description of the item by an Apex
    QString m_floranDescription; ///!< Description of the item by a Floran
    QString m_glitchDescription; ///!< Description of the item by a Glith
    QString m_humanDescription; ///!< Description of the item by a Human
    QString m_hylotlDescription; ///!< Description of the item by a Hylotl

    QStringList m_learnBlueprintsOnPickup; ///!< Blueprints learnt after the first pickup of this item
    QList<QList< JsonStatusEffect > > m_effects; ///!< List of effects the item can provide

    StarRecipe m_recipe;

    QString m_itemLocation; ///!< Item location in the hard drive
    QString m_filesType; ///!< List of files in Starbound assets this class concerns
    QString m_idObject; ///!< Used to recognize the item type

    QSet<QString> m_properties; //!< Hold all existing properties
};

QDebug operator <<( QDebug dbg, StarItem &item );
QDebug operator <<( QDebug dbg, StarItem *item );

typedef QSharedPointer< StarItem > StarItemPtr;

Q_DECLARE_METATYPE( StarItem )
Q_DECLARE_METATYPE( QSharedPointer< StarItem > )

#endif // STARITEM_H
