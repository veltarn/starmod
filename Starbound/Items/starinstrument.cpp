#include "starinstrument.h"

StarInstrument::StarInstrument() : StarEquipableItem(),
    m_activeHandPosition( STARINSTRUMENT_DEFAULT_ACTIVE_HAND_POSITION ),
    m_largeImage( STARINSTRUMENT_DEFAULT_LARGE_IMAGE ),
    m_image( STARINSTRUMENT_DEFAULT_IMAGE ),
    m_activeImage( STARINSTRUMENT_DEFAULT_ACTIVE_IMAGE ),
    m_inspectionKind( STARINSTRUMENT_DEFAULT_INSPECTION_KIND ),
    m_kind( STARINSTRUMENT_DEFAULT_KIND ),
    m_activeAngle( STARINSTRUMENT_DEFAULT_ACTIVE_ANGLE )
{
    m_filesType = "instrument";
    m_idObject = "instrument";
}

StarInstrument::StarInstrument(const StarInstrument &orig) : StarEquipableItem( orig )
{
    m_activeHandPosition =      orig.m_activeHandPosition;
    m_largeImage =              orig.m_largeImage;
    m_image =                   orig.m_image;
    m_activeImage =             orig.m_activeImage;
    m_inspectionKind =          orig.m_inspectionKind;
    m_kind =                    orig.m_kind;
    m_activeAngle =             orig.m_activeAngle;
}

StarInstrument::~StarInstrument()
{

}

QStringList StarInstrument::save(QString modPath)
{
    return QStringList();
}

ItemMap StarInstrument::serialize()
{
    ItemMap map = StarEquipableItem::serialize();

    QJsonArray activeHandPos;
    activeHandPos.append( m_activeHandPosition.x() );
    activeHandPos.append( m_activeHandPosition.y() );

    map[ "activeHandPosition" ] = activeHandPos;
    map[ "largeImage" ] = m_largeImage;
    map[ "image" ] = m_image;
    map[ "activeImage" ] = m_activeImage;
    map[ "inspectionKind" ] = m_inspectionKind;
    map[ "kind" ] = m_kind;
    map[ "activeAngle" ] = m_activeAngle;

    return map;
}
int StarInstrument::activeAngle() const
{
    return m_activeAngle;
}

void StarInstrument::setActiveAngle(int activeAngle)
{
    m_activeAngle = activeAngle;
}

QString StarInstrument::kind() const
{
    return m_kind;
}

void StarInstrument::setKind(const QString &kind)
{
    m_kind = kind;
}

QString StarInstrument::inspectionKind() const
{
    return m_inspectionKind;
}

void StarInstrument::setInspectionKind(const QString &inspectionKind)
{
    m_inspectionKind = inspectionKind;
}

QString StarInstrument::activeImage() const
{
    return m_activeImage;
}

void StarInstrument::setActiveImage(const QString &activeImage)
{
    m_activeImage = activeImage;
}

QString StarInstrument::image() const
{
    return m_image;
}

void StarInstrument::setImage(const QString &image)
{
    m_image = image;
}

QString StarInstrument::largeImage() const
{
    return m_largeImage;
}

void StarInstrument::setLargeImage(const QString &largeImage)
{
    m_largeImage = largeImage;
}

QPointF StarInstrument::activeHandPosition() const
{
    return m_activeHandPosition;
}

void StarInstrument::setActiveHandPosition(const QPointF &activeHandPosition)
{
    m_activeHandPosition = activeHandPosition;
}

