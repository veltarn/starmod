#include "starconsumable.h"

StarConsumable::StarConsumable() :
    StarHandedItem(),
    m_emote( STARCONSUMABLE_DEFAULT_EMOTE )
{
    m_filesType = "consumable";
    m_idObject = "consumable";
}

StarConsumable::StarConsumable( StarConsumable const& orig ) : StarHandedItem( orig ) {
    m_emote =                   orig.m_emote;
    m_handPosition =            orig.m_handPosition;
}

StarConsumable::~StarConsumable() {

}

QStringList StarConsumable::save( QString modPath ) {
    return QStringList();
}

QString StarConsumable::emote() const
{
    return m_emote;
}

void StarConsumable::setEmote(const QString &emote)
{
    m_emote = emote;
}

ItemMap StarConsumable::serialize() {
    ItemMap map = StarItem::serialize();

    map[ "emote" ] = m_emote;

    return map;
}
