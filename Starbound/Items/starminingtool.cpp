#include "starminingtool.h"

StarMiningTool::StarMiningTool() : StarEquipableItem(),
    m_pointable( STARTMININGTOOL_DEFAULT_POINTABLE ),
    m_strikeSound( STARTMININGTOOL_DEFAULT_STRIKE_SOUND ),
    m_idleSound( STARTMININGTOOL_DEFAULT_IDLE_SOUND ),
    m_swingStart( STARTMININGTOOL_DEFAULT_SWING_START ),
    m_swingFinish( STARTMININGTOOL_DEFAULT_SWING_FINISH ),
    m_largeImage( STARTMININGTOOL_DEFAULT_LARGE_IMAGE ),
    m_image( STARTMININGTOOL_DEFAULT_IMAGE ),
    m_inspectionKind( STARTMININGTOOL_DEFAULT_INSPECTION_KIND ),
    m_tileDamage( STARTMININGTOOL_DEFAULT_TILE_DAMAGE ),
    m_tileDamageBlunted( STARTMININGTOOL_DEFAULT_TILE_DAMAGE_BLUNTED ),
    m_animationCycle( STARTMININGTOOL_DEFAULT_ANIMATION_CYCLE ),
    m_durability( STARTMININGTOOL_DEFAULT_DURABILITY ),
    m_durabilityPerUse( STARTMININGTOOL_DEFAULT_DURABILITY_PER_USE ),
    m_blockRadius( STARTMININGTOOL_DEFAULT_BLOCK_RADIUS ),
    m_altBlockRadius( STARTMININGTOOL_DEFAULT_BLOCK_ALT_RADIUS ),
    m_fireTime( STARTMININGTOOL_DEFAULT_FIRE_TIME ),
    m_frames( STARTMININGTOOL_DEFAULT_FRAMES )
{
    m_filesType = "miningtool";
    m_idObject = "miningtool";
}

StarMiningTool::StarMiningTool(const StarMiningTool &orig) : StarEquipableItem( orig ) {
    m_fireTime =                orig.m_fireTime;

    m_pointable =               orig.m_pointable;
    m_strikeSound =             orig.m_strikeSound;
    m_idleSound =               orig.m_idleSound;
    m_swingStart =              orig.m_swingStart;
    m_swingFinish =             orig.m_swingFinish;
    m_largeImage =              orig.m_largeImage;
    m_image =                   orig.m_image;
    m_inspectionKind =          orig.m_inspectionKind;
    m_tileDamage =              orig.m_tileDamage;
    m_tileDamageBlunted =       orig.m_tileDamageBlunted;
    m_animationCycle =          orig.m_animationCycle;
    m_durabilityRegenChart =    orig.m_durabilityRegenChart;
    m_durability =              orig.m_durability;
    m_durabilityPerUse =        orig.m_durabilityPerUse;
    m_blockRadius =             orig.m_blockRadius;
    m_altBlockRadius =          orig.m_altBlockRadius;
    m_frames =                  orig.m_frames;
}

StarMiningTool::~StarMiningTool() {

}

QStringList StarMiningTool::save(QString modPath)
{
    return QStringList();
}

QMap< QString, QJsonValue> StarMiningTool::serialize() {
    QMap< QString, QJsonValue> map = StarEquipableItem::serialize();


    map[ "pointable" ] = m_pointable;
    map[ "strikeSound" ] = m_strikeSound;
    map[ "idleSound" ] = m_idleSound;
    map[ "swingStart" ] = m_swingStart;
    map[ "swingFinish" ] = m_swingFinish;
    map[ "largeImage" ] = m_largeImage;
    map[ "image" ] = m_image;
    map[ "inspectionKind" ] = m_inspectionKind;
    map[ "tileDamage" ] = m_tileDamage;
    map[ "tileDamageBlunted" ] = m_tileDamageBlunted;
    map[ "animationCycle" ] = m_animationCycle;

    QJsonArray durabilityRegenChart;

    for( int i = 0; i < m_durabilityRegenChart.size(); i++ ) {
        QJsonArray regenItems;
        QJsonArray durElem;
        foreach( QString item, m_durabilityRegenChart[i].regenItems ) {
            /*
             * Putting every regenItems into the array
             * We affect several items for one regen item value
             */
            regenItems.append( item );
        }
        //Putting regenItems and amount into durElem array...
        durElem.append( regenItems );
        durElem.append( m_durabilityRegenChart[i].regenAmount );

        //And appending durabilityRegenChart array with the new element
        durabilityRegenChart.append( durElem );
    }

    map[ "durability" ] = m_durability;
    map[ "durabilityPerUse" ] = m_durabilityPerUse;
    map[ "blockRadius" ] = m_blockRadius;
    map[ "altBlockRadius" ] = m_altBlockRadius;
    map[ "fireTime" ] = m_fireTime;

    if( m_frames > 0 ) {
        map[ "frames" ] = m_frames;
    }

    return map;
}

int StarMiningTool::frames() const
{
    return m_frames;
}

void StarMiningTool::setFrames(int frames)
{
    m_frames = frames;
}

double StarMiningTool::fireTime() const
{
    return m_fireTime;
}

void StarMiningTool::setFireTime(double fireTime)
{
    m_fireTime = fireTime;
}

int StarMiningTool::durabilityPerUse() const
{
    return m_durabilityPerUse;
}

void StarMiningTool::setDurabilityPerUse(int durabilityPerUse)
{
    m_durabilityPerUse = durabilityPerUse;
}

int StarMiningTool::durability() const
{
    return m_durability;
}

void StarMiningTool::setDurability(int durability)
{
    m_durability = durability;
}

int StarMiningTool::altBlockRadius() const
{
    return m_altBlockRadius;
}

void StarMiningTool::setAltBlockRadius(int altBlockRadius)
{
    m_altBlockRadius = altBlockRadius;
}

int StarMiningTool::blockRadius() const
{
    return m_blockRadius;
}

void StarMiningTool::setBlockRadius(int blockRadius)
{
    m_blockRadius = blockRadius;
}

QVector<DurabilityRegen> StarMiningTool::durabilityRegenChart() const
{
    return m_durabilityRegenChart;
}

void StarMiningTool::setDurabilityRegenChart(const QVector<DurabilityRegen> &durabilityRegenChart)
{
    m_durabilityRegenChart = durabilityRegenChart;
}

double StarMiningTool::animationCycle() const
{
    return m_animationCycle;
}

void StarMiningTool::setAnimationCycle(double animationCycle)
{
    m_animationCycle = animationCycle;
}

double StarMiningTool::tileDamageBlunted() const
{
    return m_tileDamageBlunted;
}

void StarMiningTool::setTileDamageBlunted(double tileDamageBlunted)
{
    m_tileDamageBlunted = tileDamageBlunted;
}

double StarMiningTool::tileDamage() const
{
    return m_tileDamage;
}

void StarMiningTool::setTileDamage(double tileDamage)
{
    m_tileDamage = tileDamage;
}

QString StarMiningTool::inspectionKind() const
{
    return m_inspectionKind;
}

void StarMiningTool::setInspectionKind(const QString &inspectionKind)
{
    m_inspectionKind = inspectionKind;
}

QString StarMiningTool::image() const
{
    return m_image;
}

void StarMiningTool::setImage(const QString &image)
{
    m_image = image;
}

QString StarMiningTool::largeImage() const
{
    return m_largeImage;
}

void StarMiningTool::setLargeImage(const QString &largeImage)
{
    m_largeImage = largeImage;
}

int StarMiningTool::swingFinish() const
{
    return m_swingFinish;
}

void StarMiningTool::setSwingFinish(int swingFinish)
{
    m_swingFinish = swingFinish;
}

int StarMiningTool::swingStart() const
{
    return m_swingStart;
}

void StarMiningTool::setSwingStart(int swingStart)
{
    m_swingStart = swingStart;
}

QString StarMiningTool::idleSound() const
{
    return m_idleSound;
}

void StarMiningTool::setIdleSound(const QString &idleSound)
{
    m_idleSound = idleSound;
}

QString StarMiningTool::strikeSound() const
{
    return m_strikeSound;
}

void StarMiningTool::setStrikeSound(const QString &strikeSound)
{
    m_strikeSound = strikeSound;
}

bool StarMiningTool::pointable() const
{
    return m_pointable;
}

void StarMiningTool::setPointable(bool pointable)
{
    m_pointable = pointable;
}

