#include "staritem.h"

StarItem::StarItem() :
    m_itemName( STARITEM_DEFAULT_ITEM_NAME ),
    m_shortDescription( STARITEM_DEFAULT_SHORT_DESCRIPTION ),
    m_description( STARITEM_DEFAULT_DESCRIPTION ),
    m_rarity( STARITEM_DEFAULT_RARITY ),
    m_inventoryIcon( STARITEM_DEFAULT_INVENTORY_ICON ),
    m_maxStack( STARITEM_DEFAULT_MAX_STACK ), //-1 == Default value: No Max Stack in Json
    m_fuelAmount( STARITEM_DEFAULT_FUEL_AMOUNT ),
    m_avianDescription( STARITEM_DEFAULT_AVIAN_DESCRIPTION ),
    m_apexDescription( STARITEM_DEFAULT_APEX_DESCRIPTION ),
    m_floranDescription( STARITEM_DEFAULT_FLORAN_DESCRIPTION ),
    m_glitchDescription( STARITEM_DEFAULT_GLITCH_DESCRIPTION ),
    m_humanDescription( STARITEM_DEFAULT_HUMAN_DESCRIPTION ),
    m_hylotlDescription( STARITEM_DEFAULT_HYLOTL_DESCRIPTION ),
    m_itemLocation( "" ),
    m_filesType( "item" ),
    m_idObject( "item" )
{
    m_properties.insert( m_rarity );
}


StarItem::StarItem( StarItem const &orig ) {
    m_itemName =                orig.m_itemName;
    m_shortDescription =        orig.m_shortDescription;
    m_description =             orig.m_description;
    m_rarity =                  orig.m_rarity;
    m_inventoryIcon =           orig.m_inventoryIcon;
    m_maxStack =                orig.m_maxStack;
    m_fuelAmount =              orig.m_fuelAmount;
    m_avianDescription =        orig.m_avianDescription;
    m_apexDescription =         orig.m_apexDescription;
    m_floranDescription =       orig.m_floranDescription;
    m_glitchDescription =       orig.m_glitchDescription;
    m_humanDescription =        orig.m_humanDescription;
    m_hylotlDescription =       orig.m_hylotlDescription;
    m_learnBlueprintsOnPickup = orig.m_learnBlueprintsOnPickup;
    m_effects =                 orig.m_effects;
    m_itemLocation =            orig.m_itemLocation;
    m_filesType =               orig.m_filesType;
    m_idObject =                orig.m_idObject;
    m_properties =              orig.m_properties;
}

StarItem::~StarItem() {
    
}

QString StarItem::hylotlDescription() const
{
    return m_hylotlDescription;
}

void StarItem::setHylotlDescription(const QString &hylotlDescription)
{
    m_properties.insert( "hylotlDescription" );
    m_hylotlDescription = hylotlDescription;
}

QString StarItem::humanDescription() const
{
    return m_humanDescription;
}

void StarItem::setHumanDescription(const QString &humanDescription)
{
    m_properties.insert( "humanDescription" );
    m_humanDescription = humanDescription;
}



QString StarItem::glitchDescription() const
{
    return m_glitchDescription;
}

void StarItem::setGlitchDescription(const QString &glitchDescription)
{
    m_properties.insert( "glitchDescription" );
    m_glitchDescription = glitchDescription;
}

QString StarItem::floranDescription() const
{
    return m_floranDescription;
}

void StarItem::setFloranDescription(const QString &floranDescription)
{
    m_properties.insert( "floranDescription" );
    m_floranDescription = floranDescription;
}

QString StarItem::apexDescription() const
{
    return m_apexDescription;
}

void StarItem::setApexDescription(const QString &apexDescription)
{
    m_properties.insert( "apexDescription" );
    m_apexDescription = apexDescription;
}

QString StarItem::avianDescription() const
{
    return m_avianDescription;
}

void StarItem::setAvianDescription(const QString &avianDescription)
{
    m_properties.insert( "avianDescription" );
    m_avianDescription = avianDescription;
}

int StarItem::fuelAmount() const
{
    return m_fuelAmount;
}

void StarItem::setFuelAmount(int fuelAmount)
{
    m_properties.insert( "fuelAmount" );
    m_fuelAmount = fuelAmount;
}

int StarItem::maxStack() const
{
    return m_maxStack;
}

void StarItem::setMaxStack(int maxStack)
{
    m_properties.insert( "maxStack" );
    m_maxStack = maxStack;
}

QString StarItem::inventoryIcon() const
{
    return m_inventoryIcon;
}

void StarItem::setInventoryIcon(const QString &inventoryIcon)
{
    m_properties.insert( "inventoryIcon" );
    m_inventoryIcon = inventoryIcon;
}

QString StarItem::rarity() const
{
    return m_rarity;
}

void StarItem::setRarity(const QString &rarity)
{
    m_properties.insert( "rarity" );
    m_rarity = rarity;
}

QString StarItem::description() const
{
    return m_description;
}

void StarItem::setDescription(const QString &description)
{
    m_properties.insert( "description" );
    m_description = description;
}

QString StarItem::shortDescription() const
{
    return m_shortDescription;
}

void StarItem::setShortDescription(const QString &shortDescription)
{
    m_properties.insert( "shortDescription" );
    m_shortDescription = shortDescription;
}

QString StarItem::itemName() const
{
    return m_itemName;
}

void StarItem::setItemName(const QString &itemName)
{
    m_properties.insert( "itemName" );
    m_itemName = itemName;
}

QStringList StarItem::learnBlueprintsOnPickup() const {
    return m_learnBlueprintsOnPickup;
}

void StarItem::setLearnBlueprintsOnPickup(QStringList &blueprints)
{
    m_properties.insert( "learnBlueprintsOnPickup" );
    m_learnBlueprintsOnPickup = blueprints;
}

void StarItem::addLearnBlueprintOnPickup( QString blueprint ) {
    if( !Utility::exists( blueprint, m_learnBlueprintsOnPickup ) ) {
        m_properties.insert( "learnBlueprintsOnPickup" );
        m_learnBlueprintsOnPickup.append( blueprint );
    }
}

void StarItem::clearBlueprintsList() {
    m_learnBlueprintsOnPickup.clear();
}

QList<QList<JsonStatusEffect> > StarItem::effects()
{
    return m_effects;
}

void StarItem::setEffects(QList<JsonStatusEffect> effects)
{
    m_effects.clear();
    m_effects.append( effects );
}

QStringList StarItem::save(QString modPath, QString itemPath, QJsonDocument &jsonFile) {
    QStringList files;/*
    QString itemPath = "items/items/" + m_itemName;

    QDir modDirectory( modPath );
    if( !modDirectory.mkpath( itemPath ) ) {
        qDebug() << "Cannot create item directory" << endl;
        return files;
    }*/
    
    //Copying inventory icon
    QFileInfo inventoryIcon( m_inventoryIcon );
    //Building icon path
    QString iconPath = m_itemName + "Icon." + inventoryIcon.suffix();
    QString absoluteIconPath = modPath + "/" + itemPath + "/" + iconPath;

    if( QFile::exists( absoluteIconPath ) )
        QFile::remove( absoluteIconPath );
    QFile::copy( m_inventoryIcon, absoluteIconPath );
    m_inventoryIcon = iconPath; //Replacing the current inventory icon ( by the relative path in the item folder)

    //Writing recipe file
    if( hasProperty( "recipe" ) ) {
        QJsonDocument recipeJson;
        QString recipePath = "recipes/" + m_idObject + "s";
        QString recipeFullFile = recipePath + "/" + m_itemName + ".recipe";
        QDir modDirectory( modPath );

        if( !modDirectory.mkpath( recipePath ) ) {
            qDebug() << "Cannot create recipe folder" << endl;
        }

        QFile recipeFile( modPath + "/" + recipeFullFile );

        if( recipeFile.exists() )
            recipeFile.remove();

        if( !recipeFile.open( QIODevice::WriteOnly ) ) {
            qDebug() << "Cannot open" << recipeFile.fileName() << "in writing mode, " << recipeFile.errorString();
        } else {
            m_recipe.save( modPath, recipeJson );
            QTextStream stream( &recipeFile );
            stream.setCodec( "UTF-8" );
            stream << recipeJson.toJson();

            recipeFile.flush();
            recipeFile.close();
            files.append( recipeFile.fileName() );
        }
    }

    //Serialize and writing
    QMap<QString, QJsonValue> serialized = serialize();

    //QJsonDocument jsonFile;
    QJsonObject root = jsonFile.object();

    for( QMap<QString, QJsonValue>::iterator it = serialized.begin(); it != serialized.end(); ++it ) {
        qDebug() << "[" << it.key() << "]: " << it.value().toVariant() << endl;
        root.insert( it.key(), it.value() );
    }

    jsonFile.setObject( root );

  /*  QTextStream stream( &itemFile );
    stream.setCodec( "UTF-8" );
    stream << jsonFile.toJson();

    itemFile.flush();
    itemFile.close();
*/
    //files.append( itemFile.fileName() );
    files.append( absoluteIconPath );
    
    return files;
}

ItemMap StarItem::serialize() {
    ItemMap map;
    
    map[ "itemName" ] = m_itemName;
    map[ "shortdescription" ] = m_shortDescription;
    map[ "description" ] = m_description;
    map[ "rarity" ] = m_rarity;
    map[ "inventoryIcon" ] = m_inventoryIcon;

    if( hasProperty( "fuelAmount" ) )
        map[ "fuelAmount" ] = m_fuelAmount;

    
    if( m_maxStack != -1 )
        map[ "maxStack" ] = m_maxStack;
    
    if( m_fuelAmount != 0 )
        map[ "fuelAmount" ] = m_fuelAmount;
    
    if( m_avianDescription != "" )
        map[ "avianDescription" ] = m_avianDescription;
    
    if( m_apexDescription != "" )
        map[ "apexDescription" ] = m_apexDescription;
    
    if( m_floranDescription != "" )
        map[ "floranDescription" ] = m_floranDescription;
    
    if( m_glitchDescription != "" )
        map[ "glitchDescription" ] = m_glitchDescription;
    
    if( m_hylotlDescription != "" )
        map[ "hylotlDescription" ] = m_hylotlDescription;
    
    if( m_learnBlueprintsOnPickup.size() != 0 ) {
        QJsonArray arr;
        foreach( QString blueprint, m_learnBlueprintsOnPickup ) {
            arr.append( blueprint );
        }
        map[ "learnBlueprintOnPickup" ] = arr;
    }
    
    if( m_effects.size() != 0 ) {
        QJsonArray arr;
        for( int i = 0; i < m_effects.size(); i++ ) {
            QList< JsonStatusEffect > subArray = m_effects.at( i );
            QJsonArray subArr;
            for( int j = 0; j < subArray.size(); j++ ) {
                QJsonObject statusObject = StatusEffect::jsonStatusEffectToJsonObject( subArray[j] );
                
                subArr.append( statusObject );
            }
            arr.append( subArr );
        }
        map[ "effects" ] = arr;
    }
    
    return map;
}

StarRecipe StarItem::recipe() const
{
    return m_recipe;
}

void StarItem::setRecipe(const StarRecipe &recipe)
{
    m_properties.insert( "recipe" );
    m_recipe = recipe;
}


QJsonArray StarItem::qPointToJsonArray(QPoint point)
{
    QJsonArray arr;
    arr.append( point.x() );
    arr.append( point.y() );
    return arr;
}

QJsonArray StarItem::qPointToJsonArray(QPointF point)
{
    QJsonArray arr;
    arr.append( point.x() );
    arr.append( point.y() );
    return arr;

}

QJsonArray StarItem::qColorToJsonArray(QColor color)
{
    QJsonArray arr;
    arr.append( color.red() );
    arr.append( color.green() );
    arr.append( color.blue() );
    return arr;
}

QColor StarItem::qJsonArrayToQColor(QJsonArray arr)
{
    Q_ASSERT( arr.size() >= 3 );
    QColor c;

    c.setRed( arr[0].toInt() );
    c.setGreen( arr[1].toInt() );
    c.setBlue( arr[2].toInt() );

    if( arr.size() == 4 )
        c.setAlpha( arr[3].toInt() );

    return c;
}

QJsonArray StarItem::qStringListToJsonArray(QStringList list)
{
    QJsonArray arr;

    foreach( QString elem, list ) {
        arr.append( elem );
    }
    return arr;
}

QStringList StarItem::qJsonArrayToQStringList(QJsonArray arr)
{
    QStringList list;

    for( int i = 0; i < arr.size(); ++i ) {
        list.append( arr[i].toString() );
    }
    return list;
}

QPoint StarItem::qJsonArrayToQPoint(QJsonArray arr)
{
    Q_ASSERT( arr.size() > 0 );

    QPoint pt;
    pt.setX( arr[0].toInt() );
    pt.setY( arr[1].toInt() );

    return pt;
}

QPointF StarItem::qJsonArrayToQPointF(QJsonArray arr)
{
    Q_ASSERT( arr.size() > 0 );

    QPointF pt;
    pt.setX( arr[0].toDouble() );
    pt.setX( arr[1].toDouble() );

    return pt;
}

QString StarItem::idObject() const
{
    return m_idObject;
}

bool StarItem::hasProperty(QString property)
{
    for( QSet<QString>::iterator it = m_properties.begin(); it != m_properties.end(); ++it ) {
        QString val = *it;

        if( val == property )
            return true;
    }
    return false;
}

QString StarItem::filesType() const
{
    return m_filesType;
}

void StarItem::setFilesType(const QString &filesType)
{
    m_filesType = filesType;
}

QString StarItem::itemLocation() const
{
    return m_itemLocation;
}

void StarItem::setItemLocation(const QString &itemLocation)
{
    m_itemLocation = itemLocation;
}

QString StarItem::dump() {
    QString msg;
    msg += "ItemName: \t" + itemName() + "\n" +
            "ShortDescription: \t" + shortDescription() + "\n" +
            "Description: \t" + description() + "\n" +
            "Avian Description: \t" + avianDescription() + "\n" +
            "Apex Description: \t" + apexDescription() + "\n" +
            "Floran Description: \t" + floranDescription() + "\n" +
            "Glitch Description: \t" + glitchDescription() + "\n" +
            "Human Description: \t" + humanDescription() + "\n" +
            "Hylotl Description: \t" + hylotlDescription() + "\n" +
            "Rarity: \t" + rarity() + "\n" +
            "InventoryIcon: \t" + inventoryIcon() + "\n" +
            "MaxStack: \t" + maxStack() + "\n" +
            "FuelAmount: \t" + fuelAmount() + "\n";
    
    QStringList blueprints = learnBlueprintsOnPickup();
    if( blueprints.size() > 0 ) {
        msg += "LearnBluesprints: \t";
        
        foreach( QString blueprint, learnBlueprintsOnPickup() ) {
            msg += "\t\t" + blueprint + "\n";
        }
    }
    
    return msg;
}

QDebug operator <<( QDebug dbg, StarItem &item ) {
    dbg.nospace() << item.dump() << endl;
    return dbg.space();
}

QDebug operator <<( QDebug dbg, StarItem *item ) {
    if( item != NULL )
        dbg.nospace() << item->dump() << endl;
    else
        dbg.nospace() << "Unallocated item" << endl;
    
    return dbg.space();
}
