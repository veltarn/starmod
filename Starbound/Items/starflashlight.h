#ifndef STARFLASHLIGHT_H
#define STARFLASHLIGHT_H

#include <QColor>
#include "starhandeditem.h"

class StarFlashlight : public StarHandedItem
{
public:
    StarFlashlight();
    StarFlashlight( StarFlashlight const &orig );
    virtual ~StarFlashlight();

    virtual QStringList save( QString modPath );

    QString largeImage() const;
    void setLargeImage(const QString &largeImage);

    QString inspectionKind() const;
    void setInspectionKind(const QString &inspectionKind);

    QPoint lightPosition() const;
    void setLightPosition(const QPoint &lightPosition);

    QColor lightColor() const;
    void setLightColor(const QColor &lightColor);

    double beamWidth() const;
    void setBeamWidth(double beamWidth);

    double ambientFactor() const;
    void setAmbientFactor(double ambientFactor);

protected:
    virtual ItemMap serialize();
protected:
    QString m_largeImage; //!< Path
    QString m_inspectionKind;
    QPoint m_lightPosition;
    QColor m_lightColor;
    double m_beamWidth;
    double m_ambientFactor;
};

typedef QSharedPointer< StarFlashlight > StarFlashlightPtr;

Q_DECLARE_METATYPE( StarFlashlight )
Q_DECLARE_METATYPE( QSharedPointer< StarFlashlight > )

#endif // STARFLASHLIGHT_H
