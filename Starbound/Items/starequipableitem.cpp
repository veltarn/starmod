#include "starequipableitem.h"

StarEquipableItem::StarEquipableItem() : StarHandedItem(),
    m_twoHanded( STAREQUIPABLEITEM_DEFAULT_TWO_HANDED )
{
}

StarEquipableItem::StarEquipableItem(const StarEquipableItem &orig) : StarHandedItem( orig )
{
    m_twoHanded = orig.m_twoHanded;
}

StarEquipableItem::~StarEquipableItem() {

}

bool StarEquipableItem::twoHanded() const
{
    return m_twoHanded;
}

void StarEquipableItem::setTwoHanded(bool twoHanded)
{
    m_twoHanded = twoHanded;
}

QStringList StarEquipableItem::save(QString modPath, QString itemPath, QJsonDocument &jsonFile)
{
    QStringList filesList;
    filesList += StarHandedItem::save( modPath, itemPath, jsonFile );

    ItemMap map = serialize();

    QJsonObject root = jsonFile.object();

    for( ItemMap::iterator it = map.begin(); it != map.end(); ++it ) {
        root.insert( it.key(), it.value() );
    }

    jsonFile.setObject( root );


    return filesList;
}

ItemMap StarEquipableItem::serialize() {
    ItemMap map = StarHandedItem::serialize();

    map[ "twoHanded" ] = m_twoHanded;

    return map;
}
