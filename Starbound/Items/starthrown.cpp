#include "starthrown.h"

StarThrown::StarThrown() : StarItem(),
    m_ammoUsage( STARTHROWN_DEFAULT_AMMO_USAGE ),
    m_image( STARTHROWN_DEFAULT_IMAGE ),
    m_cooldown( STARTHROWN_DEFAULT_COOLDOWN ),
    m_windupTime( STARTHROWN_DEFAULT_WINDUP_TIME ),
    m_edgeTrigger( STARTHROWN_DEFAULT_EDGE_TRIGGER ),
    m_projectileType( STARTHROWN_DEFAULT_PROJECTILE_TYPE )
{
    m_filesType = "thrownitem";
    m_idObject = "thrownitem";
}

StarThrown::StarThrown(const StarThrown &orig) : StarItem( orig )
{
    m_ammoUsage =               orig.m_ammoUsage;
    m_image =                   orig.m_image;
    m_cooldown =                orig.m_cooldown;
    m_windupTime =              orig.m_windupTime;
    m_edgeTrigger =             orig.m_edgeTrigger;
    m_projectileType =          orig.m_projectileType;
    m_projectileConfig =        orig.m_projectileConfig;
}

StarThrown::~StarThrown()
{

}

QStringList StarThrown::save(QString modPath)
{
    return QStringList();
}

ItemMap StarThrown::serialize()
{
    ItemMap map = StarItem::serialize();

    map[ "ammoUsage" ] = m_ammoUsage;
    map[ "image" ] = m_image;
    map[ "cooldown" ] = m_cooldown;
    map[ "windupTime" ] = m_windupTime;
    map[ "edgeTrigger" ] = m_edgeTrigger;
    map[ "projectileType" ] = m_projectileType;

    QJsonObject projectileConfig;
    projectileConfig[ "armorPenetration" ] = m_projectileConfig.armorPenetration;
    projectileConfig[ "level" ] = m_projectileConfig.level;
    projectileConfig[ "speed" ] = m_projectileConfig.speed;
    projectileConfig[ "power" ] = m_projectileConfig.power;

    map[ "projectileConfig" ] = projectileConfig;

    return map;
}
ProjectileConfig *StarThrown::projectileConfig()
{
    return &m_projectileConfig;
}

void StarThrown::setProjectileConfig(const ProjectileConfig &projectileConfig)
{
    m_projectileConfig = projectileConfig;
}

QString StarThrown::projectileType() const
{
    return m_projectileType;
}

void StarThrown::setProjectileType(const QString &projectileType)
{
    m_projectileType = projectileType;
}

bool StarThrown::edgeTrigger() const
{
    return m_edgeTrigger;
}

void StarThrown::setEdgeTrigger(bool edgeTrigger)
{
    m_edgeTrigger = edgeTrigger;
}

double StarThrown::windupTime() const
{
    return m_windupTime;
}

void StarThrown::setWindupTime(double windupTime)
{
    m_windupTime = windupTime;
}

double StarThrown::cooldown() const
{
    return m_cooldown;
}

void StarThrown::setCooldown(double cooldown)
{
    m_cooldown = cooldown;
}

QString StarThrown::image() const
{
    return m_image;
}

void StarThrown::setImage(const QString &image)
{
    m_image = image;
}

int StarThrown::ammoUsage() const
{
    return m_ammoUsage;
}

void StarThrown::setAmmoUsage(int ammoUsage)
{
    m_ammoUsage = ammoUsage;
}

