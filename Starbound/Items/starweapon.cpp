#include "starweapon.h"

StarWeapon::StarWeapon() :
    StarEquipableItem(),
    m_isMelee( STARWEAPON_DEFAULT_IS_MELEE),
    m_fireTime( STARWEAPON_DEFAULT_FIRE_TIME ),
    m_firePosition( STARWEAPON_DEFAULT_FIRE_POSITION ),
    m_level( STARWEAPON_DEFAULT_LEVEL ),
    m_energyCost( STARWEAPON_DEFAULT_ENERGY_COST ),
    m_classMultiplier( STARWEAPON_DEFAULT_CLASS_MULTIPLIER ),
    m_weaponImagePath( STARWEAPON_DEFAULT_WEAPON_IMAGE_PATH ),
    m_hasProjectile( STARWEAPON_DEFAULT_HAS_PROJECTILE )
{
    m_filesType = "gun";
    m_idObject = "weapon";
}

StarWeapon::StarWeapon( StarWeapon const& orig ) : StarEquipableItem( orig ) {
    m_isMelee =                 orig.m_isMelee;
    m_fireTime =                orig.m_fireTime;
    m_firePosition =            orig.m_firePosition;
    m_level =                   orig.m_level;
    m_energyCost =              orig.m_energyCost;
    m_classMultiplier =         orig.m_classMultiplier;
    m_weaponImagePath =         orig.m_weaponImagePath;
    m_hasProjectile =           orig.m_hasProjectile;
    m_projectileName =          orig.m_projectileName;
    m_projectileSpeed =         orig.m_projectileSpeed;
    m_projectilePower =         orig.m_projectilePower;
    m_projectileColor =         orig.m_projectileColor;
    m_projectileLifeTime =      orig.m_projectileLifeTime;
}

StarWeapon::~StarWeapon() {

}

bool StarWeapon::isMelee() const {
    return m_isMelee;
}

void StarWeapon::setIsMelee( bool melee ) {
    m_isMelee = melee;
}

QString StarWeapon::projectileName() const
{
    return m_projectileName;
}

void StarWeapon::setProjectileName(QString projectile)
{
    m_projectileName = projectile;
}

QString StarWeapon::weaponImagePath() const
{
    return m_weaponImagePath;
}

void StarWeapon::setWeaponImagePath(const QString &weaponImagePath)
{
    m_weaponImagePath = weaponImagePath;
}

int StarWeapon::classMultiplier() const
{
    return m_classMultiplier;
}

void StarWeapon::setClassMultiplier(int classMultiplier)
{
    m_classMultiplier = classMultiplier;
}

int StarWeapon::energyCost() const
{
    return m_energyCost;
}

void StarWeapon::setEnergyCost(int energyCost)
{
    m_energyCost = energyCost;
}

int StarWeapon::level() const
{
    return m_level;
}

void StarWeapon::setLevel(int level)
{
    m_level = level;
}

QPoint StarWeapon::firePosition() const
{
    return m_firePosition;
}

void StarWeapon::setFirePosition(const QPoint &firePosition)
{
    m_firePosition = firePosition;
}

double StarWeapon::fireTime() const
{
    return m_fireTime;
}

void StarWeapon::setFireTime(double fireTime)
{
    m_fireTime = fireTime;
}

ItemMap StarWeapon::serialize() {
    ItemMap map = StarItem::serialize();

    map[ "fireTime" ] = m_fireTime;

    QJsonArray firePos;
    firePos.append( m_firePosition.x() );
    firePos.append( m_firePosition.y() );

    map[ "firePosition" ] = firePos;
    map[ "level" ] = m_level;
    map[ "energyCost" ] = m_energyCost;
    map[ "image" ] = QFileInfo( m_weaponImagePath ).fileName();

    if( m_hasProjectile ) {
        QJsonObject projectile;
        QJsonArray colorArray;
        projectile.insert( "speed", m_projectileSpeed );
        projectile.insert( "life", m_projectileLifeTime );
        projectile.insert( "power", m_projectilePower );
        colorArray.append( m_projectileColor.red() );
        colorArray.append( m_projectileColor.green() );
        colorArray.append( m_projectileColor.blue() );
        projectile.insert( "color", colorArray );

        map["projectile"] = projectile;
        map["projectileType"] = m_projectileName;
    }

        return map;
}

bool StarWeapon::hasProjectile() const
{
    return m_hasProjectile;
}

void StarWeapon::setHasProjectile(bool hasProjectile)
{
    m_hasProjectile = hasProjectile;
}

QStringList StarWeapon::save( QString modPath, QString itemPath, QJsonDocument &jsonFile ) {
    QStringList filesList;
    /*QString weaponPath = "items/guns/" + m_itemName;

    QDir modDir( modPath );
    if( !modDir.mkpath( weaponPath ) ) {
        qDebug() << "Cannot create weapon directory..." << endl;
        return QStringList();
    }*/
    filesList += StarEquipableItem::save( modPath, itemPath, jsonFile );
    QMap< QString, QJsonValue> map = serialize();

    QString fileExtension = "gun";

    QFileInfo weaponImageInfo( m_weaponImagePath );
    //QFileInfo weaponIconInfo( m_inventoryIcon );
    QString weaponImageProjectPath = modPath + "/" + itemPath + "/" + weaponImageInfo.fileName();
    //QString iconPathName = weaponImageInfo.baseName() + "Icon." + weaponIconInfo.suffix();
    //QString absoluteIconPath  = modPath + "/" + weaponPath + "/" + iconPathName;
    //Moving image of the weapon
    if( QFile::exists( weaponImageProjectPath ) )
        QFile::remove( weaponImageProjectPath );

    /*if( QFile::exists( absoluteIconPath ) )
        QFile::remove( absoluteIconPath );*/


    QFile::copy( m_weaponImagePath, weaponImageProjectPath );
    //QFile::copy( m_inventoryIcon, absoluteIconPath );

    //map["inventoryIcon"] = iconPathName;

    //qDebug() << modPath + "/" + weaponPath + "/" + m_itemName + "." + fileExtension << endl;
    /*QFile weapFile( modPath + "/" + weaponPath + "/" + m_itemName + "." + fileExtension );

    if( weapFile.exists() )
        weapFile.remove();

    if( !weapFile.open( QIODevice::WriteOnly ) ) {
        qDebug() << "Cannot open " << weapFile.fileName() << ": " << weapFile.errorString() << endl;
        return QStringList();
    }

    QJsonDocument json;*/
    QJsonObject root = jsonFile.object();
    for( QMap<QString, QJsonValue>::iterator it = map.begin(); it != map.end(); ++it ) {
        root.insert( it.key(), it.value() );
    }

    jsonFile.setObject( root );

    /*QTextStream stream( &weapFile );
    stream.setCodec( "UTF-8" );
    stream << json.toJson();
    qDebug() << json.toJson() << endl;
    weapFile.flush();
    weapFile.close();*/
    filesList.append( weaponImageProjectPath );

    return filesList;
}

int StarWeapon::projectileSpeed() const
{
    return m_projectileSpeed;
}

int StarWeapon::projectileLifeTime() const
{
    return m_projectileLifeTime;
}

void StarWeapon::setProjectileLifeTime(int projectileLifeTime)
{
    m_projectileLifeTime = projectileLifeTime;
}

QColor StarWeapon::projectileColor() const
{
    return m_projectileColor;
}

void StarWeapon::setProjectileColor(const QColor &projectileColor)
{
    m_projectileColor = projectileColor;
}

double StarWeapon::projectilePower() const
{
    return m_projectilePower;
}

void StarWeapon::setProjectilePower(double projectilePower)
{
    m_projectilePower = projectilePower;
}
void StarWeapon::setProjectileSpeed(int projectileSpeed)
{
    m_projectileSpeed = projectileSpeed;
}


