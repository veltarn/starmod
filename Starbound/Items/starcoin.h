#ifndef STARCOIN_H
#define STARCOIN_H

#include "staritem.h"

class StarCoin : public StarItem
{
public:
    StarCoin();
    StarCoin( StarCoin &orig );
    virtual ~StarCoin();

    virtual ItemMap serialize();
    virtual QStringList save(QString modPath, QString itemPath, QJsonDocument &jsonFile);

    int value() const;
    void setValue(int value);

    QStringList pickupSoundsSmall() const;
    void setPickupSoundsSmall(const QStringList &pickupSoundsSmall);

    QStringList pickupSoundsMedium() const;
    void setPickupSoundsMedium(const QStringList &pickupSoundsMedium);

    QStringList pickupSoundsLarge() const;
    void setPickupSoundsLarge(const QStringList &pickupSoundsLarge);

    int smallStackLimit() const;
    void setSmallStackLimit(int smallStackLimit);

    int mediumStackLimit() const;
    void setMediumStackLimit(int mediumStackLimit);

private:
    int m_value;
    QStringList m_pickupSoundsSmall;
    QStringList m_pickupSoundsMedium;
    QStringList m_pickupSoundsLarge;
    int m_smallStackLimit;
    int m_mediumStackLimit;
};

#endif // STARCOIN_H
