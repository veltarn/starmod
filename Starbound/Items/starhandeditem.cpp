#include "starhandeditem.h"

StarHandedItem::StarHandedItem() : StarItem(),
    m_handPosition( STARHANDEDITEM_DEFAULT_HAND_POSITION )
{
}

StarHandedItem::StarHandedItem(const StarHandedItem &orig) : StarItem( orig )
{
    m_handPosition = orig.m_handPosition;
}

StarHandedItem::~StarHandedItem()
{

}

QStringList StarHandedItem::save(QString modPath, QString itemPath, QJsonDocument &jsonFile)
{
    QStringList filesList;
    filesList += StarItem::save( modPath, itemPath, jsonFile );

    ItemMap map = serialize();

    QJsonObject root = jsonFile.object();

    for( ItemMap::iterator it = map.begin(); it != map.end(); ++it ) {
        root.insert( it.key(), it.value() );
    }

    jsonFile.setObject( root );

    return filesList;
}

ItemMap StarHandedItem::serialize() {
    ItemMap map = StarItem::serialize();

    QJsonArray handPos;
    handPos.append( m_handPosition.x() );
    handPos.append( m_handPosition.y() );

    map[ "handPosition" ] = handPos;

    return map;
}

QPoint StarHandedItem::handPosition() const
{
    return m_handPosition;
}

void StarHandedItem::setHandPosition(const QPoint &handPosition)
{
    m_handPosition = handPosition;
}

