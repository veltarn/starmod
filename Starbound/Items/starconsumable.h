#ifndef STARCONSUMABLE_H
#define STARCONSUMABLE_H

#include <QPoint>
#include "starhandeditem.h"

#define STARCONSUMABLE_DEFAULT_EMOTE QString( "" )

class StarConsumable : public StarHandedItem
{
public:
    StarConsumable();
    StarConsumable( StarConsumable const& orig );
    virtual ~StarConsumable();

    virtual QStringList save( QString modPath );

    QString emote() const;
    void setEmote(const QString &emote);


protected:
    virtual ItemMap serialize();

protected:
    QString m_emote;
};

typedef QSharedPointer< StarConsumable > StarConsumablePtr;

Q_DECLARE_METATYPE( StarConsumable )
Q_DECLARE_METATYPE( QSharedPointer< StarConsumable > )

#endif // STARCONSUMABLE_H
