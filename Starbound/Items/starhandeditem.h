#ifndef STARHANDEDITEM_H
#define STARHANDEDITEM_H

#include "staritem.h"

#define STARHANDEDITEM_DEFAULT_HAND_POSITION QPoint( 0, 0 )

class StarHandedItem : public StarItem
{
public:
    StarHandedItem();
    StarHandedItem( StarHandedItem const& orig );
    virtual ~StarHandedItem();

    virtual QStringList save( QString modPath, QString itemPath, QJsonDocument &jsonFile );

    QPoint handPosition() const;
    void setHandPosition(const QPoint &handPosition);

protected:
    virtual QMap<QString, QJsonValue> serialize();
protected:
    QPoint m_handPosition;
};

typedef QSharedPointer< StarHandedItem > StarHandedItemPtr;

Q_DECLARE_METATYPE( StarHandedItem )
Q_DECLARE_METATYPE( QSharedPointer< StarHandedItem > )

#endif // STARHANDEDITEM_H
