#include "starflashlight.h"

StarFlashlight::StarFlashlight() : StarHandedItem(),
    m_largeImage( "" ),
    m_inspectionKind( "" ),
    m_lightPosition( QPoint( 0, 0 ) ),
    m_lightColor( QColor( 255, 255, 255 ) ),
    m_beamWidth( 0.1 ),
    m_ambientFactor( 0.5 )
{
    m_filesType = "flashlight";
    m_idObject = "flashlight";
}

StarFlashlight::StarFlashlight(const StarFlashlight &orig) : StarHandedItem( orig )
{
    m_largeImage =              orig.m_largeImage;
    m_inspectionKind =          orig.m_inspectionKind;
    m_lightPosition =           orig.m_lightPosition;
    m_lightColor =              orig.m_lightColor;
    m_beamWidth =               orig.m_beamWidth;
    m_ambientFactor =           orig.m_ambientFactor;
}

StarFlashlight::~StarFlashlight() {

}

QString StarFlashlight::largeImage() const
{
    return m_largeImage;
}

void StarFlashlight::setLargeImage(const QString &largeImage)
{
    m_largeImage = largeImage;
}

QString StarFlashlight::inspectionKind() const
{
    return m_inspectionKind;
}

void StarFlashlight::setInspectionKind(const QString &inspectionKind)
{
    m_inspectionKind = inspectionKind;
}

QPoint StarFlashlight::lightPosition() const
{
    return m_lightPosition;
}

void StarFlashlight::setLightPosition(const QPoint &lightPosition)
{
    m_lightPosition = lightPosition;
}
QColor StarFlashlight::lightColor() const
{
    return m_lightColor;
}

void StarFlashlight::setLightColor(const QColor &lightColor)
{
    m_lightColor = lightColor;
}

double StarFlashlight::beamWidth() const
{
    return m_beamWidth;
}

void StarFlashlight::setBeamWidth(double beamWidth)
{
    m_beamWidth = beamWidth;
}

double StarFlashlight::ambientFactor() const
{
    return m_ambientFactor;
}

void StarFlashlight::setAmbientFactor(double ambientFactor)
{
    m_ambientFactor = ambientFactor;
}

QStringList StarFlashlight::save( QString modPath ) {
    return QStringList();
}

ItemMap StarFlashlight::serialize() {
    ItemMap map = StarHandedItem::serialize();

    map[ "largeImage" ] = m_largeImage;
    map[ "inspectionKind" ] = m_inspectionKind;

    QJsonArray lightPos;
    lightPos.append( m_lightPosition.x() );
    lightPos.append( m_lightPosition.y() );

    map[ "lightPosition" ] = lightPos;

    QJsonArray color;
    color.append( m_lightColor.red() );
    color.append( m_lightColor.green() );
    color.append( m_lightColor.blue() );

    map[ "lightColor" ] = color;

    map[ "beamWidth" ] = m_beamWidth;
    map[ "ambientFactor" ]  = m_ambientFactor;

    return map;
}
