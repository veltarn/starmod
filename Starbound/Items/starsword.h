#ifndef STARSWORD_H
#define STARSWORD_H

#include "staritem.h"

#define STARSWORD_DEFAULT_KIND QString()
#define STARSWORD_DEFAULT_FIRE_POSITION QPoint( 0.0, 0.0 )
#define STARSWORD_DEFAULT_FIRE_AFTER_WINDUP false
#define STARSWORD_DEFAULT_TWO_HANDED false
#define STARSWORD_DEFAULT_LEVEL 1
#define STARSWORD_DEFAULT_FIRE_TIME 0.1

struct SoundEffect {
    QList<QMap< QString, QString> > fireSound;
};

struct PrimaryStances {
    // Projectile
    QString projectileType;
    double projectileSpeed;
    int projectilePower;

    //Idle
    double idleDuration;
    int idleArmAngle;
    QString armFrameOverride;
    bool idleTwoHanded;
    int idleSwordAngle;
    QPoint idleHandPosition;

    //Windup
    double windupDuration;
    QList<StatusEffect> windupStatusEffects;
    int windupArmAngle;
    bool windupTwoHanded;
    int windupSwordAngle;
    QPoint windupHandPosition;

    //Cooldown
    double cooldownDuration;
    QList<StatusEffect> cooldownStatusEffects;
    int cooldownArmAngle;
    bool cooldownTwoHanded;
    int cooldownSwordAngle;
    QPoint cooldownHandPosition;
};

typedef QList< QMap< QString, QString> > ColorOptionsList;

/**
 * @brief The StarSword class
 * This class is not a starweapon because it have too much odd properties that can't be derived
 */
class StarSword : public StarItem
{
public:
    StarSword();
    StarSword( StarSword const &orig );
    ~StarSword();

    virtual QStringList save(QString modPath);

    QString kind() const;
    void setKind(const QString &kind);

    QPointF firePosition() const;
    void setFirePosition(const QPointF &firePosition);

    SoundEffect soundEffect() const;
    void setSoundEffect(const SoundEffect &soundEffect);

    bool fireAfterWindup() const;
    void setFireAfterWindup(bool fireAfterWindup);

    bool twoHanded() const;
    void setTwoHanded(bool twoHanded);

    ColorOptionsList colorOptions() const;
    void setColorOptions(const ColorOptionsList &colorOptions);

    int level() const;
    void setLevel(int level);

    double fireTime() const;
    void setFireTime(double fireTime);

    PrimaryStances *primaryStances();
    void setPrimaryStances(const PrimaryStances &primaryStances);

protected:
    virtual ItemMap serialize();
protected:
    QString m_kind;
    QPointF m_firePosition;
    SoundEffect m_soundEffect;
    bool m_fireAfterWindup;
    bool m_twoHanded;
    ColorOptionsList m_colorOptions;
    int m_level;
    double m_fireTime;
    PrimaryStances m_primaryStances;
};

typedef QSharedPointer< StarSword > StarSwordPtr;

Q_DECLARE_METATYPE( StarSword )
Q_DECLARE_METATYPE( QSharedPointer< StarSword > )

#endif // STARSWORD_H
