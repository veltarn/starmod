 #ifndef STARMININGTOOL_H
#define STARMININGTOOL_H

#include "starequipableitem.h"

struct DurabilityRegen {
    QStringList regenItems; //!< Item that can regen the Mining tool
    int regenAmount;
};

#define STARTMININGTOOL_DEFAULT_POINTABLE false
#define STARTMININGTOOL_DEFAULT_STRIKE_SOUND QString( "" )
#define STARTMININGTOOL_DEFAULT_IDLE_SOUND QString( "" )
#define STARTMININGTOOL_DEFAULT_SWING_START 0
#define STARTMININGTOOL_DEFAULT_SWING_FINISH 0
#define STARTMININGTOOL_DEFAULT_LARGE_IMAGE QString( "" )
#define STARTMININGTOOL_DEFAULT_IMAGE QString( "" )
#define STARTMININGTOOL_DEFAULT_INSPECTION_KIND QString( "tool" )
#define STARTMININGTOOL_DEFAULT_TILE_DAMAGE 0
#define STARTMININGTOOL_DEFAULT_TILE_DAMAGE_BLUNTED 0
#define STARTMININGTOOL_DEFAULT_ANIMATION_CYCLE 0.25
#define STARTMININGTOOL_DEFAULT_DURABILITY 500
#define STARTMININGTOOL_DEFAULT_DURABILITY_PER_USE 1
#define STARTMININGTOOL_DEFAULT_BLOCK_RADIUS 1
#define STARTMININGTOOL_DEFAULT_BLOCK_ALT_RADIUS 1
#define STARTMININGTOOL_DEFAULT_FIRE_TIME 0.25
#define STARTMININGTOOL_DEFAULT_FRAMES 0

class StarMiningTool : public StarEquipableItem
{
public:
    StarMiningTool();
    StarMiningTool( StarMiningTool const& orig );
    virtual ~StarMiningTool();

    virtual QStringList save( QString modPath );

    bool pointable() const;
    void setPointable(bool pointable);

    QString strikeSound() const;
    void setStrikeSound(const QString &strikeSound);

    QString idleSound() const;
    void setIdleSound(const QString &idleSound);

    int swingStart() const;
    void setSwingStart(int swingStart);

    int swingFinish() const;
    void setSwingFinish(int swingFinish);

    QString largeImage() const;
    void setLargeImage(const QString &largeImage);

    QString image() const;
    void setImage(const QString &image);

    QString inspectionKind() const;
    void setInspectionKind(const QString &inspectionKind);

    double tileDamage() const;
    void setTileDamage(double tileDamage);

    double tileDamageBlunted() const;
    void setTileDamageBlunted(double tileDamageBlunted);

    double animationCycle() const;
    void setAnimationCycle(double animationCycle);

    QVector<DurabilityRegen> durabilityRegenChart() const;
    void setDurabilityRegenChart(const QVector<DurabilityRegen> &durabilityRegenChart);

    int blockRadius() const;
    void setBlockRadius(int blockRadius);

    int altBlockRadius() const;
    void setAltBlockRadius(int altBlockRadius);

    int durability() const;
    void setDurability(int durability);

    int durabilityPerUse() const;
    void setDurabilityPerUse(int durabilityPerUse);

    double fireTime() const;
    void setFireTime(double fireTime);

    int frames() const;
    void setFrames(int frames);

protected:
    virtual QMap<QString, QJsonValue> serialize();

protected:
    bool m_pointable;
    QString m_strikeSound; //!< Path
    QString m_idleSound; //!< Path
    int m_swingStart; //!< Angle
    int m_swingFinish; //!< Angle;
    QString m_largeImage; //!< Path
    QString m_image; //!< Path If m_frames if defined ( != 0), the path should be like this myImage.png:{frame} to correctly animate the item
    QString m_inspectionKind;
    double m_tileDamage;
    double m_tileDamageBlunted; //!< Tile damages when the tool has it's durability bar empty
    double m_animationCycle;
    QVector<DurabilityRegen> m_durabilityRegenChart;
    int m_durability;
    int m_durabilityPerUse; //!< How much the tool lose durability each time it be used on ore
    int m_blockRadius;
    int m_altBlockRadius;
    double m_fireTime; //!< Fire rate
    int m_frames; //!< Frames number
};

typedef QSharedPointer< StarMiningTool > StarMiningToolPtr;

Q_DECLARE_METATYPE( StarMiningTool )
Q_DECLARE_METATYPE( QSharedPointer< StarMiningTool > )

#endif // STARMININGTOOL_H
