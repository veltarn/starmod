#ifndef STARBEAMAXE_H
#define STARBEAMAXE_H

#include "starequipableitem.h"

#define STARBEAMAXE_DEFAULT_CATEGORY QString()
#define STARBEAMAXE_DEFAULT_INSPECTION_KIND QString()
#define STARBEAMAXE_DEFAULT_FIRE_TIME 0.0
#define STARBEAMAXE_DEFAULT_BLOCK_RADIUS 1
#define STARBEAMAXE_DEFAULT_ALT_BLOCK_RADIUS 1
#define STARBEAMAXE_DEFAULT_CRITICAL false
#define STARBEAMAXE_DEFAULT_STRIKE_SOUND QString()
#define STARBEAMAXE_DEFAULT_IMAGE QString()
#define STARBEAMAXE_DEFAULT_END_IMAGES QStringList()
#define STARBEAMAXE_DEFAULT_FIRE_POSITION QPoint( 0, 0 )
#define STARBEAMAXE_DEFAULT_SEGMENT_PER_UNIT 0
#define STARBEAMAXE_DEFAULT_NEAR_CONTROL_POINT_ELASTICITY 0.0
#define STARBEAMAXE_DEFAULT_FAR_CONTROL_POINT_ELASTICITY 0.0
#define STARBEAMAXE_DEFAULT_NEAR_CONTROL_POINT_DISTANCE 0.0
#define STARBEAMAXE_DEFAULT_TARGET_SEGMENT_RUN 0
#define STARBEAMAXE_DEFAULT_INNER_BRIGHTNESS_SCALE 0
#define STARBEAMAXE_DEFAULT_FIRST_STRIPE_THICKNESS 0.0
#define STARBEAMAXE_DEFAULT_SECOND_STRIPE_THICKNESS 0.0
#define STARBEAMAXE_DEFAULT_MIN_BEAM_WIDTH 0
#define STARBEAMAXE_DEFAULT_MAX_BEAM_WIDTH 0
#define STARBEAMAXE_DEFAULT_MIN_BEAM_JITTER 0.0
#define STARBEAMAXE_DEFAULT_MAX_BEAM_JITTER 0.0
#define STARBEAMAXE_DEFAULT_MIN_BEAM_TRANS 0.0
#define STARBEAMAXE_DEFAULT_MAX_BEAM_TRANS 0.0
#define STARBEAMAXE_DEFAULT_MIN_BEAM_LINES 0
#define STARBEAMAXE_DEFAULT_MAX_BEAM_LINES 0

class StarBeamaxe : public StarEquipableItem
{
public:
    StarBeamaxe();
    StarBeamaxe( StarBeamaxe const &orig );
    virtual ~StarBeamaxe();

    virtual QStringList save( QString modPath );

    QString category() const;
    void setCategory(const QString &category);

    double fireTime() const;
    void setFireTime(double fireTime);

    int blockRadius() const;
    void setBlockRadius(int blockRadius);

    int altBlockRadius() const;
    void setAltBlockRadius(int altBlockRadius);

    bool critical() const;
    void setCritical(bool critical);

    QString strikeSound() const;
    void setStrikeSound(const QString &strikeSound);

    QString image() const;
    void setImage(const QString &image);

    QStringList endImages() const;
    void setEndImages(const QStringList &endImages);

    QPoint firePosition() const;
    void setFirePosition(const QPoint &firePosition);

    int segmentPerUnit() const;
    void setSegmentPerUnit(int segmentPerUnit);

    double nearControlPointElasticity() const;
    void setNearControlPointElasticity(double nearControlPointElasticity);

    double farControlPointElasticity() const;
    void setFarControlPointElasticity(double farControlPointElasticity);

    double nearControlPointDistance() const;
    void setNearControlPointDistance(double nearControlPointDistance);

    int targetSegmentRun() const;
    void setTargetSegmentRun(int targetSegmentRun);

    int innerBrightnessScale() const;
    void setInnerBrightnessScale(int innerBrightnessScale);

    double firstStripeThickness() const;
    void setFirstStripeThickness(double firstStripeThickness);

    double secondStripeThickness() const;
    void setSecondStripeThickness(double secondStripeThickness);

    int minBeamWidth() const;
    void setMinBeamWidth(int minBeamWidth);

    int maxBeamWidth() const;
    void setMaxBeamWidth(int maxBeamWidth);

    double maxBeamJitter() const;
    void setMaxBeamJitter(double maxBeamJitter);

    double minBeamJitter() const;
    void setMinBeamJitter(double minBeamJitter);

    double minBeamTrans() const;
    void setMinBeamTrans(double minBeamTrans);

    double maxBeamTrans() const;
    void setMaxBeamTrans(double maxBeamTrans);

    int minBeamLines() const;
    void setMinBeamLines(int minBeamLines);

    int maxBeamLines() const;
    void setMaxBeamLines(int maxBeamLines);

    QString inspectionKind() const;
    void setInspectionKind(const QString &inspectionKind);

protected:
    virtual ItemMap serialize();
protected:
    QString m_category;
    QString m_inspectionKind;
    double m_fireTime;
    int m_blockRadius;
    int m_altBlockRadius;
    bool m_critical;
    QString m_strikeSound;
    QString m_image;
    QStringList m_endImages;
    QPoint m_firePosition;
    int m_segmentPerUnit;
    double m_nearControlPointElasticity;
    double m_farControlPointElasticity;
    double m_nearControlPointDistance;
    int m_targetSegmentRun;
    int m_innerBrightnessScale;
    double m_firstStripeThickness;
    double m_secondStripeThickness;
    int m_minBeamWidth;
    int m_maxBeamWidth;
    double m_maxBeamJitter;
    double m_minBeamJitter;
    double m_minBeamTrans;
    double m_maxBeamTrans;
    int m_minBeamLines;
    int m_maxBeamLines;
};

typedef QSharedPointer< StarBeamaxe > StarBeamaxePtr;

Q_DECLARE_METATYPE( StarBeamaxe )
Q_DECLARE_METATYPE( QSharedPointer< StarBeamaxe > )
#endif // STARBEAMAXE_H
