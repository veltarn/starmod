#include "statuseffect.h"

StatusEffect::StatusEffect() :
    m_kind( "" ),
    m_interval( 0.d ),
    m_percentage( false ),
    m_suppressTooltip( false ),
    m_tooltipLabel( "" ),
    m_damageKind( "default" ),
    m_initialTick( false ),
    m_duration( 0 ),
    m_mainInterfaceIcon( "" ),
    m_tooltipAmountLabel( "" )
{
}

StatusEffect::StatusEffect(const StatusEffect &orig)
{
    m_kind = orig.m_kind;
    m_interval = orig.m_interval;
    m_percentage = orig.m_percentage;
    m_suppressTooltip = orig.m_suppressTooltip;
    m_primitives = orig.m_primitives;
    m_tooltipLabel = orig.m_tooltipLabel;
    m_tick = orig.m_tick;
    m_damageKind = orig.m_damageKind;
    m_initialTick = orig.m_initialTick;
    m_duration = orig.m_duration;
    m_effectSources = orig.m_effectSources;
    m_mainInterfaceIcon = orig.m_mainInterfaceIcon;
    m_tooltipAmountLabel = orig.m_tooltipAmountLabel;
}

StatusEffect::~StatusEffect()
{
    if( m_tick.statusEffects.size() > 0 ) {
        foreach( StatusEffect *ptr, m_tick.statusEffects ) {
            delete ptr;
            ptr = NULL;
        }
    }
}

StatusEffect &StatusEffect::operator =( StatusEffect const& orig ) {
    m_kind = orig.m_kind;
    m_interval = orig.m_interval;
    m_percentage = orig.m_percentage;
    m_suppressTooltip = orig.m_suppressTooltip;
    m_primitives = orig.m_primitives;
    m_tooltipLabel = orig.m_tooltipLabel;
    m_tick = orig.m_tick;
    m_damageKind = orig.m_damageKind;
    m_initialTick = orig.m_initialTick;
    m_duration = orig.m_duration;
    m_effectSources = orig.m_effectSources;
    m_mainInterfaceIcon = orig.m_mainInterfaceIcon;
    m_tooltipAmountLabel = orig.m_tooltipAmountLabel;
}
PrimitivesList StatusEffect::primitives() const
{
    return m_primitives;
}

void StatusEffect::setPrimitives(const PrimitivesList &primitives)
{
    m_primitives = primitives;
}


QString StatusEffect::tooltipAmountLabel() const
{
    return m_tooltipAmountLabel;
}

void StatusEffect::setTooltipAmountLabel(const QString &tooltipAmountLabel)
{
    m_tooltipAmountLabel = tooltipAmountLabel;
}

QString StatusEffect::mainInterfaceIcon() const
{
    return m_mainInterfaceIcon;
}

void StatusEffect::setMainInterfaceIcon(const QString &mainInterfaceIcon)
{
    m_mainInterfaceIcon = mainInterfaceIcon;
}

int StatusEffect::duration() const
{
    return m_duration;
}

void StatusEffect::setDuration(int duration)
{
    m_duration = duration;
}

bool StatusEffect::initialTick() const
{
    return m_initialTick;
}

void StatusEffect::setInitialTick(bool initialTick)
{
    m_initialTick = initialTick;
}

QString StatusEffect::damageKind() const
{
    return m_damageKind;
}

void StatusEffect::setDamageKind(const QString &damageKind)
{
    m_damageKind = damageKind;
}

Tick StatusEffect::tick() const
{
    return m_tick;
}

void StatusEffect::setTick(const Tick &tick)
{
    m_tick = tick;
}

QString StatusEffect::tooltipLabel() const
{
    return m_tooltipLabel;
}

void StatusEffect::setTooltipLabel(const QString &tooltipLabel)
{
    m_tooltipLabel = tooltipLabel;
}

bool StatusEffect::suppressTooltip() const
{
    return m_suppressTooltip;
}

void StatusEffect::setSuppressTooltip(bool suppressTooltip)
{
    m_suppressTooltip = suppressTooltip;
}

bool StatusEffect::percentage() const
{
    return m_percentage;
}

void StatusEffect::setPercentage(bool percentage)
{
    m_percentage = percentage;
}

double StatusEffect::interval() const
{
    return m_interval;
}

void StatusEffect::setInterval(const double &interval)
{
    m_interval = interval;
}

QString StatusEffect::kind() const
{
    return m_kind;
}

void StatusEffect::setKind(const QString &kind)
{
    m_kind = kind;
}

QStringList StatusEffect::effectSources() const {
    return m_effectSources;
}

void StatusEffect::addEffectSource( QString effect ) {
    if( !Utility::exists( effect, m_effectSources ) ) {
        m_effectSources.append( effect );
    }
}

void StatusEffect::clearEffectSources() {
    m_effectSources.clear();
}

QJsonObject StatusEffect::toJsonObject() {
    QJsonObject root;

    root.insert( "kind", m_kind );

    if( m_interval > 0.0 )
        root.insert( "interval", m_interval );

    if( m_percentage )
        root.insert( "percentage", m_percentage );

    if( m_suppressTooltip )
        root.insert( "suppressTooltip", m_suppressTooltip );

    if( m_primitives.size() > 0 ) {
        QJsonArray primitives;
        foreach( Primitive primitive, m_primitives ) {
            QJsonObject primitiveObj;

            if( primitive.amount >= 0 ) {
                primitiveObj[ "amount" ] = primitive.amount;
            }

            if( primitive.mode != "" ) {
                primitiveObj[ "mode" ] = primitive.mode;
            }

            if( primitive.name != "" ) {
                primitiveObj[ "name" ] = primitive.name;
            }

            if( primitive.percentage > 0 ) {
                primitiveObj[ "percentage" ] = primitive.percentage;
            }

            primitives.append( primitiveObj );
        }

        root.insert( "primitives", primitives );
    }

    if( m_tooltipLabel != "" )
        root.insert( "tooltipLabel", m_tooltipLabel );

    if( m_tick.statusEffects.size() > 0 ) {
        QJsonObject tickObject;
        QJsonArray tickArray;
        for( QList< StatusEffect* >::iterator it = m_tick.statusEffects.begin(); it != m_tick.statusEffects.end(); ++it ) {
            QJsonObject stObj = (*it)->toJsonObject();
            tickArray.append( stObj );
        }
        tickObject.insert( "statusEffects", tickArray );
        root.insert( "tick", tickObject );
    }

    if( m_damageKind != "" )
        root.insert( "damageKind", m_damageKind );

    if( m_initialTick )
        root.insert( "initialTick", m_initialTick );

    if( m_duration != 0 )
        root.insert( "duration", m_duration );

    if( m_effectSources.size() >  0 ) {
        QJsonArray effectsSources;
        foreach( QString effsrc, m_effectSources ) {
            effectsSources.append( effsrc );
        }

        root.insert( "effectSources", effectsSources );
    }

    if( m_mainInterfaceIcon != "" )
        root.insert( "mainInterfaceIcon", m_mainInterfaceIcon );

    if( m_tooltipAmountLabel != "" )
        root.insert( "tooltipAmountLabel", m_tooltipAmountLabel );

    return root;
}

StatusEffect *StatusEffect::fromJsonObject(QJsonObject &rootObject)
{
    StatusEffect *effect = new StatusEffect;

    QJsonValue kind =              rootObject[ "kind" ];
    QJsonValue interval =          rootObject[ "interval" ];
    QJsonValue percentage =        rootObject[ "percentage" ];
    QJsonValue suppressTooltip =   rootObject[ "suppressTooltip" ];
    QJsonValue primitives =        rootObject[ "primitives" ];
    QJsonValue tooltipLabel =      rootObject[ "tooltipLabel" ];
    QJsonValue tick =              rootObject[ "tick" ];
    QJsonValue damageKind =        rootObject[ "damageKind" ];
    QJsonValue initialTick =       rootObject[ "initialTick" ];
    QJsonValue duration =          rootObject[ "duration" ];
    QJsonValue effectSources =     rootObject[ "effectSources" ];
    QJsonValue mainInterfaceIcon = rootObject[ "mainInterfaceIcon" ];
    QJsonValue tooltipAmountLabel = rootObject[ "tooltipAmountLabel" ];

    if( !kind.isNull() ) {
        effect->setKind( kind.toString() );
    } else {
        qDebug() << "Kind property is null " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !interval.isNull() ) {
        effect->setInterval( interval.toDouble() );
    } else {
        qDebug() << "Interval property is null " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !percentage.isNull() ) {
        effect->setPercentage( percentage.toBool() );
    } else {
        qDebug() << "Percentage property is null " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !suppressTooltip.isNull() ) {
        effect->setSuppressTooltip( suppressTooltip.toBool() );
    } else {
        qDebug() << "Suppress Tooltip property is null " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !primitives.isNull() ) {
        QJsonArray array = primitives.toArray();
        PrimitivesList list;
        for( int i = 0; i < array.size(); i++ ) {
            Primitive primitive;
            QJsonObject objPrimitive = array[i].toObject();

            QJsonValue name =       objPrimitive[ "name" ];
            QJsonValue mode =       objPrimitive[ "mode" ];
            QJsonValue percentage = objPrimitive[ "percentage" ];
            QJsonValue amount =     objPrimitive[ "amount" ];

            if( !name.isNull() )
                primitive.name = name.toString();

            if( !mode.isNull() )
                primitive.mode = mode.toString();

            if( !percentage.isNull() )
                primitive.percentage = percentage.toInt();

            if( !amount.isNull() )
                primitive.amount = amount.toInt();

            list.append( primitive );
        }

        effect->setPrimitives( list );
    } else {
        qDebug() << "Primitives property is null " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !tooltipLabel.isNull() ) {
        effect->setTooltipLabel( tooltipLabel.toString() );
    } else {
        qDebug() << "Tooltip Label property is null " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !tick.isNull() ) {
        QJsonObject obj = tick.toObject();

        QJsonValue statusEffectsValue = obj[ "statusEffects" ];
        Tick tickValue;

        if( !statusEffectsValue.isNull() ) {
            QJsonArray statusEffectsArr = statusEffectsValue.toArray();
            QList< StatusEffect* > statusEffectList;

            for( int i = 0; i < statusEffectsArr.size(); i++ ) {
                QJsonObject obj = statusEffectsArr[i].toObject();

                StatusEffect *convertedEffect = StatusEffect::fromJsonObject( obj );
                statusEffectList.append( convertedEffect );
            }

            tickValue.statusEffects = statusEffectList;

            effect->setTick( tickValue );
        }
    }

    if( !damageKind.isNull() ) {
        effect->setDamageKind( damageKind.toString() );
    } else {
        qDebug() << "Damage Kind property is null " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !initialTick.isNull() ) {
        effect->setInitialTick( initialTick.toBool() );
    } else {
        qDebug() << "Initial Tick is null " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !duration.isNull() ) {
        effect->setDuration( duration.toInt() );
    } else {
        qDebug() << "Duration is null " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !effectSources.isNull() ) {
        QJsonArray effectSourcesArray = effectSources.toArray();

        for( int i = 0; i < effectSourcesArray.size(); i++ ) {
           effect->addEffectSource( effectSourcesArray[i].toString() );
        }
    } else {
        qDebug() << "Effect Sources is null " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !mainInterfaceIcon.isNull() ) {
        effect->setMainInterfaceIcon( mainInterfaceIcon.toString() );
    } else {
        qDebug() << "Main Interface Icon is null " << __FILE__ << ":" << __LINE__ << endl;
    }

    if( !tooltipAmountLabel.isNull() ) {
        effect->setTooltipAmountLabel( tooltipAmountLabel.toString() );
    } else {
        qDebug() << "Tooltip Amount Label is null " << __FILE__ << ":" << __LINE__ << endl;
    }

    return effect;
}

QJsonObject StatusEffect::jsonStatusEffectToJsonObject(JsonStatusEffect effect)
{
    QJsonObject statusEffect;

    if( effect.kind != "" )
        statusEffect[ "kind" ] = effect.kind;

    if( effect.amount != 0.0 )
        statusEffect[ "amount" ] = effect.amount;

    if( effect.range != -1 )
        statusEffect[ "range" ] = effect.range;

    if( effect.percentage != -1 )
        statusEffect[ "percentage" ] = effect.percentage;

    if( effect.color != "" )
        statusEffect[ "color" ] = effect.color;

    return statusEffect;
}

JsonStatusEffect StatusEffect::qJsonObjectToJsonStatusEffect(QJsonObject obj)
{
    JsonStatusEffect effect;

    QJsonValue kind = obj[ "kind" ];
    QJsonValue amount = obj[ "amount" ];
    QJsonValue range = obj[ "range" ];
    QJsonValue percentage = obj[ "percentage" ];
    QJsonValue color = obj[ "color" ];

    if( !kind.isUndefined() )
        effect.kind = kind.toString();

    if( !amount.isUndefined() )
        effect.amount = amount.toDouble();

    if( !range.isUndefined() )
        effect.range = range.toInt();

    if( !percentage.isUndefined() )
        effect.percentage = percentage.toInt();

    if( !color.isUndefined() )
        effect.color = color.toString();

    return effect;
}
