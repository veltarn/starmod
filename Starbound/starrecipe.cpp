#include "starrecipe.h"

StarRecipe::StarRecipe()
{
}

StarRecipe::StarRecipe(const StarRecipe &orig)
{
    m_input = orig.m_input;
    m_output = orig.m_output;
    m_groups = orig.m_groups;
}

StarRecipe::~StarRecipe()
{

}



void StarRecipe::save(QString modPath, QJsonDocument &jsonFile)
{
    ItemMap map = serialize();

    QJsonObject root = jsonFile.object();

    for( ItemMap::iterator it = map.begin(); it != map.end(); ++it ) {
        root.insert( it.key(), it.value() );
    }

    jsonFile.setObject( root );
}
QList<Input> StarRecipe::input() const
{
    return m_input;
}

void StarRecipe::setInput(const QList<Input> &input)
{
    m_input = input;
}

Output StarRecipe::output() const
{
    return m_output;
}

void StarRecipe::setOutput(const Output &output)
{
    m_output = output;
}

QStringList StarRecipe::groups() const
{
    return m_groups;
}

void StarRecipe::setGroups(const QStringList &groups)
{
    m_groups = groups;
}

ItemMap StarRecipe::serialize()
{
    ItemMap map;

    QJsonArray inputArray;

    foreach( Input input, m_input ) {
        QJsonObject inputObj;
        inputObj[ "item" ] = input.item;
        inputObj[ "count" ] = input.count;

        inputArray.append( inputObj );
    }

    map[ "input" ] = inputArray;

    QJsonObject outputObj;

    outputObj[ "item" ] = m_output.item;
    outputObj[ "count" ] = m_output.count;

    map[ "output" ] = outputObj;

    QJsonArray groupsArray;

    foreach( QString group, m_groups ) {
        groupsArray.append( group );
    }

    map[ "groups" ] = groupsArray;

    return map;
}





void StarRecipe::removeInput(QString input, QList<Input> &list)
{
    for( QList< Input >::iterator it = list.begin(); it != list.end(); ++it ) {
        if( it->item == input ) {
            list.erase( it );
            break;
        }
    }
}
