#ifndef STARTYPES_H
#define STARTYPES_H

#include <QMap>
#include <QVector>
#include <QJsonValue>
#include <QPoint>
#include <QSharedPointer>

typedef QMap< QString, QJsonValue > ItemMap;
typedef QMap< QString, QJsonValue > AssetMap;
typedef QVector< QPointF > Polygon;
typedef QVector< QPoint > PolygonI;

#endif // STARTYPES_H
