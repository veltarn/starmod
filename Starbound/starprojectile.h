#ifndef STARPROJECTILE_H
#define STARPROJECTILE_H

#include <QString>
#include <QStringList>
#include <QFile>
#include <QTextStream>
#include <QFileInfo>
#include <QList>
#include <QVector>
#include <QPoint>
#include <QColor>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include "StarTypes.h"
#include "statuseffect.h"
#include "../Core/utility.h"

enum LiquidId {
    Water = 1,
    EndlessWater = 2,
    Lava = 3,
    Acid = 4,
    EndlessLava = 5,
    TentacleJuice = 6,
    Tar = 7
};

enum ActionType {
    NoAction = 0,
    Projectile = 1,
    Explosion = 2,
    Liquid = 3,
    Sounds = 4,
    Config = 5
};

QString actionTypeToString( int actionType );
int stringToActionType( QString action );

struct ActionOnReap {
    ActionOnReap() : action( NoAction ),
                     projectileType( QString() ),
                     inheritDamageFactor( 0.0 ),
                     angle( -1 ),
                     adjustAngle( -1 ),
                     fuzzAngle( -1 ),
                     liquidId( 0 ),
                     quantity( -1 ),
                     foregroundRadius( 0 ),
                     backgroundRadius( 0 ),
                     explosiveDamageAmount( 0 ),
                     color( QString() ) {}

    int action; // ActionType enum
    int delaySteps; //!< Delay before that action triggers, time unit is unknown and it only works on some kind of action

    /**
     * Projectiles
     */
    QString projectileType; //!<Projectile
    double inheritDamageFactor;
    int angle; //!< Absolute projectile angle
    int adjustAngle; //!< Relative adjustment of the angle
    int fuzzAngle; //!< Angle variance in relation with base angle

    /**
      * Liquid
      * 1 - Water
      * 2 - Infinite water
      * 3 - Lava
      * 4 - Acid
      * 5 - Endless lava
      * 6 - Tentacle Juice
      * 7 - Tar
      */
    int liquidId; // Liquid id enum
    int quantity;

    /**
      * Explosions
      */
    int foregroundRadius; //!< Radius of the foreground
    int backgroundRadius; //!< Radius of the background
    int explosiveDamageAmount;

    /**
      * Light
      */
    QString color;

    /**
      * Sounds
      */
    QStringList sounds;

    /**
      * Config
      */
    QString configFile;

};

class StarProjectile
{
public:
    StarProjectile();

    /**
     * @brief This method saves the item into the current mod path
     * @param modPath Path of the mod
     * @return List of wrote files
     */
    QStringList save( QString modPath );
    QJsonArray saveActionOnReap( QList<ActionOnReap> &actions );
    bool load( QString projPath );

    QString projectileName() const;
    void setProjectileName(const QString &projectileName);

    int timeToLive() const;
    void setTimeToLive(int timeToLive);

    int level() const;
    void setLevel(int level);

    bool hydrophobic() const;
    void setHydrophobic(bool hydrophobic);

    QString framePath() const;
    void setFramePath(const QString &framePath);

    uint frameNumber() const;
    void setFrameNumber(const uint &frameNumber);

    double animationCycle() const;
    void setAnimationCycle(double animationCycle);

    bool animationLoop() const;
    void setAnimationLoop(bool animationLoop);

    QString damageType() const;
    void setDamageType(const QString &damageType);

    QString damageKind() const;
    void setDamageKind(const QString &damageKind);

    QString damageKindImage() const;
    void setDamageKindImage(const QString &damageKindImage);

    double power() const;
    void setPower(double power);

    QString physics() const;
    void setPhysics(const QString &physics);

    int bounces() const;
    void setBounces(int bounces);

    PolygonI damagePoly() const;
    void setDamagePoly(const PolygonI &damagePoly);

    double fallSpeed() const;
    void setFallSpeed(double fallSpeed);

    int knockbackPower() const;
    void setKnockbackPower(int knockbackPower);

    double speed() const;
    void setSpeed(double speed);

    double initialVelocity() const;
    void setInitialVelocity(double initialVelocity);

    bool pointLight() const;
    void setPointLight(bool pointLight);

    QColor lightColor() const;
    void setLightColor(const QColor &lightColor);

    QList< JsonStatusEffect > effects() const;
    void setEffects(const QList< JsonStatusEffect > &effects);

    QStringList emitters() const;
    void setEmitters(const QStringList &emitters);


    QString projectileLocation() const;
    void setProjectileLocation(const QString &projectileLocation);

    QList<ActionOnReap> actionOnReap() const;
    void addActionOnReap( ActionOnReap actionOnReap );
    void setActionOnReap(const QList<ActionOnReap> &actionOnReap);

private:
    QString m_projectileName;
    int m_timeToLive;
    int m_level;
    bool m_hydrophobic;
    //bool m_flippable;

    /*
     * Frames
     */
    QString m_framePath; //!< Path of the frame file
    uint m_frameNumber; //!< Number of frames of the animation
    double m_animationCycle; //!< Rate of the animation
    bool m_animationLoop; //!< Loops of not the animation

    /*
     * Damage processing
     */
    QString m_damageType;
    QString m_damageKind;
    QString m_damageKindImage;
    double m_power;
    //bool m_universalDamage;

    /*
     * Physics
     */
    QString m_physics; //!< Physical properties of the projectile (eg: grenade ... ), blank if nothing
    int m_bounces; //!< Number of bounces of the projectile;
    PolygonI m_damagePoly; //!< Polygon that describes the shape of the damage radius of the projectile
    double m_fallSpeed;
    int m_knockbackPower;
    double m_speed;
    double m_initialVelocity;

    /*
     * Light
     */
    bool m_pointLight;
    QColor m_lightColor;

    /*
     * Effects & SFX
     */
    QList< JsonStatusEffect > m_effects;
    QStringList m_emitters;

    /*
     * Events
     */
    QList< ActionOnReap > m_actionOnReap;

    QString m_projectileLocation;
};

typedef QSharedPointer< StarProjectile > StarProjectilePtr;

Q_DECLARE_METATYPE( StarProjectile )
Q_DECLARE_METATYPE( QSharedPointer< StarProjectile> )

#endif // STARPROJECTILE_H
