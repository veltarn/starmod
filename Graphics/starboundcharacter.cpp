#include "starboundcharacter.h"

StarboundCharacter::StarboundCharacter( QString starboundAssetsPath, QGraphicsItem  *parent ) :
    QGraphicsItemGroup(parent),
    m_head( NULL ),
    m_hair( NULL ),
    m_body( NULL ),
    m_frontarm( NULL ),
    m_backArm( NULL ),
    m_equipedItem( NULL ),
    m_starboundAssetsPath( starboundAssetsPath ),
    m_handtype( StarIdle ),
    m_hasItem( false )
{
    QString characterPath = m_starboundAssetsPath + "/humanoid/glitch";

    QImage bodyMainImg, headMainImg, hairMainImg, frontArmImg, backArmImg, tmp;

    if( !bodyMainImg.load( characterPath + "/malebody.png" ) ) {
        qDebug() << "Cannot load " << ( characterPath + "/malebody.png" ) << endl;
    }

    if( !headMainImg.load( characterPath + "/malehead.png" ) ) {
        qDebug() << "Cannot load malehead.png" << endl;
    }

    if( !hairMainImg.load( characterPath + "/hair/14.png" ) ) {
        qDebug() << "Cannot load hair/14.png" << endl;
    }

    if( !frontArmImg.load( characterPath + "/frontarm.png" ) ) {
        qDebug() << "Cannot load frontarm.png" << endl;
    }

    if( !backArmImg.load( characterPath + "/backarm.png" ) ) {
        qDebug() << "Cannot load backarm.png" << endl;
    }

    FrameGrid bodyFrame = getFrameInfo( starboundAssetsPath + "/humanoid/malebody.frames" );
    FrameGrid frontArmFrame = getFrameInfo( starboundAssetsPath + "/humanoid/frontarm.frames" );
    FrameGrid backArmFrame = getFrameInfo( starboundAssetsPath + "/humanoid/backarm.frames" );

    m_framesInfo[ "body" ] = bodyFrame;
    m_framesInfo[ "frontarm" ] = frontArmFrame;
    m_framesInfo[ "backarm" ] = backArmFrame;


    /*m_frontarms[ StarIdle ] =       QRectF( frontArmFrame.size.x(), 0, frontArmFrame.size.x(), frontArmFrame.size.y() );
    m_frontarms[ StarRotation ] =   QRectF( frontArmFrame.size.x() * 8, frontArmFrame.size.y(), frontArmFrame.size.x(), frontArmFrame.size.y() );
    m_frontarms[ StarMelee ] =      QRectF( frontArmFrame.size.x() * 3, frontArmFrame.size.y() * 6, frontArmFrame.size.x(), frontArmFrame.size.y() );*/

    QRect malebodyRect( bodyFrame.size.x(), 0, bodyFrame.size.x(), bodyFrame.size.y() );
    tmp = bodyMainImg.copy( malebodyRect );

    m_body = new QGraphicsPixmapItem( QPixmap::fromImage( tmp ) );
    //m_body = tmp;

    QRect headRect( 43, 0, 43, 43 );
    tmp = headMainImg.copy( headRect );

    m_head = new QGraphicsPixmapItem( QPixmap::fromImage( tmp )/*, m_body*/ );
    //m_head = tmp;

    QRect hairRect( 43, 0, 43, 43 );
    tmp = hairMainImg.copy( hairRect );

    m_hair = new QGraphicsPixmapItem( QPixmap::fromImage( tmp )/*, m_body*/ );
    //m_hair = tmp;

    QRect frontArmRect( frontArmFrame.size.x(), 0, frontArmFrame.size.x(), frontArmFrame.size.y() );
    tmp = frontArmImg.copy( frontArmRect );
    m_frontarms[ StarIdle ] = tmp;
    m_frontarm = new QGraphicsPixmapItem( QPixmap::fromImage( tmp ), m_body );
    //m_frontarms[ StarIdle ] = tmp;

    frontArmRect = QRect( frontArmFrame.size.x() * 8, frontArmFrame.size.y(), frontArmFrame.size.x(), frontArmFrame.size.y() );
    m_frontarms[ StarRotation ] = frontArmImg.copy( frontArmRect );

    //m_frontarms[ StarRotation ] = new QGraphicsPixmapItem( QPixmap::fromImage( tmp ), m_body );
    //m_frontarms[ StarRotation ] = tmp;

    frontArmRect = QRect( frontArmFrame.size.x() * 3, frontArmFrame.size.y() * 6, frontArmFrame.size.x(), frontArmFrame.size.y() );
    m_frontarms[ StarMelee ] = frontArmImg.copy( frontArmRect );

    //m_frontarms[ StarMelee ] = new QGraphicsPixmapItem( QPixmap::fromImage( tmp ), m_body );
    //m_frontarms[ StarMelee ] = tmp;

    //m_frontArm = &m_frontArms[ "idle" ];

    QRect backArmRect( backArmFrame.size.x(), 0, backArmFrame.size.x(), backArmFrame.size.y());
    tmp = backArmImg.copy( backArmRect );

    m_backArm = new QGraphicsPixmapItem( QPixmap::fromImage( tmp )/*, m_body*/ );
    //m_backArm = tmp;

    m_head->setPos( 0, 1 );
    m_hair->setPos( 0, 1 );

    addToGroup( m_backArm );
    addToGroup( m_body );
    addToGroup( m_head );
    addToGroup( m_hair );
    addToGroup( m_frontarm );
}

/*QRectF StarboundCharacter::boundingRect() const {
    return QRectF( 0, 0, 43, 43 );
}*/

/*void StarboundCharacter::paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget ) {
    FrameGrid frameInfo = m_framesInfo[ "backarm" ];
    QRectF sourceRect( 0.f, 0.f, frameInfo.size.x(), frameInfo.size.y() );
    QRectF targetRect( 1.f, 0.f, frameInfo.size.x(), frameInfo.size.y() );
    painter->drawImage( targetRect, m_backArm, sourceRect ); //Draw backarm

    frameInfo = m_framesInfo[ "body" ];
    sourceRect = QRectF( 0.f, 0.f, frameInfo.size.x(), frameInfo.size.y() );
    targetRect = QRectF( 0.f, 0.f, frameInfo.size.x(), frameInfo.size.y() );
    painter->drawImage( targetRect, m_body, sourceRect );


    sourceRect = QRectF( 0.f, 0.f, 43.f, 43.f );
    targetRect = QRectF( 0.f, 1, 43.f, 43.f );
    painter->drawImage( targetRect, m_head, sourceRect );

    //Drawing hairs
    sourceRect = QRectF( 0.f, 0.f, 43.f, 43.f );
    targetRect = QRectF( 0.f, 1, 43.f, 43.f );
    painter->drawImage( targetRect, m_hair, sourceRect );

    //Drawing item before frontarm
    frameInfo = m_framesInfo[ "frontarm" ];
    targetRect = QRectF( -1.f, 0.f, frameInfo.size.x(), frameInfo.size.y() );

    if( m_hasItem ) {
        QRectF targetItem( 4.f, 17.f, m_equipedItem.width(), m_equipedItem.height() );
        QRectF sourceItem( 0.f, 0.f, m_equipedItem.width(), m_equipedItem.height() );

        painter->drawImage( targetItem, m_equipedItem, sourceItem );
    }

    sourceRect = QRectF( 0.f, 0.f, 43.f, 43.f );
    painter->drawImage( targetRect, m_frontarms[m_handtype], sourceRect );

}*/

void StarboundCharacter::setHandType( HandType type ) {    
    removeFromGroup( m_frontarm );
    m_handtype = type;

    delete m_frontarm;

    m_frontarm = new QGraphicsPixmapItem( QPixmap::fromImage( m_frontarms[ m_handtype ] ), m_body );

    if( m_handtype == StarRotation ) {
        originPoint = new QGraphicsRectItem( 0, 0, 2, 2 );
        originPoint->setBrush( QBrush( QColor( 0, 130, 240) ) );
        originPoint->setPen( Qt::NoPen );
        QTransform matrix;
        matrix.translate( -0.35, 0.6 );
        m_frontarm->setTransform( matrix );
        addToGroup( originPoint );
    }

    addToGroup( m_frontarm );
}

int StarboundCharacter::handType() const {
    return m_handtype;
}

FrameGrid StarboundCharacter::getFrameInfo( QString jsonFile ) {
    FrameGrid nullFrameGrid;
    nullFrameGrid.framesNumber = QPoint( 0, 0 );
    nullFrameGrid.size = QPoint( 0, 0 );

    QFile file( jsonFile );

    if( !file.open( QIODevice::ReadOnly ) ) {
        qDebug() << "Cannot open " << file.fileName() << ": " << file.errorString() << endl;
        return nullFrameGrid;
    }

    QJsonDocument jsonDoc = QJsonDocument::fromJson( file.readAll() );

    QJsonObject rootObject = jsonDoc.object();

    QJsonObjectIterator frameGridIter = rootObject.find( "frameGrid" );

    if( frameGridIter != rootObject.end() ) {
        QJsonObject frameGridObject = frameGridIter.value().toObject();

        QJsonObjectIterator sizeIter = frameGridObject.find( "size" );
        QJsonObjectIterator dimensionIter = frameGridObject.find( "dimensions" );

        QJsonArray size = sizeIter.value().toArray();
        QJsonArray dimensions = sizeIter.value().toArray();

        if( size.size() != 2 && dimensions.size() != 2 ) {
            qDebug() << "Dimensions or size doesn't have correct values" << endl;
            return nullFrameGrid;
        }

        FrameGrid fg;
        QPoint sizeP;
        sizeP.setX( size.at( 0 ).toVariant().toInt() );
        sizeP.setY( size.at( 1 ).toVariant().toInt() );

        fg.size = sizeP;

        QPoint dimen;
        dimen.setX( dimensions.at( 0 ).toVariant().toInt() );
        dimen.setY( dimensions.at( 1 ).toVariant().toInt() );

        fg.framesNumber = dimen;

        return fg;
    }

    qDebug() << "FrameGrid object doesn't exists File: " << jsonFile << endl;
    return nullFrameGrid;
}

void StarboundCharacter::setItemInHand( QString itemPath ) {
    QImage tmp;
    if( !tmp.load( itemPath ) ) {
        m_hasItem = false;
        qDebug() << "Cannot load item " << itemPath << endl;
    } else {
        /*QTransform matrix;
        matrix.rotate( 90 );
        matrix.scale( 1.0, 1.0 );
        m_equipedItem = m_equipedItem.transformed( matrix );*/
        if( m_equipedItem != NULL ) {
            removeFromGroup( m_equipedItem );
            delete m_equipedItem;
            m_equipedItem = NULL;
        }

        m_equipedItem = new QGraphicsPixmapItem( QPixmap::fromImage( tmp ), m_body );

        int width = tmp.width();
        int height = tmp.height();

        QRectF rect = m_equipedItem->boundingRect();
        QPointF orig;
        orig.setX( rect.left() + ( width / 2.f ) );
        orig.setY( rect.top() + ( height / 2.f ) );

        m_equipedItem->setTransformOriginPoint( orig );
        m_equipedItem->setRotation( 90 );

        removeFromGroup( m_frontarm );
        addToGroup( m_equipedItem );
        addToGroup( m_frontarm );

        m_hasItem = true;
    }
}

void StarboundCharacter::setItemPosition( QPointF pos ) {
    if( m_equipedItem != NULL ) {
        QPointF nPos = m_equipedItem->mapFromParent( pos );
        nPos = m_equipedItem->mapToScene( nPos );
        qDebug() << nPos << pos << endl;
        m_equipedItem->setPos( nPos );
    }
}

void StarboundCharacter::setArmRotation( float angle ) {
   /* QTransform matrix;
    matrix.rotate( angle );
    matrix.scale( 1.0, 1.0 );
    m_frontarms[ StarRotation ] = m_frontarms[ StarRotation ].transformed( matrix );*/
    if( m_handtype == StarRotation ) {
        QRectF b = m_frontarm->boundingRect();
        QPointF pos = m_frontarm->pos();
        QPointF orig;
        orig.setX( ( b.left() + ( m_framesInfo[ "frontarm" ].size.x() / 2.f ) ) - 3.20f );
        orig.setY( b.top() + ( m_framesInfo[ "frontarm" ].size.y() / 2.f ) + 0.15f );

        originPoint->setPos( orig );

        m_frontarm->setTransformOriginPoint( orig );
        m_frontarm->setRotation( angle );
    }
}
