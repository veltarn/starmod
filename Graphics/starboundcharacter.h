#ifndef STARBOUNDCHARACTER_H
#define STARBOUNDCHARACTER_H

#include <QGraphicsItem>
#include <QGraphicsRectItem>
#include <QGraphicsItemGroup>
#include <QGraphicsPixmapItem>
#include <QDebug>
#include <QImage>
#include <QMap>
#include <QRect>
#include <QPoint>
#include <QSize>
#include <QPainter>
#include <QBrush>
#include <QStyleOptionGraphicsItem>

#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QTransform>


struct FrameGrid {
    QPoint size;
    QPoint framesNumber;
};


enum HandType {
    StarIdle, StarRotation, StarMelee
};

typedef QJsonObject::iterator QJsonObjectIterator;

class StarboundCharacter : public QGraphicsItemGroup
{
public:
    explicit StarboundCharacter( QString starboundAssetsPath, QGraphicsItem  *parent = 0 );
    FrameGrid getFrameInfo( QString jsonFile );
    /*virtual QRectF boundingRect() const;
    virtual void paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget );*/

    void setHandType( HandType type );
    int handType() const;

    void setArmRotation( float angle );

    void setItemInHand( QString itemPath );
    void setItemPosition( QPointF pos );

signals:
    
public slots:

protected:

private:
    /*QImage m_head;
    QImage m_hair;
    QImage m_body;
    QImage m_backArm;
    //QImage m_frontArm;
    QImage m_equipedItem;

    QMap< int, QImage > m_frontarms;*/

    QGraphicsPixmapItem *m_head;
    QGraphicsPixmapItem *m_hair;
    QGraphicsPixmapItem *m_body;
    QGraphicsPixmapItem *m_frontarm;
    QGraphicsPixmapItem *m_backArm;
    QGraphicsPixmapItem *m_equipedItem;

    QGraphicsRectItem *originPoint;

    QMap< int, QImage> m_frontarms;

    QString m_starboundAssetsPath;
    
    QMap< QString, FrameGrid> m_framesInfo;
    HandType m_handtype;
    bool m_hasItem;

};

#endif // STARBOUNDCHARACTER_H
